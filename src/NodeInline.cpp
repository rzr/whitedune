/*
 * NodeInline.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeInline.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFString.h"
#include "SFVec3f.h"
#include "SFFloat.h"
#include "Field.h"  // for FF_URL
#include "Scene.h"
#include "NodeInlineLoadControl.h"

ProtoInline::ProtoInline(Scene *scene)
  : Proto(scene, "Inline")
{
    addElements();
}

ProtoInline::ProtoInline(Scene *scene, const char *name)
  : Proto(scene, name)
{
    addElements();
}

void ProtoInline::addElements(void) 
{
// in X3D Inline will be replaced with InlineLoadControl
    load.set(
          addExposedField(SFBOOL, "load", new SFBool(true), 
                          new SFBool(true)));
    setFieldFlags(load, FF_X3D_ONLY);
    url.set(
          addExposedField(MFSTRING, "url", new MFString(), FF_URL, NULL));
    bboxCenter.set(
          addField(SFVEC3F, "bboxCenter", new SFVec3f(0.0f, 0.0f, 0.0f)));
    bboxSize.set(
          addField(SFVEC3F, "bboxSize", new SFVec3f(-1.0f, -1.0f, -1.0f), 
                   new SFFloat(-1.0f)));
}

Node *
ProtoInline::create(Scene *scene)
{
    return new NodeInline(scene, this); 
}

NodeInline::NodeInline(Scene *scene, Proto *def)
  : Node(scene, def)
{
    _loadedNodes = NULL;
}

NodeInline::NodeInline(NodeInlineLoadControl *inlineLoadControl)
  : Node(inlineLoadControl->getScene(), 
         inlineLoadControl->getScene()->getProto("Inline"))
{
    url(inlineLoadControl->url());
    bboxCenter(inlineLoadControl->bboxCenter());
    bboxSize(inlineLoadControl->bboxSize());
}

void    
NodeInline::addFieldNodeList(int index, NodeList *childList)
{
    _loadedNodes = childList;
}

void
NodeInline::draw(int pass)
{
    if (_loadedNodes == NULL)
        return;

    glPushName(url_Field());  // field offset

    int i;
    for (i = 0; i < _loadedNodes->size(); i++)
        _loadedNodes->get(i)->bind();

    glPushName(0);
    for (i = 0; i < _loadedNodes->size(); i++) {
        glLoadName(i);
        _loadedNodes->get(i)->draw(pass);
    }
    glPopName();

    for (i = 0; i < _loadedNodes->size(); i++)
        _loadedNodes->get(i)->unbind();

    glPopName();
}

void
NodeInline::preDraw()
{
    if (_loadedNodes == NULL)
        return;

    for (int i = 0; i < _loadedNodes->size(); i++)
        _loadedNodes->get(i)->preDraw();
}

void
NodeInline::setField(int index, FieldValue *value)
{
    Node::setField(index, value);
    if (!_scene->isParsing())
        if (index == url_Field()) 
            if (TheApp->loadNewInline()) {
                _scene->readInline(this);
                _scene->scanForInlines(_loadedNodes);
            }
}


int         
NodeInline::countPolygons(void)
{
    if (_loadedNodes == NULL)
        return 0;

    int ret = 0;

    for (int i = 0; i < _loadedNodes->size(); i++)
        ret += _loadedNodes->get(i)->countPolygons();

    return ret;
}

int         
NodeInline::countPrimitives(void)
{
    if (_loadedNodes == NULL)
        return 0;

    int ret = 0;

    for (int i = 0; i < _loadedNodes->size(); i++)
        ret += _loadedNodes->get(i)->countPrimitives();

    return ret;
}

int         
NodeInline::countPolygons1Sided(void)
{
    if (_loadedNodes == NULL)
        return 0;

    int ret = 0;

    for (int i = 0; i < _loadedNodes->size(); i++)
        ret += _loadedNodes->get(i)->countPolygons1Sided();

    return ret;
}

int         
NodeInline::countPolygons2Sided(void)
{
    if (_loadedNodes == NULL)
        return 0;

    int ret = 0;

    for (int i = 0; i < _loadedNodes->size(); i++)
        ret += _loadedNodes->get(i)->countPolygons2Sided();

    return ret;
}

int
NodeInline::getProfile(void) const
{ 
    int profile = PROFILE_INTERACTIVE;
    if (_loadedNodes == NULL)
        return profile;
    for (int i = 0; i < _loadedNodes->size(); i++) {
        int nodeProfile = _loadedNodes->get(i)->getProfile();
        if (nodeProfile > profile)
            profile = nodeProfile;
    }
    return profile; 
}


int
NodeInline::getComponentLevel(void) const
{
    if (!isDefault(load_Field()))
        return 3;
    return -1;
}

const char* 
NodeInline::getComponentName(void) const
{
    static const char* name = "Networking";
    return name;
}

bool 
NodeInline::canWriteAc3d()
{ 
    if (_loadedNodes == NULL)
        return false;
    for (int i = 0; i < _loadedNodes->size(); i++)
        if (_loadedNodes->get(i) != NULL)
            if (_loadedNodes->get(i)->canWriteAc3d())
                return true; 
    return false; 
}

int
NodeInline::writeAc3d(int filedes, int indent)
{
    if (_loadedNodes == NULL)
        return 0;
    RET_ONERROR( _loadedNodes->writeAc3d(filedes, indent) )
    return 0;
}

void    
NodeInline::handleAc3dMaterial(ac3dMaterialCallback callback, Scene* scene)
{
    if (_loadedNodes == NULL)
        return;
    for (int i = 0; i < _loadedNodes->size(); i++)    
        _loadedNodes->get(i)->handleAc3dMaterial(callback, scene);
}

bool 
NodeInline::canWriteCattGeo()
{ 
    if (_loadedNodes == NULL)
        return false;
    for (int i = 0; i < _loadedNodes->size(); i++)
        if (_loadedNodes->get(i) != NULL)
            if (_loadedNodes->get(i)->canWriteCattGeo())
                return true; 
    return false; 
}

int
NodeInline::writeCattGeo(int filedes, int indent)
{
    RET_ONERROR( _loadedNodes->writeCattGeo(this, filedes, indent) )
    return 0;
}

