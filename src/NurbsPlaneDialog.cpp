/*
 * NurbsPlaneDialog.cpp
 *
 * Copyright (C) 2000 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "NurbsPlaneDialog.h"
#include <stdio.h>
#include <stdlib.h>
#include "swt.h"
#include "resource.h"

NurbsPlaneDialog::NurbsPlaneDialog(SWND parent, int width, int depth, 
                                   int uDegree, int vDegree)
  : Dialog(parent, IDD_NEW_NURBS_PLANE)
{
    _width = width;
    _depth = depth;
    _uDegree = uDegree;
    _vDegree = vDegree;
    LoadData();
}

NurbsPlaneDialog::~NurbsPlaneDialog()
{
}

void
NurbsPlaneDialog::SaveData()
{
    char buf[128];

    swGetText(swGetDialogItem(_dlg, IDC_WIDTH), buf, 128);
    _width = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_DEPTH), buf, 128);
    _depth = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_U_DEGREE), buf, 128);
    _uDegree = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_V_DEGREE), buf, 128);
    _vDegree = atoi(buf);
}

bool
NurbsPlaneDialog::Validate()
{
    return _uDegree > 0 && _vDegree > 0 &&
           _width > _uDegree && _depth > _vDegree;
}

void
NurbsPlaneDialog::LoadData()
{
    char buf[128];

    mysnprintf(buf, 128, "%d", _width);
    swSetText(swGetDialogItem(_dlg, IDC_WIDTH), buf);
    mysnprintf(buf, 128, "%d", _depth);
    swSetText(swGetDialogItem(_dlg, IDC_DEPTH), buf);
    mysnprintf(buf, 128, "%d", _uDegree);
    swSetText(swGetDialogItem(_dlg, IDC_U_DEGREE), buf);
    mysnprintf(buf, 128, "%d", _vDegree);
    swSetText(swGetDialogItem(_dlg, IDC_V_DEGREE), buf);
}
