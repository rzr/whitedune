/*
 * InteractionDialog.h
 *
 * Copyright (C) 2005 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _INTERACTION_DIALOG_H
#define _INTERACTION_DIALOG_H

#include "Scene.h"
#include "CheckBoxWindow.h"

class RouteData {
public:
//  Node*               sensor;
    Proto              *proto;
    int                 eventInField;
    int                 eventOutField;
    int                 type;
};

class InteractionDialog : public Dialog {
public:
                        InteractionDialog(SWND parent, Node* oldNode);

    Array<RouteData>   &getRouteData() { return _routeData; }
    Array<bool>        &getEventInIsInteractive() 
                           { return _eventInIsInteractive; }
    Node               *getSensor(void) { return _sensor; }
    void                buildInterfaceData(void);

protected:
    virtual SWND        dialog(void) { return _dlg; }
    void                LoadData();
    void                SaveData();
    virtual bool        Validate();
    int                 numRoutes() { return _routeData.size(); }
    void                OnCommand(int id);
    
protected:
    CheckBoxWindow      _window;
    Node               *_sensor;
    Node               *_interactionNode;
    Array<RouteData>    _routeData;
    Array<MyString>     _sensors;
    bool                _okFlag;
    Array<bool>         _eventInIsInteractive;
    Array<bool>         _initIsInteractive;
    int                 _level;
};

#endif
