/*
 * NodeText.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_TEXT_H
#define _NODE_TEXT_H

#ifndef _GEOMETRY_NODE_H
#include "GeometryNode.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

class ProtoText : public GeometryProto {
public:
                    ProtoText(Scene *scene);
                    ProtoText(Scene *scene, const char *name);
    void            addElements(void);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_TEXT; }

    FieldIndex fontStyle;
    FieldIndex length;
    FieldIndex maxExtent;
    FieldIndex string;
    FieldIndex solid;

};

class NodeText : public GeometryNode {
public:
                    NodeText(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_IMMERSIVE; }
    virtual Node   *copy() const { return new NodeText(*this); }

    virtual void    draw();

    void            update(void) { _textDirty = true; }
    void            reInit(void) { _textDirty = true; }

    virtual bool    isInvalidChildNode(void) { return true; }

    fieldMacros(SFNode,   fontStyle, ProtoText)
    fieldMacros(MFFloat,  length,    ProtoText)
    fieldMacros(MFString, string,    ProtoText)
    fieldMacros(SFFloat,  maxExtent, ProtoText)
    fieldMacros(SFBool,   solid,     ProtoText)
protected:
    void            cleanText(void);
protected:
    MFString       *_deBackslashedStrings;
    bool            _textDirty;
};

#endif // _NODE_TEXT_H
