/*
 * not implemented yet, to be done later
 * vrml2dune.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "swt.h"

#include "DuneApp.h"
#include "MainWindow.h"
#include "MoveCommand.h"
#include "xerrorhandler.h"
#include "Field.h"
#include "Types.h"

extern bool parseCommandlineArgument(int & i,int argc, char** argv);
#define errorprintf fprintf   

static void printFirstPart()
{
    printf("#include <stdio.h>\n");
    printf("#include \"stdafx.h\"\n");
    printf("\n");
    printf("#include \"swt.h\"\n");
    printf("\n");
    printf("#include \"DuneApp.h\"\n");
    printf("#include \"MainWindow.h\"\n");
    printf("#include \"MoveCommand.h\"\n");
    printf("#include \"xerrorhandler.h\"\n");
    printf("#include \"Field.h\"\n");
    printf("#include \"AllNodes.h\"\n");
    printf("\n");
    printf("#define errorprintf fprintf   \n");
    printf("\n");
    printf("int main(int argc, char *argv[])\n");
    printf("{\n");
    printf("    TheApp = new DuneApp();\n");
    printf("    Scene* scene = new Scene();\n");
    printf("    int stereoflag=1;\n");
    printf("    bool wantStereo=TheApp->GetBoolPreference(\"WantStereo\", true);\n");
    printf("    if (!wantStereo)\n");
    printf("        stereoflag=0;\n");
    printf("    stereoflag=swInitialize(&argc, argv, stereoflag);\n");
    printf("    if (stereoflag) {\n");
    printf("       TheApp->setCanStereo(true);\n");
    printf("       TheApp->setUseStereo(true);\n");
    printf("    } else {\n");
    printf("       TheApp->setCanStereo(false);\n");
    printf("    }\n");
}

static void printLastPart()
{
    printf("    swMainLoop();\n");
    printf("    delete TheApp;\n");
    printf("    return 0;\n");
    printf("}\n");
}


int vrml2dune(int argc, char *argv[])
{
    TheApp = new DuneApp();
    int i;
    bool fileflag=false;
    Scene* scene = new Scene();
    for (i=2;i<argc;i++) {
        fileflag=true;
        parseCommandlineArgument(i,argc,argv);
        if (!TheApp->ImportFile(argv[i], scene)) {
            delete scene;
            perror(argv[i]);
            return(1);
        }
    }
    int numberVariables = 0;
    if (fileflag == true) {
        Node root = scene->getRoot();
        for (int i = 0; i < root->getNumFields(); i++) {
            Field *field = node->getProto()->getField(i);
            FieldValue *value = _fields[i];
            int type = field->getType();
            if (value) {
                if (!value->equals(field->getDefault())) {
                MyString varibleName = "root";
                varibleName += atoi(numberVariables++);                
                RET_ONERROR( value->writeCC(f, (const char*) varibleName) );
            }
        }
       FieldValue *value = _fields[i];
        int type = field->getType();
        if (value) {
            // see bug: testing in NodeData::writeFields in TODO file
            if (!value->equals(field->getDefault())) { 

       StringArray *names = scene->getAllNodeNames();
       printFirstPart();
       for (i=0; i < names->size(); i++) {
           Node* node = scene->createNode(names->get(i)); 
           const char *str = names->get(i);
           // skip comment node
           if (strcmp(str,"#") == 0)
               continue;
           for (int j = 0; j < node->getProto()->getNumFields(); j++) {
               Field *field = node->getProto()->getField(j);
               printf("    node%s->%s(new %s());\n", str,
                        (const char *)field->getName(),
                        typeEnumToString(field->getType()));
           }
           printf("    scene->execute(new MoveCommand(node%s, NULL, -1, scene->getRoot(), \n",
                  str);
           printf("                                   scene->getRootField()));\n");
       }
       printLastPart();
    }
    delete TheApp;
    return 0;
}

