/*
 * Stack.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _STACK_H
#define _STACK_H

#ifndef _ARRAY_H
#include "Array.h"
#endif

template<class T> 
class Stack {
public:
                        Stack()
                           { _top = 0; }

    void                push(T t)
                           { _data[_top++] = t; }
    T                   pop()
                           { 
                           if (_top == 0)
                               return NULL;
                           else
                               return _data[--_top]; 
                           }
    T                   peek() const
                           { 
                           if (_top == 0)
                               return NULL;
                           else
                               return _data[_top-1]; 
                           }
    T                   peek(int i) const
                           { 
                           if ((i <= _top) && (i > 0))
                               return _data[i-1]; 
                           else 
                               return NULL;
                           }
    int                 empty() const { return _top == 0; }

    int                 getTop() const { return _top; }
    
private:
    Array<T>            _data;
    int                 _top;
};

#endif // _STACK_H
