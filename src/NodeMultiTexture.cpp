/*
 * NodeMultiTexture.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2006 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeMultiTexture.h"
#include "Proto.h"
#include "FieldValue.h"
#include "Node.h"
#include "Util.h"

ProtoMultiTexture::ProtoMultiTexture(Scene *scene)
  : Proto(scene, "MultiTexture")
{
    alpha.set(
          addExposedField(SFFLOAT, "alpha", new SFFloat(1),
                          new SFFloat(0.0f), new SFFloat(1.0f)));
    color.set(
          addExposedField(SFCOLOR, "color", new SFColor(1, 1, 1)));
    function.set(
          addExposedField(MFSTRING, "function", new MFString()));
    mode.set(
          addExposedField(MFSTRING, "mode", new MFString()));
    source.set(
          addExposedField(MFSTRING, "source", new MFString()));
    texture.set(
          addExposedField(MFNODE, "texture", new MFNode(), 
                          TEXTURE_NODE | NOT_SELF_NODE));
}

Node *
ProtoMultiTexture::create(Scene *scene)
{ 
    return new NodeMultiTexture(scene, this); 
}

NodeMultiTexture::NodeMultiTexture(Scene *scene, Proto *def)
  : Node(scene, def)
{
}

void NodeMultiTexture::bind()
{
    MFNode *mfTexture = (MFNode *) getField(texture_Field());
    if (mfTexture->getSize() <= 0) 
        return;
    Node *nTexture = mfTexture->getValue(0);
//    Node    *nTextureTransform = ((MFNode *) getField(textureTransform_Field()))
//                                                      ->getValue(0);
    if (nTexture) {
        nTexture->bind();
        // ignore material's diffuse component for components > 2
        // FIXME:  right now we ignore for all textures
        float dc[4];
        dc[0] = dc[1] = dc[2] = 1.0f;  dc[3] = 1.0f;
        Util::myGlMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, dc);
    }
//    if (nTextureTransform) nTextureTransform->bind();
}

void NodeMultiTexture::unbind()
{
    MFNode *mfTexture = (MFNode *) getField(texture_Field());
    if (mfTexture->getSize() <= 0) 
        return;
    Node *nTexture = mfTexture->getValue(0);
//    Node *nTextureTransform = ((SFNode *) getField(textureTransform_Field()))
//                                                   ->getValue(0);

    if (nTexture) nTexture->unbind();
//    if (nTextureTransform) nTextureTransform->unbind();
}

bool NodeMultiTexture::isTransparent(void)
{
    MFNode *mfTexture = (MFNode *) getField(texture_Field());
    if (mfTexture->getSize() <= 0) 
        return false;
    Node *nTexture = mfTexture->getValue(0);
    if (nTexture) 
        return nTexture->isTransparent();
    return false; 
}
