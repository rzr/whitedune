/*
 * GeometryNode.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2004 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "GeometryNode.h"
#include "NodeShape.h"

GeometryNode::GeometryNode(Scene *scene, Proto *proto)
  : Node(scene, proto)
{
}

NodeAppearance *
GeometryNode::getAppearance(void)
{
    Node *parent = NULL;
    if (hasParent())
        parent = getParent();

    NodeShape *nshape = NULL;
    if (parent)
        if (parent->getType() == VRML_SHAPE)
            nshape = (NodeShape *) parent;

    NodeAppearance *nappearance = NULL;
    if (nshape)
        nappearance = (NodeAppearance *)(nshape->appearance()->getValue());
    return nappearance;
}

NodeMaterial *
GeometryNode::getMaterial(void)
{
    NodeAppearance *nappearance = getAppearance();

    NodeMaterial *nmaterial = NULL;
    if (nappearance)
        nmaterial = (NodeMaterial *)nappearance->material()->getValue();
    return nmaterial;
}


NodeImageTexture *
GeometryNode::getImageTexture(void)
{
    NodeAppearance *nappearance = getAppearance();

    Node *ntexture = NULL;
    NodeImageTexture *nimageTexture = NULL;
    if (nappearance) {
        ntexture = nappearance->texture()->getValue();
        if (ntexture)
            if (ntexture->getType() == VRML_IMAGE_TEXTURE)
                nimageTexture = (NodeImageTexture *)ntexture;
    }
    return nimageTexture;
}

NodeTextureTransform *
GeometryNode::getTextureTransform(void)
{
    NodeAppearance *nappearance = getAppearance();

    NodeTextureTransform *ntextureTransfrom = NULL;
    if (nappearance)
        ntextureTransfrom = (NodeTextureTransform  *)
                            nappearance->textureTransform()->getValue();
    return ntextureTransfrom;
}

