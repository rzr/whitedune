// VRML97/X3D file "helloWorld.x3dv" converted to java with white_dune

class Pre_Node {
    void treeRender() {
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
    }
    void treeDoWithData() {
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
    }
    Pre_Node m_parent;
    Object m_data;
    Pre_Node m_protoRoot;
};

class RenderCallback {
   public void render(Pre_Node node) {}
}
class TreeRenderCallback {
   public void treeRender(Pre_Node node) {}
}

class DoWithDataCallback {
   public void doWithData(Pre_Node node) {}
}
class TreeDoWithDataCallback {
   public void treeDoWithData(Pre_Node node) {}
}

class EventsProcessedCallback {
   public void eventsProcessed(Pre_Node node) {}
}


class Pre_ColorRGBA extends Pre_Node {
    Pre_Node metadata;
    float[] color;
    static Pre_ColorRGBARenderCallback Pre_renderCallbackColorRGBA;
    static void setPre_ColorRGBARenderCallback(Pre_ColorRGBARenderCallback node) {
        Pre_renderCallbackColorRGBA = node;
    }

    static Pre_ColorRGBATreeRenderCallback Pre_treeRenderCallbackColorRGBA;
    static void setPre_ColorRGBATreeRenderCallback(Pre_ColorRGBATreeRenderCallback node) {
        Pre_treeRenderCallbackColorRGBA = node;
    }

    static Pre_ColorRGBADoWithDataCallback Pre_doWithDataCallbackColorRGBA;
    static void setPre_ColorRGBADoWithDataCallback(Pre_ColorRGBADoWithDataCallback node) {
        Pre_doWithDataCallbackColorRGBA = node;
    }

    static Pre_ColorRGBATreeDoWithDataCallback Pre_treeDoWithDataCallbackColorRGBA;
    static void setPre_ColorRGBATreeDoWithDataCallback(Pre_ColorRGBATreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackColorRGBA = node;

    }

    static Pre_ColorRGBAEventsProcessedCallback Pre_eventsProcessedCallbackColorRGBA;
    static void setPre_ColorRGBAEventsProcessedCallback(Pre_ColorRGBAEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackColorRGBA = node;
    }

    Pre_ColorRGBA() {
    }
    void treeRender() {
        if (Pre_ColorRGBA.Pre_treeRenderCallbackColorRGBA != null) {
            Pre_ColorRGBA.Pre_treeRenderCallbackColorRGBA.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ColorRGBA.Pre_renderCallbackColorRGBA != null)
            Pre_ColorRGBA.Pre_renderCallbackColorRGBA.render(this);
    }
    void treeDoWithData() {
        if (Pre_ColorRGBA.Pre_treeDoWithDataCallbackColorRGBA != null) {
            Pre_ColorRGBA.Pre_treeDoWithDataCallbackColorRGBA.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ColorRGBA.Pre_doWithDataCallbackColorRGBA != null)
            Pre_ColorRGBA.Pre_doWithDataCallbackColorRGBA.doWithData(this);
    }
}

class Pre_ColorRGBARenderCallback extends RenderCallback {}
class Pre_ColorRGBATreeRenderCallback extends TreeRenderCallback {}
class Pre_ColorRGBADoWithDataCallback extends DoWithDataCallback {};
class Pre_ColorRGBATreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ColorRGBAEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CoordinateDamper extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_CoordinateDamperRenderCallback Pre_renderCallbackCoordinateDamper;
    static void setPre_CoordinateDamperRenderCallback(Pre_CoordinateDamperRenderCallback node) {
        Pre_renderCallbackCoordinateDamper = node;
    }

    static Pre_CoordinateDamperTreeRenderCallback Pre_treeRenderCallbackCoordinateDamper;
    static void setPre_CoordinateDamperTreeRenderCallback(Pre_CoordinateDamperTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinateDamper = node;
    }

    static Pre_CoordinateDamperDoWithDataCallback Pre_doWithDataCallbackCoordinateDamper;
    static void setPre_CoordinateDamperDoWithDataCallback(Pre_CoordinateDamperDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinateDamper = node;
    }

    static Pre_CoordinateDamperTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinateDamper;
    static void setPre_CoordinateDamperTreeDoWithDataCallback(Pre_CoordinateDamperTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinateDamper = node;

    }

    static Pre_CoordinateDamperEventsProcessedCallback Pre_eventsProcessedCallbackCoordinateDamper;
    static void setPre_CoordinateDamperEventsProcessedCallback(Pre_CoordinateDamperEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinateDamper = node;
    }

    Pre_CoordinateDamper() {
    }
    void treeRender() {
        if (Pre_CoordinateDamper.Pre_treeRenderCallbackCoordinateDamper != null) {
            Pre_CoordinateDamper.Pre_treeRenderCallbackCoordinateDamper.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CoordinateDamper.Pre_renderCallbackCoordinateDamper != null)
            Pre_CoordinateDamper.Pre_renderCallbackCoordinateDamper.render(this);
    }
    void treeDoWithData() {
        if (Pre_CoordinateDamper.Pre_treeDoWithDataCallbackCoordinateDamper != null) {
            Pre_CoordinateDamper.Pre_treeDoWithDataCallbackCoordinateDamper.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CoordinateDamper.Pre_doWithDataCallbackCoordinateDamper != null)
            Pre_CoordinateDamper.Pre_doWithDataCallbackCoordinateDamper.doWithData(this);
    }
}

class Pre_CoordinateDamperRenderCallback extends RenderCallback {}
class Pre_CoordinateDamperTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateDamperDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateDamperTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateDamperEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_HAnimDisplacer extends Pre_Node {
    Pre_Node metadata;
    int[] coordIndex;
    float[] displacements;
    String name;
    float weight;
    static Pre_HAnimDisplacerRenderCallback Pre_renderCallbackHAnimDisplacer;
    static void setPre_HAnimDisplacerRenderCallback(Pre_HAnimDisplacerRenderCallback node) {
        Pre_renderCallbackHAnimDisplacer = node;
    }

    static Pre_HAnimDisplacerTreeRenderCallback Pre_treeRenderCallbackHAnimDisplacer;
    static void setPre_HAnimDisplacerTreeRenderCallback(Pre_HAnimDisplacerTreeRenderCallback node) {
        Pre_treeRenderCallbackHAnimDisplacer = node;
    }

    static Pre_HAnimDisplacerDoWithDataCallback Pre_doWithDataCallbackHAnimDisplacer;
    static void setPre_HAnimDisplacerDoWithDataCallback(Pre_HAnimDisplacerDoWithDataCallback node) {
        Pre_doWithDataCallbackHAnimDisplacer = node;
    }

    static Pre_HAnimDisplacerTreeDoWithDataCallback Pre_treeDoWithDataCallbackHAnimDisplacer;
    static void setPre_HAnimDisplacerTreeDoWithDataCallback(Pre_HAnimDisplacerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackHAnimDisplacer = node;

    }

    static Pre_HAnimDisplacerEventsProcessedCallback Pre_eventsProcessedCallbackHAnimDisplacer;
    static void setPre_HAnimDisplacerEventsProcessedCallback(Pre_HAnimDisplacerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackHAnimDisplacer = node;
    }

    Pre_HAnimDisplacer() {
    }
    void treeRender() {
        if (Pre_HAnimDisplacer.Pre_treeRenderCallbackHAnimDisplacer != null) {
            Pre_HAnimDisplacer.Pre_treeRenderCallbackHAnimDisplacer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_HAnimDisplacer.Pre_renderCallbackHAnimDisplacer != null)
            Pre_HAnimDisplacer.Pre_renderCallbackHAnimDisplacer.render(this);
    }
    void treeDoWithData() {
        if (Pre_HAnimDisplacer.Pre_treeDoWithDataCallbackHAnimDisplacer != null) {
            Pre_HAnimDisplacer.Pre_treeDoWithDataCallbackHAnimDisplacer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_HAnimDisplacer.Pre_doWithDataCallbackHAnimDisplacer != null)
            Pre_HAnimDisplacer.Pre_doWithDataCallbackHAnimDisplacer.doWithData(this);
    }
}

class Pre_HAnimDisplacerRenderCallback extends RenderCallback {}
class Pre_HAnimDisplacerTreeRenderCallback extends TreeRenderCallback {}
class Pre_HAnimDisplacerDoWithDataCallback extends DoWithDataCallback {};
class Pre_HAnimDisplacerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_HAnimDisplacerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SplineScalarInterpolator extends Pre_Node {
    Pre_Node metadata;
    boolean closed;
    float[] key;
    float[] keyValue;
    float[] keyVelocity;
    boolean normalizeVelocity;
    static Pre_SplineScalarInterpolatorRenderCallback Pre_renderCallbackSplineScalarInterpolator;
    static void setPre_SplineScalarInterpolatorRenderCallback(Pre_SplineScalarInterpolatorRenderCallback node) {
        Pre_renderCallbackSplineScalarInterpolator = node;
    }

    static Pre_SplineScalarInterpolatorTreeRenderCallback Pre_treeRenderCallbackSplineScalarInterpolator;
    static void setPre_SplineScalarInterpolatorTreeRenderCallback(Pre_SplineScalarInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackSplineScalarInterpolator = node;
    }

    static Pre_SplineScalarInterpolatorDoWithDataCallback Pre_doWithDataCallbackSplineScalarInterpolator;
    static void setPre_SplineScalarInterpolatorDoWithDataCallback(Pre_SplineScalarInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackSplineScalarInterpolator = node;
    }

    static Pre_SplineScalarInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackSplineScalarInterpolator;
    static void setPre_SplineScalarInterpolatorTreeDoWithDataCallback(Pre_SplineScalarInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSplineScalarInterpolator = node;

    }

    static Pre_SplineScalarInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackSplineScalarInterpolator;
    static void setPre_SplineScalarInterpolatorEventsProcessedCallback(Pre_SplineScalarInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSplineScalarInterpolator = node;
    }

    Pre_SplineScalarInterpolator() {
    }
    void treeRender() {
        if (Pre_SplineScalarInterpolator.Pre_treeRenderCallbackSplineScalarInterpolator != null) {
            Pre_SplineScalarInterpolator.Pre_treeRenderCallbackSplineScalarInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SplineScalarInterpolator.Pre_renderCallbackSplineScalarInterpolator != null)
            Pre_SplineScalarInterpolator.Pre_renderCallbackSplineScalarInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_SplineScalarInterpolator.Pre_treeDoWithDataCallbackSplineScalarInterpolator != null) {
            Pre_SplineScalarInterpolator.Pre_treeDoWithDataCallbackSplineScalarInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SplineScalarInterpolator.Pre_doWithDataCallbackSplineScalarInterpolator != null)
            Pre_SplineScalarInterpolator.Pre_doWithDataCallbackSplineScalarInterpolator.doWithData(this);
    }
}

class Pre_SplineScalarInterpolatorRenderCallback extends RenderCallback {}
class Pre_SplineScalarInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_SplineScalarInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_SplineScalarInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SplineScalarInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_VolumeEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    Pre_Node coord;
    float[] direction;
    int[] coordIndex;
    boolean internal;
    static Pre_VolumeEmitterRenderCallback Pre_renderCallbackVolumeEmitter;
    static void setPre_VolumeEmitterRenderCallback(Pre_VolumeEmitterRenderCallback node) {
        Pre_renderCallbackVolumeEmitter = node;
    }

    static Pre_VolumeEmitterTreeRenderCallback Pre_treeRenderCallbackVolumeEmitter;
    static void setPre_VolumeEmitterTreeRenderCallback(Pre_VolumeEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackVolumeEmitter = node;
    }

    static Pre_VolumeEmitterDoWithDataCallback Pre_doWithDataCallbackVolumeEmitter;
    static void setPre_VolumeEmitterDoWithDataCallback(Pre_VolumeEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackVolumeEmitter = node;
    }

    static Pre_VolumeEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackVolumeEmitter;
    static void setPre_VolumeEmitterTreeDoWithDataCallback(Pre_VolumeEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackVolumeEmitter = node;

    }

    static Pre_VolumeEmitterEventsProcessedCallback Pre_eventsProcessedCallbackVolumeEmitter;
    static void setPre_VolumeEmitterEventsProcessedCallback(Pre_VolumeEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackVolumeEmitter = node;
    }

    Pre_VolumeEmitter() {
    }
    void treeRender() {
        if (Pre_VolumeEmitter.Pre_treeRenderCallbackVolumeEmitter != null) {
            Pre_VolumeEmitter.Pre_treeRenderCallbackVolumeEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (coord != null)
            coord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_VolumeEmitter.Pre_renderCallbackVolumeEmitter != null)
            Pre_VolumeEmitter.Pre_renderCallbackVolumeEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_VolumeEmitter.Pre_treeDoWithDataCallbackVolumeEmitter != null) {
            Pre_VolumeEmitter.Pre_treeDoWithDataCallbackVolumeEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_VolumeEmitter.Pre_doWithDataCallbackVolumeEmitter != null)
            Pre_VolumeEmitter.Pre_doWithDataCallbackVolumeEmitter.doWithData(this);
    }
}

class Pre_VolumeEmitterRenderCallback extends RenderCallback {}
class Pre_VolumeEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_VolumeEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_VolumeEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_VolumeEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Contour2D extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    static Pre_Contour2DRenderCallback Pre_renderCallbackContour2D;
    static void setPre_Contour2DRenderCallback(Pre_Contour2DRenderCallback node) {
        Pre_renderCallbackContour2D = node;
    }

    static Pre_Contour2DTreeRenderCallback Pre_treeRenderCallbackContour2D;
    static void setPre_Contour2DTreeRenderCallback(Pre_Contour2DTreeRenderCallback node) {
        Pre_treeRenderCallbackContour2D = node;
    }

    static Pre_Contour2DDoWithDataCallback Pre_doWithDataCallbackContour2D;
    static void setPre_Contour2DDoWithDataCallback(Pre_Contour2DDoWithDataCallback node) {
        Pre_doWithDataCallbackContour2D = node;
    }

    static Pre_Contour2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackContour2D;
    static void setPre_Contour2DTreeDoWithDataCallback(Pre_Contour2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackContour2D = node;

    }

    static Pre_Contour2DEventsProcessedCallback Pre_eventsProcessedCallbackContour2D;
    static void setPre_Contour2DEventsProcessedCallback(Pre_Contour2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackContour2D = node;
    }

    Pre_Contour2D() {
    }
    void treeRender() {
        if (Pre_Contour2D.Pre_treeRenderCallbackContour2D != null) {
            Pre_Contour2D.Pre_treeRenderCallbackContour2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Contour2D.Pre_renderCallbackContour2D != null)
            Pre_Contour2D.Pre_renderCallbackContour2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Contour2D.Pre_treeDoWithDataCallbackContour2D != null) {
            Pre_Contour2D.Pre_treeDoWithDataCallbackContour2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Contour2D.Pre_doWithDataCallbackContour2D != null)
            Pre_Contour2D.Pre_doWithDataCallbackContour2D.doWithData(this);
    }
}

class Pre_Contour2DRenderCallback extends RenderCallback {}
class Pre_Contour2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Contour2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Contour2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Contour2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_HAnimJoint extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    Pre_Node [] children;
    Pre_Node [] displacers;
    float[] limitOrientation;
    float[] llimit;
    String name;
    int[] skinCoordIndex;
    float[] skinCoordWeight;
    float[] stiffness;
    float[] ulimit;
    static Pre_HAnimJointRenderCallback Pre_renderCallbackHAnimJoint;
    static void setPre_HAnimJointRenderCallback(Pre_HAnimJointRenderCallback node) {
        Pre_renderCallbackHAnimJoint = node;
    }

    static Pre_HAnimJointTreeRenderCallback Pre_treeRenderCallbackHAnimJoint;
    static void setPre_HAnimJointTreeRenderCallback(Pre_HAnimJointTreeRenderCallback node) {
        Pre_treeRenderCallbackHAnimJoint = node;
    }

    static Pre_HAnimJointDoWithDataCallback Pre_doWithDataCallbackHAnimJoint;
    static void setPre_HAnimJointDoWithDataCallback(Pre_HAnimJointDoWithDataCallback node) {
        Pre_doWithDataCallbackHAnimJoint = node;
    }

    static Pre_HAnimJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackHAnimJoint;
    static void setPre_HAnimJointTreeDoWithDataCallback(Pre_HAnimJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackHAnimJoint = node;

    }

    static Pre_HAnimJointEventsProcessedCallback Pre_eventsProcessedCallbackHAnimJoint;
    static void setPre_HAnimJointEventsProcessedCallback(Pre_HAnimJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackHAnimJoint = node;
    }

    Pre_HAnimJoint() {
    }
    void treeRender() {
        if (Pre_HAnimJoint.Pre_treeRenderCallbackHAnimJoint != null) {
            Pre_HAnimJoint.Pre_treeRenderCallbackHAnimJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (displacers != null)
            for (int i = 0; i < displacers.length; i++)
                if (displacers[i] != null)
                    displacers[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_HAnimJoint.Pre_renderCallbackHAnimJoint != null)
            Pre_HAnimJoint.Pre_renderCallbackHAnimJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_HAnimJoint.Pre_treeDoWithDataCallbackHAnimJoint != null) {
            Pre_HAnimJoint.Pre_treeDoWithDataCallbackHAnimJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (displacers != null)
            for (int i = 0; i < displacers.length; i++)
                if (displacers[i] != null)
                    displacers[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_HAnimJoint.Pre_doWithDataCallbackHAnimJoint != null)
            Pre_HAnimJoint.Pre_doWithDataCallbackHAnimJoint.doWithData(this);
    }
}

class Pre_HAnimJointRenderCallback extends RenderCallback {}
class Pre_HAnimJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_HAnimJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_HAnimJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_HAnimJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TexCoordDamper2D extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_TexCoordDamper2DRenderCallback Pre_renderCallbackTexCoordDamper2D;
    static void setPre_TexCoordDamper2DRenderCallback(Pre_TexCoordDamper2DRenderCallback node) {
        Pre_renderCallbackTexCoordDamper2D = node;
    }

    static Pre_TexCoordDamper2DTreeRenderCallback Pre_treeRenderCallbackTexCoordDamper2D;
    static void setPre_TexCoordDamper2DTreeRenderCallback(Pre_TexCoordDamper2DTreeRenderCallback node) {
        Pre_treeRenderCallbackTexCoordDamper2D = node;
    }

    static Pre_TexCoordDamper2DDoWithDataCallback Pre_doWithDataCallbackTexCoordDamper2D;
    static void setPre_TexCoordDamper2DDoWithDataCallback(Pre_TexCoordDamper2DDoWithDataCallback node) {
        Pre_doWithDataCallbackTexCoordDamper2D = node;
    }

    static Pre_TexCoordDamper2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTexCoordDamper2D;
    static void setPre_TexCoordDamper2DTreeDoWithDataCallback(Pre_TexCoordDamper2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTexCoordDamper2D = node;

    }

    static Pre_TexCoordDamper2DEventsProcessedCallback Pre_eventsProcessedCallbackTexCoordDamper2D;
    static void setPre_TexCoordDamper2DEventsProcessedCallback(Pre_TexCoordDamper2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTexCoordDamper2D = node;
    }

    Pre_TexCoordDamper2D() {
    }
    void treeRender() {
        if (Pre_TexCoordDamper2D.Pre_treeRenderCallbackTexCoordDamper2D != null) {
            Pre_TexCoordDamper2D.Pre_treeRenderCallbackTexCoordDamper2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TexCoordDamper2D.Pre_renderCallbackTexCoordDamper2D != null)
            Pre_TexCoordDamper2D.Pre_renderCallbackTexCoordDamper2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TexCoordDamper2D.Pre_treeDoWithDataCallbackTexCoordDamper2D != null) {
            Pre_TexCoordDamper2D.Pre_treeDoWithDataCallbackTexCoordDamper2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TexCoordDamper2D.Pre_doWithDataCallbackTexCoordDamper2D != null)
            Pre_TexCoordDamper2D.Pre_doWithDataCallbackTexCoordDamper2D.doWithData(this);
    }
}

class Pre_TexCoordDamper2DRenderCallback extends RenderCallback {}
class Pre_TexCoordDamper2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TexCoordDamper2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TexCoordDamper2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TexCoordDamper2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureCoordinate3D extends Pre_Node {
    Pre_Node metadata;
    float[] point;
    static Pre_TextureCoordinate3DRenderCallback Pre_renderCallbackTextureCoordinate3D;
    static void setPre_TextureCoordinate3DRenderCallback(Pre_TextureCoordinate3DRenderCallback node) {
        Pre_renderCallbackTextureCoordinate3D = node;
    }

    static Pre_TextureCoordinate3DTreeRenderCallback Pre_treeRenderCallbackTextureCoordinate3D;
    static void setPre_TextureCoordinate3DTreeRenderCallback(Pre_TextureCoordinate3DTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureCoordinate3D = node;
    }

    static Pre_TextureCoordinate3DDoWithDataCallback Pre_doWithDataCallbackTextureCoordinate3D;
    static void setPre_TextureCoordinate3DDoWithDataCallback(Pre_TextureCoordinate3DDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureCoordinate3D = node;
    }

    static Pre_TextureCoordinate3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureCoordinate3D;
    static void setPre_TextureCoordinate3DTreeDoWithDataCallback(Pre_TextureCoordinate3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureCoordinate3D = node;

    }

    static Pre_TextureCoordinate3DEventsProcessedCallback Pre_eventsProcessedCallbackTextureCoordinate3D;
    static void setPre_TextureCoordinate3DEventsProcessedCallback(Pre_TextureCoordinate3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureCoordinate3D = node;
    }

    Pre_TextureCoordinate3D() {
    }
    void treeRender() {
        if (Pre_TextureCoordinate3D.Pre_treeRenderCallbackTextureCoordinate3D != null) {
            Pre_TextureCoordinate3D.Pre_treeRenderCallbackTextureCoordinate3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureCoordinate3D.Pre_renderCallbackTextureCoordinate3D != null)
            Pre_TextureCoordinate3D.Pre_renderCallbackTextureCoordinate3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureCoordinate3D.Pre_treeDoWithDataCallbackTextureCoordinate3D != null) {
            Pre_TextureCoordinate3D.Pre_treeDoWithDataCallbackTextureCoordinate3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureCoordinate3D.Pre_doWithDataCallbackTextureCoordinate3D != null)
            Pre_TextureCoordinate3D.Pre_doWithDataCallbackTextureCoordinate3D.doWithData(this);
    }
}

class Pre_TextureCoordinate3DRenderCallback extends RenderCallback {}
class Pre_TextureCoordinate3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureCoordinate3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureCoordinate3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureCoordinate3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_UniversalJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] anchorPoint;
    float[] axis1;
    float[] axis2;
    float stop1Bounce;
    float stop1ErrorCorrection;
    float stop2Bounce;
    float stop2ErrorCorrection;
    static Pre_UniversalJointRenderCallback Pre_renderCallbackUniversalJoint;
    static void setPre_UniversalJointRenderCallback(Pre_UniversalJointRenderCallback node) {
        Pre_renderCallbackUniversalJoint = node;
    }

    static Pre_UniversalJointTreeRenderCallback Pre_treeRenderCallbackUniversalJoint;
    static void setPre_UniversalJointTreeRenderCallback(Pre_UniversalJointTreeRenderCallback node) {
        Pre_treeRenderCallbackUniversalJoint = node;
    }

    static Pre_UniversalJointDoWithDataCallback Pre_doWithDataCallbackUniversalJoint;
    static void setPre_UniversalJointDoWithDataCallback(Pre_UniversalJointDoWithDataCallback node) {
        Pre_doWithDataCallbackUniversalJoint = node;
    }

    static Pre_UniversalJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackUniversalJoint;
    static void setPre_UniversalJointTreeDoWithDataCallback(Pre_UniversalJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackUniversalJoint = node;

    }

    static Pre_UniversalJointEventsProcessedCallback Pre_eventsProcessedCallbackUniversalJoint;
    static void setPre_UniversalJointEventsProcessedCallback(Pre_UniversalJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackUniversalJoint = node;
    }

    Pre_UniversalJoint() {
    }
    void treeRender() {
        if (Pre_UniversalJoint.Pre_treeRenderCallbackUniversalJoint != null) {
            Pre_UniversalJoint.Pre_treeRenderCallbackUniversalJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_UniversalJoint.Pre_renderCallbackUniversalJoint != null)
            Pre_UniversalJoint.Pre_renderCallbackUniversalJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_UniversalJoint.Pre_treeDoWithDataCallbackUniversalJoint != null) {
            Pre_UniversalJoint.Pre_treeDoWithDataCallbackUniversalJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_UniversalJoint.Pre_doWithDataCallbackUniversalJoint != null)
            Pre_UniversalJoint.Pre_doWithDataCallbackUniversalJoint.doWithData(this);
    }
}

class Pre_UniversalJointRenderCallback extends RenderCallback {}
class Pre_UniversalJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_UniversalJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_UniversalJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_UniversalJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_FloatVertexAttribute extends Pre_Node {
    Pre_Node metadata;
    float[] value;
    String name;
    int numComponents;
    static Pre_FloatVertexAttributeRenderCallback Pre_renderCallbackFloatVertexAttribute;
    static void setPre_FloatVertexAttributeRenderCallback(Pre_FloatVertexAttributeRenderCallback node) {
        Pre_renderCallbackFloatVertexAttribute = node;
    }

    static Pre_FloatVertexAttributeTreeRenderCallback Pre_treeRenderCallbackFloatVertexAttribute;
    static void setPre_FloatVertexAttributeTreeRenderCallback(Pre_FloatVertexAttributeTreeRenderCallback node) {
        Pre_treeRenderCallbackFloatVertexAttribute = node;
    }

    static Pre_FloatVertexAttributeDoWithDataCallback Pre_doWithDataCallbackFloatVertexAttribute;
    static void setPre_FloatVertexAttributeDoWithDataCallback(Pre_FloatVertexAttributeDoWithDataCallback node) {
        Pre_doWithDataCallbackFloatVertexAttribute = node;
    }

    static Pre_FloatVertexAttributeTreeDoWithDataCallback Pre_treeDoWithDataCallbackFloatVertexAttribute;
    static void setPre_FloatVertexAttributeTreeDoWithDataCallback(Pre_FloatVertexAttributeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackFloatVertexAttribute = node;

    }

    static Pre_FloatVertexAttributeEventsProcessedCallback Pre_eventsProcessedCallbackFloatVertexAttribute;
    static void setPre_FloatVertexAttributeEventsProcessedCallback(Pre_FloatVertexAttributeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackFloatVertexAttribute = node;
    }

    Pre_FloatVertexAttribute() {
    }
    void treeRender() {
        if (Pre_FloatVertexAttribute.Pre_treeRenderCallbackFloatVertexAttribute != null) {
            Pre_FloatVertexAttribute.Pre_treeRenderCallbackFloatVertexAttribute.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_FloatVertexAttribute.Pre_renderCallbackFloatVertexAttribute != null)
            Pre_FloatVertexAttribute.Pre_renderCallbackFloatVertexAttribute.render(this);
    }
    void treeDoWithData() {
        if (Pre_FloatVertexAttribute.Pre_treeDoWithDataCallbackFloatVertexAttribute != null) {
            Pre_FloatVertexAttribute.Pre_treeDoWithDataCallbackFloatVertexAttribute.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_FloatVertexAttribute.Pre_doWithDataCallbackFloatVertexAttribute != null)
            Pre_FloatVertexAttribute.Pre_doWithDataCallbackFloatVertexAttribute.doWithData(this);
    }
}

class Pre_FloatVertexAttributeRenderCallback extends RenderCallback {}
class Pre_FloatVertexAttributeTreeRenderCallback extends TreeRenderCallback {}
class Pre_FloatVertexAttributeDoWithDataCallback extends DoWithDataCallback {};
class Pre_FloatVertexAttributeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_FloatVertexAttributeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SuperShape extends Pre_Node {
    Pre_Node metadata;
    float ua;
    float ub;
    float um;
    float un1;
    float un2;
    float un3;
    float va;
    float vb;
    float vm;
    float vn1;
    float vn2;
    float vn3;
    boolean bottom;
    float bottomBorder;
    float border;
    boolean ccw;
    float creaseAngle;
    float[] size;
    boolean solid;
    Pre_Node texCoord;
    boolean top;
    int uTessellation;
    int vTessellation;
    static Pre_SuperShapeRenderCallback Pre_renderCallbackSuperShape;
    static void setPre_SuperShapeRenderCallback(Pre_SuperShapeRenderCallback node) {
        Pre_renderCallbackSuperShape = node;
    }

    static Pre_SuperShapeTreeRenderCallback Pre_treeRenderCallbackSuperShape;
    static void setPre_SuperShapeTreeRenderCallback(Pre_SuperShapeTreeRenderCallback node) {
        Pre_treeRenderCallbackSuperShape = node;
    }

    static Pre_SuperShapeDoWithDataCallback Pre_doWithDataCallbackSuperShape;
    static void setPre_SuperShapeDoWithDataCallback(Pre_SuperShapeDoWithDataCallback node) {
        Pre_doWithDataCallbackSuperShape = node;
    }

    static Pre_SuperShapeTreeDoWithDataCallback Pre_treeDoWithDataCallbackSuperShape;
    static void setPre_SuperShapeTreeDoWithDataCallback(Pre_SuperShapeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSuperShape = node;

    }

    static Pre_SuperShapeEventsProcessedCallback Pre_eventsProcessedCallbackSuperShape;
    static void setPre_SuperShapeEventsProcessedCallback(Pre_SuperShapeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSuperShape = node;
    }

    Pre_SuperShape() {
    }
    void treeRender() {
        if (Pre_SuperShape.Pre_treeRenderCallbackSuperShape != null) {
            Pre_SuperShape.Pre_treeRenderCallbackSuperShape.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SuperShape.Pre_renderCallbackSuperShape != null)
            Pre_SuperShape.Pre_renderCallbackSuperShape.render(this);
    }
    void treeDoWithData() {
        if (Pre_SuperShape.Pre_treeDoWithDataCallbackSuperShape != null) {
            Pre_SuperShape.Pre_treeDoWithDataCallbackSuperShape.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SuperShape.Pre_doWithDataCallbackSuperShape != null)
            Pre_SuperShape.Pre_doWithDataCallbackSuperShape.doWithData(this);
    }
}

class Pre_SuperShapeRenderCallback extends RenderCallback {}
class Pre_SuperShapeTreeRenderCallback extends TreeRenderCallback {}
class Pre_SuperShapeDoWithDataCallback extends DoWithDataCallback {};
class Pre_SuperShapeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SuperShapeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TransmitterPdu extends Pre_Node {
    Pre_Node metadata;
    float[] antennaLocation;
    int antennaPatternLength;
    int antennaPatternType;
    int cryptoKeyID;
    int cryptoSystem;
    int frequency;
    int inputSource;
    int lengthOfModulationParameters;
    int modulationTypeDetail;
    int modulationTypeMajor;
    int modulationTypeSpreadSpectrum;
    int modulationTypeSystem;
    float power;
    int radioEntityTypeCategory;
    int radioEntityTypeCountry;
    int radioEntityTypeDomain;
    int radioEntityTypeKind;
    int radioEntityTypeNomenclature;
    int radioEntityTypeNomenclatureVersion;
    int radioID;
    float[] relativeAntennaLocation;
    float transmitFrequencyBandwidth;
    int transmitState;
    int whichGeometry;
    float[] bboxCenter;
    float[] bboxSize;
    boolean enabled;
    String networkMode;
    double readInterval;
    double writeInterval;
    int siteID;
    int applicationID;
    int entityID;
    String address;
    int port;
    String multicastRelayHost;
    int multicastRelayPort;
    boolean rtpHeaderExpected;
    static Pre_TransmitterPduRenderCallback Pre_renderCallbackTransmitterPdu;
    static void setPre_TransmitterPduRenderCallback(Pre_TransmitterPduRenderCallback node) {
        Pre_renderCallbackTransmitterPdu = node;
    }

    static Pre_TransmitterPduTreeRenderCallback Pre_treeRenderCallbackTransmitterPdu;
    static void setPre_TransmitterPduTreeRenderCallback(Pre_TransmitterPduTreeRenderCallback node) {
        Pre_treeRenderCallbackTransmitterPdu = node;
    }

    static Pre_TransmitterPduDoWithDataCallback Pre_doWithDataCallbackTransmitterPdu;
    static void setPre_TransmitterPduDoWithDataCallback(Pre_TransmitterPduDoWithDataCallback node) {
        Pre_doWithDataCallbackTransmitterPdu = node;
    }

    static Pre_TransmitterPduTreeDoWithDataCallback Pre_treeDoWithDataCallbackTransmitterPdu;
    static void setPre_TransmitterPduTreeDoWithDataCallback(Pre_TransmitterPduTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTransmitterPdu = node;

    }

    static Pre_TransmitterPduEventsProcessedCallback Pre_eventsProcessedCallbackTransmitterPdu;
    static void setPre_TransmitterPduEventsProcessedCallback(Pre_TransmitterPduEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTransmitterPdu = node;
    }

    Pre_TransmitterPdu() {
    }
    void treeRender() {
        if (Pre_TransmitterPdu.Pre_treeRenderCallbackTransmitterPdu != null) {
            Pre_TransmitterPdu.Pre_treeRenderCallbackTransmitterPdu.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TransmitterPdu.Pre_renderCallbackTransmitterPdu != null)
            Pre_TransmitterPdu.Pre_renderCallbackTransmitterPdu.render(this);
    }
    void treeDoWithData() {
        if (Pre_TransmitterPdu.Pre_treeDoWithDataCallbackTransmitterPdu != null) {
            Pre_TransmitterPdu.Pre_treeDoWithDataCallbackTransmitterPdu.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TransmitterPdu.Pre_doWithDataCallbackTransmitterPdu != null)
            Pre_TransmitterPdu.Pre_doWithDataCallbackTransmitterPdu.doWithData(this);
    }
}

class Pre_TransmitterPduRenderCallback extends RenderCallback {}
class Pre_TransmitterPduTreeRenderCallback extends TreeRenderCallback {}
class Pre_TransmitterPduDoWithDataCallback extends DoWithDataCallback {};
class Pre_TransmitterPduTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TransmitterPduEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TwoSidedMaterial extends Pre_Node {
    Pre_Node metadata;
    float ambientIntensity;
    float backAmbientIntensity;
    float[] backDiffuseColor;
    float[] backEmissiveColor;
    float backShininess;
    float[] backSpecularColor;
    float backTransparency;
    float[] diffuseColor;
    float[] emissiveColor;
    float shininess;
    boolean separateBackColor;
    float[] specularColor;
    float transparency;
    static Pre_TwoSidedMaterialRenderCallback Pre_renderCallbackTwoSidedMaterial;
    static void setPre_TwoSidedMaterialRenderCallback(Pre_TwoSidedMaterialRenderCallback node) {
        Pre_renderCallbackTwoSidedMaterial = node;
    }

    static Pre_TwoSidedMaterialTreeRenderCallback Pre_treeRenderCallbackTwoSidedMaterial;
    static void setPre_TwoSidedMaterialTreeRenderCallback(Pre_TwoSidedMaterialTreeRenderCallback node) {
        Pre_treeRenderCallbackTwoSidedMaterial = node;
    }

    static Pre_TwoSidedMaterialDoWithDataCallback Pre_doWithDataCallbackTwoSidedMaterial;
    static void setPre_TwoSidedMaterialDoWithDataCallback(Pre_TwoSidedMaterialDoWithDataCallback node) {
        Pre_doWithDataCallbackTwoSidedMaterial = node;
    }

    static Pre_TwoSidedMaterialTreeDoWithDataCallback Pre_treeDoWithDataCallbackTwoSidedMaterial;
    static void setPre_TwoSidedMaterialTreeDoWithDataCallback(Pre_TwoSidedMaterialTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTwoSidedMaterial = node;

    }

    static Pre_TwoSidedMaterialEventsProcessedCallback Pre_eventsProcessedCallbackTwoSidedMaterial;
    static void setPre_TwoSidedMaterialEventsProcessedCallback(Pre_TwoSidedMaterialEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTwoSidedMaterial = node;
    }

    Pre_TwoSidedMaterial() {
    }
    void treeRender() {
        if (Pre_TwoSidedMaterial.Pre_treeRenderCallbackTwoSidedMaterial != null) {
            Pre_TwoSidedMaterial.Pre_treeRenderCallbackTwoSidedMaterial.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TwoSidedMaterial.Pre_renderCallbackTwoSidedMaterial != null)
            Pre_TwoSidedMaterial.Pre_renderCallbackTwoSidedMaterial.render(this);
    }
    void treeDoWithData() {
        if (Pre_TwoSidedMaterial.Pre_treeDoWithDataCallbackTwoSidedMaterial != null) {
            Pre_TwoSidedMaterial.Pre_treeDoWithDataCallbackTwoSidedMaterial.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TwoSidedMaterial.Pre_doWithDataCallbackTwoSidedMaterial != null)
            Pre_TwoSidedMaterial.Pre_doWithDataCallbackTwoSidedMaterial.doWithData(this);
    }
}

class Pre_TwoSidedMaterialRenderCallback extends RenderCallback {}
class Pre_TwoSidedMaterialTreeRenderCallback extends TreeRenderCallback {}
class Pre_TwoSidedMaterialDoWithDataCallback extends DoWithDataCallback {};
class Pre_TwoSidedMaterialTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TwoSidedMaterialEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_WindPhysicsModel extends Pre_Node {
    Pre_Node metadata;
    float[] direction;
    boolean enabled;
    float gustiness;
    float speed;
    float turbulence;
    static Pre_WindPhysicsModelRenderCallback Pre_renderCallbackWindPhysicsModel;
    static void setPre_WindPhysicsModelRenderCallback(Pre_WindPhysicsModelRenderCallback node) {
        Pre_renderCallbackWindPhysicsModel = node;
    }

    static Pre_WindPhysicsModelTreeRenderCallback Pre_treeRenderCallbackWindPhysicsModel;
    static void setPre_WindPhysicsModelTreeRenderCallback(Pre_WindPhysicsModelTreeRenderCallback node) {
        Pre_treeRenderCallbackWindPhysicsModel = node;
    }

    static Pre_WindPhysicsModelDoWithDataCallback Pre_doWithDataCallbackWindPhysicsModel;
    static void setPre_WindPhysicsModelDoWithDataCallback(Pre_WindPhysicsModelDoWithDataCallback node) {
        Pre_doWithDataCallbackWindPhysicsModel = node;
    }

    static Pre_WindPhysicsModelTreeDoWithDataCallback Pre_treeDoWithDataCallbackWindPhysicsModel;
    static void setPre_WindPhysicsModelTreeDoWithDataCallback(Pre_WindPhysicsModelTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackWindPhysicsModel = node;

    }

    static Pre_WindPhysicsModelEventsProcessedCallback Pre_eventsProcessedCallbackWindPhysicsModel;
    static void setPre_WindPhysicsModelEventsProcessedCallback(Pre_WindPhysicsModelEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackWindPhysicsModel = node;
    }

    Pre_WindPhysicsModel() {
    }
    void treeRender() {
        if (Pre_WindPhysicsModel.Pre_treeRenderCallbackWindPhysicsModel != null) {
            Pre_WindPhysicsModel.Pre_treeRenderCallbackWindPhysicsModel.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_WindPhysicsModel.Pre_renderCallbackWindPhysicsModel != null)
            Pre_WindPhysicsModel.Pre_renderCallbackWindPhysicsModel.render(this);
    }
    void treeDoWithData() {
        if (Pre_WindPhysicsModel.Pre_treeDoWithDataCallbackWindPhysicsModel != null) {
            Pre_WindPhysicsModel.Pre_treeDoWithDataCallbackWindPhysicsModel.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_WindPhysicsModel.Pre_doWithDataCallbackWindPhysicsModel != null)
            Pre_WindPhysicsModel.Pre_doWithDataCallbackWindPhysicsModel.doWithData(this);
    }
}

class Pre_WindPhysicsModelRenderCallback extends RenderCallback {}
class Pre_WindPhysicsModelTreeRenderCallback extends TreeRenderCallback {}
class Pre_WindPhysicsModelDoWithDataCallback extends DoWithDataCallback {};
class Pre_WindPhysicsModelTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_WindPhysicsModelEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoTransform extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    Pre_Node [] children;
    double[] geoCenter;
    float[] rotation;
    float[] scale;
    float[] scaleOrientation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_GeoTransformRenderCallback Pre_renderCallbackGeoTransform;
    static void setPre_GeoTransformRenderCallback(Pre_GeoTransformRenderCallback node) {
        Pre_renderCallbackGeoTransform = node;
    }

    static Pre_GeoTransformTreeRenderCallback Pre_treeRenderCallbackGeoTransform;
    static void setPre_GeoTransformTreeRenderCallback(Pre_GeoTransformTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoTransform = node;
    }

    static Pre_GeoTransformDoWithDataCallback Pre_doWithDataCallbackGeoTransform;
    static void setPre_GeoTransformDoWithDataCallback(Pre_GeoTransformDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoTransform = node;
    }

    static Pre_GeoTransformTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoTransform;
    static void setPre_GeoTransformTreeDoWithDataCallback(Pre_GeoTransformTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoTransform = node;

    }

    static Pre_GeoTransformEventsProcessedCallback Pre_eventsProcessedCallbackGeoTransform;
    static void setPre_GeoTransformEventsProcessedCallback(Pre_GeoTransformEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoTransform = node;
    }

    Pre_GeoTransform() {
    }
    void treeRender() {
        if (Pre_GeoTransform.Pre_treeRenderCallbackGeoTransform != null) {
            Pre_GeoTransform.Pre_treeRenderCallbackGeoTransform.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoTransform.Pre_renderCallbackGeoTransform != null)
            Pre_GeoTransform.Pre_renderCallbackGeoTransform.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoTransform.Pre_treeDoWithDataCallbackGeoTransform != null) {
            Pre_GeoTransform.Pre_treeDoWithDataCallbackGeoTransform.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoTransform.Pre_doWithDataCallbackGeoTransform != null)
            Pre_GeoTransform.Pre_doWithDataCallbackGeoTransform.doWithData(this);
    }
}

class Pre_GeoTransformRenderCallback extends RenderCallback {}
class Pre_GeoTransformTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoTransformDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoTransformTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoTransformEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Transform extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    Pre_Node [] children;
    static Pre_TransformRenderCallback Pre_renderCallbackTransform;
    static void setPre_TransformRenderCallback(Pre_TransformRenderCallback node) {
        Pre_renderCallbackTransform = node;
    }

    static Pre_TransformTreeRenderCallback Pre_treeRenderCallbackTransform;
    static void setPre_TransformTreeRenderCallback(Pre_TransformTreeRenderCallback node) {
        Pre_treeRenderCallbackTransform = node;
    }

    static Pre_TransformDoWithDataCallback Pre_doWithDataCallbackTransform;
    static void setPre_TransformDoWithDataCallback(Pre_TransformDoWithDataCallback node) {
        Pre_doWithDataCallbackTransform = node;
    }

    static Pre_TransformTreeDoWithDataCallback Pre_treeDoWithDataCallbackTransform;
    static void setPre_TransformTreeDoWithDataCallback(Pre_TransformTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTransform = node;

    }

    static Pre_TransformEventsProcessedCallback Pre_eventsProcessedCallbackTransform;
    static void setPre_TransformEventsProcessedCallback(Pre_TransformEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTransform = node;
    }

    Pre_Transform() {
    }
    void treeRender() {
        if (Pre_Transform.Pre_treeRenderCallbackTransform != null) {
            Pre_Transform.Pre_treeRenderCallbackTransform.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Transform.Pre_renderCallbackTransform != null)
            Pre_Transform.Pre_renderCallbackTransform.render(this);
    }
    void treeDoWithData() {
        if (Pre_Transform.Pre_treeDoWithDataCallbackTransform != null) {
            Pre_Transform.Pre_treeDoWithDataCallbackTransform.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Transform.Pre_doWithDataCallbackTransform != null)
            Pre_Transform.Pre_doWithDataCallbackTransform.doWithData(this);
    }
}

class Pre_TransformRenderCallback extends RenderCallback {}
class Pre_TransformTreeRenderCallback extends TreeRenderCallback {}
class Pre_TransformDoWithDataCallback extends DoWithDataCallback {};
class Pre_TransformTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TransformEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SuperExtrusion extends Pre_Node {
    Pre_Node metadata;
    float a;
    float b;
    float m;
    float n1;
    float n2;
    float n3;
    float border;
    float bottomBorder;
    int superTessellation;
    int spineTessellation;
    float[] controlPoint;
    float[] weight;
    float[] knot;
    int order;
    float creaseAngle;
    boolean beginCap;
    boolean endCap;
    boolean solid;
    float[] scale;
    static Pre_SuperExtrusionRenderCallback Pre_renderCallbackSuperExtrusion;
    static void setPre_SuperExtrusionRenderCallback(Pre_SuperExtrusionRenderCallback node) {
        Pre_renderCallbackSuperExtrusion = node;
    }

    static Pre_SuperExtrusionTreeRenderCallback Pre_treeRenderCallbackSuperExtrusion;
    static void setPre_SuperExtrusionTreeRenderCallback(Pre_SuperExtrusionTreeRenderCallback node) {
        Pre_treeRenderCallbackSuperExtrusion = node;
    }

    static Pre_SuperExtrusionDoWithDataCallback Pre_doWithDataCallbackSuperExtrusion;
    static void setPre_SuperExtrusionDoWithDataCallback(Pre_SuperExtrusionDoWithDataCallback node) {
        Pre_doWithDataCallbackSuperExtrusion = node;
    }

    static Pre_SuperExtrusionTreeDoWithDataCallback Pre_treeDoWithDataCallbackSuperExtrusion;
    static void setPre_SuperExtrusionTreeDoWithDataCallback(Pre_SuperExtrusionTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSuperExtrusion = node;

    }

    static Pre_SuperExtrusionEventsProcessedCallback Pre_eventsProcessedCallbackSuperExtrusion;
    static void setPre_SuperExtrusionEventsProcessedCallback(Pre_SuperExtrusionEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSuperExtrusion = node;
    }

    Pre_SuperExtrusion() {
    }
    void treeRender() {
        if (Pre_SuperExtrusion.Pre_treeRenderCallbackSuperExtrusion != null) {
            Pre_SuperExtrusion.Pre_treeRenderCallbackSuperExtrusion.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SuperExtrusion.Pre_renderCallbackSuperExtrusion != null)
            Pre_SuperExtrusion.Pre_renderCallbackSuperExtrusion.render(this);
    }
    void treeDoWithData() {
        if (Pre_SuperExtrusion.Pre_treeDoWithDataCallbackSuperExtrusion != null) {
            Pre_SuperExtrusion.Pre_treeDoWithDataCallbackSuperExtrusion.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SuperExtrusion.Pre_doWithDataCallbackSuperExtrusion != null)
            Pre_SuperExtrusion.Pre_doWithDataCallbackSuperExtrusion.doWithData(this);
    }
}

class Pre_SuperExtrusionRenderCallback extends RenderCallback {}
class Pre_SuperExtrusionTreeRenderCallback extends TreeRenderCallback {}
class Pre_SuperExtrusionDoWithDataCallback extends DoWithDataCallback {};
class Pre_SuperExtrusionTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SuperExtrusionEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CylinderSensor extends Pre_Node {
    Pre_Node metadata;
    boolean autoOffset;
    float[] axisRotation;
    String description;
    float diskAngle;
    boolean enabled;
    float maxAngle;
    float minAngle;
    float offset;
    static Pre_CylinderSensorRenderCallback Pre_renderCallbackCylinderSensor;
    static void setPre_CylinderSensorRenderCallback(Pre_CylinderSensorRenderCallback node) {
        Pre_renderCallbackCylinderSensor = node;
    }

    static Pre_CylinderSensorTreeRenderCallback Pre_treeRenderCallbackCylinderSensor;
    static void setPre_CylinderSensorTreeRenderCallback(Pre_CylinderSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackCylinderSensor = node;
    }

    static Pre_CylinderSensorDoWithDataCallback Pre_doWithDataCallbackCylinderSensor;
    static void setPre_CylinderSensorDoWithDataCallback(Pre_CylinderSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackCylinderSensor = node;
    }

    static Pre_CylinderSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackCylinderSensor;
    static void setPre_CylinderSensorTreeDoWithDataCallback(Pre_CylinderSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCylinderSensor = node;

    }

    static Pre_CylinderSensorEventsProcessedCallback Pre_eventsProcessedCallbackCylinderSensor;
    static void setPre_CylinderSensorEventsProcessedCallback(Pre_CylinderSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCylinderSensor = node;
    }

    Pre_CylinderSensor() {
    }
    void treeRender() {
        if (Pre_CylinderSensor.Pre_treeRenderCallbackCylinderSensor != null) {
            Pre_CylinderSensor.Pre_treeRenderCallbackCylinderSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CylinderSensor.Pre_renderCallbackCylinderSensor != null)
            Pre_CylinderSensor.Pre_renderCallbackCylinderSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_CylinderSensor.Pre_treeDoWithDataCallbackCylinderSensor != null) {
            Pre_CylinderSensor.Pre_treeDoWithDataCallbackCylinderSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CylinderSensor.Pre_doWithDataCallbackCylinderSensor != null)
            Pre_CylinderSensor.Pre_doWithDataCallbackCylinderSensor.doWithData(this);
    }
}

class Pre_CylinderSensorRenderCallback extends RenderCallback {}
class Pre_CylinderSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_CylinderSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_CylinderSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CylinderSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LoadSensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    double timeOut;
    Pre_Node [] watchList;
    static Pre_LoadSensorRenderCallback Pre_renderCallbackLoadSensor;
    static void setPre_LoadSensorRenderCallback(Pre_LoadSensorRenderCallback node) {
        Pre_renderCallbackLoadSensor = node;
    }

    static Pre_LoadSensorTreeRenderCallback Pre_treeRenderCallbackLoadSensor;
    static void setPre_LoadSensorTreeRenderCallback(Pre_LoadSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackLoadSensor = node;
    }

    static Pre_LoadSensorDoWithDataCallback Pre_doWithDataCallbackLoadSensor;
    static void setPre_LoadSensorDoWithDataCallback(Pre_LoadSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackLoadSensor = node;
    }

    static Pre_LoadSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackLoadSensor;
    static void setPre_LoadSensorTreeDoWithDataCallback(Pre_LoadSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLoadSensor = node;

    }

    static Pre_LoadSensorEventsProcessedCallback Pre_eventsProcessedCallbackLoadSensor;
    static void setPre_LoadSensorEventsProcessedCallback(Pre_LoadSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLoadSensor = node;
    }

    Pre_LoadSensor() {
    }
    void treeRender() {
        if (Pre_LoadSensor.Pre_treeRenderCallbackLoadSensor != null) {
            Pre_LoadSensor.Pre_treeRenderCallbackLoadSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (watchList != null)
            for (int i = 0; i < watchList.length; i++)
                if (watchList[i] != null)
                    watchList[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LoadSensor.Pre_renderCallbackLoadSensor != null)
            Pre_LoadSensor.Pre_renderCallbackLoadSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_LoadSensor.Pre_treeDoWithDataCallbackLoadSensor != null) {
            Pre_LoadSensor.Pre_treeDoWithDataCallbackLoadSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (watchList != null)
            for (int i = 0; i < watchList.length; i++)
                if (watchList[i] != null)
                    watchList[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LoadSensor.Pre_doWithDataCallbackLoadSensor != null)
            Pre_LoadSensor.Pre_doWithDataCallbackLoadSensor.doWithData(this);
    }
}

class Pre_LoadSensorRenderCallback extends RenderCallback {}
class Pre_LoadSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_LoadSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_LoadSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LoadSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PackagedShader extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    String language;
    static Pre_PackagedShaderRenderCallback Pre_renderCallbackPackagedShader;
    static void setPre_PackagedShaderRenderCallback(Pre_PackagedShaderRenderCallback node) {
        Pre_renderCallbackPackagedShader = node;
    }

    static Pre_PackagedShaderTreeRenderCallback Pre_treeRenderCallbackPackagedShader;
    static void setPre_PackagedShaderTreeRenderCallback(Pre_PackagedShaderTreeRenderCallback node) {
        Pre_treeRenderCallbackPackagedShader = node;
    }

    static Pre_PackagedShaderDoWithDataCallback Pre_doWithDataCallbackPackagedShader;
    static void setPre_PackagedShaderDoWithDataCallback(Pre_PackagedShaderDoWithDataCallback node) {
        Pre_doWithDataCallbackPackagedShader = node;
    }

    static Pre_PackagedShaderTreeDoWithDataCallback Pre_treeDoWithDataCallbackPackagedShader;
    static void setPre_PackagedShaderTreeDoWithDataCallback(Pre_PackagedShaderTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPackagedShader = node;

    }

    static Pre_PackagedShaderEventsProcessedCallback Pre_eventsProcessedCallbackPackagedShader;
    static void setPre_PackagedShaderEventsProcessedCallback(Pre_PackagedShaderEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPackagedShader = node;
    }

    Pre_PackagedShader() {
    }
    void treeRender() {
        if (Pre_PackagedShader.Pre_treeRenderCallbackPackagedShader != null) {
            Pre_PackagedShader.Pre_treeRenderCallbackPackagedShader.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PackagedShader.Pre_renderCallbackPackagedShader != null)
            Pre_PackagedShader.Pre_renderCallbackPackagedShader.render(this);
    }
    void treeDoWithData() {
        if (Pre_PackagedShader.Pre_treeDoWithDataCallbackPackagedShader != null) {
            Pre_PackagedShader.Pre_treeDoWithDataCallbackPackagedShader.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PackagedShader.Pre_doWithDataCallbackPackagedShader != null)
            Pre_PackagedShader.Pre_doWithDataCallbackPackagedShader.doWithData(this);
    }
}

class Pre_PackagedShaderRenderCallback extends RenderCallback {}
class Pre_PackagedShaderTreeRenderCallback extends TreeRenderCallback {}
class Pre_PackagedShaderDoWithDataCallback extends DoWithDataCallback {};
class Pre_PackagedShaderTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PackagedShaderEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SuperRevolver extends Pre_Node {
    Pre_Node metadata;
    float a;
    float b;
    float m;
    float n1;
    float n2;
    float n3;
    float border;
    float bottomBorder;
    int superTessellation;
    int nurbsTessellation;
    float[] controlPoint;
    float[] weight;
    float[] knot;
    int order;
    float creaseAngle;
    boolean ccw;
    boolean solid;
    boolean pieceOfCake;
    static Pre_SuperRevolverRenderCallback Pre_renderCallbackSuperRevolver;
    static void setPre_SuperRevolverRenderCallback(Pre_SuperRevolverRenderCallback node) {
        Pre_renderCallbackSuperRevolver = node;
    }

    static Pre_SuperRevolverTreeRenderCallback Pre_treeRenderCallbackSuperRevolver;
    static void setPre_SuperRevolverTreeRenderCallback(Pre_SuperRevolverTreeRenderCallback node) {
        Pre_treeRenderCallbackSuperRevolver = node;
    }

    static Pre_SuperRevolverDoWithDataCallback Pre_doWithDataCallbackSuperRevolver;
    static void setPre_SuperRevolverDoWithDataCallback(Pre_SuperRevolverDoWithDataCallback node) {
        Pre_doWithDataCallbackSuperRevolver = node;
    }

    static Pre_SuperRevolverTreeDoWithDataCallback Pre_treeDoWithDataCallbackSuperRevolver;
    static void setPre_SuperRevolverTreeDoWithDataCallback(Pre_SuperRevolverTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSuperRevolver = node;

    }

    static Pre_SuperRevolverEventsProcessedCallback Pre_eventsProcessedCallbackSuperRevolver;
    static void setPre_SuperRevolverEventsProcessedCallback(Pre_SuperRevolverEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSuperRevolver = node;
    }

    Pre_SuperRevolver() {
    }
    void treeRender() {
        if (Pre_SuperRevolver.Pre_treeRenderCallbackSuperRevolver != null) {
            Pre_SuperRevolver.Pre_treeRenderCallbackSuperRevolver.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SuperRevolver.Pre_renderCallbackSuperRevolver != null)
            Pre_SuperRevolver.Pre_renderCallbackSuperRevolver.render(this);
    }
    void treeDoWithData() {
        if (Pre_SuperRevolver.Pre_treeDoWithDataCallbackSuperRevolver != null) {
            Pre_SuperRevolver.Pre_treeDoWithDataCallbackSuperRevolver.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SuperRevolver.Pre_doWithDataCallbackSuperRevolver != null)
            Pre_SuperRevolver.Pre_doWithDataCallbackSuperRevolver.doWithData(this);
    }
}

class Pre_SuperRevolverRenderCallback extends RenderCallback {}
class Pre_SuperRevolverTreeRenderCallback extends TreeRenderCallback {}
class Pre_SuperRevolverDoWithDataCallback extends DoWithDataCallback {};
class Pre_SuperRevolverTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SuperRevolverEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureCoordinateGenerator extends Pre_Node {
    Pre_Node metadata;
    String mode;
    float[] parameter;
    static Pre_TextureCoordinateGeneratorRenderCallback Pre_renderCallbackTextureCoordinateGenerator;
    static void setPre_TextureCoordinateGeneratorRenderCallback(Pre_TextureCoordinateGeneratorRenderCallback node) {
        Pre_renderCallbackTextureCoordinateGenerator = node;
    }

    static Pre_TextureCoordinateGeneratorTreeRenderCallback Pre_treeRenderCallbackTextureCoordinateGenerator;
    static void setPre_TextureCoordinateGeneratorTreeRenderCallback(Pre_TextureCoordinateGeneratorTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureCoordinateGenerator = node;
    }

    static Pre_TextureCoordinateGeneratorDoWithDataCallback Pre_doWithDataCallbackTextureCoordinateGenerator;
    static void setPre_TextureCoordinateGeneratorDoWithDataCallback(Pre_TextureCoordinateGeneratorDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureCoordinateGenerator = node;
    }

    static Pre_TextureCoordinateGeneratorTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureCoordinateGenerator;
    static void setPre_TextureCoordinateGeneratorTreeDoWithDataCallback(Pre_TextureCoordinateGeneratorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureCoordinateGenerator = node;

    }

    static Pre_TextureCoordinateGeneratorEventsProcessedCallback Pre_eventsProcessedCallbackTextureCoordinateGenerator;
    static void setPre_TextureCoordinateGeneratorEventsProcessedCallback(Pre_TextureCoordinateGeneratorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureCoordinateGenerator = node;
    }

    Pre_TextureCoordinateGenerator() {
    }
    void treeRender() {
        if (Pre_TextureCoordinateGenerator.Pre_treeRenderCallbackTextureCoordinateGenerator != null) {
            Pre_TextureCoordinateGenerator.Pre_treeRenderCallbackTextureCoordinateGenerator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureCoordinateGenerator.Pre_renderCallbackTextureCoordinateGenerator != null)
            Pre_TextureCoordinateGenerator.Pre_renderCallbackTextureCoordinateGenerator.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureCoordinateGenerator.Pre_treeDoWithDataCallbackTextureCoordinateGenerator != null) {
            Pre_TextureCoordinateGenerator.Pre_treeDoWithDataCallbackTextureCoordinateGenerator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureCoordinateGenerator.Pre_doWithDataCallbackTextureCoordinateGenerator != null)
            Pre_TextureCoordinateGenerator.Pre_doWithDataCallbackTextureCoordinateGenerator.doWithData(this);
    }
}

class Pre_TextureCoordinateGeneratorRenderCallback extends RenderCallback {}
class Pre_TextureCoordinateGeneratorTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureCoordinateGeneratorDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureCoordinateGeneratorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureCoordinateGeneratorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureProperties extends Pre_Node {
    Pre_Node metadata;
    float anisotropicDegree;
    float[] borderColor;
    int borderWidth;
    String boundaryModeS;
    String boundaryModeT;
    String boundaryModeR;
    String magnificationFilter;
    String minificationFilter;
    String textureCompression;
    float texturePriority;
    boolean generateMipMaps;
    static Pre_TexturePropertiesRenderCallback Pre_renderCallbackTextureProperties;
    static void setPre_TexturePropertiesRenderCallback(Pre_TexturePropertiesRenderCallback node) {
        Pre_renderCallbackTextureProperties = node;
    }

    static Pre_TexturePropertiesTreeRenderCallback Pre_treeRenderCallbackTextureProperties;
    static void setPre_TexturePropertiesTreeRenderCallback(Pre_TexturePropertiesTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureProperties = node;
    }

    static Pre_TexturePropertiesDoWithDataCallback Pre_doWithDataCallbackTextureProperties;
    static void setPre_TexturePropertiesDoWithDataCallback(Pre_TexturePropertiesDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureProperties = node;
    }

    static Pre_TexturePropertiesTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureProperties;
    static void setPre_TexturePropertiesTreeDoWithDataCallback(Pre_TexturePropertiesTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureProperties = node;

    }

    static Pre_TexturePropertiesEventsProcessedCallback Pre_eventsProcessedCallbackTextureProperties;
    static void setPre_TexturePropertiesEventsProcessedCallback(Pre_TexturePropertiesEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureProperties = node;
    }

    Pre_TextureProperties() {
    }
    void treeRender() {
        if (Pre_TextureProperties.Pre_treeRenderCallbackTextureProperties != null) {
            Pre_TextureProperties.Pre_treeRenderCallbackTextureProperties.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureProperties.Pre_renderCallbackTextureProperties != null)
            Pre_TextureProperties.Pre_renderCallbackTextureProperties.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureProperties.Pre_treeDoWithDataCallbackTextureProperties != null) {
            Pre_TextureProperties.Pre_treeDoWithDataCallbackTextureProperties.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureProperties.Pre_doWithDataCallbackTextureProperties != null)
            Pre_TextureProperties.Pre_doWithDataCallbackTextureProperties.doWithData(this);
    }
}

class Pre_TexturePropertiesRenderCallback extends RenderCallback {}
class Pre_TexturePropertiesTreeRenderCallback extends TreeRenderCallback {}
class Pre_TexturePropertiesDoWithDataCallback extends DoWithDataCallback {};
class Pre_TexturePropertiesTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TexturePropertiesEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ArcClose2D extends Pre_Node {
    Pre_Node metadata;
    String closureType;
    float endAngle;
    float radius;
    boolean solid;
    float startAngle;
    static Pre_ArcClose2DRenderCallback Pre_renderCallbackArcClose2D;
    static void setPre_ArcClose2DRenderCallback(Pre_ArcClose2DRenderCallback node) {
        Pre_renderCallbackArcClose2D = node;
    }

    static Pre_ArcClose2DTreeRenderCallback Pre_treeRenderCallbackArcClose2D;
    static void setPre_ArcClose2DTreeRenderCallback(Pre_ArcClose2DTreeRenderCallback node) {
        Pre_treeRenderCallbackArcClose2D = node;
    }

    static Pre_ArcClose2DDoWithDataCallback Pre_doWithDataCallbackArcClose2D;
    static void setPre_ArcClose2DDoWithDataCallback(Pre_ArcClose2DDoWithDataCallback node) {
        Pre_doWithDataCallbackArcClose2D = node;
    }

    static Pre_ArcClose2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackArcClose2D;
    static void setPre_ArcClose2DTreeDoWithDataCallback(Pre_ArcClose2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackArcClose2D = node;

    }

    static Pre_ArcClose2DEventsProcessedCallback Pre_eventsProcessedCallbackArcClose2D;
    static void setPre_ArcClose2DEventsProcessedCallback(Pre_ArcClose2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackArcClose2D = node;
    }

    Pre_ArcClose2D() {
    }
    void treeRender() {
        if (Pre_ArcClose2D.Pre_treeRenderCallbackArcClose2D != null) {
            Pre_ArcClose2D.Pre_treeRenderCallbackArcClose2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ArcClose2D.Pre_renderCallbackArcClose2D != null)
            Pre_ArcClose2D.Pre_renderCallbackArcClose2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_ArcClose2D.Pre_treeDoWithDataCallbackArcClose2D != null) {
            Pre_ArcClose2D.Pre_treeDoWithDataCallbackArcClose2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ArcClose2D.Pre_doWithDataCallbackArcClose2D != null)
            Pre_ArcClose2D.Pre_doWithDataCallbackArcClose2D.doWithData(this);
    }
}

class Pre_ArcClose2DRenderCallback extends RenderCallback {}
class Pre_ArcClose2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_ArcClose2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_ArcClose2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ArcClose2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ElevationGrid extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] attrib;
    Pre_Node color;
    Pre_Node fogCoord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    float creaseAngle;
    float[] height;
    boolean normalPerVertex;
    boolean solid;
    int xDimension;
    float xSpacing;
    int zDimension;
    float zSpacing;
    static Pre_ElevationGridRenderCallback Pre_renderCallbackElevationGrid;
    static void setPre_ElevationGridRenderCallback(Pre_ElevationGridRenderCallback node) {
        Pre_renderCallbackElevationGrid = node;
    }

    static Pre_ElevationGridTreeRenderCallback Pre_treeRenderCallbackElevationGrid;
    static void setPre_ElevationGridTreeRenderCallback(Pre_ElevationGridTreeRenderCallback node) {
        Pre_treeRenderCallbackElevationGrid = node;
    }

    static Pre_ElevationGridDoWithDataCallback Pre_doWithDataCallbackElevationGrid;
    static void setPre_ElevationGridDoWithDataCallback(Pre_ElevationGridDoWithDataCallback node) {
        Pre_doWithDataCallbackElevationGrid = node;
    }

    static Pre_ElevationGridTreeDoWithDataCallback Pre_treeDoWithDataCallbackElevationGrid;
    static void setPre_ElevationGridTreeDoWithDataCallback(Pre_ElevationGridTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackElevationGrid = node;

    }

    static Pre_ElevationGridEventsProcessedCallback Pre_eventsProcessedCallbackElevationGrid;
    static void setPre_ElevationGridEventsProcessedCallback(Pre_ElevationGridEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackElevationGrid = node;
    }

    Pre_ElevationGrid() {
    }
    void treeRender() {
        if (Pre_ElevationGrid.Pre_treeRenderCallbackElevationGrid != null) {
            Pre_ElevationGrid.Pre_treeRenderCallbackElevationGrid.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (color != null)
            color.treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ElevationGrid.Pre_renderCallbackElevationGrid != null)
            Pre_ElevationGrid.Pre_renderCallbackElevationGrid.render(this);
    }
    void treeDoWithData() {
        if (Pre_ElevationGrid.Pre_treeDoWithDataCallbackElevationGrid != null) {
            Pre_ElevationGrid.Pre_treeDoWithDataCallbackElevationGrid.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ElevationGrid.Pre_doWithDataCallbackElevationGrid != null)
            Pre_ElevationGrid.Pre_doWithDataCallbackElevationGrid.doWithData(this);
    }
}

class Pre_ElevationGridRenderCallback extends RenderCallback {}
class Pre_ElevationGridTreeRenderCallback extends TreeRenderCallback {}
class Pre_ElevationGridDoWithDataCallback extends DoWithDataCallback {};
class Pre_ElevationGridTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ElevationGridEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedTriangleFanSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean normalPerVertex;
    boolean solid;
    int[] index;
    boolean colorPerVertex;
    static Pre_IndexedTriangleFanSetRenderCallback Pre_renderCallbackIndexedTriangleFanSet;
    static void setPre_IndexedTriangleFanSetRenderCallback(Pre_IndexedTriangleFanSetRenderCallback node) {
        Pre_renderCallbackIndexedTriangleFanSet = node;
    }

    static Pre_IndexedTriangleFanSetTreeRenderCallback Pre_treeRenderCallbackIndexedTriangleFanSet;
    static void setPre_IndexedTriangleFanSetTreeRenderCallback(Pre_IndexedTriangleFanSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedTriangleFanSet = node;
    }

    static Pre_IndexedTriangleFanSetDoWithDataCallback Pre_doWithDataCallbackIndexedTriangleFanSet;
    static void setPre_IndexedTriangleFanSetDoWithDataCallback(Pre_IndexedTriangleFanSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedTriangleFanSet = node;
    }

    static Pre_IndexedTriangleFanSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedTriangleFanSet;
    static void setPre_IndexedTriangleFanSetTreeDoWithDataCallback(Pre_IndexedTriangleFanSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedTriangleFanSet = node;

    }

    static Pre_IndexedTriangleFanSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedTriangleFanSet;
    static void setPre_IndexedTriangleFanSetEventsProcessedCallback(Pre_IndexedTriangleFanSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedTriangleFanSet = node;
    }

    Pre_IndexedTriangleFanSet() {
    }
    void treeRender() {
        if (Pre_IndexedTriangleFanSet.Pre_treeRenderCallbackIndexedTriangleFanSet != null) {
            Pre_IndexedTriangleFanSet.Pre_treeRenderCallbackIndexedTriangleFanSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedTriangleFanSet.Pre_renderCallbackIndexedTriangleFanSet != null)
            Pre_IndexedTriangleFanSet.Pre_renderCallbackIndexedTriangleFanSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedTriangleFanSet.Pre_treeDoWithDataCallbackIndexedTriangleFanSet != null) {
            Pre_IndexedTriangleFanSet.Pre_treeDoWithDataCallbackIndexedTriangleFanSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedTriangleFanSet.Pre_doWithDataCallbackIndexedTriangleFanSet != null)
            Pre_IndexedTriangleFanSet.Pre_doWithDataCallbackIndexedTriangleFanSet.doWithData(this);
    }
}

class Pre_IndexedTriangleFanSetRenderCallback extends RenderCallback {}
class Pre_IndexedTriangleFanSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedTriangleFanSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedTriangleFanSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedTriangleFanSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsCurve2D extends Pre_Node {
    Pre_Node metadata;
    float[] controlPoint;
    int tessellation;
    float[] weight;
    boolean closed;
    float[] knot;
    int order;
    static Pre_NurbsCurve2DRenderCallback Pre_renderCallbackNurbsCurve2D;
    static void setPre_NurbsCurve2DRenderCallback(Pre_NurbsCurve2DRenderCallback node) {
        Pre_renderCallbackNurbsCurve2D = node;
    }

    static Pre_NurbsCurve2DTreeRenderCallback Pre_treeRenderCallbackNurbsCurve2D;
    static void setPre_NurbsCurve2DTreeRenderCallback(Pre_NurbsCurve2DTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsCurve2D = node;
    }

    static Pre_NurbsCurve2DDoWithDataCallback Pre_doWithDataCallbackNurbsCurve2D;
    static void setPre_NurbsCurve2DDoWithDataCallback(Pre_NurbsCurve2DDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsCurve2D = node;
    }

    static Pre_NurbsCurve2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsCurve2D;
    static void setPre_NurbsCurve2DTreeDoWithDataCallback(Pre_NurbsCurve2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsCurve2D = node;

    }

    static Pre_NurbsCurve2DEventsProcessedCallback Pre_eventsProcessedCallbackNurbsCurve2D;
    static void setPre_NurbsCurve2DEventsProcessedCallback(Pre_NurbsCurve2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsCurve2D = node;
    }

    Pre_NurbsCurve2D() {
    }
    void treeRender() {
        if (Pre_NurbsCurve2D.Pre_treeRenderCallbackNurbsCurve2D != null) {
            Pre_NurbsCurve2D.Pre_treeRenderCallbackNurbsCurve2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsCurve2D.Pre_renderCallbackNurbsCurve2D != null)
            Pre_NurbsCurve2D.Pre_renderCallbackNurbsCurve2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsCurve2D.Pre_treeDoWithDataCallbackNurbsCurve2D != null) {
            Pre_NurbsCurve2D.Pre_treeDoWithDataCallbackNurbsCurve2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsCurve2D.Pre_doWithDataCallbackNurbsCurve2D != null)
            Pre_NurbsCurve2D.Pre_doWithDataCallbackNurbsCurve2D.doWithData(this);
    }
}

class Pre_NurbsCurve2DRenderCallback extends RenderCallback {}
class Pre_NurbsCurve2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsCurve2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsCurve2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsCurve2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsSurface extends Pre_Node {
    Pre_Node metadata;
    int uDimension;
    int vDimension;
    float[] uKnot;
    float[] vKnot;
    int uOrder;
    int vOrder;
    Pre_Node controlPoint;
    float[] weight;
    int uTessellation;
    int vTessellation;
    Pre_Node texCoord;
    boolean uClosed;
    boolean vClosed;
    boolean solid;
    static Pre_NurbsSurfaceRenderCallback Pre_renderCallbackNurbsSurface;
    static void setPre_NurbsSurfaceRenderCallback(Pre_NurbsSurfaceRenderCallback node) {
        Pre_renderCallbackNurbsSurface = node;
    }

    static Pre_NurbsSurfaceTreeRenderCallback Pre_treeRenderCallbackNurbsSurface;
    static void setPre_NurbsSurfaceTreeRenderCallback(Pre_NurbsSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsSurface = node;
    }

    static Pre_NurbsSurfaceDoWithDataCallback Pre_doWithDataCallbackNurbsSurface;
    static void setPre_NurbsSurfaceDoWithDataCallback(Pre_NurbsSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsSurface = node;
    }

    static Pre_NurbsSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsSurface;
    static void setPre_NurbsSurfaceTreeDoWithDataCallback(Pre_NurbsSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsSurface = node;

    }

    static Pre_NurbsSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackNurbsSurface;
    static void setPre_NurbsSurfaceEventsProcessedCallback(Pre_NurbsSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsSurface = node;
    }

    Pre_NurbsSurface() {
    }
    void treeRender() {
        if (Pre_NurbsSurface.Pre_treeRenderCallbackNurbsSurface != null) {
            Pre_NurbsSurface.Pre_treeRenderCallbackNurbsSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoint != null)
            controlPoint.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsSurface.Pre_renderCallbackNurbsSurface != null)
            Pre_NurbsSurface.Pre_renderCallbackNurbsSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsSurface.Pre_treeDoWithDataCallbackNurbsSurface != null) {
            Pre_NurbsSurface.Pre_treeDoWithDataCallbackNurbsSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoint != null)
            controlPoint.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsSurface.Pre_doWithDataCallbackNurbsSurface != null)
            Pre_NurbsSurface.Pre_doWithDataCallbackNurbsSurface.doWithData(this);
    }
}

class Pre_NurbsSurfaceRenderCallback extends RenderCallback {}
class Pre_NurbsSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PickableGroup extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    String[] objectType;
    boolean pickable;
    static Pre_PickableGroupRenderCallback Pre_renderCallbackPickableGroup;
    static void setPre_PickableGroupRenderCallback(Pre_PickableGroupRenderCallback node) {
        Pre_renderCallbackPickableGroup = node;
    }

    static Pre_PickableGroupTreeRenderCallback Pre_treeRenderCallbackPickableGroup;
    static void setPre_PickableGroupTreeRenderCallback(Pre_PickableGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackPickableGroup = node;
    }

    static Pre_PickableGroupDoWithDataCallback Pre_doWithDataCallbackPickableGroup;
    static void setPre_PickableGroupDoWithDataCallback(Pre_PickableGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackPickableGroup = node;
    }

    static Pre_PickableGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackPickableGroup;
    static void setPre_PickableGroupTreeDoWithDataCallback(Pre_PickableGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPickableGroup = node;

    }

    static Pre_PickableGroupEventsProcessedCallback Pre_eventsProcessedCallbackPickableGroup;
    static void setPre_PickableGroupEventsProcessedCallback(Pre_PickableGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPickableGroup = node;
    }

    Pre_PickableGroup() {
    }
    void treeRender() {
        if (Pre_PickableGroup.Pre_treeRenderCallbackPickableGroup != null) {
            Pre_PickableGroup.Pre_treeRenderCallbackPickableGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PickableGroup.Pre_renderCallbackPickableGroup != null)
            Pre_PickableGroup.Pre_renderCallbackPickableGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_PickableGroup.Pre_treeDoWithDataCallbackPickableGroup != null) {
            Pre_PickableGroup.Pre_treeDoWithDataCallbackPickableGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PickableGroup.Pre_doWithDataCallbackPickableGroup != null)
            Pre_PickableGroup.Pre_doWithDataCallbackPickableGroup.doWithData(this);
    }
}

class Pre_PickableGroupRenderCallback extends RenderCallback {}
class Pre_PickableGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_PickableGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_PickableGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PickableGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CollisionSensor extends Pre_Node {
    Pre_Node metadata;
    Pre_Node collidables;
    boolean enabled;
    static Pre_CollisionSensorRenderCallback Pre_renderCallbackCollisionSensor;
    static void setPre_CollisionSensorRenderCallback(Pre_CollisionSensorRenderCallback node) {
        Pre_renderCallbackCollisionSensor = node;
    }

    static Pre_CollisionSensorTreeRenderCallback Pre_treeRenderCallbackCollisionSensor;
    static void setPre_CollisionSensorTreeRenderCallback(Pre_CollisionSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackCollisionSensor = node;
    }

    static Pre_CollisionSensorDoWithDataCallback Pre_doWithDataCallbackCollisionSensor;
    static void setPre_CollisionSensorDoWithDataCallback(Pre_CollisionSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackCollisionSensor = node;
    }

    static Pre_CollisionSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollisionSensor;
    static void setPre_CollisionSensorTreeDoWithDataCallback(Pre_CollisionSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollisionSensor = node;

    }

    static Pre_CollisionSensorEventsProcessedCallback Pre_eventsProcessedCallbackCollisionSensor;
    static void setPre_CollisionSensorEventsProcessedCallback(Pre_CollisionSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollisionSensor = node;
    }

    Pre_CollisionSensor() {
    }
    void treeRender() {
        if (Pre_CollisionSensor.Pre_treeRenderCallbackCollisionSensor != null) {
            Pre_CollisionSensor.Pre_treeRenderCallbackCollisionSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (collidables != null)
            collidables.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CollisionSensor.Pre_renderCallbackCollisionSensor != null)
            Pre_CollisionSensor.Pre_renderCallbackCollisionSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_CollisionSensor.Pre_treeDoWithDataCallbackCollisionSensor != null) {
            Pre_CollisionSensor.Pre_treeDoWithDataCallbackCollisionSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (collidables != null)
            collidables.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CollisionSensor.Pre_doWithDataCallbackCollisionSensor != null)
            Pre_CollisionSensor.Pre_doWithDataCallbackCollisionSensor.doWithData(this);
    }
}

class Pre_CollisionSensorRenderCallback extends RenderCallback {}
class Pre_CollisionSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollisionSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollisionSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollisionSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoTouchSensor extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    String description;
    boolean enabled;
    static Pre_GeoTouchSensorRenderCallback Pre_renderCallbackGeoTouchSensor;
    static void setPre_GeoTouchSensorRenderCallback(Pre_GeoTouchSensorRenderCallback node) {
        Pre_renderCallbackGeoTouchSensor = node;
    }

    static Pre_GeoTouchSensorTreeRenderCallback Pre_treeRenderCallbackGeoTouchSensor;
    static void setPre_GeoTouchSensorTreeRenderCallback(Pre_GeoTouchSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoTouchSensor = node;
    }

    static Pre_GeoTouchSensorDoWithDataCallback Pre_doWithDataCallbackGeoTouchSensor;
    static void setPre_GeoTouchSensorDoWithDataCallback(Pre_GeoTouchSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoTouchSensor = node;
    }

    static Pre_GeoTouchSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoTouchSensor;
    static void setPre_GeoTouchSensorTreeDoWithDataCallback(Pre_GeoTouchSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoTouchSensor = node;

    }

    static Pre_GeoTouchSensorEventsProcessedCallback Pre_eventsProcessedCallbackGeoTouchSensor;
    static void setPre_GeoTouchSensorEventsProcessedCallback(Pre_GeoTouchSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoTouchSensor = node;
    }

    Pre_GeoTouchSensor() {
    }
    void treeRender() {
        if (Pre_GeoTouchSensor.Pre_treeRenderCallbackGeoTouchSensor != null) {
            Pre_GeoTouchSensor.Pre_treeRenderCallbackGeoTouchSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoTouchSensor.Pre_renderCallbackGeoTouchSensor != null)
            Pre_GeoTouchSensor.Pre_renderCallbackGeoTouchSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoTouchSensor.Pre_treeDoWithDataCallbackGeoTouchSensor != null) {
            Pre_GeoTouchSensor.Pre_treeDoWithDataCallbackGeoTouchSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoTouchSensor.Pre_doWithDataCallbackGeoTouchSensor != null)
            Pre_GeoTouchSensor.Pre_doWithDataCallbackGeoTouchSensor.doWithData(this);
    }
}

class Pre_GeoTouchSensorRenderCallback extends RenderCallback {}
class Pre_GeoTouchSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoTouchSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoTouchSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoTouchSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PlaneSensor extends Pre_Node {
    Pre_Node metadata;
    boolean autoOffset;
    float[] axisRotation;
    String description;
    boolean enabled;
    float[] maxPosition;
    float[] minPosition;
    float[] offset;
    static Pre_PlaneSensorRenderCallback Pre_renderCallbackPlaneSensor;
    static void setPre_PlaneSensorRenderCallback(Pre_PlaneSensorRenderCallback node) {
        Pre_renderCallbackPlaneSensor = node;
    }

    static Pre_PlaneSensorTreeRenderCallback Pre_treeRenderCallbackPlaneSensor;
    static void setPre_PlaneSensorTreeRenderCallback(Pre_PlaneSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackPlaneSensor = node;
    }

    static Pre_PlaneSensorDoWithDataCallback Pre_doWithDataCallbackPlaneSensor;
    static void setPre_PlaneSensorDoWithDataCallback(Pre_PlaneSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackPlaneSensor = node;
    }

    static Pre_PlaneSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackPlaneSensor;
    static void setPre_PlaneSensorTreeDoWithDataCallback(Pre_PlaneSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPlaneSensor = node;

    }

    static Pre_PlaneSensorEventsProcessedCallback Pre_eventsProcessedCallbackPlaneSensor;
    static void setPre_PlaneSensorEventsProcessedCallback(Pre_PlaneSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPlaneSensor = node;
    }

    Pre_PlaneSensor() {
    }
    void treeRender() {
        if (Pre_PlaneSensor.Pre_treeRenderCallbackPlaneSensor != null) {
            Pre_PlaneSensor.Pre_treeRenderCallbackPlaneSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PlaneSensor.Pre_renderCallbackPlaneSensor != null)
            Pre_PlaneSensor.Pre_renderCallbackPlaneSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_PlaneSensor.Pre_treeDoWithDataCallbackPlaneSensor != null) {
            Pre_PlaneSensor.Pre_treeDoWithDataCallbackPlaneSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PlaneSensor.Pre_doWithDataCallbackPlaneSensor != null)
            Pre_PlaneSensor.Pre_doWithDataCallbackPlaneSensor.doWithData(this);
    }
}

class Pre_PlaneSensorRenderCallback extends RenderCallback {}
class Pre_PlaneSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_PlaneSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_PlaneSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PlaneSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_StringSensor extends Pre_Node {
    Pre_Node metadata;
    boolean deletionAllowed;
    boolean enabled;
    static Pre_StringSensorRenderCallback Pre_renderCallbackStringSensor;
    static void setPre_StringSensorRenderCallback(Pre_StringSensorRenderCallback node) {
        Pre_renderCallbackStringSensor = node;
    }

    static Pre_StringSensorTreeRenderCallback Pre_treeRenderCallbackStringSensor;
    static void setPre_StringSensorTreeRenderCallback(Pre_StringSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackStringSensor = node;
    }

    static Pre_StringSensorDoWithDataCallback Pre_doWithDataCallbackStringSensor;
    static void setPre_StringSensorDoWithDataCallback(Pre_StringSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackStringSensor = node;
    }

    static Pre_StringSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackStringSensor;
    static void setPre_StringSensorTreeDoWithDataCallback(Pre_StringSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackStringSensor = node;

    }

    static Pre_StringSensorEventsProcessedCallback Pre_eventsProcessedCallbackStringSensor;
    static void setPre_StringSensorEventsProcessedCallback(Pre_StringSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackStringSensor = node;
    }

    Pre_StringSensor() {
    }
    void treeRender() {
        if (Pre_StringSensor.Pre_treeRenderCallbackStringSensor != null) {
            Pre_StringSensor.Pre_treeRenderCallbackStringSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_StringSensor.Pre_renderCallbackStringSensor != null)
            Pre_StringSensor.Pre_renderCallbackStringSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_StringSensor.Pre_treeDoWithDataCallbackStringSensor != null) {
            Pre_StringSensor.Pre_treeDoWithDataCallbackStringSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_StringSensor.Pre_doWithDataCallbackStringSensor != null)
            Pre_StringSensor.Pre_doWithDataCallbackStringSensor.doWithData(this);
    }
}

class Pre_StringSensorRenderCallback extends RenderCallback {}
class Pre_StringSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_StringSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_StringSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_StringSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CoordinateInterpolator2D extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_CoordinateInterpolator2DRenderCallback Pre_renderCallbackCoordinateInterpolator2D;
    static void setPre_CoordinateInterpolator2DRenderCallback(Pre_CoordinateInterpolator2DRenderCallback node) {
        Pre_renderCallbackCoordinateInterpolator2D = node;
    }

    static Pre_CoordinateInterpolator2DTreeRenderCallback Pre_treeRenderCallbackCoordinateInterpolator2D;
    static void setPre_CoordinateInterpolator2DTreeRenderCallback(Pre_CoordinateInterpolator2DTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinateInterpolator2D = node;
    }

    static Pre_CoordinateInterpolator2DDoWithDataCallback Pre_doWithDataCallbackCoordinateInterpolator2D;
    static void setPre_CoordinateInterpolator2DDoWithDataCallback(Pre_CoordinateInterpolator2DDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinateInterpolator2D = node;
    }

    static Pre_CoordinateInterpolator2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinateInterpolator2D;
    static void setPre_CoordinateInterpolator2DTreeDoWithDataCallback(Pre_CoordinateInterpolator2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinateInterpolator2D = node;

    }

    static Pre_CoordinateInterpolator2DEventsProcessedCallback Pre_eventsProcessedCallbackCoordinateInterpolator2D;
    static void setPre_CoordinateInterpolator2DEventsProcessedCallback(Pre_CoordinateInterpolator2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinateInterpolator2D = node;
    }

    Pre_CoordinateInterpolator2D() {
    }
    void treeRender() {
        if (Pre_CoordinateInterpolator2D.Pre_treeRenderCallbackCoordinateInterpolator2D != null) {
            Pre_CoordinateInterpolator2D.Pre_treeRenderCallbackCoordinateInterpolator2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CoordinateInterpolator2D.Pre_renderCallbackCoordinateInterpolator2D != null)
            Pre_CoordinateInterpolator2D.Pre_renderCallbackCoordinateInterpolator2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_CoordinateInterpolator2D.Pre_treeDoWithDataCallbackCoordinateInterpolator2D != null) {
            Pre_CoordinateInterpolator2D.Pre_treeDoWithDataCallbackCoordinateInterpolator2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CoordinateInterpolator2D.Pre_doWithDataCallbackCoordinateInterpolator2D != null)
            Pre_CoordinateInterpolator2D.Pre_doWithDataCallbackCoordinateInterpolator2D.doWithData(this);
    }
}

class Pre_CoordinateInterpolator2DRenderCallback extends RenderCallback {}
class Pre_CoordinateInterpolator2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateInterpolator2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateInterpolator2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateInterpolator2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_DoubleAxisHingeJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] anchorPoint;
    float[] axis1;
    float[] axis2;
    float desiredAngularVelocity1;
    float desiredAngularVelocity2;
    float maxAngle1;
    float maxTorque1;
    float maxTorque2;
    float minAngle1;
    float stopBounce1;
    float stopConstantForceMix1;
    float stopErrorCorrection1;
    float suspensionErrorCorrection;
    float suspensionForce;
    static Pre_DoubleAxisHingeJointRenderCallback Pre_renderCallbackDoubleAxisHingeJoint;
    static void setPre_DoubleAxisHingeJointRenderCallback(Pre_DoubleAxisHingeJointRenderCallback node) {
        Pre_renderCallbackDoubleAxisHingeJoint = node;
    }

    static Pre_DoubleAxisHingeJointTreeRenderCallback Pre_treeRenderCallbackDoubleAxisHingeJoint;
    static void setPre_DoubleAxisHingeJointTreeRenderCallback(Pre_DoubleAxisHingeJointTreeRenderCallback node) {
        Pre_treeRenderCallbackDoubleAxisHingeJoint = node;
    }

    static Pre_DoubleAxisHingeJointDoWithDataCallback Pre_doWithDataCallbackDoubleAxisHingeJoint;
    static void setPre_DoubleAxisHingeJointDoWithDataCallback(Pre_DoubleAxisHingeJointDoWithDataCallback node) {
        Pre_doWithDataCallbackDoubleAxisHingeJoint = node;
    }

    static Pre_DoubleAxisHingeJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackDoubleAxisHingeJoint;
    static void setPre_DoubleAxisHingeJointTreeDoWithDataCallback(Pre_DoubleAxisHingeJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackDoubleAxisHingeJoint = node;

    }

    static Pre_DoubleAxisHingeJointEventsProcessedCallback Pre_eventsProcessedCallbackDoubleAxisHingeJoint;
    static void setPre_DoubleAxisHingeJointEventsProcessedCallback(Pre_DoubleAxisHingeJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackDoubleAxisHingeJoint = node;
    }

    Pre_DoubleAxisHingeJoint() {
    }
    void treeRender() {
        if (Pre_DoubleAxisHingeJoint.Pre_treeRenderCallbackDoubleAxisHingeJoint != null) {
            Pre_DoubleAxisHingeJoint.Pre_treeRenderCallbackDoubleAxisHingeJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_DoubleAxisHingeJoint.Pre_renderCallbackDoubleAxisHingeJoint != null)
            Pre_DoubleAxisHingeJoint.Pre_renderCallbackDoubleAxisHingeJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_DoubleAxisHingeJoint.Pre_treeDoWithDataCallbackDoubleAxisHingeJoint != null) {
            Pre_DoubleAxisHingeJoint.Pre_treeDoWithDataCallbackDoubleAxisHingeJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_DoubleAxisHingeJoint.Pre_doWithDataCallbackDoubleAxisHingeJoint != null)
            Pre_DoubleAxisHingeJoint.Pre_doWithDataCallbackDoubleAxisHingeJoint.doWithData(this);
    }
}

class Pre_DoubleAxisHingeJointRenderCallback extends RenderCallback {}
class Pre_DoubleAxisHingeJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_DoubleAxisHingeJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_DoubleAxisHingeJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_DoubleAxisHingeJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Polyline2D extends Pre_Node {
    Pre_Node metadata;
    float[] lineSegments;
    static Pre_Polyline2DRenderCallback Pre_renderCallbackPolyline2D;
    static void setPre_Polyline2DRenderCallback(Pre_Polyline2DRenderCallback node) {
        Pre_renderCallbackPolyline2D = node;
    }

    static Pre_Polyline2DTreeRenderCallback Pre_treeRenderCallbackPolyline2D;
    static void setPre_Polyline2DTreeRenderCallback(Pre_Polyline2DTreeRenderCallback node) {
        Pre_treeRenderCallbackPolyline2D = node;
    }

    static Pre_Polyline2DDoWithDataCallback Pre_doWithDataCallbackPolyline2D;
    static void setPre_Polyline2DDoWithDataCallback(Pre_Polyline2DDoWithDataCallback node) {
        Pre_doWithDataCallbackPolyline2D = node;
    }

    static Pre_Polyline2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPolyline2D;
    static void setPre_Polyline2DTreeDoWithDataCallback(Pre_Polyline2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPolyline2D = node;

    }

    static Pre_Polyline2DEventsProcessedCallback Pre_eventsProcessedCallbackPolyline2D;
    static void setPre_Polyline2DEventsProcessedCallback(Pre_Polyline2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPolyline2D = node;
    }

    Pre_Polyline2D() {
    }
    void treeRender() {
        if (Pre_Polyline2D.Pre_treeRenderCallbackPolyline2D != null) {
            Pre_Polyline2D.Pre_treeRenderCallbackPolyline2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Polyline2D.Pre_renderCallbackPolyline2D != null)
            Pre_Polyline2D.Pre_renderCallbackPolyline2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Polyline2D.Pre_treeDoWithDataCallbackPolyline2D != null) {
            Pre_Polyline2D.Pre_treeDoWithDataCallbackPolyline2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Polyline2D.Pre_doWithDataCallbackPolyline2D != null)
            Pre_Polyline2D.Pre_doWithDataCallbackPolyline2D.doWithData(this);
    }
}

class Pre_Polyline2DRenderCallback extends RenderCallback {}
class Pre_Polyline2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Polyline2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Polyline2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Polyline2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionInterpolator2D extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_PositionInterpolator2DRenderCallback Pre_renderCallbackPositionInterpolator2D;
    static void setPre_PositionInterpolator2DRenderCallback(Pre_PositionInterpolator2DRenderCallback node) {
        Pre_renderCallbackPositionInterpolator2D = node;
    }

    static Pre_PositionInterpolator2DTreeRenderCallback Pre_treeRenderCallbackPositionInterpolator2D;
    static void setPre_PositionInterpolator2DTreeRenderCallback(Pre_PositionInterpolator2DTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionInterpolator2D = node;
    }

    static Pre_PositionInterpolator2DDoWithDataCallback Pre_doWithDataCallbackPositionInterpolator2D;
    static void setPre_PositionInterpolator2DDoWithDataCallback(Pre_PositionInterpolator2DDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionInterpolator2D = node;
    }

    static Pre_PositionInterpolator2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionInterpolator2D;
    static void setPre_PositionInterpolator2DTreeDoWithDataCallback(Pre_PositionInterpolator2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionInterpolator2D = node;

    }

    static Pre_PositionInterpolator2DEventsProcessedCallback Pre_eventsProcessedCallbackPositionInterpolator2D;
    static void setPre_PositionInterpolator2DEventsProcessedCallback(Pre_PositionInterpolator2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionInterpolator2D = node;
    }

    Pre_PositionInterpolator2D() {
    }
    void treeRender() {
        if (Pre_PositionInterpolator2D.Pre_treeRenderCallbackPositionInterpolator2D != null) {
            Pre_PositionInterpolator2D.Pre_treeRenderCallbackPositionInterpolator2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionInterpolator2D.Pre_renderCallbackPositionInterpolator2D != null)
            Pre_PositionInterpolator2D.Pre_renderCallbackPositionInterpolator2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionInterpolator2D.Pre_treeDoWithDataCallbackPositionInterpolator2D != null) {
            Pre_PositionInterpolator2D.Pre_treeDoWithDataCallbackPositionInterpolator2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionInterpolator2D.Pre_doWithDataCallbackPositionInterpolator2D != null)
            Pre_PositionInterpolator2D.Pre_doWithDataCallbackPositionInterpolator2D.doWithData(this);
    }
}

class Pre_PositionInterpolator2DRenderCallback extends RenderCallback {}
class Pre_PositionInterpolator2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionInterpolator2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionInterpolator2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionInterpolator2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_HAnimSite extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    Pre_Node [] children;
    String name;
    static Pre_HAnimSiteRenderCallback Pre_renderCallbackHAnimSite;
    static void setPre_HAnimSiteRenderCallback(Pre_HAnimSiteRenderCallback node) {
        Pre_renderCallbackHAnimSite = node;
    }

    static Pre_HAnimSiteTreeRenderCallback Pre_treeRenderCallbackHAnimSite;
    static void setPre_HAnimSiteTreeRenderCallback(Pre_HAnimSiteTreeRenderCallback node) {
        Pre_treeRenderCallbackHAnimSite = node;
    }

    static Pre_HAnimSiteDoWithDataCallback Pre_doWithDataCallbackHAnimSite;
    static void setPre_HAnimSiteDoWithDataCallback(Pre_HAnimSiteDoWithDataCallback node) {
        Pre_doWithDataCallbackHAnimSite = node;
    }

    static Pre_HAnimSiteTreeDoWithDataCallback Pre_treeDoWithDataCallbackHAnimSite;
    static void setPre_HAnimSiteTreeDoWithDataCallback(Pre_HAnimSiteTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackHAnimSite = node;

    }

    static Pre_HAnimSiteEventsProcessedCallback Pre_eventsProcessedCallbackHAnimSite;
    static void setPre_HAnimSiteEventsProcessedCallback(Pre_HAnimSiteEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackHAnimSite = node;
    }

    Pre_HAnimSite() {
    }
    void treeRender() {
        if (Pre_HAnimSite.Pre_treeRenderCallbackHAnimSite != null) {
            Pre_HAnimSite.Pre_treeRenderCallbackHAnimSite.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_HAnimSite.Pre_renderCallbackHAnimSite != null)
            Pre_HAnimSite.Pre_renderCallbackHAnimSite.render(this);
    }
    void treeDoWithData() {
        if (Pre_HAnimSite.Pre_treeDoWithDataCallbackHAnimSite != null) {
            Pre_HAnimSite.Pre_treeDoWithDataCallbackHAnimSite.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_HAnimSite.Pre_doWithDataCallbackHAnimSite != null)
            Pre_HAnimSite.Pre_doWithDataCallbackHAnimSite.doWithData(this);
    }
}

class Pre_HAnimSiteRenderCallback extends RenderCallback {}
class Pre_HAnimSiteTreeRenderCallback extends TreeRenderCallback {}
class Pre_HAnimSiteDoWithDataCallback extends DoWithDataCallback {};
class Pre_HAnimSiteTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_HAnimSiteEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ImageCubeMapTexture extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    Pre_Node textureProperties;
    static Pre_ImageCubeMapTextureRenderCallback Pre_renderCallbackImageCubeMapTexture;
    static void setPre_ImageCubeMapTextureRenderCallback(Pre_ImageCubeMapTextureRenderCallback node) {
        Pre_renderCallbackImageCubeMapTexture = node;
    }

    static Pre_ImageCubeMapTextureTreeRenderCallback Pre_treeRenderCallbackImageCubeMapTexture;
    static void setPre_ImageCubeMapTextureTreeRenderCallback(Pre_ImageCubeMapTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackImageCubeMapTexture = node;
    }

    static Pre_ImageCubeMapTextureDoWithDataCallback Pre_doWithDataCallbackImageCubeMapTexture;
    static void setPre_ImageCubeMapTextureDoWithDataCallback(Pre_ImageCubeMapTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackImageCubeMapTexture = node;
    }

    static Pre_ImageCubeMapTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackImageCubeMapTexture;
    static void setPre_ImageCubeMapTextureTreeDoWithDataCallback(Pre_ImageCubeMapTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackImageCubeMapTexture = node;

    }

    static Pre_ImageCubeMapTextureEventsProcessedCallback Pre_eventsProcessedCallbackImageCubeMapTexture;
    static void setPre_ImageCubeMapTextureEventsProcessedCallback(Pre_ImageCubeMapTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackImageCubeMapTexture = node;
    }

    Pre_ImageCubeMapTexture() {
    }
    void treeRender() {
        if (Pre_ImageCubeMapTexture.Pre_treeRenderCallbackImageCubeMapTexture != null) {
            Pre_ImageCubeMapTexture.Pre_treeRenderCallbackImageCubeMapTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ImageCubeMapTexture.Pre_renderCallbackImageCubeMapTexture != null)
            Pre_ImageCubeMapTexture.Pre_renderCallbackImageCubeMapTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_ImageCubeMapTexture.Pre_treeDoWithDataCallbackImageCubeMapTexture != null) {
            Pre_ImageCubeMapTexture.Pre_treeDoWithDataCallbackImageCubeMapTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ImageCubeMapTexture.Pre_doWithDataCallbackImageCubeMapTexture != null)
            Pre_ImageCubeMapTexture.Pre_doWithDataCallbackImageCubeMapTexture.doWithData(this);
    }
}

class Pre_ImageCubeMapTextureRenderCallback extends RenderCallback {}
class Pre_ImageCubeMapTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_ImageCubeMapTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_ImageCubeMapTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ImageCubeMapTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ImageTexture extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    boolean repeatS;
    boolean repeatT;
    String alphaChannel;
    Pre_Node textureProperties;
    static Pre_ImageTextureRenderCallback Pre_renderCallbackImageTexture;
    static void setPre_ImageTextureRenderCallback(Pre_ImageTextureRenderCallback node) {
        Pre_renderCallbackImageTexture = node;
    }

    static Pre_ImageTextureTreeRenderCallback Pre_treeRenderCallbackImageTexture;
    static void setPre_ImageTextureTreeRenderCallback(Pre_ImageTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackImageTexture = node;
    }

    static Pre_ImageTextureDoWithDataCallback Pre_doWithDataCallbackImageTexture;
    static void setPre_ImageTextureDoWithDataCallback(Pre_ImageTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackImageTexture = node;
    }

    static Pre_ImageTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackImageTexture;
    static void setPre_ImageTextureTreeDoWithDataCallback(Pre_ImageTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackImageTexture = node;

    }

    static Pre_ImageTextureEventsProcessedCallback Pre_eventsProcessedCallbackImageTexture;
    static void setPre_ImageTextureEventsProcessedCallback(Pre_ImageTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackImageTexture = node;
    }

    Pre_ImageTexture() {
    }
    void treeRender() {
        if (Pre_ImageTexture.Pre_treeRenderCallbackImageTexture != null) {
            Pre_ImageTexture.Pre_treeRenderCallbackImageTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ImageTexture.Pre_renderCallbackImageTexture != null)
            Pre_ImageTexture.Pre_renderCallbackImageTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_ImageTexture.Pre_treeDoWithDataCallbackImageTexture != null) {
            Pre_ImageTexture.Pre_treeDoWithDataCallbackImageTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ImageTexture.Pre_doWithDataCallbackImageTexture != null)
            Pre_ImageTexture.Pre_doWithDataCallbackImageTexture.doWithData(this);
    }
}

class Pre_ImageTextureRenderCallback extends RenderCallback {}
class Pre_ImageTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_ImageTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_ImageTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ImageTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MovieTexture extends Pre_Node {
    Pre_Node metadata;
    boolean loop;
    float speed;
    double pauseTime;
    double resumeTime;
    double startTime;
    double stopTime;
    String[] url;
    boolean repeatS;
    boolean repeatT;
    String alphaChannel;
    Pre_Node textureProperties;
    static Pre_MovieTextureRenderCallback Pre_renderCallbackMovieTexture;
    static void setPre_MovieTextureRenderCallback(Pre_MovieTextureRenderCallback node) {
        Pre_renderCallbackMovieTexture = node;
    }

    static Pre_MovieTextureTreeRenderCallback Pre_treeRenderCallbackMovieTexture;
    static void setPre_MovieTextureTreeRenderCallback(Pre_MovieTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackMovieTexture = node;
    }

    static Pre_MovieTextureDoWithDataCallback Pre_doWithDataCallbackMovieTexture;
    static void setPre_MovieTextureDoWithDataCallback(Pre_MovieTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackMovieTexture = node;
    }

    static Pre_MovieTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackMovieTexture;
    static void setPre_MovieTextureTreeDoWithDataCallback(Pre_MovieTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMovieTexture = node;

    }

    static Pre_MovieTextureEventsProcessedCallback Pre_eventsProcessedCallbackMovieTexture;
    static void setPre_MovieTextureEventsProcessedCallback(Pre_MovieTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMovieTexture = node;
    }

    Pre_MovieTexture() {
    }
    void treeRender() {
        if (Pre_MovieTexture.Pre_treeRenderCallbackMovieTexture != null) {
            Pre_MovieTexture.Pre_treeRenderCallbackMovieTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MovieTexture.Pre_renderCallbackMovieTexture != null)
            Pre_MovieTexture.Pre_renderCallbackMovieTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_MovieTexture.Pre_treeDoWithDataCallbackMovieTexture != null) {
            Pre_MovieTexture.Pre_treeDoWithDataCallbackMovieTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MovieTexture.Pre_doWithDataCallbackMovieTexture != null)
            Pre_MovieTexture.Pre_doWithDataCallbackMovieTexture.doWithData(this);
    }
}

class Pre_MovieTextureRenderCallback extends RenderCallback {}
class Pre_MovieTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_MovieTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_MovieTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MovieTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ScreenFontStyle extends Pre_Node {
    Pre_Node metadata;
    String[] family;
    boolean horizontal;
    String[] justify;
    String language;
    boolean leftToRight;
    float pointSize;
    float spacing;
    String style;
    boolean topToBottom;
    static Pre_ScreenFontStyleRenderCallback Pre_renderCallbackScreenFontStyle;
    static void setPre_ScreenFontStyleRenderCallback(Pre_ScreenFontStyleRenderCallback node) {
        Pre_renderCallbackScreenFontStyle = node;
    }

    static Pre_ScreenFontStyleTreeRenderCallback Pre_treeRenderCallbackScreenFontStyle;
    static void setPre_ScreenFontStyleTreeRenderCallback(Pre_ScreenFontStyleTreeRenderCallback node) {
        Pre_treeRenderCallbackScreenFontStyle = node;
    }

    static Pre_ScreenFontStyleDoWithDataCallback Pre_doWithDataCallbackScreenFontStyle;
    static void setPre_ScreenFontStyleDoWithDataCallback(Pre_ScreenFontStyleDoWithDataCallback node) {
        Pre_doWithDataCallbackScreenFontStyle = node;
    }

    static Pre_ScreenFontStyleTreeDoWithDataCallback Pre_treeDoWithDataCallbackScreenFontStyle;
    static void setPre_ScreenFontStyleTreeDoWithDataCallback(Pre_ScreenFontStyleTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackScreenFontStyle = node;

    }

    static Pre_ScreenFontStyleEventsProcessedCallback Pre_eventsProcessedCallbackScreenFontStyle;
    static void setPre_ScreenFontStyleEventsProcessedCallback(Pre_ScreenFontStyleEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackScreenFontStyle = node;
    }

    Pre_ScreenFontStyle() {
    }
    void treeRender() {
        if (Pre_ScreenFontStyle.Pre_treeRenderCallbackScreenFontStyle != null) {
            Pre_ScreenFontStyle.Pre_treeRenderCallbackScreenFontStyle.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ScreenFontStyle.Pre_renderCallbackScreenFontStyle != null)
            Pre_ScreenFontStyle.Pre_renderCallbackScreenFontStyle.render(this);
    }
    void treeDoWithData() {
        if (Pre_ScreenFontStyle.Pre_treeDoWithDataCallbackScreenFontStyle != null) {
            Pre_ScreenFontStyle.Pre_treeDoWithDataCallbackScreenFontStyle.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ScreenFontStyle.Pre_doWithDataCallbackScreenFontStyle != null)
            Pre_ScreenFontStyle.Pre_doWithDataCallbackScreenFontStyle.doWithData(this);
    }
}

class Pre_ScreenFontStyleRenderCallback extends RenderCallback {}
class Pre_ScreenFontStyleTreeRenderCallback extends TreeRenderCallback {}
class Pre_ScreenFontStyleDoWithDataCallback extends DoWithDataCallback {};
class Pre_ScreenFontStyleTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ScreenFontStyleEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NavigationInfo extends Pre_Node {
    Pre_Node metadata;
    float[] avatarSize;
    boolean headlight;
    float speed;
    double transitionTime;
    String[] transitionType;
    String[] type;
    float visibilityLimit;
    static Pre_NavigationInfoRenderCallback Pre_renderCallbackNavigationInfo;
    static void setPre_NavigationInfoRenderCallback(Pre_NavigationInfoRenderCallback node) {
        Pre_renderCallbackNavigationInfo = node;
    }

    static Pre_NavigationInfoTreeRenderCallback Pre_treeRenderCallbackNavigationInfo;
    static void setPre_NavigationInfoTreeRenderCallback(Pre_NavigationInfoTreeRenderCallback node) {
        Pre_treeRenderCallbackNavigationInfo = node;
    }

    static Pre_NavigationInfoDoWithDataCallback Pre_doWithDataCallbackNavigationInfo;
    static void setPre_NavigationInfoDoWithDataCallback(Pre_NavigationInfoDoWithDataCallback node) {
        Pre_doWithDataCallbackNavigationInfo = node;
    }

    static Pre_NavigationInfoTreeDoWithDataCallback Pre_treeDoWithDataCallbackNavigationInfo;
    static void setPre_NavigationInfoTreeDoWithDataCallback(Pre_NavigationInfoTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNavigationInfo = node;

    }

    static Pre_NavigationInfoEventsProcessedCallback Pre_eventsProcessedCallbackNavigationInfo;
    static void setPre_NavigationInfoEventsProcessedCallback(Pre_NavigationInfoEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNavigationInfo = node;
    }

    Pre_NavigationInfo() {
    }
    void treeRender() {
        if (Pre_NavigationInfo.Pre_treeRenderCallbackNavigationInfo != null) {
            Pre_NavigationInfo.Pre_treeRenderCallbackNavigationInfo.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NavigationInfo.Pre_renderCallbackNavigationInfo != null)
            Pre_NavigationInfo.Pre_renderCallbackNavigationInfo.render(this);
    }
    void treeDoWithData() {
        if (Pre_NavigationInfo.Pre_treeDoWithDataCallbackNavigationInfo != null) {
            Pre_NavigationInfo.Pre_treeDoWithDataCallbackNavigationInfo.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NavigationInfo.Pre_doWithDataCallbackNavigationInfo != null)
            Pre_NavigationInfo.Pre_doWithDataCallbackNavigationInfo.doWithData(this);
    }
}

class Pre_NavigationInfoRenderCallback extends RenderCallback {}
class Pre_NavigationInfoTreeRenderCallback extends TreeRenderCallback {}
class Pre_NavigationInfoDoWithDataCallback extends DoWithDataCallback {};
class Pre_NavigationInfoTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NavigationInfoEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CoordinateInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_CoordinateInterpolatorRenderCallback Pre_renderCallbackCoordinateInterpolator;
    static void setPre_CoordinateInterpolatorRenderCallback(Pre_CoordinateInterpolatorRenderCallback node) {
        Pre_renderCallbackCoordinateInterpolator = node;
    }

    static Pre_CoordinateInterpolatorTreeRenderCallback Pre_treeRenderCallbackCoordinateInterpolator;
    static void setPre_CoordinateInterpolatorTreeRenderCallback(Pre_CoordinateInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinateInterpolator = node;
    }

    static Pre_CoordinateInterpolatorDoWithDataCallback Pre_doWithDataCallbackCoordinateInterpolator;
    static void setPre_CoordinateInterpolatorDoWithDataCallback(Pre_CoordinateInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinateInterpolator = node;
    }

    static Pre_CoordinateInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinateInterpolator;
    static void setPre_CoordinateInterpolatorTreeDoWithDataCallback(Pre_CoordinateInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinateInterpolator = node;

    }

    static Pre_CoordinateInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackCoordinateInterpolator;
    static void setPre_CoordinateInterpolatorEventsProcessedCallback(Pre_CoordinateInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinateInterpolator = node;
    }

    Pre_CoordinateInterpolator() {
    }
    void treeRender() {
        if (Pre_CoordinateInterpolator.Pre_treeRenderCallbackCoordinateInterpolator != null) {
            Pre_CoordinateInterpolator.Pre_treeRenderCallbackCoordinateInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CoordinateInterpolator.Pre_renderCallbackCoordinateInterpolator != null)
            Pre_CoordinateInterpolator.Pre_renderCallbackCoordinateInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_CoordinateInterpolator.Pre_treeDoWithDataCallbackCoordinateInterpolator != null) {
            Pre_CoordinateInterpolator.Pre_treeDoWithDataCallbackCoordinateInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CoordinateInterpolator.Pre_doWithDataCallbackCoordinateInterpolator != null)
            Pre_CoordinateInterpolator.Pre_doWithDataCallbackCoordinateInterpolator.doWithData(this);
    }
}

class Pre_CoordinateInterpolatorRenderCallback extends RenderCallback {}
class Pre_CoordinateInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_DirectionalLight extends Pre_Node {
    Pre_Node metadata;
    float ambientIntensity;
    float[] color;
    float[] direction;
    boolean global;
    float intensity;
    boolean on;
    boolean shadows;
    float projectionNear;
    float projectionFar;
    float[] up;
    Pre_Node defaultShadowMap;
    boolean kambiShadows;
    boolean kambiShadowsMain;
    float[] projectionRectangle;
    float[] projectionLocation;
    static Pre_DirectionalLightRenderCallback Pre_renderCallbackDirectionalLight;
    static void setPre_DirectionalLightRenderCallback(Pre_DirectionalLightRenderCallback node) {
        Pre_renderCallbackDirectionalLight = node;
    }

    static Pre_DirectionalLightTreeRenderCallback Pre_treeRenderCallbackDirectionalLight;
    static void setPre_DirectionalLightTreeRenderCallback(Pre_DirectionalLightTreeRenderCallback node) {
        Pre_treeRenderCallbackDirectionalLight = node;
    }

    static Pre_DirectionalLightDoWithDataCallback Pre_doWithDataCallbackDirectionalLight;
    static void setPre_DirectionalLightDoWithDataCallback(Pre_DirectionalLightDoWithDataCallback node) {
        Pre_doWithDataCallbackDirectionalLight = node;
    }

    static Pre_DirectionalLightTreeDoWithDataCallback Pre_treeDoWithDataCallbackDirectionalLight;
    static void setPre_DirectionalLightTreeDoWithDataCallback(Pre_DirectionalLightTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackDirectionalLight = node;

    }

    static Pre_DirectionalLightEventsProcessedCallback Pre_eventsProcessedCallbackDirectionalLight;
    static void setPre_DirectionalLightEventsProcessedCallback(Pre_DirectionalLightEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackDirectionalLight = node;
    }

    Pre_DirectionalLight() {
    }
    void treeRender() {
        if (Pre_DirectionalLight.Pre_treeRenderCallbackDirectionalLight != null) {
            Pre_DirectionalLight.Pre_treeRenderCallbackDirectionalLight.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (defaultShadowMap != null)
            defaultShadowMap.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_DirectionalLight.Pre_renderCallbackDirectionalLight != null)
            Pre_DirectionalLight.Pre_renderCallbackDirectionalLight.render(this);
    }
    void treeDoWithData() {
        if (Pre_DirectionalLight.Pre_treeDoWithDataCallbackDirectionalLight != null) {
            Pre_DirectionalLight.Pre_treeDoWithDataCallbackDirectionalLight.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (defaultShadowMap != null)
            defaultShadowMap.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_DirectionalLight.Pre_doWithDataCallbackDirectionalLight != null)
            Pre_DirectionalLight.Pre_doWithDataCallbackDirectionalLight.doWithData(this);
    }
}

class Pre_DirectionalLightRenderCallback extends RenderCallback {}
class Pre_DirectionalLightTreeRenderCallback extends TreeRenderCallback {}
class Pre_DirectionalLightDoWithDataCallback extends DoWithDataCallback {};
class Pre_DirectionalLightTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_DirectionalLightEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoLOD extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    double[] center;
    String[] child1Url;
    String[] child2Url;
    String[] child3Url;
    String[] child4Url;
    float[] range;
    String[] rootUrl;
    Pre_Node [] rootNode;
    static Pre_GeoLODRenderCallback Pre_renderCallbackGeoLOD;
    static void setPre_GeoLODRenderCallback(Pre_GeoLODRenderCallback node) {
        Pre_renderCallbackGeoLOD = node;
    }

    static Pre_GeoLODTreeRenderCallback Pre_treeRenderCallbackGeoLOD;
    static void setPre_GeoLODTreeRenderCallback(Pre_GeoLODTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoLOD = node;
    }

    static Pre_GeoLODDoWithDataCallback Pre_doWithDataCallbackGeoLOD;
    static void setPre_GeoLODDoWithDataCallback(Pre_GeoLODDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoLOD = node;
    }

    static Pre_GeoLODTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoLOD;
    static void setPre_GeoLODTreeDoWithDataCallback(Pre_GeoLODTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoLOD = node;

    }

    static Pre_GeoLODEventsProcessedCallback Pre_eventsProcessedCallbackGeoLOD;
    static void setPre_GeoLODEventsProcessedCallback(Pre_GeoLODEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoLOD = node;
    }

    Pre_GeoLOD() {
    }
    void treeRender() {
        if (Pre_GeoLOD.Pre_treeRenderCallbackGeoLOD != null) {
            Pre_GeoLOD.Pre_treeRenderCallbackGeoLOD.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (rootNode != null)
            for (int i = 0; i < rootNode.length; i++)
                if (rootNode[i] != null)
                    rootNode[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoLOD.Pre_renderCallbackGeoLOD != null)
            Pre_GeoLOD.Pre_renderCallbackGeoLOD.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoLOD.Pre_treeDoWithDataCallbackGeoLOD != null) {
            Pre_GeoLOD.Pre_treeDoWithDataCallbackGeoLOD.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (rootNode != null)
            for (int i = 0; i < rootNode.length; i++)
                if (rootNode[i] != null)
                    rootNode[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoLOD.Pre_doWithDataCallbackGeoLOD != null)
            Pre_GeoLOD.Pre_doWithDataCallbackGeoLOD.doWithData(this);
    }
}

class Pre_GeoLODRenderCallback extends RenderCallback {}
class Pre_GeoLODTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoLODDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoLODTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoLODEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LdrawDatExport extends Pre_Node {
    Pre_Node metadata;
    String partDescription;
    String author;
    String fileType;
    String license;
    String bfc;
    String[] help;
    String category;
    String[] keywords;
    String cmdline;
    String[] history;
    String[] commands;
    static Pre_LdrawDatExportRenderCallback Pre_renderCallbackLdrawDatExport;
    static void setPre_LdrawDatExportRenderCallback(Pre_LdrawDatExportRenderCallback node) {
        Pre_renderCallbackLdrawDatExport = node;
    }

    static Pre_LdrawDatExportTreeRenderCallback Pre_treeRenderCallbackLdrawDatExport;
    static void setPre_LdrawDatExportTreeRenderCallback(Pre_LdrawDatExportTreeRenderCallback node) {
        Pre_treeRenderCallbackLdrawDatExport = node;
    }

    static Pre_LdrawDatExportDoWithDataCallback Pre_doWithDataCallbackLdrawDatExport;
    static void setPre_LdrawDatExportDoWithDataCallback(Pre_LdrawDatExportDoWithDataCallback node) {
        Pre_doWithDataCallbackLdrawDatExport = node;
    }

    static Pre_LdrawDatExportTreeDoWithDataCallback Pre_treeDoWithDataCallbackLdrawDatExport;
    static void setPre_LdrawDatExportTreeDoWithDataCallback(Pre_LdrawDatExportTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLdrawDatExport = node;

    }

    static Pre_LdrawDatExportEventsProcessedCallback Pre_eventsProcessedCallbackLdrawDatExport;
    static void setPre_LdrawDatExportEventsProcessedCallback(Pre_LdrawDatExportEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLdrawDatExport = node;
    }

    Pre_LdrawDatExport() {
    }
    void treeRender() {
        if (Pre_LdrawDatExport.Pre_treeRenderCallbackLdrawDatExport != null) {
            Pre_LdrawDatExport.Pre_treeRenderCallbackLdrawDatExport.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LdrawDatExport.Pre_renderCallbackLdrawDatExport != null)
            Pre_LdrawDatExport.Pre_renderCallbackLdrawDatExport.render(this);
    }
    void treeDoWithData() {
        if (Pre_LdrawDatExport.Pre_treeDoWithDataCallbackLdrawDatExport != null) {
            Pre_LdrawDatExport.Pre_treeDoWithDataCallbackLdrawDatExport.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LdrawDatExport.Pre_doWithDataCallbackLdrawDatExport != null)
            Pre_LdrawDatExport.Pre_doWithDataCallbackLdrawDatExport.doWithData(this);
    }
}

class Pre_LdrawDatExportRenderCallback extends RenderCallback {}
class Pre_LdrawDatExportTreeRenderCallback extends TreeRenderCallback {}
class Pre_LdrawDatExportDoWithDataCallback extends DoWithDataCallback {};
class Pre_LdrawDatExportTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LdrawDatExportEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LOD extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] range;
    static Pre_LODRenderCallback Pre_renderCallbackLOD;
    static void setPre_LODRenderCallback(Pre_LODRenderCallback node) {
        Pre_renderCallbackLOD = node;
    }

    static Pre_LODTreeRenderCallback Pre_treeRenderCallbackLOD;
    static void setPre_LODTreeRenderCallback(Pre_LODTreeRenderCallback node) {
        Pre_treeRenderCallbackLOD = node;
    }

    static Pre_LODDoWithDataCallback Pre_doWithDataCallbackLOD;
    static void setPre_LODDoWithDataCallback(Pre_LODDoWithDataCallback node) {
        Pre_doWithDataCallbackLOD = node;
    }

    static Pre_LODTreeDoWithDataCallback Pre_treeDoWithDataCallbackLOD;
    static void setPre_LODTreeDoWithDataCallback(Pre_LODTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLOD = node;

    }

    static Pre_LODEventsProcessedCallback Pre_eventsProcessedCallbackLOD;
    static void setPre_LODEventsProcessedCallback(Pre_LODEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLOD = node;
    }

    Pre_LOD() {
    }
    void treeRender() {
        if (Pre_LOD.Pre_treeRenderCallbackLOD != null) {
            Pre_LOD.Pre_treeRenderCallbackLOD.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LOD.Pre_renderCallbackLOD != null)
            Pre_LOD.Pre_renderCallbackLOD.render(this);
    }
    void treeDoWithData() {
        if (Pre_LOD.Pre_treeDoWithDataCallbackLOD != null) {
            Pre_LOD.Pre_treeDoWithDataCallbackLOD.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LOD.Pre_doWithDataCallbackLOD != null)
            Pre_LOD.Pre_doWithDataCallbackLOD.doWithData(this);
    }
}

class Pre_LODRenderCallback extends RenderCallback {}
class Pre_LODTreeRenderCallback extends TreeRenderCallback {}
class Pre_LODDoWithDataCallback extends DoWithDataCallback {};
class Pre_LODTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LODEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OdeSingleAxisHingeJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] anchorPoint;
    float[] axis;
    float maxAngle;
    float minAngle;
    float stopBounce;
    float stopErrorCorrection;
    float fMax;
    static Pre_OdeSingleAxisHingeJointRenderCallback Pre_renderCallbackOdeSingleAxisHingeJoint;
    static void setPre_OdeSingleAxisHingeJointRenderCallback(Pre_OdeSingleAxisHingeJointRenderCallback node) {
        Pre_renderCallbackOdeSingleAxisHingeJoint = node;
    }

    static Pre_OdeSingleAxisHingeJointTreeRenderCallback Pre_treeRenderCallbackOdeSingleAxisHingeJoint;
    static void setPre_OdeSingleAxisHingeJointTreeRenderCallback(Pre_OdeSingleAxisHingeJointTreeRenderCallback node) {
        Pre_treeRenderCallbackOdeSingleAxisHingeJoint = node;
    }

    static Pre_OdeSingleAxisHingeJointDoWithDataCallback Pre_doWithDataCallbackOdeSingleAxisHingeJoint;
    static void setPre_OdeSingleAxisHingeJointDoWithDataCallback(Pre_OdeSingleAxisHingeJointDoWithDataCallback node) {
        Pre_doWithDataCallbackOdeSingleAxisHingeJoint = node;
    }

    static Pre_OdeSingleAxisHingeJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackOdeSingleAxisHingeJoint;
    static void setPre_OdeSingleAxisHingeJointTreeDoWithDataCallback(Pre_OdeSingleAxisHingeJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOdeSingleAxisHingeJoint = node;

    }

    static Pre_OdeSingleAxisHingeJointEventsProcessedCallback Pre_eventsProcessedCallbackOdeSingleAxisHingeJoint;
    static void setPre_OdeSingleAxisHingeJointEventsProcessedCallback(Pre_OdeSingleAxisHingeJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOdeSingleAxisHingeJoint = node;
    }

    Pre_OdeSingleAxisHingeJoint() {
    }
    void treeRender() {
        if (Pre_OdeSingleAxisHingeJoint.Pre_treeRenderCallbackOdeSingleAxisHingeJoint != null) {
            Pre_OdeSingleAxisHingeJoint.Pre_treeRenderCallbackOdeSingleAxisHingeJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OdeSingleAxisHingeJoint.Pre_renderCallbackOdeSingleAxisHingeJoint != null)
            Pre_OdeSingleAxisHingeJoint.Pre_renderCallbackOdeSingleAxisHingeJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_OdeSingleAxisHingeJoint.Pre_treeDoWithDataCallbackOdeSingleAxisHingeJoint != null) {
            Pre_OdeSingleAxisHingeJoint.Pre_treeDoWithDataCallbackOdeSingleAxisHingeJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OdeSingleAxisHingeJoint.Pre_doWithDataCallbackOdeSingleAxisHingeJoint != null)
            Pre_OdeSingleAxisHingeJoint.Pre_doWithDataCallbackOdeSingleAxisHingeJoint.doWithData(this);
    }
}

class Pre_OdeSingleAxisHingeJointRenderCallback extends RenderCallback {}
class Pre_OdeSingleAxisHingeJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_OdeSingleAxisHingeJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_OdeSingleAxisHingeJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OdeSingleAxisHingeJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SingleAxisHingeJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] anchorPoint;
    float[] axis;
    float maxAngle;
    float minAngle;
    float stopBounce;
    float stopErrorCorrection;
    static Pre_SingleAxisHingeJointRenderCallback Pre_renderCallbackSingleAxisHingeJoint;
    static void setPre_SingleAxisHingeJointRenderCallback(Pre_SingleAxisHingeJointRenderCallback node) {
        Pre_renderCallbackSingleAxisHingeJoint = node;
    }

    static Pre_SingleAxisHingeJointTreeRenderCallback Pre_treeRenderCallbackSingleAxisHingeJoint;
    static void setPre_SingleAxisHingeJointTreeRenderCallback(Pre_SingleAxisHingeJointTreeRenderCallback node) {
        Pre_treeRenderCallbackSingleAxisHingeJoint = node;
    }

    static Pre_SingleAxisHingeJointDoWithDataCallback Pre_doWithDataCallbackSingleAxisHingeJoint;
    static void setPre_SingleAxisHingeJointDoWithDataCallback(Pre_SingleAxisHingeJointDoWithDataCallback node) {
        Pre_doWithDataCallbackSingleAxisHingeJoint = node;
    }

    static Pre_SingleAxisHingeJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackSingleAxisHingeJoint;
    static void setPre_SingleAxisHingeJointTreeDoWithDataCallback(Pre_SingleAxisHingeJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSingleAxisHingeJoint = node;

    }

    static Pre_SingleAxisHingeJointEventsProcessedCallback Pre_eventsProcessedCallbackSingleAxisHingeJoint;
    static void setPre_SingleAxisHingeJointEventsProcessedCallback(Pre_SingleAxisHingeJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSingleAxisHingeJoint = node;
    }

    Pre_SingleAxisHingeJoint() {
    }
    void treeRender() {
        if (Pre_SingleAxisHingeJoint.Pre_treeRenderCallbackSingleAxisHingeJoint != null) {
            Pre_SingleAxisHingeJoint.Pre_treeRenderCallbackSingleAxisHingeJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SingleAxisHingeJoint.Pre_renderCallbackSingleAxisHingeJoint != null)
            Pre_SingleAxisHingeJoint.Pre_renderCallbackSingleAxisHingeJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_SingleAxisHingeJoint.Pre_treeDoWithDataCallbackSingleAxisHingeJoint != null) {
            Pre_SingleAxisHingeJoint.Pre_treeDoWithDataCallbackSingleAxisHingeJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SingleAxisHingeJoint.Pre_doWithDataCallbackSingleAxisHingeJoint != null)
            Pre_SingleAxisHingeJoint.Pre_doWithDataCallbackSingleAxisHingeJoint.doWithData(this);
    }
}

class Pre_SingleAxisHingeJointRenderCallback extends RenderCallback {}
class Pre_SingleAxisHingeJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_SingleAxisHingeJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_SingleAxisHingeJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SingleAxisHingeJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ComposedCubeMapTexture extends Pre_Node {
    Pre_Node metadata;
    Pre_Node back;
    Pre_Node bottom;
    Pre_Node front;
    Pre_Node left;
    Pre_Node right;
    Pre_Node top;
    static Pre_ComposedCubeMapTextureRenderCallback Pre_renderCallbackComposedCubeMapTexture;
    static void setPre_ComposedCubeMapTextureRenderCallback(Pre_ComposedCubeMapTextureRenderCallback node) {
        Pre_renderCallbackComposedCubeMapTexture = node;
    }

    static Pre_ComposedCubeMapTextureTreeRenderCallback Pre_treeRenderCallbackComposedCubeMapTexture;
    static void setPre_ComposedCubeMapTextureTreeRenderCallback(Pre_ComposedCubeMapTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackComposedCubeMapTexture = node;
    }

    static Pre_ComposedCubeMapTextureDoWithDataCallback Pre_doWithDataCallbackComposedCubeMapTexture;
    static void setPre_ComposedCubeMapTextureDoWithDataCallback(Pre_ComposedCubeMapTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackComposedCubeMapTexture = node;
    }

    static Pre_ComposedCubeMapTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackComposedCubeMapTexture;
    static void setPre_ComposedCubeMapTextureTreeDoWithDataCallback(Pre_ComposedCubeMapTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackComposedCubeMapTexture = node;

    }

    static Pre_ComposedCubeMapTextureEventsProcessedCallback Pre_eventsProcessedCallbackComposedCubeMapTexture;
    static void setPre_ComposedCubeMapTextureEventsProcessedCallback(Pre_ComposedCubeMapTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackComposedCubeMapTexture = node;
    }

    Pre_ComposedCubeMapTexture() {
    }
    void treeRender() {
        if (Pre_ComposedCubeMapTexture.Pre_treeRenderCallbackComposedCubeMapTexture != null) {
            Pre_ComposedCubeMapTexture.Pre_treeRenderCallbackComposedCubeMapTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (back != null)
            back.treeRender();
        if (bottom != null)
            bottom.treeRender();
        if (front != null)
            front.treeRender();
        if (left != null)
            left.treeRender();
        if (right != null)
            right.treeRender();
        if (top != null)
            top.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ComposedCubeMapTexture.Pre_renderCallbackComposedCubeMapTexture != null)
            Pre_ComposedCubeMapTexture.Pre_renderCallbackComposedCubeMapTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_ComposedCubeMapTexture.Pre_treeDoWithDataCallbackComposedCubeMapTexture != null) {
            Pre_ComposedCubeMapTexture.Pre_treeDoWithDataCallbackComposedCubeMapTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (back != null)
            back.treeDoWithData();
        if (bottom != null)
            bottom.treeDoWithData();
        if (front != null)
            front.treeDoWithData();
        if (left != null)
            left.treeDoWithData();
        if (right != null)
            right.treeDoWithData();
        if (top != null)
            top.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ComposedCubeMapTexture.Pre_doWithDataCallbackComposedCubeMapTexture != null)
            Pre_ComposedCubeMapTexture.Pre_doWithDataCallbackComposedCubeMapTexture.doWithData(this);
    }
}

class Pre_ComposedCubeMapTextureRenderCallback extends RenderCallback {}
class Pre_ComposedCubeMapTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_ComposedCubeMapTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_ComposedCubeMapTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ComposedCubeMapTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Collision extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    boolean enabled;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node proxy;
    static Pre_CollisionRenderCallback Pre_renderCallbackCollision;
    static void setPre_CollisionRenderCallback(Pre_CollisionRenderCallback node) {
        Pre_renderCallbackCollision = node;
    }

    static Pre_CollisionTreeRenderCallback Pre_treeRenderCallbackCollision;
    static void setPre_CollisionTreeRenderCallback(Pre_CollisionTreeRenderCallback node) {
        Pre_treeRenderCallbackCollision = node;
    }

    static Pre_CollisionDoWithDataCallback Pre_doWithDataCallbackCollision;
    static void setPre_CollisionDoWithDataCallback(Pre_CollisionDoWithDataCallback node) {
        Pre_doWithDataCallbackCollision = node;
    }

    static Pre_CollisionTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollision;
    static void setPre_CollisionTreeDoWithDataCallback(Pre_CollisionTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollision = node;

    }

    static Pre_CollisionEventsProcessedCallback Pre_eventsProcessedCallbackCollision;
    static void setPre_CollisionEventsProcessedCallback(Pre_CollisionEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollision = node;
    }

    Pre_Collision() {
    }
    void treeRender() {
        if (Pre_Collision.Pre_treeRenderCallbackCollision != null) {
            Pre_Collision.Pre_treeRenderCallbackCollision.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (proxy != null)
            proxy.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Collision.Pre_renderCallbackCollision != null)
            Pre_Collision.Pre_renderCallbackCollision.render(this);
    }
    void treeDoWithData() {
        if (Pre_Collision.Pre_treeDoWithDataCallbackCollision != null) {
            Pre_Collision.Pre_treeDoWithDataCallbackCollision.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (proxy != null)
            proxy.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Collision.Pre_doWithDataCallbackCollision != null)
            Pre_Collision.Pre_doWithDataCallbackCollision.doWithData(this);
    }
}

class Pre_CollisionRenderCallback extends RenderCallback {}
class Pre_CollisionTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollisionDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollisionTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollisionEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoOrigin extends Pre_Node {
    Pre_Node metadata;
    double[] geoCoords;
    String[] geoSystem;
    boolean rotateYUp;
    static Pre_GeoOriginRenderCallback Pre_renderCallbackGeoOrigin;
    static void setPre_GeoOriginRenderCallback(Pre_GeoOriginRenderCallback node) {
        Pre_renderCallbackGeoOrigin = node;
    }

    static Pre_GeoOriginTreeRenderCallback Pre_treeRenderCallbackGeoOrigin;
    static void setPre_GeoOriginTreeRenderCallback(Pre_GeoOriginTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoOrigin = node;
    }

    static Pre_GeoOriginDoWithDataCallback Pre_doWithDataCallbackGeoOrigin;
    static void setPre_GeoOriginDoWithDataCallback(Pre_GeoOriginDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoOrigin = node;
    }

    static Pre_GeoOriginTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoOrigin;
    static void setPre_GeoOriginTreeDoWithDataCallback(Pre_GeoOriginTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoOrigin = node;

    }

    static Pre_GeoOriginEventsProcessedCallback Pre_eventsProcessedCallbackGeoOrigin;
    static void setPre_GeoOriginEventsProcessedCallback(Pre_GeoOriginEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoOrigin = node;
    }

    Pre_GeoOrigin() {
    }
    void treeRender() {
        if (Pre_GeoOrigin.Pre_treeRenderCallbackGeoOrigin != null) {
            Pre_GeoOrigin.Pre_treeRenderCallbackGeoOrigin.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoOrigin.Pre_renderCallbackGeoOrigin != null)
            Pre_GeoOrigin.Pre_renderCallbackGeoOrigin.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoOrigin.Pre_treeDoWithDataCallbackGeoOrigin != null) {
            Pre_GeoOrigin.Pre_treeDoWithDataCallbackGeoOrigin.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoOrigin.Pre_doWithDataCallbackGeoOrigin != null)
            Pre_GeoOrigin.Pre_doWithDataCallbackGeoOrigin.doWithData(this);
    }
}

class Pre_GeoOriginRenderCallback extends RenderCallback {}
class Pre_GeoOriginTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoOriginDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoOriginTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoOriginEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsGroup extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    float tessellationScale;
    static Pre_NurbsGroupRenderCallback Pre_renderCallbackNurbsGroup;
    static void setPre_NurbsGroupRenderCallback(Pre_NurbsGroupRenderCallback node) {
        Pre_renderCallbackNurbsGroup = node;
    }

    static Pre_NurbsGroupTreeRenderCallback Pre_treeRenderCallbackNurbsGroup;
    static void setPre_NurbsGroupTreeRenderCallback(Pre_NurbsGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsGroup = node;
    }

    static Pre_NurbsGroupDoWithDataCallback Pre_doWithDataCallbackNurbsGroup;
    static void setPre_NurbsGroupDoWithDataCallback(Pre_NurbsGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsGroup = node;
    }

    static Pre_NurbsGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsGroup;
    static void setPre_NurbsGroupTreeDoWithDataCallback(Pre_NurbsGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsGroup = node;

    }

    static Pre_NurbsGroupEventsProcessedCallback Pre_eventsProcessedCallbackNurbsGroup;
    static void setPre_NurbsGroupEventsProcessedCallback(Pre_NurbsGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsGroup = node;
    }

    Pre_NurbsGroup() {
    }
    void treeRender() {
        if (Pre_NurbsGroup.Pre_treeRenderCallbackNurbsGroup != null) {
            Pre_NurbsGroup.Pre_treeRenderCallbackNurbsGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsGroup.Pre_renderCallbackNurbsGroup != null)
            Pre_NurbsGroup.Pre_renderCallbackNurbsGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsGroup.Pre_treeDoWithDataCallbackNurbsGroup != null) {
            Pre_NurbsGroup.Pre_treeDoWithDataCallbackNurbsGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsGroup.Pre_doWithDataCallbackNurbsGroup != null)
            Pre_NurbsGroup.Pre_doWithDataCallbackNurbsGroup.doWithData(this);
    }
}

class Pre_NurbsGroupRenderCallback extends RenderCallback {}
class Pre_NurbsGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoProximitySensor extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    boolean enabled;
    double[] geoCenter;
    float[] size;
    static Pre_GeoProximitySensorRenderCallback Pre_renderCallbackGeoProximitySensor;
    static void setPre_GeoProximitySensorRenderCallback(Pre_GeoProximitySensorRenderCallback node) {
        Pre_renderCallbackGeoProximitySensor = node;
    }

    static Pre_GeoProximitySensorTreeRenderCallback Pre_treeRenderCallbackGeoProximitySensor;
    static void setPre_GeoProximitySensorTreeRenderCallback(Pre_GeoProximitySensorTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoProximitySensor = node;
    }

    static Pre_GeoProximitySensorDoWithDataCallback Pre_doWithDataCallbackGeoProximitySensor;
    static void setPre_GeoProximitySensorDoWithDataCallback(Pre_GeoProximitySensorDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoProximitySensor = node;
    }

    static Pre_GeoProximitySensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoProximitySensor;
    static void setPre_GeoProximitySensorTreeDoWithDataCallback(Pre_GeoProximitySensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoProximitySensor = node;

    }

    static Pre_GeoProximitySensorEventsProcessedCallback Pre_eventsProcessedCallbackGeoProximitySensor;
    static void setPre_GeoProximitySensorEventsProcessedCallback(Pre_GeoProximitySensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoProximitySensor = node;
    }

    Pre_GeoProximitySensor() {
    }
    void treeRender() {
        if (Pre_GeoProximitySensor.Pre_treeRenderCallbackGeoProximitySensor != null) {
            Pre_GeoProximitySensor.Pre_treeRenderCallbackGeoProximitySensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoProximitySensor.Pre_renderCallbackGeoProximitySensor != null)
            Pre_GeoProximitySensor.Pre_renderCallbackGeoProximitySensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoProximitySensor.Pre_treeDoWithDataCallbackGeoProximitySensor != null) {
            Pre_GeoProximitySensor.Pre_treeDoWithDataCallbackGeoProximitySensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoProximitySensor.Pre_doWithDataCallbackGeoProximitySensor != null)
            Pre_GeoProximitySensor.Pre_doWithDataCallbackGeoProximitySensor.doWithData(this);
    }
}

class Pre_GeoProximitySensorRenderCallback extends RenderCallback {}
class Pre_GeoProximitySensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoProximitySensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoProximitySensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoProximitySensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PointEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    float[] direction;
    float[] position;
    static Pre_PointEmitterRenderCallback Pre_renderCallbackPointEmitter;
    static void setPre_PointEmitterRenderCallback(Pre_PointEmitterRenderCallback node) {
        Pre_renderCallbackPointEmitter = node;
    }

    static Pre_PointEmitterTreeRenderCallback Pre_treeRenderCallbackPointEmitter;
    static void setPre_PointEmitterTreeRenderCallback(Pre_PointEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackPointEmitter = node;
    }

    static Pre_PointEmitterDoWithDataCallback Pre_doWithDataCallbackPointEmitter;
    static void setPre_PointEmitterDoWithDataCallback(Pre_PointEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackPointEmitter = node;
    }

    static Pre_PointEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackPointEmitter;
    static void setPre_PointEmitterTreeDoWithDataCallback(Pre_PointEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPointEmitter = node;

    }

    static Pre_PointEmitterEventsProcessedCallback Pre_eventsProcessedCallbackPointEmitter;
    static void setPre_PointEmitterEventsProcessedCallback(Pre_PointEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPointEmitter = node;
    }

    Pre_PointEmitter() {
    }
    void treeRender() {
        if (Pre_PointEmitter.Pre_treeRenderCallbackPointEmitter != null) {
            Pre_PointEmitter.Pre_treeRenderCallbackPointEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PointEmitter.Pre_renderCallbackPointEmitter != null)
            Pre_PointEmitter.Pre_renderCallbackPointEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_PointEmitter.Pre_treeDoWithDataCallbackPointEmitter != null) {
            Pre_PointEmitter.Pre_treeDoWithDataCallbackPointEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PointEmitter.Pre_doWithDataCallbackPointEmitter != null)
            Pre_PointEmitter.Pre_doWithDataCallbackPointEmitter.doWithData(this);
    }
}

class Pre_PointEmitterRenderCallback extends RenderCallback {}
class Pre_PointEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_PointEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_PointEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PointEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ProximitySensor extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    boolean enabled;
    float[] size;
    static Pre_ProximitySensorRenderCallback Pre_renderCallbackProximitySensor;
    static void setPre_ProximitySensorRenderCallback(Pre_ProximitySensorRenderCallback node) {
        Pre_renderCallbackProximitySensor = node;
    }

    static Pre_ProximitySensorTreeRenderCallback Pre_treeRenderCallbackProximitySensor;
    static void setPre_ProximitySensorTreeRenderCallback(Pre_ProximitySensorTreeRenderCallback node) {
        Pre_treeRenderCallbackProximitySensor = node;
    }

    static Pre_ProximitySensorDoWithDataCallback Pre_doWithDataCallbackProximitySensor;
    static void setPre_ProximitySensorDoWithDataCallback(Pre_ProximitySensorDoWithDataCallback node) {
        Pre_doWithDataCallbackProximitySensor = node;
    }

    static Pre_ProximitySensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackProximitySensor;
    static void setPre_ProximitySensorTreeDoWithDataCallback(Pre_ProximitySensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackProximitySensor = node;

    }

    static Pre_ProximitySensorEventsProcessedCallback Pre_eventsProcessedCallbackProximitySensor;
    static void setPre_ProximitySensorEventsProcessedCallback(Pre_ProximitySensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackProximitySensor = node;
    }

    Pre_ProximitySensor() {
    }
    void treeRender() {
        if (Pre_ProximitySensor.Pre_treeRenderCallbackProximitySensor != null) {
            Pre_ProximitySensor.Pre_treeRenderCallbackProximitySensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ProximitySensor.Pre_renderCallbackProximitySensor != null)
            Pre_ProximitySensor.Pre_renderCallbackProximitySensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_ProximitySensor.Pre_treeDoWithDataCallbackProximitySensor != null) {
            Pre_ProximitySensor.Pre_treeDoWithDataCallbackProximitySensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ProximitySensor.Pre_doWithDataCallbackProximitySensor != null)
            Pre_ProximitySensor.Pre_doWithDataCallbackProximitySensor.doWithData(this);
    }
}

class Pre_ProximitySensorRenderCallback extends RenderCallback {}
class Pre_ProximitySensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_ProximitySensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_ProximitySensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ProximitySensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ScalarChaser extends Pre_Node {
    Pre_Node metadata;
    double duration;
    float initialDestination;
    float initialValue;
    static Pre_ScalarChaserRenderCallback Pre_renderCallbackScalarChaser;
    static void setPre_ScalarChaserRenderCallback(Pre_ScalarChaserRenderCallback node) {
        Pre_renderCallbackScalarChaser = node;
    }

    static Pre_ScalarChaserTreeRenderCallback Pre_treeRenderCallbackScalarChaser;
    static void setPre_ScalarChaserTreeRenderCallback(Pre_ScalarChaserTreeRenderCallback node) {
        Pre_treeRenderCallbackScalarChaser = node;
    }

    static Pre_ScalarChaserDoWithDataCallback Pre_doWithDataCallbackScalarChaser;
    static void setPre_ScalarChaserDoWithDataCallback(Pre_ScalarChaserDoWithDataCallback node) {
        Pre_doWithDataCallbackScalarChaser = node;
    }

    static Pre_ScalarChaserTreeDoWithDataCallback Pre_treeDoWithDataCallbackScalarChaser;
    static void setPre_ScalarChaserTreeDoWithDataCallback(Pre_ScalarChaserTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackScalarChaser = node;

    }

    static Pre_ScalarChaserEventsProcessedCallback Pre_eventsProcessedCallbackScalarChaser;
    static void setPre_ScalarChaserEventsProcessedCallback(Pre_ScalarChaserEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackScalarChaser = node;
    }

    Pre_ScalarChaser() {
    }
    void treeRender() {
        if (Pre_ScalarChaser.Pre_treeRenderCallbackScalarChaser != null) {
            Pre_ScalarChaser.Pre_treeRenderCallbackScalarChaser.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ScalarChaser.Pre_renderCallbackScalarChaser != null)
            Pre_ScalarChaser.Pre_renderCallbackScalarChaser.render(this);
    }
    void treeDoWithData() {
        if (Pre_ScalarChaser.Pre_treeDoWithDataCallbackScalarChaser != null) {
            Pre_ScalarChaser.Pre_treeDoWithDataCallbackScalarChaser.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ScalarChaser.Pre_doWithDataCallbackScalarChaser != null)
            Pre_ScalarChaser.Pre_doWithDataCallbackScalarChaser.doWithData(this);
    }
}

class Pre_ScalarChaserRenderCallback extends RenderCallback {}
class Pre_ScalarChaserTreeRenderCallback extends TreeRenderCallback {}
class Pre_ScalarChaserDoWithDataCallback extends DoWithDataCallback {};
class Pre_ScalarChaserTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ScalarChaserEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_VisibilitySensor extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    boolean enabled;
    float[] size;
    static Pre_VisibilitySensorRenderCallback Pre_renderCallbackVisibilitySensor;
    static void setPre_VisibilitySensorRenderCallback(Pre_VisibilitySensorRenderCallback node) {
        Pre_renderCallbackVisibilitySensor = node;
    }

    static Pre_VisibilitySensorTreeRenderCallback Pre_treeRenderCallbackVisibilitySensor;
    static void setPre_VisibilitySensorTreeRenderCallback(Pre_VisibilitySensorTreeRenderCallback node) {
        Pre_treeRenderCallbackVisibilitySensor = node;
    }

    static Pre_VisibilitySensorDoWithDataCallback Pre_doWithDataCallbackVisibilitySensor;
    static void setPre_VisibilitySensorDoWithDataCallback(Pre_VisibilitySensorDoWithDataCallback node) {
        Pre_doWithDataCallbackVisibilitySensor = node;
    }

    static Pre_VisibilitySensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackVisibilitySensor;
    static void setPre_VisibilitySensorTreeDoWithDataCallback(Pre_VisibilitySensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackVisibilitySensor = node;

    }

    static Pre_VisibilitySensorEventsProcessedCallback Pre_eventsProcessedCallbackVisibilitySensor;
    static void setPre_VisibilitySensorEventsProcessedCallback(Pre_VisibilitySensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackVisibilitySensor = node;
    }

    Pre_VisibilitySensor() {
    }
    void treeRender() {
        if (Pre_VisibilitySensor.Pre_treeRenderCallbackVisibilitySensor != null) {
            Pre_VisibilitySensor.Pre_treeRenderCallbackVisibilitySensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_VisibilitySensor.Pre_renderCallbackVisibilitySensor != null)
            Pre_VisibilitySensor.Pre_renderCallbackVisibilitySensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_VisibilitySensor.Pre_treeDoWithDataCallbackVisibilitySensor != null) {
            Pre_VisibilitySensor.Pre_treeDoWithDataCallbackVisibilitySensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_VisibilitySensor.Pre_doWithDataCallbackVisibilitySensor != null)
            Pre_VisibilitySensor.Pre_doWithDataCallbackVisibilitySensor.doWithData(this);
    }
}

class Pre_VisibilitySensorRenderCallback extends RenderCallback {}
class Pre_VisibilitySensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_VisibilitySensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_VisibilitySensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_VisibilitySensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CattExportSrc extends Pre_Node {
    Pre_Node metadata;
    String id;
    float[] sourceCenterLocation;
    String sourceDirectivityName;
    float[] coordinateToAimTheSource;
    float roll;
    String[] furtherParameters;
    static Pre_CattExportSrcRenderCallback Pre_renderCallbackCattExportSrc;
    static void setPre_CattExportSrcRenderCallback(Pre_CattExportSrcRenderCallback node) {
        Pre_renderCallbackCattExportSrc = node;
    }

    static Pre_CattExportSrcTreeRenderCallback Pre_treeRenderCallbackCattExportSrc;
    static void setPre_CattExportSrcTreeRenderCallback(Pre_CattExportSrcTreeRenderCallback node) {
        Pre_treeRenderCallbackCattExportSrc = node;
    }

    static Pre_CattExportSrcDoWithDataCallback Pre_doWithDataCallbackCattExportSrc;
    static void setPre_CattExportSrcDoWithDataCallback(Pre_CattExportSrcDoWithDataCallback node) {
        Pre_doWithDataCallbackCattExportSrc = node;
    }

    static Pre_CattExportSrcTreeDoWithDataCallback Pre_treeDoWithDataCallbackCattExportSrc;
    static void setPre_CattExportSrcTreeDoWithDataCallback(Pre_CattExportSrcTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCattExportSrc = node;

    }

    static Pre_CattExportSrcEventsProcessedCallback Pre_eventsProcessedCallbackCattExportSrc;
    static void setPre_CattExportSrcEventsProcessedCallback(Pre_CattExportSrcEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCattExportSrc = node;
    }

    Pre_CattExportSrc() {
    }
    void treeRender() {
        if (Pre_CattExportSrc.Pre_treeRenderCallbackCattExportSrc != null) {
            Pre_CattExportSrc.Pre_treeRenderCallbackCattExportSrc.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CattExportSrc.Pre_renderCallbackCattExportSrc != null)
            Pre_CattExportSrc.Pre_renderCallbackCattExportSrc.render(this);
    }
    void treeDoWithData() {
        if (Pre_CattExportSrc.Pre_treeDoWithDataCallbackCattExportSrc != null) {
            Pre_CattExportSrc.Pre_treeDoWithDataCallbackCattExportSrc.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CattExportSrc.Pre_doWithDataCallbackCattExportSrc != null)
            Pre_CattExportSrc.Pre_doWithDataCallbackCattExportSrc.doWithData(this);
    }
}

class Pre_CattExportSrcRenderCallback extends RenderCallback {}
class Pre_CattExportSrcTreeRenderCallback extends TreeRenderCallback {}
class Pre_CattExportSrcDoWithDataCallback extends DoWithDataCallback {};
class Pre_CattExportSrcTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CattExportSrcEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_HAnimHumanoid extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    String[] info;
    Pre_Node [] joints;
    String name;
    Pre_Node [] segments;
    Pre_Node [] sites;
    Pre_Node [] skeleton;
    Pre_Node [] skin;
    Pre_Node skinCoord;
    Pre_Node skinNormal;
    String version;
    Pre_Node [] viewpoints;
    static Pre_HAnimHumanoidRenderCallback Pre_renderCallbackHAnimHumanoid;
    static void setPre_HAnimHumanoidRenderCallback(Pre_HAnimHumanoidRenderCallback node) {
        Pre_renderCallbackHAnimHumanoid = node;
    }

    static Pre_HAnimHumanoidTreeRenderCallback Pre_treeRenderCallbackHAnimHumanoid;
    static void setPre_HAnimHumanoidTreeRenderCallback(Pre_HAnimHumanoidTreeRenderCallback node) {
        Pre_treeRenderCallbackHAnimHumanoid = node;
    }

    static Pre_HAnimHumanoidDoWithDataCallback Pre_doWithDataCallbackHAnimHumanoid;
    static void setPre_HAnimHumanoidDoWithDataCallback(Pre_HAnimHumanoidDoWithDataCallback node) {
        Pre_doWithDataCallbackHAnimHumanoid = node;
    }

    static Pre_HAnimHumanoidTreeDoWithDataCallback Pre_treeDoWithDataCallbackHAnimHumanoid;
    static void setPre_HAnimHumanoidTreeDoWithDataCallback(Pre_HAnimHumanoidTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackHAnimHumanoid = node;

    }

    static Pre_HAnimHumanoidEventsProcessedCallback Pre_eventsProcessedCallbackHAnimHumanoid;
    static void setPre_HAnimHumanoidEventsProcessedCallback(Pre_HAnimHumanoidEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackHAnimHumanoid = node;
    }

    Pre_HAnimHumanoid() {
    }
    void treeRender() {
        if (Pre_HAnimHumanoid.Pre_treeRenderCallbackHAnimHumanoid != null) {
            Pre_HAnimHumanoid.Pre_treeRenderCallbackHAnimHumanoid.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (joints != null)
            for (int i = 0; i < joints.length; i++)
                if (joints[i] != null)
                    joints[i].treeRender();
        if (segments != null)
            for (int i = 0; i < segments.length; i++)
                if (segments[i] != null)
                    segments[i].treeRender();
        if (sites != null)
            for (int i = 0; i < sites.length; i++)
                if (sites[i] != null)
                    sites[i].treeRender();
        if (skeleton != null)
            for (int i = 0; i < skeleton.length; i++)
                if (skeleton[i] != null)
                    skeleton[i].treeRender();
        if (skin != null)
            for (int i = 0; i < skin.length; i++)
                if (skin[i] != null)
                    skin[i].treeRender();
        if (skinCoord != null)
            skinCoord.treeRender();
        if (skinNormal != null)
            skinNormal.treeRender();
        if (viewpoints != null)
            for (int i = 0; i < viewpoints.length; i++)
                if (viewpoints[i] != null)
                    viewpoints[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_HAnimHumanoid.Pre_renderCallbackHAnimHumanoid != null)
            Pre_HAnimHumanoid.Pre_renderCallbackHAnimHumanoid.render(this);
    }
    void treeDoWithData() {
        if (Pre_HAnimHumanoid.Pre_treeDoWithDataCallbackHAnimHumanoid != null) {
            Pre_HAnimHumanoid.Pre_treeDoWithDataCallbackHAnimHumanoid.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (joints != null)
            for (int i = 0; i < joints.length; i++)
                if (joints[i] != null)
                    joints[i].treeDoWithData();
        if (segments != null)
            for (int i = 0; i < segments.length; i++)
                if (segments[i] != null)
                    segments[i].treeDoWithData();
        if (sites != null)
            for (int i = 0; i < sites.length; i++)
                if (sites[i] != null)
                    sites[i].treeDoWithData();
        if (skeleton != null)
            for (int i = 0; i < skeleton.length; i++)
                if (skeleton[i] != null)
                    skeleton[i].treeDoWithData();
        if (skin != null)
            for (int i = 0; i < skin.length; i++)
                if (skin[i] != null)
                    skin[i].treeDoWithData();
        if (skinCoord != null)
            skinCoord.treeDoWithData();
        if (skinNormal != null)
            skinNormal.treeDoWithData();
        if (viewpoints != null)
            for (int i = 0; i < viewpoints.length; i++)
                if (viewpoints[i] != null)
                    viewpoints[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_HAnimHumanoid.Pre_doWithDataCallbackHAnimHumanoid != null)
            Pre_HAnimHumanoid.Pre_doWithDataCallbackHAnimHumanoid.doWithData(this);
    }
}

class Pre_HAnimHumanoidRenderCallback extends RenderCallback {}
class Pre_HAnimHumanoidTreeRenderCallback extends TreeRenderCallback {}
class Pre_HAnimHumanoidDoWithDataCallback extends DoWithDataCallback {};
class Pre_HAnimHumanoidTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_HAnimHumanoidEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedFaceSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    int[] colorIndex;
    boolean colorPerVertex;
    boolean convex;
    int[] coordIndex;
    float creaseAngle;
    int[] normalIndex;
    boolean normalPerVertex;
    boolean solid;
    int[] texCoordIndex;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_IndexedFaceSetRenderCallback Pre_renderCallbackIndexedFaceSet;
    static void setPre_IndexedFaceSetRenderCallback(Pre_IndexedFaceSetRenderCallback node) {
        Pre_renderCallbackIndexedFaceSet = node;
    }

    static Pre_IndexedFaceSetTreeRenderCallback Pre_treeRenderCallbackIndexedFaceSet;
    static void setPre_IndexedFaceSetTreeRenderCallback(Pre_IndexedFaceSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedFaceSet = node;
    }

    static Pre_IndexedFaceSetDoWithDataCallback Pre_doWithDataCallbackIndexedFaceSet;
    static void setPre_IndexedFaceSetDoWithDataCallback(Pre_IndexedFaceSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedFaceSet = node;
    }

    static Pre_IndexedFaceSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedFaceSet;
    static void setPre_IndexedFaceSetTreeDoWithDataCallback(Pre_IndexedFaceSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedFaceSet = node;

    }

    static Pre_IndexedFaceSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedFaceSet;
    static void setPre_IndexedFaceSetEventsProcessedCallback(Pre_IndexedFaceSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedFaceSet = node;
    }

    Pre_IndexedFaceSet() {
    }
    void treeRender() {
        if (Pre_IndexedFaceSet.Pre_treeRenderCallbackIndexedFaceSet != null) {
            Pre_IndexedFaceSet.Pre_treeRenderCallbackIndexedFaceSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedFaceSet.Pre_renderCallbackIndexedFaceSet != null)
            Pre_IndexedFaceSet.Pre_renderCallbackIndexedFaceSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedFaceSet.Pre_treeDoWithDataCallbackIndexedFaceSet != null) {
            Pre_IndexedFaceSet.Pre_treeDoWithDataCallbackIndexedFaceSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedFaceSet.Pre_doWithDataCallbackIndexedFaceSet != null)
            Pre_IndexedFaceSet.Pre_doWithDataCallbackIndexedFaceSet.doWithData(this);
    }
}

class Pre_IndexedFaceSetRenderCallback extends RenderCallback {}
class Pre_IndexedFaceSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedFaceSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedFaceSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedFaceSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Sound extends Pre_Node {
    Pre_Node metadata;
    float[] direction;
    float intensity;
    float[] location;
    float maxBack;
    float maxFront;
    float minBack;
    float minFront;
    float priority;
    Pre_Node source;
    boolean spatialize;
    static Pre_SoundRenderCallback Pre_renderCallbackSound;
    static void setPre_SoundRenderCallback(Pre_SoundRenderCallback node) {
        Pre_renderCallbackSound = node;
    }

    static Pre_SoundTreeRenderCallback Pre_treeRenderCallbackSound;
    static void setPre_SoundTreeRenderCallback(Pre_SoundTreeRenderCallback node) {
        Pre_treeRenderCallbackSound = node;
    }

    static Pre_SoundDoWithDataCallback Pre_doWithDataCallbackSound;
    static void setPre_SoundDoWithDataCallback(Pre_SoundDoWithDataCallback node) {
        Pre_doWithDataCallbackSound = node;
    }

    static Pre_SoundTreeDoWithDataCallback Pre_treeDoWithDataCallbackSound;
    static void setPre_SoundTreeDoWithDataCallback(Pre_SoundTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSound = node;

    }

    static Pre_SoundEventsProcessedCallback Pre_eventsProcessedCallbackSound;
    static void setPre_SoundEventsProcessedCallback(Pre_SoundEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSound = node;
    }

    Pre_Sound() {
    }
    void treeRender() {
        if (Pre_Sound.Pre_treeRenderCallbackSound != null) {
            Pre_Sound.Pre_treeRenderCallbackSound.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (source != null)
            source.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Sound.Pre_renderCallbackSound != null)
            Pre_Sound.Pre_renderCallbackSound.render(this);
    }
    void treeDoWithData() {
        if (Pre_Sound.Pre_treeDoWithDataCallbackSound != null) {
            Pre_Sound.Pre_treeDoWithDataCallbackSound.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (source != null)
            source.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Sound.Pre_doWithDataCallbackSound != null)
            Pre_Sound.Pre_doWithDataCallbackSound.doWithData(this);
    }
}

class Pre_SoundRenderCallback extends RenderCallback {}
class Pre_SoundTreeRenderCallback extends TreeRenderCallback {}
class Pre_SoundDoWithDataCallback extends DoWithDataCallback {};
class Pre_SoundTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SoundEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TriangleSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    boolean normalPerVertex;
    boolean solid;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_TriangleSetRenderCallback Pre_renderCallbackTriangleSet;
    static void setPre_TriangleSetRenderCallback(Pre_TriangleSetRenderCallback node) {
        Pre_renderCallbackTriangleSet = node;
    }

    static Pre_TriangleSetTreeRenderCallback Pre_treeRenderCallbackTriangleSet;
    static void setPre_TriangleSetTreeRenderCallback(Pre_TriangleSetTreeRenderCallback node) {
        Pre_treeRenderCallbackTriangleSet = node;
    }

    static Pre_TriangleSetDoWithDataCallback Pre_doWithDataCallbackTriangleSet;
    static void setPre_TriangleSetDoWithDataCallback(Pre_TriangleSetDoWithDataCallback node) {
        Pre_doWithDataCallbackTriangleSet = node;
    }

    static Pre_TriangleSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackTriangleSet;
    static void setPre_TriangleSetTreeDoWithDataCallback(Pre_TriangleSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTriangleSet = node;

    }

    static Pre_TriangleSetEventsProcessedCallback Pre_eventsProcessedCallbackTriangleSet;
    static void setPre_TriangleSetEventsProcessedCallback(Pre_TriangleSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTriangleSet = node;
    }

    Pre_TriangleSet() {
    }
    void treeRender() {
        if (Pre_TriangleSet.Pre_treeRenderCallbackTriangleSet != null) {
            Pre_TriangleSet.Pre_treeRenderCallbackTriangleSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TriangleSet.Pre_renderCallbackTriangleSet != null)
            Pre_TriangleSet.Pre_renderCallbackTriangleSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_TriangleSet.Pre_treeDoWithDataCallbackTriangleSet != null) {
            Pre_TriangleSet.Pre_treeDoWithDataCallbackTriangleSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TriangleSet.Pre_doWithDataCallbackTriangleSet != null)
            Pre_TriangleSet.Pre_doWithDataCallbackTriangleSet.doWithData(this);
    }
}

class Pre_TriangleSetRenderCallback extends RenderCallback {}
class Pre_TriangleSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_TriangleSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_TriangleSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TriangleSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Cone extends Pre_Node {
    Pre_Node metadata;
    boolean bottom;
    float bottomRadius;
    float height;
    boolean side;
    boolean solid;
    Pre_Node texCoord;
    static Pre_ConeRenderCallback Pre_renderCallbackCone;
    static void setPre_ConeRenderCallback(Pre_ConeRenderCallback node) {
        Pre_renderCallbackCone = node;
    }

    static Pre_ConeTreeRenderCallback Pre_treeRenderCallbackCone;
    static void setPre_ConeTreeRenderCallback(Pre_ConeTreeRenderCallback node) {
        Pre_treeRenderCallbackCone = node;
    }

    static Pre_ConeDoWithDataCallback Pre_doWithDataCallbackCone;
    static void setPre_ConeDoWithDataCallback(Pre_ConeDoWithDataCallback node) {
        Pre_doWithDataCallbackCone = node;
    }

    static Pre_ConeTreeDoWithDataCallback Pre_treeDoWithDataCallbackCone;
    static void setPre_ConeTreeDoWithDataCallback(Pre_ConeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCone = node;

    }

    static Pre_ConeEventsProcessedCallback Pre_eventsProcessedCallbackCone;
    static void setPre_ConeEventsProcessedCallback(Pre_ConeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCone = node;
    }

    Pre_Cone() {
    }
    void treeRender() {
        if (Pre_Cone.Pre_treeRenderCallbackCone != null) {
            Pre_Cone.Pre_treeRenderCallbackCone.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Cone.Pre_renderCallbackCone != null)
            Pre_Cone.Pre_renderCallbackCone.render(this);
    }
    void treeDoWithData() {
        if (Pre_Cone.Pre_treeDoWithDataCallbackCone != null) {
            Pre_Cone.Pre_treeDoWithDataCallbackCone.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Cone.Pre_doWithDataCallbackCone != null)
            Pre_Cone.Pre_doWithDataCallbackCone.doWithData(this);
    }
}

class Pre_ConeRenderCallback extends RenderCallback {}
class Pre_ConeTreeRenderCallback extends TreeRenderCallback {}
class Pre_ConeDoWithDataCallback extends DoWithDataCallback {};
class Pre_ConeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ConeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_FontStyle extends Pre_Node {
    Pre_Node metadata;
    String[] family;
    boolean horizontal;
    String[] justify;
    String language;
    boolean leftToRight;
    float size;
    float spacing;
    String style;
    boolean topToBottom;
    static Pre_FontStyleRenderCallback Pre_renderCallbackFontStyle;
    static void setPre_FontStyleRenderCallback(Pre_FontStyleRenderCallback node) {
        Pre_renderCallbackFontStyle = node;
    }

    static Pre_FontStyleTreeRenderCallback Pre_treeRenderCallbackFontStyle;
    static void setPre_FontStyleTreeRenderCallback(Pre_FontStyleTreeRenderCallback node) {
        Pre_treeRenderCallbackFontStyle = node;
    }

    static Pre_FontStyleDoWithDataCallback Pre_doWithDataCallbackFontStyle;
    static void setPre_FontStyleDoWithDataCallback(Pre_FontStyleDoWithDataCallback node) {
        Pre_doWithDataCallbackFontStyle = node;
    }

    static Pre_FontStyleTreeDoWithDataCallback Pre_treeDoWithDataCallbackFontStyle;
    static void setPre_FontStyleTreeDoWithDataCallback(Pre_FontStyleTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackFontStyle = node;

    }

    static Pre_FontStyleEventsProcessedCallback Pre_eventsProcessedCallbackFontStyle;
    static void setPre_FontStyleEventsProcessedCallback(Pre_FontStyleEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackFontStyle = node;
    }

    Pre_FontStyle() {
    }
    void treeRender() {
        if (Pre_FontStyle.Pre_treeRenderCallbackFontStyle != null) {
            Pre_FontStyle.Pre_treeRenderCallbackFontStyle.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_FontStyle.Pre_renderCallbackFontStyle != null)
            Pre_FontStyle.Pre_renderCallbackFontStyle.render(this);
    }
    void treeDoWithData() {
        if (Pre_FontStyle.Pre_treeDoWithDataCallbackFontStyle != null) {
            Pre_FontStyle.Pre_treeDoWithDataCallbackFontStyle.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_FontStyle.Pre_doWithDataCallbackFontStyle != null)
            Pre_FontStyle.Pre_doWithDataCallbackFontStyle.doWithData(this);
    }
}

class Pre_FontStyleRenderCallback extends RenderCallback {}
class Pre_FontStyleTreeRenderCallback extends TreeRenderCallback {}
class Pre_FontStyleDoWithDataCallback extends DoWithDataCallback {};
class Pre_FontStyleTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_FontStyleEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Inline extends Pre_Node {
    Pre_Node metadata;
    boolean load;
    String[] url;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_InlineRenderCallback Pre_renderCallbackInline;
    static void setPre_InlineRenderCallback(Pre_InlineRenderCallback node) {
        Pre_renderCallbackInline = node;
    }

    static Pre_InlineTreeRenderCallback Pre_treeRenderCallbackInline;
    static void setPre_InlineTreeRenderCallback(Pre_InlineTreeRenderCallback node) {
        Pre_treeRenderCallbackInline = node;
    }

    static Pre_InlineDoWithDataCallback Pre_doWithDataCallbackInline;
    static void setPre_InlineDoWithDataCallback(Pre_InlineDoWithDataCallback node) {
        Pre_doWithDataCallbackInline = node;
    }

    static Pre_InlineTreeDoWithDataCallback Pre_treeDoWithDataCallbackInline;
    static void setPre_InlineTreeDoWithDataCallback(Pre_InlineTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackInline = node;

    }

    static Pre_InlineEventsProcessedCallback Pre_eventsProcessedCallbackInline;
    static void setPre_InlineEventsProcessedCallback(Pre_InlineEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackInline = node;
    }

    Pre_Inline() {
    }
    void treeRender() {
        if (Pre_Inline.Pre_treeRenderCallbackInline != null) {
            Pre_Inline.Pre_treeRenderCallbackInline.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Inline.Pre_renderCallbackInline != null)
            Pre_Inline.Pre_renderCallbackInline.render(this);
    }
    void treeDoWithData() {
        if (Pre_Inline.Pre_treeDoWithDataCallbackInline != null) {
            Pre_Inline.Pre_treeDoWithDataCallbackInline.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Inline.Pre_doWithDataCallbackInline != null)
            Pre_Inline.Pre_doWithDataCallbackInline.doWithData(this);
    }
}

class Pre_InlineRenderCallback extends RenderCallback {}
class Pre_InlineTreeRenderCallback extends TreeRenderCallback {}
class Pre_InlineDoWithDataCallback extends DoWithDataCallback {};
class Pre_InlineTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_InlineEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MultiTextureCoordinate extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] texCoord;
    static Pre_MultiTextureCoordinateRenderCallback Pre_renderCallbackMultiTextureCoordinate;
    static void setPre_MultiTextureCoordinateRenderCallback(Pre_MultiTextureCoordinateRenderCallback node) {
        Pre_renderCallbackMultiTextureCoordinate = node;
    }

    static Pre_MultiTextureCoordinateTreeRenderCallback Pre_treeRenderCallbackMultiTextureCoordinate;
    static void setPre_MultiTextureCoordinateTreeRenderCallback(Pre_MultiTextureCoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackMultiTextureCoordinate = node;
    }

    static Pre_MultiTextureCoordinateDoWithDataCallback Pre_doWithDataCallbackMultiTextureCoordinate;
    static void setPre_MultiTextureCoordinateDoWithDataCallback(Pre_MultiTextureCoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackMultiTextureCoordinate = node;
    }

    static Pre_MultiTextureCoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackMultiTextureCoordinate;
    static void setPre_MultiTextureCoordinateTreeDoWithDataCallback(Pre_MultiTextureCoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMultiTextureCoordinate = node;

    }

    static Pre_MultiTextureCoordinateEventsProcessedCallback Pre_eventsProcessedCallbackMultiTextureCoordinate;
    static void setPre_MultiTextureCoordinateEventsProcessedCallback(Pre_MultiTextureCoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMultiTextureCoordinate = node;
    }

    Pre_MultiTextureCoordinate() {
    }
    void treeRender() {
        if (Pre_MultiTextureCoordinate.Pre_treeRenderCallbackMultiTextureCoordinate != null) {
            Pre_MultiTextureCoordinate.Pre_treeRenderCallbackMultiTextureCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            for (int i = 0; i < texCoord.length; i++)
                if (texCoord[i] != null)
                    texCoord[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MultiTextureCoordinate.Pre_renderCallbackMultiTextureCoordinate != null)
            Pre_MultiTextureCoordinate.Pre_renderCallbackMultiTextureCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_MultiTextureCoordinate.Pre_treeDoWithDataCallbackMultiTextureCoordinate != null) {
            Pre_MultiTextureCoordinate.Pre_treeDoWithDataCallbackMultiTextureCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            for (int i = 0; i < texCoord.length; i++)
                if (texCoord[i] != null)
                    texCoord[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MultiTextureCoordinate.Pre_doWithDataCallbackMultiTextureCoordinate != null)
            Pre_MultiTextureCoordinate.Pre_doWithDataCallbackMultiTextureCoordinate.doWithData(this);
    }
}

class Pre_MultiTextureCoordinateRenderCallback extends RenderCallback {}
class Pre_MultiTextureCoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_MultiTextureCoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_MultiTextureCoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MultiTextureCoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsCurve extends Pre_Node {
    Pre_Node metadata;
    Pre_Node controlPoint;
    int tessellation;
    float[] weight;
    boolean closed;
    float[] knot;
    int order;
    static Pre_NurbsCurveRenderCallback Pre_renderCallbackNurbsCurve;
    static void setPre_NurbsCurveRenderCallback(Pre_NurbsCurveRenderCallback node) {
        Pre_renderCallbackNurbsCurve = node;
    }

    static Pre_NurbsCurveTreeRenderCallback Pre_treeRenderCallbackNurbsCurve;
    static void setPre_NurbsCurveTreeRenderCallback(Pre_NurbsCurveTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsCurve = node;
    }

    static Pre_NurbsCurveDoWithDataCallback Pre_doWithDataCallbackNurbsCurve;
    static void setPre_NurbsCurveDoWithDataCallback(Pre_NurbsCurveDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsCurve = node;
    }

    static Pre_NurbsCurveTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsCurve;
    static void setPre_NurbsCurveTreeDoWithDataCallback(Pre_NurbsCurveTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsCurve = node;

    }

    static Pre_NurbsCurveEventsProcessedCallback Pre_eventsProcessedCallbackNurbsCurve;
    static void setPre_NurbsCurveEventsProcessedCallback(Pre_NurbsCurveEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsCurve = node;
    }

    Pre_NurbsCurve() {
    }
    void treeRender() {
        if (Pre_NurbsCurve.Pre_treeRenderCallbackNurbsCurve != null) {
            Pre_NurbsCurve.Pre_treeRenderCallbackNurbsCurve.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoint != null)
            controlPoint.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsCurve.Pre_renderCallbackNurbsCurve != null)
            Pre_NurbsCurve.Pre_renderCallbackNurbsCurve.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsCurve.Pre_treeDoWithDataCallbackNurbsCurve != null) {
            Pre_NurbsCurve.Pre_treeDoWithDataCallbackNurbsCurve.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoint != null)
            controlPoint.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsCurve.Pre_doWithDataCallbackNurbsCurve != null)
            Pre_NurbsCurve.Pre_doWithDataCallbackNurbsCurve.doWithData(this);
    }
}

class Pre_NurbsCurveRenderCallback extends RenderCallback {}
class Pre_NurbsCurveTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsCurveDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsCurveTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsCurveEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SignalPdu extends Pre_Node {
    Pre_Node metadata;
    int[] data;
    int dataLength;
    int encodingScheme;
    int radioID;
    int sampleRate;
    int samples;
    int tdlType;
    int whichGeometry;
    float[] bboxCenter;
    float[] bboxSize;
    boolean enabled;
    String networkMode;
    double readInterval;
    double writeInterval;
    int siteID;
    int applicationID;
    int entityID;
    String address;
    int port;
    String multicastRelayHost;
    int multicastRelayPort;
    boolean rtpHeaderExpected;
    static Pre_SignalPduRenderCallback Pre_renderCallbackSignalPdu;
    static void setPre_SignalPduRenderCallback(Pre_SignalPduRenderCallback node) {
        Pre_renderCallbackSignalPdu = node;
    }

    static Pre_SignalPduTreeRenderCallback Pre_treeRenderCallbackSignalPdu;
    static void setPre_SignalPduTreeRenderCallback(Pre_SignalPduTreeRenderCallback node) {
        Pre_treeRenderCallbackSignalPdu = node;
    }

    static Pre_SignalPduDoWithDataCallback Pre_doWithDataCallbackSignalPdu;
    static void setPre_SignalPduDoWithDataCallback(Pre_SignalPduDoWithDataCallback node) {
        Pre_doWithDataCallbackSignalPdu = node;
    }

    static Pre_SignalPduTreeDoWithDataCallback Pre_treeDoWithDataCallbackSignalPdu;
    static void setPre_SignalPduTreeDoWithDataCallback(Pre_SignalPduTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSignalPdu = node;

    }

    static Pre_SignalPduEventsProcessedCallback Pre_eventsProcessedCallbackSignalPdu;
    static void setPre_SignalPduEventsProcessedCallback(Pre_SignalPduEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSignalPdu = node;
    }

    Pre_SignalPdu() {
    }
    void treeRender() {
        if (Pre_SignalPdu.Pre_treeRenderCallbackSignalPdu != null) {
            Pre_SignalPdu.Pre_treeRenderCallbackSignalPdu.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SignalPdu.Pre_renderCallbackSignalPdu != null)
            Pre_SignalPdu.Pre_renderCallbackSignalPdu.render(this);
    }
    void treeDoWithData() {
        if (Pre_SignalPdu.Pre_treeDoWithDataCallbackSignalPdu != null) {
            Pre_SignalPdu.Pre_treeDoWithDataCallbackSignalPdu.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SignalPdu.Pre_doWithDataCallbackSignalPdu != null)
            Pre_SignalPdu.Pre_doWithDataCallbackSignalPdu.doWithData(this);
    }
}

class Pre_SignalPduRenderCallback extends RenderCallback {}
class Pre_SignalPduTreeRenderCallback extends TreeRenderCallback {}
class Pre_SignalPduDoWithDataCallback extends DoWithDataCallback {};
class Pre_SignalPduTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SignalPduEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_RigidBody extends Pre_Node {
    Pre_Node metadata;
    float angularDampingFactor;
    float[] angularVelocity;
    boolean autoDamp;
    boolean autoDisable;
    float[] centerOfMass;
    float disableAngularSpeed;
    float disableLinearSpeed;
    float disableTime;
    boolean enabled;
    float[] finiteRotationAxis;
    boolean fixed;
    float[] forces;
    Pre_Node [] geometry;
    float[] inertia;
    float linearDampingFactor;
    float[] linearVelocity;
    float mass;
    Pre_Node massDensityModel;
    float[] orientation;
    float[] position;
    float[] torques;
    boolean useFiniteRotation;
    boolean useGlobalGravity;
    static Pre_RigidBodyRenderCallback Pre_renderCallbackRigidBody;
    static void setPre_RigidBodyRenderCallback(Pre_RigidBodyRenderCallback node) {
        Pre_renderCallbackRigidBody = node;
    }

    static Pre_RigidBodyTreeRenderCallback Pre_treeRenderCallbackRigidBody;
    static void setPre_RigidBodyTreeRenderCallback(Pre_RigidBodyTreeRenderCallback node) {
        Pre_treeRenderCallbackRigidBody = node;
    }

    static Pre_RigidBodyDoWithDataCallback Pre_doWithDataCallbackRigidBody;
    static void setPre_RigidBodyDoWithDataCallback(Pre_RigidBodyDoWithDataCallback node) {
        Pre_doWithDataCallbackRigidBody = node;
    }

    static Pre_RigidBodyTreeDoWithDataCallback Pre_treeDoWithDataCallbackRigidBody;
    static void setPre_RigidBodyTreeDoWithDataCallback(Pre_RigidBodyTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackRigidBody = node;

    }

    static Pre_RigidBodyEventsProcessedCallback Pre_eventsProcessedCallbackRigidBody;
    static void setPre_RigidBodyEventsProcessedCallback(Pre_RigidBodyEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackRigidBody = node;
    }

    Pre_RigidBody() {
    }
    void treeRender() {
        if (Pre_RigidBody.Pre_treeRenderCallbackRigidBody != null) {
            Pre_RigidBody.Pre_treeRenderCallbackRigidBody.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geometry != null)
            for (int i = 0; i < geometry.length; i++)
                if (geometry[i] != null)
                    geometry[i].treeRender();
        if (massDensityModel != null)
            massDensityModel.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_RigidBody.Pre_renderCallbackRigidBody != null)
            Pre_RigidBody.Pre_renderCallbackRigidBody.render(this);
    }
    void treeDoWithData() {
        if (Pre_RigidBody.Pre_treeDoWithDataCallbackRigidBody != null) {
            Pre_RigidBody.Pre_treeDoWithDataCallbackRigidBody.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geometry != null)
            for (int i = 0; i < geometry.length; i++)
                if (geometry[i] != null)
                    geometry[i].treeDoWithData();
        if (massDensityModel != null)
            massDensityModel.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_RigidBody.Pre_doWithDataCallbackRigidBody != null)
            Pre_RigidBody.Pre_doWithDataCallbackRigidBody.doWithData(this);
    }
}

class Pre_RigidBodyRenderCallback extends RenderCallback {}
class Pre_RigidBodyTreeRenderCallback extends TreeRenderCallback {}
class Pre_RigidBodyDoWithDataCallback extends DoWithDataCallback {};
class Pre_RigidBodyTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_RigidBodyEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Material extends Pre_Node {
    Pre_Node metadata;
    float ambientIntensity;
    float[] diffuseColor;
    float[] emissiveColor;
    float shininess;
    float[] specularColor;
    float transparency;
    boolean fogImmune;
    float mirror;
    float[] reflSpecular;
    float[] reflDiffuse;
    float[] transSpecular;
    float[] transDiffuse;
    float reflSpecularExp;
    float transSpecularExp;
    static Pre_MaterialRenderCallback Pre_renderCallbackMaterial;
    static void setPre_MaterialRenderCallback(Pre_MaterialRenderCallback node) {
        Pre_renderCallbackMaterial = node;
    }

    static Pre_MaterialTreeRenderCallback Pre_treeRenderCallbackMaterial;
    static void setPre_MaterialTreeRenderCallback(Pre_MaterialTreeRenderCallback node) {
        Pre_treeRenderCallbackMaterial = node;
    }

    static Pre_MaterialDoWithDataCallback Pre_doWithDataCallbackMaterial;
    static void setPre_MaterialDoWithDataCallback(Pre_MaterialDoWithDataCallback node) {
        Pre_doWithDataCallbackMaterial = node;
    }

    static Pre_MaterialTreeDoWithDataCallback Pre_treeDoWithDataCallbackMaterial;
    static void setPre_MaterialTreeDoWithDataCallback(Pre_MaterialTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMaterial = node;

    }

    static Pre_MaterialEventsProcessedCallback Pre_eventsProcessedCallbackMaterial;
    static void setPre_MaterialEventsProcessedCallback(Pre_MaterialEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMaterial = node;
    }

    Pre_Material() {
    }
    void treeRender() {
        if (Pre_Material.Pre_treeRenderCallbackMaterial != null) {
            Pre_Material.Pre_treeRenderCallbackMaterial.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Material.Pre_renderCallbackMaterial != null)
            Pre_Material.Pre_renderCallbackMaterial.render(this);
    }
    void treeDoWithData() {
        if (Pre_Material.Pre_treeDoWithDataCallbackMaterial != null) {
            Pre_Material.Pre_treeDoWithDataCallbackMaterial.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Material.Pre_doWithDataCallbackMaterial != null)
            Pre_Material.Pre_doWithDataCallbackMaterial.doWithData(this);
    }
}

class Pre_MaterialRenderCallback extends RenderCallback {}
class Pre_MaterialTreeRenderCallback extends TreeRenderCallback {}
class Pre_MaterialDoWithDataCallback extends DoWithDataCallback {};
class Pre_MaterialTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MaterialEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MultiTextureTransform extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] textureTransform;
    static Pre_MultiTextureTransformRenderCallback Pre_renderCallbackMultiTextureTransform;
    static void setPre_MultiTextureTransformRenderCallback(Pre_MultiTextureTransformRenderCallback node) {
        Pre_renderCallbackMultiTextureTransform = node;
    }

    static Pre_MultiTextureTransformTreeRenderCallback Pre_treeRenderCallbackMultiTextureTransform;
    static void setPre_MultiTextureTransformTreeRenderCallback(Pre_MultiTextureTransformTreeRenderCallback node) {
        Pre_treeRenderCallbackMultiTextureTransform = node;
    }

    static Pre_MultiTextureTransformDoWithDataCallback Pre_doWithDataCallbackMultiTextureTransform;
    static void setPre_MultiTextureTransformDoWithDataCallback(Pre_MultiTextureTransformDoWithDataCallback node) {
        Pre_doWithDataCallbackMultiTextureTransform = node;
    }

    static Pre_MultiTextureTransformTreeDoWithDataCallback Pre_treeDoWithDataCallbackMultiTextureTransform;
    static void setPre_MultiTextureTransformTreeDoWithDataCallback(Pre_MultiTextureTransformTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMultiTextureTransform = node;

    }

    static Pre_MultiTextureTransformEventsProcessedCallback Pre_eventsProcessedCallbackMultiTextureTransform;
    static void setPre_MultiTextureTransformEventsProcessedCallback(Pre_MultiTextureTransformEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMultiTextureTransform = node;
    }

    Pre_MultiTextureTransform() {
    }
    void treeRender() {
        if (Pre_MultiTextureTransform.Pre_treeRenderCallbackMultiTextureTransform != null) {
            Pre_MultiTextureTransform.Pre_treeRenderCallbackMultiTextureTransform.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureTransform != null)
            for (int i = 0; i < textureTransform.length; i++)
                if (textureTransform[i] != null)
                    textureTransform[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MultiTextureTransform.Pre_renderCallbackMultiTextureTransform != null)
            Pre_MultiTextureTransform.Pre_renderCallbackMultiTextureTransform.render(this);
    }
    void treeDoWithData() {
        if (Pre_MultiTextureTransform.Pre_treeDoWithDataCallbackMultiTextureTransform != null) {
            Pre_MultiTextureTransform.Pre_treeDoWithDataCallbackMultiTextureTransform.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureTransform != null)
            for (int i = 0; i < textureTransform.length; i++)
                if (textureTransform[i] != null)
                    textureTransform[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MultiTextureTransform.Pre_doWithDataCallbackMultiTextureTransform != null)
            Pre_MultiTextureTransform.Pre_doWithDataCallbackMultiTextureTransform.doWithData(this);
    }
}

class Pre_MultiTextureTransformRenderCallback extends RenderCallback {}
class Pre_MultiTextureTransformTreeRenderCallback extends TreeRenderCallback {}
class Pre_MultiTextureTransformDoWithDataCallback extends DoWithDataCallback {};
class Pre_MultiTextureTransformTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MultiTextureTransformEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ConeEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    float angle;
    float[] direction;
    float[] position;
    static Pre_ConeEmitterRenderCallback Pre_renderCallbackConeEmitter;
    static void setPre_ConeEmitterRenderCallback(Pre_ConeEmitterRenderCallback node) {
        Pre_renderCallbackConeEmitter = node;
    }

    static Pre_ConeEmitterTreeRenderCallback Pre_treeRenderCallbackConeEmitter;
    static void setPre_ConeEmitterTreeRenderCallback(Pre_ConeEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackConeEmitter = node;
    }

    static Pre_ConeEmitterDoWithDataCallback Pre_doWithDataCallbackConeEmitter;
    static void setPre_ConeEmitterDoWithDataCallback(Pre_ConeEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackConeEmitter = node;
    }

    static Pre_ConeEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackConeEmitter;
    static void setPre_ConeEmitterTreeDoWithDataCallback(Pre_ConeEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackConeEmitter = node;

    }

    static Pre_ConeEmitterEventsProcessedCallback Pre_eventsProcessedCallbackConeEmitter;
    static void setPre_ConeEmitterEventsProcessedCallback(Pre_ConeEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackConeEmitter = node;
    }

    Pre_ConeEmitter() {
    }
    void treeRender() {
        if (Pre_ConeEmitter.Pre_treeRenderCallbackConeEmitter != null) {
            Pre_ConeEmitter.Pre_treeRenderCallbackConeEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ConeEmitter.Pre_renderCallbackConeEmitter != null)
            Pre_ConeEmitter.Pre_renderCallbackConeEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_ConeEmitter.Pre_treeDoWithDataCallbackConeEmitter != null) {
            Pre_ConeEmitter.Pre_treeDoWithDataCallbackConeEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ConeEmitter.Pre_doWithDataCallbackConeEmitter != null)
            Pre_ConeEmitter.Pre_doWithDataCallbackConeEmitter.doWithData(this);
    }
}

class Pre_ConeEmitterRenderCallback extends RenderCallback {}
class Pre_ConeEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_ConeEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_ConeEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ConeEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MetadataInteger extends Pre_Node {
    Pre_Node metadata;
    String name;
    String reference;
    int[] value;
    static Pre_MetadataIntegerRenderCallback Pre_renderCallbackMetadataInteger;
    static void setPre_MetadataIntegerRenderCallback(Pre_MetadataIntegerRenderCallback node) {
        Pre_renderCallbackMetadataInteger = node;
    }

    static Pre_MetadataIntegerTreeRenderCallback Pre_treeRenderCallbackMetadataInteger;
    static void setPre_MetadataIntegerTreeRenderCallback(Pre_MetadataIntegerTreeRenderCallback node) {
        Pre_treeRenderCallbackMetadataInteger = node;
    }

    static Pre_MetadataIntegerDoWithDataCallback Pre_doWithDataCallbackMetadataInteger;
    static void setPre_MetadataIntegerDoWithDataCallback(Pre_MetadataIntegerDoWithDataCallback node) {
        Pre_doWithDataCallbackMetadataInteger = node;
    }

    static Pre_MetadataIntegerTreeDoWithDataCallback Pre_treeDoWithDataCallbackMetadataInteger;
    static void setPre_MetadataIntegerTreeDoWithDataCallback(Pre_MetadataIntegerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMetadataInteger = node;

    }

    static Pre_MetadataIntegerEventsProcessedCallback Pre_eventsProcessedCallbackMetadataInteger;
    static void setPre_MetadataIntegerEventsProcessedCallback(Pre_MetadataIntegerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMetadataInteger = node;
    }

    Pre_MetadataInteger() {
    }
    void treeRender() {
        if (Pre_MetadataInteger.Pre_treeRenderCallbackMetadataInteger != null) {
            Pre_MetadataInteger.Pre_treeRenderCallbackMetadataInteger.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MetadataInteger.Pre_renderCallbackMetadataInteger != null)
            Pre_MetadataInteger.Pre_renderCallbackMetadataInteger.render(this);
    }
    void treeDoWithData() {
        if (Pre_MetadataInteger.Pre_treeDoWithDataCallbackMetadataInteger != null) {
            Pre_MetadataInteger.Pre_treeDoWithDataCallbackMetadataInteger.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MetadataInteger.Pre_doWithDataCallbackMetadataInteger != null)
            Pre_MetadataInteger.Pre_doWithDataCallbackMetadataInteger.doWithData(this);
    }
}

class Pre_MetadataIntegerRenderCallback extends RenderCallback {}
class Pre_MetadataIntegerTreeRenderCallback extends TreeRenderCallback {}
class Pre_MetadataIntegerDoWithDataCallback extends DoWithDataCallback {};
class Pre_MetadataIntegerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MetadataIntegerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OrientationInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_OrientationInterpolatorRenderCallback Pre_renderCallbackOrientationInterpolator;
    static void setPre_OrientationInterpolatorRenderCallback(Pre_OrientationInterpolatorRenderCallback node) {
        Pre_renderCallbackOrientationInterpolator = node;
    }

    static Pre_OrientationInterpolatorTreeRenderCallback Pre_treeRenderCallbackOrientationInterpolator;
    static void setPre_OrientationInterpolatorTreeRenderCallback(Pre_OrientationInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackOrientationInterpolator = node;
    }

    static Pre_OrientationInterpolatorDoWithDataCallback Pre_doWithDataCallbackOrientationInterpolator;
    static void setPre_OrientationInterpolatorDoWithDataCallback(Pre_OrientationInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackOrientationInterpolator = node;
    }

    static Pre_OrientationInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackOrientationInterpolator;
    static void setPre_OrientationInterpolatorTreeDoWithDataCallback(Pre_OrientationInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOrientationInterpolator = node;

    }

    static Pre_OrientationInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackOrientationInterpolator;
    static void setPre_OrientationInterpolatorEventsProcessedCallback(Pre_OrientationInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOrientationInterpolator = node;
    }

    Pre_OrientationInterpolator() {
    }
    void treeRender() {
        if (Pre_OrientationInterpolator.Pre_treeRenderCallbackOrientationInterpolator != null) {
            Pre_OrientationInterpolator.Pre_treeRenderCallbackOrientationInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OrientationInterpolator.Pre_renderCallbackOrientationInterpolator != null)
            Pre_OrientationInterpolator.Pre_renderCallbackOrientationInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_OrientationInterpolator.Pre_treeDoWithDataCallbackOrientationInterpolator != null) {
            Pre_OrientationInterpolator.Pre_treeDoWithDataCallbackOrientationInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OrientationInterpolator.Pre_doWithDataCallbackOrientationInterpolator != null)
            Pre_OrientationInterpolator.Pre_doWithDataCallbackOrientationInterpolator.doWithData(this);
    }
}

class Pre_OrientationInterpolatorRenderCallback extends RenderCallback {}
class Pre_OrientationInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_OrientationInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_OrientationInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OrientationInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TimeSensor extends Pre_Node {
    Pre_Node metadata;
    double cycleInterval;
    boolean enabled;
    boolean loop;
    double pauseTime;
    double resumeTime;
    double startTime;
    double stopTime;
    static Pre_TimeSensorRenderCallback Pre_renderCallbackTimeSensor;
    static void setPre_TimeSensorRenderCallback(Pre_TimeSensorRenderCallback node) {
        Pre_renderCallbackTimeSensor = node;
    }

    static Pre_TimeSensorTreeRenderCallback Pre_treeRenderCallbackTimeSensor;
    static void setPre_TimeSensorTreeRenderCallback(Pre_TimeSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackTimeSensor = node;
    }

    static Pre_TimeSensorDoWithDataCallback Pre_doWithDataCallbackTimeSensor;
    static void setPre_TimeSensorDoWithDataCallback(Pre_TimeSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackTimeSensor = node;
    }

    static Pre_TimeSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackTimeSensor;
    static void setPre_TimeSensorTreeDoWithDataCallback(Pre_TimeSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTimeSensor = node;

    }

    static Pre_TimeSensorEventsProcessedCallback Pre_eventsProcessedCallbackTimeSensor;
    static void setPre_TimeSensorEventsProcessedCallback(Pre_TimeSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTimeSensor = node;
    }

    Pre_TimeSensor() {
    }
    void treeRender() {
        if (Pre_TimeSensor.Pre_treeRenderCallbackTimeSensor != null) {
            Pre_TimeSensor.Pre_treeRenderCallbackTimeSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TimeSensor.Pre_renderCallbackTimeSensor != null)
            Pre_TimeSensor.Pre_renderCallbackTimeSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_TimeSensor.Pre_treeDoWithDataCallbackTimeSensor != null) {
            Pre_TimeSensor.Pre_treeDoWithDataCallbackTimeSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TimeSensor.Pre_doWithDataCallbackTimeSensor != null)
            Pre_TimeSensor.Pre_doWithDataCallbackTimeSensor.doWithData(this);
    }
}

class Pre_TimeSensorRenderCallback extends RenderCallback {}
class Pre_TimeSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_TimeSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_TimeSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TimeSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedLineSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] attrib;
    Pre_Node color;
    Pre_Node coord;
    int[] colorIndex;
    boolean colorPerVertex;
    int[] coordIndex;
    Pre_Node fogCoord;
    static Pre_IndexedLineSetRenderCallback Pre_renderCallbackIndexedLineSet;
    static void setPre_IndexedLineSetRenderCallback(Pre_IndexedLineSetRenderCallback node) {
        Pre_renderCallbackIndexedLineSet = node;
    }

    static Pre_IndexedLineSetTreeRenderCallback Pre_treeRenderCallbackIndexedLineSet;
    static void setPre_IndexedLineSetTreeRenderCallback(Pre_IndexedLineSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedLineSet = node;
    }

    static Pre_IndexedLineSetDoWithDataCallback Pre_doWithDataCallbackIndexedLineSet;
    static void setPre_IndexedLineSetDoWithDataCallback(Pre_IndexedLineSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedLineSet = node;
    }

    static Pre_IndexedLineSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedLineSet;
    static void setPre_IndexedLineSetTreeDoWithDataCallback(Pre_IndexedLineSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedLineSet = node;

    }

    static Pre_IndexedLineSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedLineSet;
    static void setPre_IndexedLineSetEventsProcessedCallback(Pre_IndexedLineSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedLineSet = node;
    }

    Pre_IndexedLineSet() {
    }
    void treeRender() {
        if (Pre_IndexedLineSet.Pre_treeRenderCallbackIndexedLineSet != null) {
            Pre_IndexedLineSet.Pre_treeRenderCallbackIndexedLineSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedLineSet.Pre_renderCallbackIndexedLineSet != null)
            Pre_IndexedLineSet.Pre_renderCallbackIndexedLineSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedLineSet.Pre_treeDoWithDataCallbackIndexedLineSet != null) {
            Pre_IndexedLineSet.Pre_treeDoWithDataCallbackIndexedLineSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedLineSet.Pre_doWithDataCallbackIndexedLineSet != null)
            Pre_IndexedLineSet.Pre_doWithDataCallbackIndexedLineSet.doWithData(this);
    }
}

class Pre_IndexedLineSetRenderCallback extends RenderCallback {}
class Pre_IndexedLineSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedLineSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedLineSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedLineSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedQuadSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    boolean normalPerVertex;
    boolean solid;
    int[] index;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_IndexedQuadSetRenderCallback Pre_renderCallbackIndexedQuadSet;
    static void setPre_IndexedQuadSetRenderCallback(Pre_IndexedQuadSetRenderCallback node) {
        Pre_renderCallbackIndexedQuadSet = node;
    }

    static Pre_IndexedQuadSetTreeRenderCallback Pre_treeRenderCallbackIndexedQuadSet;
    static void setPre_IndexedQuadSetTreeRenderCallback(Pre_IndexedQuadSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedQuadSet = node;
    }

    static Pre_IndexedQuadSetDoWithDataCallback Pre_doWithDataCallbackIndexedQuadSet;
    static void setPre_IndexedQuadSetDoWithDataCallback(Pre_IndexedQuadSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedQuadSet = node;
    }

    static Pre_IndexedQuadSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedQuadSet;
    static void setPre_IndexedQuadSetTreeDoWithDataCallback(Pre_IndexedQuadSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedQuadSet = node;

    }

    static Pre_IndexedQuadSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedQuadSet;
    static void setPre_IndexedQuadSetEventsProcessedCallback(Pre_IndexedQuadSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedQuadSet = node;
    }

    Pre_IndexedQuadSet() {
    }
    void treeRender() {
        if (Pre_IndexedQuadSet.Pre_treeRenderCallbackIndexedQuadSet != null) {
            Pre_IndexedQuadSet.Pre_treeRenderCallbackIndexedQuadSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedQuadSet.Pre_renderCallbackIndexedQuadSet != null)
            Pre_IndexedQuadSet.Pre_renderCallbackIndexedQuadSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedQuadSet.Pre_treeDoWithDataCallbackIndexedQuadSet != null) {
            Pre_IndexedQuadSet.Pre_treeDoWithDataCallbackIndexedQuadSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedQuadSet.Pre_doWithDataCallbackIndexedQuadSet != null)
            Pre_IndexedQuadSet.Pre_doWithDataCallbackIndexedQuadSet.doWithData(this);
    }
}

class Pre_IndexedQuadSetRenderCallback extends RenderCallback {}
class Pre_IndexedQuadSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedQuadSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedQuadSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedQuadSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedTriangleStripSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean normalPerVertex;
    boolean solid;
    int[] index;
    float creaseAngle;
    static Pre_IndexedTriangleStripSetRenderCallback Pre_renderCallbackIndexedTriangleStripSet;
    static void setPre_IndexedTriangleStripSetRenderCallback(Pre_IndexedTriangleStripSetRenderCallback node) {
        Pre_renderCallbackIndexedTriangleStripSet = node;
    }

    static Pre_IndexedTriangleStripSetTreeRenderCallback Pre_treeRenderCallbackIndexedTriangleStripSet;
    static void setPre_IndexedTriangleStripSetTreeRenderCallback(Pre_IndexedTriangleStripSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedTriangleStripSet = node;
    }

    static Pre_IndexedTriangleStripSetDoWithDataCallback Pre_doWithDataCallbackIndexedTriangleStripSet;
    static void setPre_IndexedTriangleStripSetDoWithDataCallback(Pre_IndexedTriangleStripSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedTriangleStripSet = node;
    }

    static Pre_IndexedTriangleStripSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedTriangleStripSet;
    static void setPre_IndexedTriangleStripSetTreeDoWithDataCallback(Pre_IndexedTriangleStripSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedTriangleStripSet = node;

    }

    static Pre_IndexedTriangleStripSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedTriangleStripSet;
    static void setPre_IndexedTriangleStripSetEventsProcessedCallback(Pre_IndexedTriangleStripSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedTriangleStripSet = node;
    }

    Pre_IndexedTriangleStripSet() {
    }
    void treeRender() {
        if (Pre_IndexedTriangleStripSet.Pre_treeRenderCallbackIndexedTriangleStripSet != null) {
            Pre_IndexedTriangleStripSet.Pre_treeRenderCallbackIndexedTriangleStripSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedTriangleStripSet.Pre_renderCallbackIndexedTriangleStripSet != null)
            Pre_IndexedTriangleStripSet.Pre_renderCallbackIndexedTriangleStripSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedTriangleStripSet.Pre_treeDoWithDataCallbackIndexedTriangleStripSet != null) {
            Pre_IndexedTriangleStripSet.Pre_treeDoWithDataCallbackIndexedTriangleStripSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedTriangleStripSet.Pre_doWithDataCallbackIndexedTriangleStripSet != null)
            Pre_IndexedTriangleStripSet.Pre_doWithDataCallbackIndexedTriangleStripSet.doWithData(this);
    }
}

class Pre_IndexedTriangleStripSetRenderCallback extends RenderCallback {}
class Pre_IndexedTriangleStripSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedTriangleStripSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedTriangleStripSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedTriangleStripSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MetadataFloat extends Pre_Node {
    Pre_Node metadata;
    String name;
    String reference;
    float[] value;
    static Pre_MetadataFloatRenderCallback Pre_renderCallbackMetadataFloat;
    static void setPre_MetadataFloatRenderCallback(Pre_MetadataFloatRenderCallback node) {
        Pre_renderCallbackMetadataFloat = node;
    }

    static Pre_MetadataFloatTreeRenderCallback Pre_treeRenderCallbackMetadataFloat;
    static void setPre_MetadataFloatTreeRenderCallback(Pre_MetadataFloatTreeRenderCallback node) {
        Pre_treeRenderCallbackMetadataFloat = node;
    }

    static Pre_MetadataFloatDoWithDataCallback Pre_doWithDataCallbackMetadataFloat;
    static void setPre_MetadataFloatDoWithDataCallback(Pre_MetadataFloatDoWithDataCallback node) {
        Pre_doWithDataCallbackMetadataFloat = node;
    }

    static Pre_MetadataFloatTreeDoWithDataCallback Pre_treeDoWithDataCallbackMetadataFloat;
    static void setPre_MetadataFloatTreeDoWithDataCallback(Pre_MetadataFloatTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMetadataFloat = node;

    }

    static Pre_MetadataFloatEventsProcessedCallback Pre_eventsProcessedCallbackMetadataFloat;
    static void setPre_MetadataFloatEventsProcessedCallback(Pre_MetadataFloatEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMetadataFloat = node;
    }

    Pre_MetadataFloat() {
    }
    void treeRender() {
        if (Pre_MetadataFloat.Pre_treeRenderCallbackMetadataFloat != null) {
            Pre_MetadataFloat.Pre_treeRenderCallbackMetadataFloat.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MetadataFloat.Pre_renderCallbackMetadataFloat != null)
            Pre_MetadataFloat.Pre_renderCallbackMetadataFloat.render(this);
    }
    void treeDoWithData() {
        if (Pre_MetadataFloat.Pre_treeDoWithDataCallbackMetadataFloat != null) {
            Pre_MetadataFloat.Pre_treeDoWithDataCallbackMetadataFloat.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MetadataFloat.Pre_doWithDataCallbackMetadataFloat != null)
            Pre_MetadataFloat.Pre_doWithDataCallbackMetadataFloat.doWithData(this);
    }
}

class Pre_MetadataFloatRenderCallback extends RenderCallback {}
class Pre_MetadataFloatTreeRenderCallback extends TreeRenderCallback {}
class Pre_MetadataFloatDoWithDataCallback extends DoWithDataCallback {};
class Pre_MetadataFloatTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MetadataFloatEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OdeMotorJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float axis1Angle;
    float axis1Torque;
    float axis2Angle;
    float axis2Torque;
    float axis3Angle;
    float axis3Torque;
    int enabledAxes;
    float[] motor1Axis;
    float[] motor2Axis;
    float[] motor3Axis;
    float stop1Bounce;
    float stop1ErrorCorrection;
    float stop2Bounce;
    float stop2ErrorCorrection;
    float stop3Bounce;
    float stop3ErrorCorrection;
    boolean autoCalc;
    float fMax1;
    float fMax2;
    float fMax3;
    static Pre_OdeMotorJointRenderCallback Pre_renderCallbackOdeMotorJoint;
    static void setPre_OdeMotorJointRenderCallback(Pre_OdeMotorJointRenderCallback node) {
        Pre_renderCallbackOdeMotorJoint = node;
    }

    static Pre_OdeMotorJointTreeRenderCallback Pre_treeRenderCallbackOdeMotorJoint;
    static void setPre_OdeMotorJointTreeRenderCallback(Pre_OdeMotorJointTreeRenderCallback node) {
        Pre_treeRenderCallbackOdeMotorJoint = node;
    }

    static Pre_OdeMotorJointDoWithDataCallback Pre_doWithDataCallbackOdeMotorJoint;
    static void setPre_OdeMotorJointDoWithDataCallback(Pre_OdeMotorJointDoWithDataCallback node) {
        Pre_doWithDataCallbackOdeMotorJoint = node;
    }

    static Pre_OdeMotorJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackOdeMotorJoint;
    static void setPre_OdeMotorJointTreeDoWithDataCallback(Pre_OdeMotorJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOdeMotorJoint = node;

    }

    static Pre_OdeMotorJointEventsProcessedCallback Pre_eventsProcessedCallbackOdeMotorJoint;
    static void setPre_OdeMotorJointEventsProcessedCallback(Pre_OdeMotorJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOdeMotorJoint = node;
    }

    Pre_OdeMotorJoint() {
    }
    void treeRender() {
        if (Pre_OdeMotorJoint.Pre_treeRenderCallbackOdeMotorJoint != null) {
            Pre_OdeMotorJoint.Pre_treeRenderCallbackOdeMotorJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OdeMotorJoint.Pre_renderCallbackOdeMotorJoint != null)
            Pre_OdeMotorJoint.Pre_renderCallbackOdeMotorJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_OdeMotorJoint.Pre_treeDoWithDataCallbackOdeMotorJoint != null) {
            Pre_OdeMotorJoint.Pre_treeDoWithDataCallbackOdeMotorJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OdeMotorJoint.Pre_doWithDataCallbackOdeMotorJoint != null)
            Pre_OdeMotorJoint.Pre_doWithDataCallbackOdeMotorJoint.doWithData(this);
    }
}

class Pre_OdeMotorJointRenderCallback extends RenderCallback {}
class Pre_OdeMotorJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_OdeMotorJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_OdeMotorJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OdeMotorJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionChaser2D extends Pre_Node {
    Pre_Node metadata;
    double duration;
    float[] initialDestination;
    float[] initialValue;
    static Pre_PositionChaser2DRenderCallback Pre_renderCallbackPositionChaser2D;
    static void setPre_PositionChaser2DRenderCallback(Pre_PositionChaser2DRenderCallback node) {
        Pre_renderCallbackPositionChaser2D = node;
    }

    static Pre_PositionChaser2DTreeRenderCallback Pre_treeRenderCallbackPositionChaser2D;
    static void setPre_PositionChaser2DTreeRenderCallback(Pre_PositionChaser2DTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionChaser2D = node;
    }

    static Pre_PositionChaser2DDoWithDataCallback Pre_doWithDataCallbackPositionChaser2D;
    static void setPre_PositionChaser2DDoWithDataCallback(Pre_PositionChaser2DDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionChaser2D = node;
    }

    static Pre_PositionChaser2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionChaser2D;
    static void setPre_PositionChaser2DTreeDoWithDataCallback(Pre_PositionChaser2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionChaser2D = node;

    }

    static Pre_PositionChaser2DEventsProcessedCallback Pre_eventsProcessedCallbackPositionChaser2D;
    static void setPre_PositionChaser2DEventsProcessedCallback(Pre_PositionChaser2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionChaser2D = node;
    }

    Pre_PositionChaser2D() {
    }
    void treeRender() {
        if (Pre_PositionChaser2D.Pre_treeRenderCallbackPositionChaser2D != null) {
            Pre_PositionChaser2D.Pre_treeRenderCallbackPositionChaser2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionChaser2D.Pre_renderCallbackPositionChaser2D != null)
            Pre_PositionChaser2D.Pre_renderCallbackPositionChaser2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionChaser2D.Pre_treeDoWithDataCallbackPositionChaser2D != null) {
            Pre_PositionChaser2D.Pre_treeDoWithDataCallbackPositionChaser2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionChaser2D.Pre_doWithDataCallbackPositionChaser2D != null)
            Pre_PositionChaser2D.Pre_doWithDataCallbackPositionChaser2D.doWithData(this);
    }
}

class Pre_PositionChaser2DRenderCallback extends RenderCallback {}
class Pre_PositionChaser2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionChaser2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionChaser2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionChaser2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TriangleFanSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    int[] fanCount;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    boolean normalPerVertex;
    boolean solid;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_TriangleFanSetRenderCallback Pre_renderCallbackTriangleFanSet;
    static void setPre_TriangleFanSetRenderCallback(Pre_TriangleFanSetRenderCallback node) {
        Pre_renderCallbackTriangleFanSet = node;
    }

    static Pre_TriangleFanSetTreeRenderCallback Pre_treeRenderCallbackTriangleFanSet;
    static void setPre_TriangleFanSetTreeRenderCallback(Pre_TriangleFanSetTreeRenderCallback node) {
        Pre_treeRenderCallbackTriangleFanSet = node;
    }

    static Pre_TriangleFanSetDoWithDataCallback Pre_doWithDataCallbackTriangleFanSet;
    static void setPre_TriangleFanSetDoWithDataCallback(Pre_TriangleFanSetDoWithDataCallback node) {
        Pre_doWithDataCallbackTriangleFanSet = node;
    }

    static Pre_TriangleFanSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackTriangleFanSet;
    static void setPre_TriangleFanSetTreeDoWithDataCallback(Pre_TriangleFanSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTriangleFanSet = node;

    }

    static Pre_TriangleFanSetEventsProcessedCallback Pre_eventsProcessedCallbackTriangleFanSet;
    static void setPre_TriangleFanSetEventsProcessedCallback(Pre_TriangleFanSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTriangleFanSet = node;
    }

    Pre_TriangleFanSet() {
    }
    void treeRender() {
        if (Pre_TriangleFanSet.Pre_treeRenderCallbackTriangleFanSet != null) {
            Pre_TriangleFanSet.Pre_treeRenderCallbackTriangleFanSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TriangleFanSet.Pre_renderCallbackTriangleFanSet != null)
            Pre_TriangleFanSet.Pre_renderCallbackTriangleFanSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_TriangleFanSet.Pre_treeDoWithDataCallbackTriangleFanSet != null) {
            Pre_TriangleFanSet.Pre_treeDoWithDataCallbackTriangleFanSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TriangleFanSet.Pre_doWithDataCallbackTriangleFanSet != null)
            Pre_TriangleFanSet.Pre_doWithDataCallbackTriangleFanSet.doWithData(this);
    }
}

class Pre_TriangleFanSetRenderCallback extends RenderCallback {}
class Pre_TriangleFanSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_TriangleFanSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_TriangleFanSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TriangleFanSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Viewport extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    float[] clipBoundary;
    static Pre_ViewportRenderCallback Pre_renderCallbackViewport;
    static void setPre_ViewportRenderCallback(Pre_ViewportRenderCallback node) {
        Pre_renderCallbackViewport = node;
    }

    static Pre_ViewportTreeRenderCallback Pre_treeRenderCallbackViewport;
    static void setPre_ViewportTreeRenderCallback(Pre_ViewportTreeRenderCallback node) {
        Pre_treeRenderCallbackViewport = node;
    }

    static Pre_ViewportDoWithDataCallback Pre_doWithDataCallbackViewport;
    static void setPre_ViewportDoWithDataCallback(Pre_ViewportDoWithDataCallback node) {
        Pre_doWithDataCallbackViewport = node;
    }

    static Pre_ViewportTreeDoWithDataCallback Pre_treeDoWithDataCallbackViewport;
    static void setPre_ViewportTreeDoWithDataCallback(Pre_ViewportTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackViewport = node;

    }

    static Pre_ViewportEventsProcessedCallback Pre_eventsProcessedCallbackViewport;
    static void setPre_ViewportEventsProcessedCallback(Pre_ViewportEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackViewport = node;
    }

    Pre_Viewport() {
    }
    void treeRender() {
        if (Pre_Viewport.Pre_treeRenderCallbackViewport != null) {
            Pre_Viewport.Pre_treeRenderCallbackViewport.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Viewport.Pre_renderCallbackViewport != null)
            Pre_Viewport.Pre_renderCallbackViewport.render(this);
    }
    void treeDoWithData() {
        if (Pre_Viewport.Pre_treeDoWithDataCallbackViewport != null) {
            Pre_Viewport.Pre_treeDoWithDataCallbackViewport.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Viewport.Pre_doWithDataCallbackViewport != null)
            Pre_Viewport.Pre_doWithDataCallbackViewport.doWithData(this);
    }
}

class Pre_ViewportRenderCallback extends RenderCallback {}
class Pre_ViewportTreeRenderCallback extends TreeRenderCallback {}
class Pre_ViewportDoWithDataCallback extends DoWithDataCallback {};
class Pre_ViewportTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ViewportEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Fog extends Pre_Node {
    Pre_Node metadata;
    float[] color;
    String fogType;
    float visibilityRange;
    boolean volumetric;
    float[] volumetricDirection;
    float volumetricVisibilityStart;
    Pre_Node alternative;
    static Pre_FogRenderCallback Pre_renderCallbackFog;
    static void setPre_FogRenderCallback(Pre_FogRenderCallback node) {
        Pre_renderCallbackFog = node;
    }

    static Pre_FogTreeRenderCallback Pre_treeRenderCallbackFog;
    static void setPre_FogTreeRenderCallback(Pre_FogTreeRenderCallback node) {
        Pre_treeRenderCallbackFog = node;
    }

    static Pre_FogDoWithDataCallback Pre_doWithDataCallbackFog;
    static void setPre_FogDoWithDataCallback(Pre_FogDoWithDataCallback node) {
        Pre_doWithDataCallbackFog = node;
    }

    static Pre_FogTreeDoWithDataCallback Pre_treeDoWithDataCallbackFog;
    static void setPre_FogTreeDoWithDataCallback(Pre_FogTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackFog = node;

    }

    static Pre_FogEventsProcessedCallback Pre_eventsProcessedCallbackFog;
    static void setPre_FogEventsProcessedCallback(Pre_FogEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackFog = node;
    }

    Pre_Fog() {
    }
    void treeRender() {
        if (Pre_Fog.Pre_treeRenderCallbackFog != null) {
            Pre_Fog.Pre_treeRenderCallbackFog.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (alternative != null)
            alternative.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Fog.Pre_renderCallbackFog != null)
            Pre_Fog.Pre_renderCallbackFog.render(this);
    }
    void treeDoWithData() {
        if (Pre_Fog.Pre_treeDoWithDataCallbackFog != null) {
            Pre_Fog.Pre_treeDoWithDataCallbackFog.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (alternative != null)
            alternative.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Fog.Pre_doWithDataCallbackFog != null)
            Pre_Fog.Pre_doWithDataCallbackFog.doWithData(this);
    }
}

class Pre_FogRenderCallback extends RenderCallback {}
class Pre_FogTreeRenderCallback extends TreeRenderCallback {}
class Pre_FogDoWithDataCallback extends DoWithDataCallback {};
class Pre_FogTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_FogEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ShaderProgram extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    String type;
    static Pre_ShaderProgramRenderCallback Pre_renderCallbackShaderProgram;
    static void setPre_ShaderProgramRenderCallback(Pre_ShaderProgramRenderCallback node) {
        Pre_renderCallbackShaderProgram = node;
    }

    static Pre_ShaderProgramTreeRenderCallback Pre_treeRenderCallbackShaderProgram;
    static void setPre_ShaderProgramTreeRenderCallback(Pre_ShaderProgramTreeRenderCallback node) {
        Pre_treeRenderCallbackShaderProgram = node;
    }

    static Pre_ShaderProgramDoWithDataCallback Pre_doWithDataCallbackShaderProgram;
    static void setPre_ShaderProgramDoWithDataCallback(Pre_ShaderProgramDoWithDataCallback node) {
        Pre_doWithDataCallbackShaderProgram = node;
    }

    static Pre_ShaderProgramTreeDoWithDataCallback Pre_treeDoWithDataCallbackShaderProgram;
    static void setPre_ShaderProgramTreeDoWithDataCallback(Pre_ShaderProgramTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackShaderProgram = node;

    }

    static Pre_ShaderProgramEventsProcessedCallback Pre_eventsProcessedCallbackShaderProgram;
    static void setPre_ShaderProgramEventsProcessedCallback(Pre_ShaderProgramEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackShaderProgram = node;
    }

    Pre_ShaderProgram() {
    }
    void treeRender() {
        if (Pre_ShaderProgram.Pre_treeRenderCallbackShaderProgram != null) {
            Pre_ShaderProgram.Pre_treeRenderCallbackShaderProgram.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ShaderProgram.Pre_renderCallbackShaderProgram != null)
            Pre_ShaderProgram.Pre_renderCallbackShaderProgram.render(this);
    }
    void treeDoWithData() {
        if (Pre_ShaderProgram.Pre_treeDoWithDataCallbackShaderProgram != null) {
            Pre_ShaderProgram.Pre_treeDoWithDataCallbackShaderProgram.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ShaderProgram.Pre_doWithDataCallbackShaderProgram != null)
            Pre_ShaderProgram.Pre_doWithDataCallbackShaderProgram.doWithData(this);
    }
}

class Pre_ShaderProgramRenderCallback extends RenderCallback {}
class Pre_ShaderProgramTreeRenderCallback extends TreeRenderCallback {}
class Pre_ShaderProgramDoWithDataCallback extends DoWithDataCallback {};
class Pre_ShaderProgramTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ShaderProgramEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CollisionCollection extends Pre_Node {
    Pre_Node metadata;
    String[] appliedParameters;
    float bounce;
    Pre_Node [] collidables;
    boolean enabled;
    float[] frictionCoefficients;
    float minBounceSpeed;
    float[] slipFactors;
    float softnessConstantForceMix;
    float softnessErrorCorrection;
    float[] surfaceSpeed;
    static Pre_CollisionCollectionRenderCallback Pre_renderCallbackCollisionCollection;
    static void setPre_CollisionCollectionRenderCallback(Pre_CollisionCollectionRenderCallback node) {
        Pre_renderCallbackCollisionCollection = node;
    }

    static Pre_CollisionCollectionTreeRenderCallback Pre_treeRenderCallbackCollisionCollection;
    static void setPre_CollisionCollectionTreeRenderCallback(Pre_CollisionCollectionTreeRenderCallback node) {
        Pre_treeRenderCallbackCollisionCollection = node;
    }

    static Pre_CollisionCollectionDoWithDataCallback Pre_doWithDataCallbackCollisionCollection;
    static void setPre_CollisionCollectionDoWithDataCallback(Pre_CollisionCollectionDoWithDataCallback node) {
        Pre_doWithDataCallbackCollisionCollection = node;
    }

    static Pre_CollisionCollectionTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollisionCollection;
    static void setPre_CollisionCollectionTreeDoWithDataCallback(Pre_CollisionCollectionTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollisionCollection = node;

    }

    static Pre_CollisionCollectionEventsProcessedCallback Pre_eventsProcessedCallbackCollisionCollection;
    static void setPre_CollisionCollectionEventsProcessedCallback(Pre_CollisionCollectionEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollisionCollection = node;
    }

    Pre_CollisionCollection() {
    }
    void treeRender() {
        if (Pre_CollisionCollection.Pre_treeRenderCallbackCollisionCollection != null) {
            Pre_CollisionCollection.Pre_treeRenderCallbackCollisionCollection.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (collidables != null)
            for (int i = 0; i < collidables.length; i++)
                if (collidables[i] != null)
                    collidables[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CollisionCollection.Pre_renderCallbackCollisionCollection != null)
            Pre_CollisionCollection.Pre_renderCallbackCollisionCollection.render(this);
    }
    void treeDoWithData() {
        if (Pre_CollisionCollection.Pre_treeDoWithDataCallbackCollisionCollection != null) {
            Pre_CollisionCollection.Pre_treeDoWithDataCallbackCollisionCollection.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (collidables != null)
            for (int i = 0; i < collidables.length; i++)
                if (collidables[i] != null)
                    collidables[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CollisionCollection.Pre_doWithDataCallbackCollisionCollection != null)
            Pre_CollisionCollection.Pre_doWithDataCallbackCollisionCollection.doWithData(this);
    }
}

class Pre_CollisionCollectionRenderCallback extends RenderCallback {}
class Pre_CollisionCollectionTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollisionCollectionDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollisionCollectionTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollisionCollectionEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_StaticGroup extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_StaticGroupRenderCallback Pre_renderCallbackStaticGroup;
    static void setPre_StaticGroupRenderCallback(Pre_StaticGroupRenderCallback node) {
        Pre_renderCallbackStaticGroup = node;
    }

    static Pre_StaticGroupTreeRenderCallback Pre_treeRenderCallbackStaticGroup;
    static void setPre_StaticGroupTreeRenderCallback(Pre_StaticGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackStaticGroup = node;
    }

    static Pre_StaticGroupDoWithDataCallback Pre_doWithDataCallbackStaticGroup;
    static void setPre_StaticGroupDoWithDataCallback(Pre_StaticGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackStaticGroup = node;
    }

    static Pre_StaticGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackStaticGroup;
    static void setPre_StaticGroupTreeDoWithDataCallback(Pre_StaticGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackStaticGroup = node;

    }

    static Pre_StaticGroupEventsProcessedCallback Pre_eventsProcessedCallbackStaticGroup;
    static void setPre_StaticGroupEventsProcessedCallback(Pre_StaticGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackStaticGroup = node;
    }

    Pre_StaticGroup() {
    }
    void treeRender() {
        if (Pre_StaticGroup.Pre_treeRenderCallbackStaticGroup != null) {
            Pre_StaticGroup.Pre_treeRenderCallbackStaticGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_StaticGroup.Pre_renderCallbackStaticGroup != null)
            Pre_StaticGroup.Pre_renderCallbackStaticGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_StaticGroup.Pre_treeDoWithDataCallbackStaticGroup != null) {
            Pre_StaticGroup.Pre_treeDoWithDataCallbackStaticGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_StaticGroup.Pre_doWithDataCallbackStaticGroup != null)
            Pre_StaticGroup.Pre_doWithDataCallbackStaticGroup.doWithData(this);
    }
}

class Pre_StaticGroupRenderCallback extends RenderCallback {}
class Pre_StaticGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_StaticGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_StaticGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_StaticGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Anchor extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    String description;
    String[] parameter;
    String[] url;
    static Pre_AnchorRenderCallback Pre_renderCallbackAnchor;
    static void setPre_AnchorRenderCallback(Pre_AnchorRenderCallback node) {
        Pre_renderCallbackAnchor = node;
    }

    static Pre_AnchorTreeRenderCallback Pre_treeRenderCallbackAnchor;
    static void setPre_AnchorTreeRenderCallback(Pre_AnchorTreeRenderCallback node) {
        Pre_treeRenderCallbackAnchor = node;
    }

    static Pre_AnchorDoWithDataCallback Pre_doWithDataCallbackAnchor;
    static void setPre_AnchorDoWithDataCallback(Pre_AnchorDoWithDataCallback node) {
        Pre_doWithDataCallbackAnchor = node;
    }

    static Pre_AnchorTreeDoWithDataCallback Pre_treeDoWithDataCallbackAnchor;
    static void setPre_AnchorTreeDoWithDataCallback(Pre_AnchorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackAnchor = node;

    }

    static Pre_AnchorEventsProcessedCallback Pre_eventsProcessedCallbackAnchor;
    static void setPre_AnchorEventsProcessedCallback(Pre_AnchorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackAnchor = node;
    }

    Pre_Anchor() {
    }
    void treeRender() {
        if (Pre_Anchor.Pre_treeRenderCallbackAnchor != null) {
            Pre_Anchor.Pre_treeRenderCallbackAnchor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Anchor.Pre_renderCallbackAnchor != null)
            Pre_Anchor.Pre_renderCallbackAnchor.render(this);
    }
    void treeDoWithData() {
        if (Pre_Anchor.Pre_treeDoWithDataCallbackAnchor != null) {
            Pre_Anchor.Pre_treeDoWithDataCallbackAnchor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Anchor.Pre_doWithDataCallbackAnchor != null)
            Pre_Anchor.Pre_doWithDataCallbackAnchor.doWithData(this);
    }
}

class Pre_AnchorRenderCallback extends RenderCallback {}
class Pre_AnchorTreeRenderCallback extends TreeRenderCallback {}
class Pre_AnchorDoWithDataCallback extends DoWithDataCallback {};
class Pre_AnchorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_AnchorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BooleanTrigger extends Pre_Node {
    Pre_Node metadata;
    static Pre_BooleanTriggerRenderCallback Pre_renderCallbackBooleanTrigger;
    static void setPre_BooleanTriggerRenderCallback(Pre_BooleanTriggerRenderCallback node) {
        Pre_renderCallbackBooleanTrigger = node;
    }

    static Pre_BooleanTriggerTreeRenderCallback Pre_treeRenderCallbackBooleanTrigger;
    static void setPre_BooleanTriggerTreeRenderCallback(Pre_BooleanTriggerTreeRenderCallback node) {
        Pre_treeRenderCallbackBooleanTrigger = node;
    }

    static Pre_BooleanTriggerDoWithDataCallback Pre_doWithDataCallbackBooleanTrigger;
    static void setPre_BooleanTriggerDoWithDataCallback(Pre_BooleanTriggerDoWithDataCallback node) {
        Pre_doWithDataCallbackBooleanTrigger = node;
    }

    static Pre_BooleanTriggerTreeDoWithDataCallback Pre_treeDoWithDataCallbackBooleanTrigger;
    static void setPre_BooleanTriggerTreeDoWithDataCallback(Pre_BooleanTriggerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBooleanTrigger = node;

    }

    static Pre_BooleanTriggerEventsProcessedCallback Pre_eventsProcessedCallbackBooleanTrigger;
    static void setPre_BooleanTriggerEventsProcessedCallback(Pre_BooleanTriggerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBooleanTrigger = node;
    }

    Pre_BooleanTrigger() {
    }
    void treeRender() {
        if (Pre_BooleanTrigger.Pre_treeRenderCallbackBooleanTrigger != null) {
            Pre_BooleanTrigger.Pre_treeRenderCallbackBooleanTrigger.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BooleanTrigger.Pre_renderCallbackBooleanTrigger != null)
            Pre_BooleanTrigger.Pre_renderCallbackBooleanTrigger.render(this);
    }
    void treeDoWithData() {
        if (Pre_BooleanTrigger.Pre_treeDoWithDataCallbackBooleanTrigger != null) {
            Pre_BooleanTrigger.Pre_treeDoWithDataCallbackBooleanTrigger.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BooleanTrigger.Pre_doWithDataCallbackBooleanTrigger != null)
            Pre_BooleanTrigger.Pre_doWithDataCallbackBooleanTrigger.doWithData(this);
    }
}

class Pre_BooleanTriggerRenderCallback extends RenderCallback {}
class Pre_BooleanTriggerTreeRenderCallback extends TreeRenderCallback {}
class Pre_BooleanTriggerDoWithDataCallback extends DoWithDataCallback {};
class Pre_BooleanTriggerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BooleanTriggerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BooleanSequencer extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    boolean[] keyValue;
    static Pre_BooleanSequencerRenderCallback Pre_renderCallbackBooleanSequencer;
    static void setPre_BooleanSequencerRenderCallback(Pre_BooleanSequencerRenderCallback node) {
        Pre_renderCallbackBooleanSequencer = node;
    }

    static Pre_BooleanSequencerTreeRenderCallback Pre_treeRenderCallbackBooleanSequencer;
    static void setPre_BooleanSequencerTreeRenderCallback(Pre_BooleanSequencerTreeRenderCallback node) {
        Pre_treeRenderCallbackBooleanSequencer = node;
    }

    static Pre_BooleanSequencerDoWithDataCallback Pre_doWithDataCallbackBooleanSequencer;
    static void setPre_BooleanSequencerDoWithDataCallback(Pre_BooleanSequencerDoWithDataCallback node) {
        Pre_doWithDataCallbackBooleanSequencer = node;
    }

    static Pre_BooleanSequencerTreeDoWithDataCallback Pre_treeDoWithDataCallbackBooleanSequencer;
    static void setPre_BooleanSequencerTreeDoWithDataCallback(Pre_BooleanSequencerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBooleanSequencer = node;

    }

    static Pre_BooleanSequencerEventsProcessedCallback Pre_eventsProcessedCallbackBooleanSequencer;
    static void setPre_BooleanSequencerEventsProcessedCallback(Pre_BooleanSequencerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBooleanSequencer = node;
    }

    Pre_BooleanSequencer() {
    }
    void treeRender() {
        if (Pre_BooleanSequencer.Pre_treeRenderCallbackBooleanSequencer != null) {
            Pre_BooleanSequencer.Pre_treeRenderCallbackBooleanSequencer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BooleanSequencer.Pre_renderCallbackBooleanSequencer != null)
            Pre_BooleanSequencer.Pre_renderCallbackBooleanSequencer.render(this);
    }
    void treeDoWithData() {
        if (Pre_BooleanSequencer.Pre_treeDoWithDataCallbackBooleanSequencer != null) {
            Pre_BooleanSequencer.Pre_treeDoWithDataCallbackBooleanSequencer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BooleanSequencer.Pre_doWithDataCallbackBooleanSequencer != null)
            Pre_BooleanSequencer.Pre_doWithDataCallbackBooleanSequencer.doWithData(this);
    }
}

class Pre_BooleanSequencerRenderCallback extends RenderCallback {}
class Pre_BooleanSequencerTreeRenderCallback extends TreeRenderCallback {}
class Pre_BooleanSequencerDoWithDataCallback extends DoWithDataCallback {};
class Pre_BooleanSequencerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BooleanSequencerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Color extends Pre_Node {
    Pre_Node metadata;
    float[] color;
    static Pre_ColorRenderCallback Pre_renderCallbackColor;
    static void setPre_ColorRenderCallback(Pre_ColorRenderCallback node) {
        Pre_renderCallbackColor = node;
    }

    static Pre_ColorTreeRenderCallback Pre_treeRenderCallbackColor;
    static void setPre_ColorTreeRenderCallback(Pre_ColorTreeRenderCallback node) {
        Pre_treeRenderCallbackColor = node;
    }

    static Pre_ColorDoWithDataCallback Pre_doWithDataCallbackColor;
    static void setPre_ColorDoWithDataCallback(Pre_ColorDoWithDataCallback node) {
        Pre_doWithDataCallbackColor = node;
    }

    static Pre_ColorTreeDoWithDataCallback Pre_treeDoWithDataCallbackColor;
    static void setPre_ColorTreeDoWithDataCallback(Pre_ColorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackColor = node;

    }

    static Pre_ColorEventsProcessedCallback Pre_eventsProcessedCallbackColor;
    static void setPre_ColorEventsProcessedCallback(Pre_ColorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackColor = node;
    }

    Pre_Color() {
    }
    void treeRender() {
        if (Pre_Color.Pre_treeRenderCallbackColor != null) {
            Pre_Color.Pre_treeRenderCallbackColor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Color.Pre_renderCallbackColor != null)
            Pre_Color.Pre_renderCallbackColor.render(this);
    }
    void treeDoWithData() {
        if (Pre_Color.Pre_treeDoWithDataCallbackColor != null) {
            Pre_Color.Pre_treeDoWithDataCallbackColor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Color.Pre_doWithDataCallbackColor != null)
            Pre_Color.Pre_doWithDataCallbackColor.doWithData(this);
    }
}

class Pre_ColorRenderCallback extends RenderCallback {}
class Pre_ColorTreeRenderCallback extends TreeRenderCallback {}
class Pre_ColorDoWithDataCallback extends DoWithDataCallback {};
class Pre_ColorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ColorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OrientationChaser extends Pre_Node {
    Pre_Node metadata;
    double duration;
    float[] initialDestination;
    float[] initialValue;
    static Pre_OrientationChaserRenderCallback Pre_renderCallbackOrientationChaser;
    static void setPre_OrientationChaserRenderCallback(Pre_OrientationChaserRenderCallback node) {
        Pre_renderCallbackOrientationChaser = node;
    }

    static Pre_OrientationChaserTreeRenderCallback Pre_treeRenderCallbackOrientationChaser;
    static void setPre_OrientationChaserTreeRenderCallback(Pre_OrientationChaserTreeRenderCallback node) {
        Pre_treeRenderCallbackOrientationChaser = node;
    }

    static Pre_OrientationChaserDoWithDataCallback Pre_doWithDataCallbackOrientationChaser;
    static void setPre_OrientationChaserDoWithDataCallback(Pre_OrientationChaserDoWithDataCallback node) {
        Pre_doWithDataCallbackOrientationChaser = node;
    }

    static Pre_OrientationChaserTreeDoWithDataCallback Pre_treeDoWithDataCallbackOrientationChaser;
    static void setPre_OrientationChaserTreeDoWithDataCallback(Pre_OrientationChaserTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOrientationChaser = node;

    }

    static Pre_OrientationChaserEventsProcessedCallback Pre_eventsProcessedCallbackOrientationChaser;
    static void setPre_OrientationChaserEventsProcessedCallback(Pre_OrientationChaserEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOrientationChaser = node;
    }

    Pre_OrientationChaser() {
    }
    void treeRender() {
        if (Pre_OrientationChaser.Pre_treeRenderCallbackOrientationChaser != null) {
            Pre_OrientationChaser.Pre_treeRenderCallbackOrientationChaser.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OrientationChaser.Pre_renderCallbackOrientationChaser != null)
            Pre_OrientationChaser.Pre_renderCallbackOrientationChaser.render(this);
    }
    void treeDoWithData() {
        if (Pre_OrientationChaser.Pre_treeDoWithDataCallbackOrientationChaser != null) {
            Pre_OrientationChaser.Pre_treeDoWithDataCallbackOrientationChaser.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OrientationChaser.Pre_doWithDataCallbackOrientationChaser != null)
            Pre_OrientationChaser.Pre_doWithDataCallbackOrientationChaser.doWithData(this);
    }
}

class Pre_OrientationChaserRenderCallback extends RenderCallback {}
class Pre_OrientationChaserTreeRenderCallback extends TreeRenderCallback {}
class Pre_OrientationChaserDoWithDataCallback extends DoWithDataCallback {};
class Pre_OrientationChaserTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OrientationChaserEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OrientationDamper extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_OrientationDamperRenderCallback Pre_renderCallbackOrientationDamper;
    static void setPre_OrientationDamperRenderCallback(Pre_OrientationDamperRenderCallback node) {
        Pre_renderCallbackOrientationDamper = node;
    }

    static Pre_OrientationDamperTreeRenderCallback Pre_treeRenderCallbackOrientationDamper;
    static void setPre_OrientationDamperTreeRenderCallback(Pre_OrientationDamperTreeRenderCallback node) {
        Pre_treeRenderCallbackOrientationDamper = node;
    }

    static Pre_OrientationDamperDoWithDataCallback Pre_doWithDataCallbackOrientationDamper;
    static void setPre_OrientationDamperDoWithDataCallback(Pre_OrientationDamperDoWithDataCallback node) {
        Pre_doWithDataCallbackOrientationDamper = node;
    }

    static Pre_OrientationDamperTreeDoWithDataCallback Pre_treeDoWithDataCallbackOrientationDamper;
    static void setPre_OrientationDamperTreeDoWithDataCallback(Pre_OrientationDamperTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOrientationDamper = node;

    }

    static Pre_OrientationDamperEventsProcessedCallback Pre_eventsProcessedCallbackOrientationDamper;
    static void setPre_OrientationDamperEventsProcessedCallback(Pre_OrientationDamperEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOrientationDamper = node;
    }

    Pre_OrientationDamper() {
    }
    void treeRender() {
        if (Pre_OrientationDamper.Pre_treeRenderCallbackOrientationDamper != null) {
            Pre_OrientationDamper.Pre_treeRenderCallbackOrientationDamper.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OrientationDamper.Pre_renderCallbackOrientationDamper != null)
            Pre_OrientationDamper.Pre_renderCallbackOrientationDamper.render(this);
    }
    void treeDoWithData() {
        if (Pre_OrientationDamper.Pre_treeDoWithDataCallbackOrientationDamper != null) {
            Pre_OrientationDamper.Pre_treeDoWithDataCallbackOrientationDamper.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OrientationDamper.Pre_doWithDataCallbackOrientationDamper != null)
            Pre_OrientationDamper.Pre_doWithDataCallbackOrientationDamper.doWithData(this);
    }
}

class Pre_OrientationDamperRenderCallback extends RenderCallback {}
class Pre_OrientationDamperTreeRenderCallback extends TreeRenderCallback {}
class Pre_OrientationDamperDoWithDataCallback extends DoWithDataCallback {};
class Pre_OrientationDamperTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OrientationDamperEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PointPickSensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    String[] objectType;
    Pre_Node pickingGeometry;
    Pre_Node [] pickTarget;
    String intersectionType;
    String sortOrder;
    static Pre_PointPickSensorRenderCallback Pre_renderCallbackPointPickSensor;
    static void setPre_PointPickSensorRenderCallback(Pre_PointPickSensorRenderCallback node) {
        Pre_renderCallbackPointPickSensor = node;
    }

    static Pre_PointPickSensorTreeRenderCallback Pre_treeRenderCallbackPointPickSensor;
    static void setPre_PointPickSensorTreeRenderCallback(Pre_PointPickSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackPointPickSensor = node;
    }

    static Pre_PointPickSensorDoWithDataCallback Pre_doWithDataCallbackPointPickSensor;
    static void setPre_PointPickSensorDoWithDataCallback(Pre_PointPickSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackPointPickSensor = node;
    }

    static Pre_PointPickSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackPointPickSensor;
    static void setPre_PointPickSensorTreeDoWithDataCallback(Pre_PointPickSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPointPickSensor = node;

    }

    static Pre_PointPickSensorEventsProcessedCallback Pre_eventsProcessedCallbackPointPickSensor;
    static void setPre_PointPickSensorEventsProcessedCallback(Pre_PointPickSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPointPickSensor = node;
    }

    Pre_PointPickSensor() {
    }
    void treeRender() {
        if (Pre_PointPickSensor.Pre_treeRenderCallbackPointPickSensor != null) {
            Pre_PointPickSensor.Pre_treeRenderCallbackPointPickSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (pickingGeometry != null)
            pickingGeometry.treeRender();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PointPickSensor.Pre_renderCallbackPointPickSensor != null)
            Pre_PointPickSensor.Pre_renderCallbackPointPickSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_PointPickSensor.Pre_treeDoWithDataCallbackPointPickSensor != null) {
            Pre_PointPickSensor.Pre_treeDoWithDataCallbackPointPickSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (pickingGeometry != null)
            pickingGeometry.treeDoWithData();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PointPickSensor.Pre_doWithDataCallbackPointPickSensor != null)
            Pre_PointPickSensor.Pre_doWithDataCallbackPointPickSensor.doWithData(this);
    }
}

class Pre_PointPickSensorRenderCallback extends RenderCallback {}
class Pre_PointPickSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_PointPickSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_PointPickSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PointPickSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SplinePositionInterpolator extends Pre_Node {
    Pre_Node metadata;
    boolean closed;
    float[] key;
    float[] keyValue;
    float[] keyVelocity;
    boolean normalizeVelocity;
    static Pre_SplinePositionInterpolatorRenderCallback Pre_renderCallbackSplinePositionInterpolator;
    static void setPre_SplinePositionInterpolatorRenderCallback(Pre_SplinePositionInterpolatorRenderCallback node) {
        Pre_renderCallbackSplinePositionInterpolator = node;
    }

    static Pre_SplinePositionInterpolatorTreeRenderCallback Pre_treeRenderCallbackSplinePositionInterpolator;
    static void setPre_SplinePositionInterpolatorTreeRenderCallback(Pre_SplinePositionInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackSplinePositionInterpolator = node;
    }

    static Pre_SplinePositionInterpolatorDoWithDataCallback Pre_doWithDataCallbackSplinePositionInterpolator;
    static void setPre_SplinePositionInterpolatorDoWithDataCallback(Pre_SplinePositionInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackSplinePositionInterpolator = node;
    }

    static Pre_SplinePositionInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackSplinePositionInterpolator;
    static void setPre_SplinePositionInterpolatorTreeDoWithDataCallback(Pre_SplinePositionInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSplinePositionInterpolator = node;

    }

    static Pre_SplinePositionInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackSplinePositionInterpolator;
    static void setPre_SplinePositionInterpolatorEventsProcessedCallback(Pre_SplinePositionInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSplinePositionInterpolator = node;
    }

    Pre_SplinePositionInterpolator() {
    }
    void treeRender() {
        if (Pre_SplinePositionInterpolator.Pre_treeRenderCallbackSplinePositionInterpolator != null) {
            Pre_SplinePositionInterpolator.Pre_treeRenderCallbackSplinePositionInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SplinePositionInterpolator.Pre_renderCallbackSplinePositionInterpolator != null)
            Pre_SplinePositionInterpolator.Pre_renderCallbackSplinePositionInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_SplinePositionInterpolator.Pre_treeDoWithDataCallbackSplinePositionInterpolator != null) {
            Pre_SplinePositionInterpolator.Pre_treeDoWithDataCallbackSplinePositionInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SplinePositionInterpolator.Pre_doWithDataCallbackSplinePositionInterpolator != null)
            Pre_SplinePositionInterpolator.Pre_doWithDataCallbackSplinePositionInterpolator.doWithData(this);
    }
}

class Pre_SplinePositionInterpolatorRenderCallback extends RenderCallback {}
class Pre_SplinePositionInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_SplinePositionInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_SplinePositionInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SplinePositionInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TimeTrigger extends Pre_Node {
    Pre_Node metadata;
    static Pre_TimeTriggerRenderCallback Pre_renderCallbackTimeTrigger;
    static void setPre_TimeTriggerRenderCallback(Pre_TimeTriggerRenderCallback node) {
        Pre_renderCallbackTimeTrigger = node;
    }

    static Pre_TimeTriggerTreeRenderCallback Pre_treeRenderCallbackTimeTrigger;
    static void setPre_TimeTriggerTreeRenderCallback(Pre_TimeTriggerTreeRenderCallback node) {
        Pre_treeRenderCallbackTimeTrigger = node;
    }

    static Pre_TimeTriggerDoWithDataCallback Pre_doWithDataCallbackTimeTrigger;
    static void setPre_TimeTriggerDoWithDataCallback(Pre_TimeTriggerDoWithDataCallback node) {
        Pre_doWithDataCallbackTimeTrigger = node;
    }

    static Pre_TimeTriggerTreeDoWithDataCallback Pre_treeDoWithDataCallbackTimeTrigger;
    static void setPre_TimeTriggerTreeDoWithDataCallback(Pre_TimeTriggerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTimeTrigger = node;

    }

    static Pre_TimeTriggerEventsProcessedCallback Pre_eventsProcessedCallbackTimeTrigger;
    static void setPre_TimeTriggerEventsProcessedCallback(Pre_TimeTriggerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTimeTrigger = node;
    }

    Pre_TimeTrigger() {
    }
    void treeRender() {
        if (Pre_TimeTrigger.Pre_treeRenderCallbackTimeTrigger != null) {
            Pre_TimeTrigger.Pre_treeRenderCallbackTimeTrigger.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TimeTrigger.Pre_renderCallbackTimeTrigger != null)
            Pre_TimeTrigger.Pre_renderCallbackTimeTrigger.render(this);
    }
    void treeDoWithData() {
        if (Pre_TimeTrigger.Pre_treeDoWithDataCallbackTimeTrigger != null) {
            Pre_TimeTrigger.Pre_treeDoWithDataCallbackTimeTrigger.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TimeTrigger.Pre_doWithDataCallbackTimeTrigger != null)
            Pre_TimeTrigger.Pre_doWithDataCallbackTimeTrigger.doWithData(this);
    }
}

class Pre_TimeTriggerRenderCallback extends RenderCallback {}
class Pre_TimeTriggerTreeRenderCallback extends TreeRenderCallback {}
class Pre_TimeTriggerDoWithDataCallback extends DoWithDataCallback {};
class Pre_TimeTriggerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TimeTriggerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Arc2D extends Pre_Node {
    Pre_Node metadata;
    float endAngle;
    float radius;
    float startAngle;
    static Pre_Arc2DRenderCallback Pre_renderCallbackArc2D;
    static void setPre_Arc2DRenderCallback(Pre_Arc2DRenderCallback node) {
        Pre_renderCallbackArc2D = node;
    }

    static Pre_Arc2DTreeRenderCallback Pre_treeRenderCallbackArc2D;
    static void setPre_Arc2DTreeRenderCallback(Pre_Arc2DTreeRenderCallback node) {
        Pre_treeRenderCallbackArc2D = node;
    }

    static Pre_Arc2DDoWithDataCallback Pre_doWithDataCallbackArc2D;
    static void setPre_Arc2DDoWithDataCallback(Pre_Arc2DDoWithDataCallback node) {
        Pre_doWithDataCallbackArc2D = node;
    }

    static Pre_Arc2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackArc2D;
    static void setPre_Arc2DTreeDoWithDataCallback(Pre_Arc2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackArc2D = node;

    }

    static Pre_Arc2DEventsProcessedCallback Pre_eventsProcessedCallbackArc2D;
    static void setPre_Arc2DEventsProcessedCallback(Pre_Arc2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackArc2D = node;
    }

    Pre_Arc2D() {
    }
    void treeRender() {
        if (Pre_Arc2D.Pre_treeRenderCallbackArc2D != null) {
            Pre_Arc2D.Pre_treeRenderCallbackArc2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Arc2D.Pre_renderCallbackArc2D != null)
            Pre_Arc2D.Pre_renderCallbackArc2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Arc2D.Pre_treeDoWithDataCallbackArc2D != null) {
            Pre_Arc2D.Pre_treeDoWithDataCallbackArc2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Arc2D.Pre_doWithDataCallbackArc2D != null)
            Pre_Arc2D.Pre_doWithDataCallbackArc2D.doWithData(this);
    }
}

class Pre_Arc2DRenderCallback extends RenderCallback {}
class Pre_Arc2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Arc2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Arc2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Arc2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Disk2D extends Pre_Node {
    Pre_Node metadata;
    float innerRadius;
    float outerRadius;
    boolean solid;
    static Pre_Disk2DRenderCallback Pre_renderCallbackDisk2D;
    static void setPre_Disk2DRenderCallback(Pre_Disk2DRenderCallback node) {
        Pre_renderCallbackDisk2D = node;
    }

    static Pre_Disk2DTreeRenderCallback Pre_treeRenderCallbackDisk2D;
    static void setPre_Disk2DTreeRenderCallback(Pre_Disk2DTreeRenderCallback node) {
        Pre_treeRenderCallbackDisk2D = node;
    }

    static Pre_Disk2DDoWithDataCallback Pre_doWithDataCallbackDisk2D;
    static void setPre_Disk2DDoWithDataCallback(Pre_Disk2DDoWithDataCallback node) {
        Pre_doWithDataCallbackDisk2D = node;
    }

    static Pre_Disk2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackDisk2D;
    static void setPre_Disk2DTreeDoWithDataCallback(Pre_Disk2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackDisk2D = node;

    }

    static Pre_Disk2DEventsProcessedCallback Pre_eventsProcessedCallbackDisk2D;
    static void setPre_Disk2DEventsProcessedCallback(Pre_Disk2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackDisk2D = node;
    }

    Pre_Disk2D() {
    }
    void treeRender() {
        if (Pre_Disk2D.Pre_treeRenderCallbackDisk2D != null) {
            Pre_Disk2D.Pre_treeRenderCallbackDisk2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Disk2D.Pre_renderCallbackDisk2D != null)
            Pre_Disk2D.Pre_renderCallbackDisk2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Disk2D.Pre_treeDoWithDataCallbackDisk2D != null) {
            Pre_Disk2D.Pre_treeDoWithDataCallbackDisk2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Disk2D.Pre_doWithDataCallbackDisk2D != null)
            Pre_Disk2D.Pre_doWithDataCallbackDisk2D.doWithData(this);
    }
}

class Pre_Disk2DRenderCallback extends RenderCallback {}
class Pre_Disk2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Disk2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Disk2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Disk2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoElevationGrid extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    Pre_Node color;
    Pre_Node normal;
    Pre_Node texCoord;
    float yScale;
    boolean ccw;
    boolean colorPerVertex;
    double creaseAngle;
    double[] geoGridOrigin;
    double[] height;
    boolean normalPerVertex;
    boolean solid;
    int xDimension;
    double xSpacing;
    int zDimension;
    double zSpacing;
    static Pre_GeoElevationGridRenderCallback Pre_renderCallbackGeoElevationGrid;
    static void setPre_GeoElevationGridRenderCallback(Pre_GeoElevationGridRenderCallback node) {
        Pre_renderCallbackGeoElevationGrid = node;
    }

    static Pre_GeoElevationGridTreeRenderCallback Pre_treeRenderCallbackGeoElevationGrid;
    static void setPre_GeoElevationGridTreeRenderCallback(Pre_GeoElevationGridTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoElevationGrid = node;
    }

    static Pre_GeoElevationGridDoWithDataCallback Pre_doWithDataCallbackGeoElevationGrid;
    static void setPre_GeoElevationGridDoWithDataCallback(Pre_GeoElevationGridDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoElevationGrid = node;
    }

    static Pre_GeoElevationGridTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoElevationGrid;
    static void setPre_GeoElevationGridTreeDoWithDataCallback(Pre_GeoElevationGridTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoElevationGrid = node;

    }

    static Pre_GeoElevationGridEventsProcessedCallback Pre_eventsProcessedCallbackGeoElevationGrid;
    static void setPre_GeoElevationGridEventsProcessedCallback(Pre_GeoElevationGridEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoElevationGrid = node;
    }

    Pre_GeoElevationGrid() {
    }
    void treeRender() {
        if (Pre_GeoElevationGrid.Pre_treeRenderCallbackGeoElevationGrid != null) {
            Pre_GeoElevationGrid.Pre_treeRenderCallbackGeoElevationGrid.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (color != null)
            color.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoElevationGrid.Pre_renderCallbackGeoElevationGrid != null)
            Pre_GeoElevationGrid.Pre_renderCallbackGeoElevationGrid.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoElevationGrid.Pre_treeDoWithDataCallbackGeoElevationGrid != null) {
            Pre_GeoElevationGrid.Pre_treeDoWithDataCallbackGeoElevationGrid.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoElevationGrid.Pre_doWithDataCallbackGeoElevationGrid != null)
            Pre_GeoElevationGrid.Pre_doWithDataCallbackGeoElevationGrid.doWithData(this);
    }
}

class Pre_GeoElevationGridRenderCallback extends RenderCallback {}
class Pre_GeoElevationGridTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoElevationGridDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoElevationGridTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoElevationGridEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IndexedTriangleSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean normalPerVertex;
    boolean solid;
    int[] index;
    boolean colorPerVertex;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_IndexedTriangleSetRenderCallback Pre_renderCallbackIndexedTriangleSet;
    static void setPre_IndexedTriangleSetRenderCallback(Pre_IndexedTriangleSetRenderCallback node) {
        Pre_renderCallbackIndexedTriangleSet = node;
    }

    static Pre_IndexedTriangleSetTreeRenderCallback Pre_treeRenderCallbackIndexedTriangleSet;
    static void setPre_IndexedTriangleSetTreeRenderCallback(Pre_IndexedTriangleSetTreeRenderCallback node) {
        Pre_treeRenderCallbackIndexedTriangleSet = node;
    }

    static Pre_IndexedTriangleSetDoWithDataCallback Pre_doWithDataCallbackIndexedTriangleSet;
    static void setPre_IndexedTriangleSetDoWithDataCallback(Pre_IndexedTriangleSetDoWithDataCallback node) {
        Pre_doWithDataCallbackIndexedTriangleSet = node;
    }

    static Pre_IndexedTriangleSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackIndexedTriangleSet;
    static void setPre_IndexedTriangleSetTreeDoWithDataCallback(Pre_IndexedTriangleSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIndexedTriangleSet = node;

    }

    static Pre_IndexedTriangleSetEventsProcessedCallback Pre_eventsProcessedCallbackIndexedTriangleSet;
    static void setPre_IndexedTriangleSetEventsProcessedCallback(Pre_IndexedTriangleSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIndexedTriangleSet = node;
    }

    Pre_IndexedTriangleSet() {
    }
    void treeRender() {
        if (Pre_IndexedTriangleSet.Pre_treeRenderCallbackIndexedTriangleSet != null) {
            Pre_IndexedTriangleSet.Pre_treeRenderCallbackIndexedTriangleSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IndexedTriangleSet.Pre_renderCallbackIndexedTriangleSet != null)
            Pre_IndexedTriangleSet.Pre_renderCallbackIndexedTriangleSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_IndexedTriangleSet.Pre_treeDoWithDataCallbackIndexedTriangleSet != null) {
            Pre_IndexedTriangleSet.Pre_treeDoWithDataCallbackIndexedTriangleSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IndexedTriangleSet.Pre_doWithDataCallbackIndexedTriangleSet != null)
            Pre_IndexedTriangleSet.Pre_doWithDataCallbackIndexedTriangleSet.doWithData(this);
    }
}

class Pre_IndexedTriangleSetRenderCallback extends RenderCallback {}
class Pre_IndexedTriangleSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_IndexedTriangleSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_IndexedTriangleSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IndexedTriangleSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Polypoint2D extends Pre_Node {
    Pre_Node metadata;
    float[] point;
    static Pre_Polypoint2DRenderCallback Pre_renderCallbackPolypoint2D;
    static void setPre_Polypoint2DRenderCallback(Pre_Polypoint2DRenderCallback node) {
        Pre_renderCallbackPolypoint2D = node;
    }

    static Pre_Polypoint2DTreeRenderCallback Pre_treeRenderCallbackPolypoint2D;
    static void setPre_Polypoint2DTreeRenderCallback(Pre_Polypoint2DTreeRenderCallback node) {
        Pre_treeRenderCallbackPolypoint2D = node;
    }

    static Pre_Polypoint2DDoWithDataCallback Pre_doWithDataCallbackPolypoint2D;
    static void setPre_Polypoint2DDoWithDataCallback(Pre_Polypoint2DDoWithDataCallback node) {
        Pre_doWithDataCallbackPolypoint2D = node;
    }

    static Pre_Polypoint2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPolypoint2D;
    static void setPre_Polypoint2DTreeDoWithDataCallback(Pre_Polypoint2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPolypoint2D = node;

    }

    static Pre_Polypoint2DEventsProcessedCallback Pre_eventsProcessedCallbackPolypoint2D;
    static void setPre_Polypoint2DEventsProcessedCallback(Pre_Polypoint2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPolypoint2D = node;
    }

    Pre_Polypoint2D() {
    }
    void treeRender() {
        if (Pre_Polypoint2D.Pre_treeRenderCallbackPolypoint2D != null) {
            Pre_Polypoint2D.Pre_treeRenderCallbackPolypoint2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Polypoint2D.Pre_renderCallbackPolypoint2D != null)
            Pre_Polypoint2D.Pre_renderCallbackPolypoint2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Polypoint2D.Pre_treeDoWithDataCallbackPolypoint2D != null) {
            Pre_Polypoint2D.Pre_treeDoWithDataCallbackPolypoint2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Polypoint2D.Pre_doWithDataCallbackPolypoint2D != null)
            Pre_Polypoint2D.Pre_doWithDataCallbackPolypoint2D.doWithData(this);
    }
}

class Pre_Polypoint2DRenderCallback extends RenderCallback {}
class Pre_Polypoint2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Polypoint2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Polypoint2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Polypoint2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionDamper2D extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_PositionDamper2DRenderCallback Pre_renderCallbackPositionDamper2D;
    static void setPre_PositionDamper2DRenderCallback(Pre_PositionDamper2DRenderCallback node) {
        Pre_renderCallbackPositionDamper2D = node;
    }

    static Pre_PositionDamper2DTreeRenderCallback Pre_treeRenderCallbackPositionDamper2D;
    static void setPre_PositionDamper2DTreeRenderCallback(Pre_PositionDamper2DTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionDamper2D = node;
    }

    static Pre_PositionDamper2DDoWithDataCallback Pre_doWithDataCallbackPositionDamper2D;
    static void setPre_PositionDamper2DDoWithDataCallback(Pre_PositionDamper2DDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionDamper2D = node;
    }

    static Pre_PositionDamper2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionDamper2D;
    static void setPre_PositionDamper2DTreeDoWithDataCallback(Pre_PositionDamper2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionDamper2D = node;

    }

    static Pre_PositionDamper2DEventsProcessedCallback Pre_eventsProcessedCallbackPositionDamper2D;
    static void setPre_PositionDamper2DEventsProcessedCallback(Pre_PositionDamper2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionDamper2D = node;
    }

    Pre_PositionDamper2D() {
    }
    void treeRender() {
        if (Pre_PositionDamper2D.Pre_treeRenderCallbackPositionDamper2D != null) {
            Pre_PositionDamper2D.Pre_treeRenderCallbackPositionDamper2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionDamper2D.Pre_renderCallbackPositionDamper2D != null)
            Pre_PositionDamper2D.Pre_renderCallbackPositionDamper2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionDamper2D.Pre_treeDoWithDataCallbackPositionDamper2D != null) {
            Pre_PositionDamper2D.Pre_treeDoWithDataCallbackPositionDamper2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionDamper2D.Pre_doWithDataCallbackPositionDamper2D != null)
            Pre_PositionDamper2D.Pre_doWithDataCallbackPositionDamper2D.doWithData(this);
    }
}

class Pre_PositionDamper2DRenderCallback extends RenderCallback {}
class Pre_PositionDamper2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionDamper2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionDamper2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionDamper2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SuperEllipsoid extends Pre_Node {
    Pre_Node metadata;
    float n1;
    float n2;
    float border;
    boolean bottom;
    float bottomBorder;
    boolean ccw;
    float creaseAngle;
    float[] size;
    boolean solid;
    Pre_Node texCoord;
    boolean top;
    int uTessellation;
    int vTessellation;
    static Pre_SuperEllipsoidRenderCallback Pre_renderCallbackSuperEllipsoid;
    static void setPre_SuperEllipsoidRenderCallback(Pre_SuperEllipsoidRenderCallback node) {
        Pre_renderCallbackSuperEllipsoid = node;
    }

    static Pre_SuperEllipsoidTreeRenderCallback Pre_treeRenderCallbackSuperEllipsoid;
    static void setPre_SuperEllipsoidTreeRenderCallback(Pre_SuperEllipsoidTreeRenderCallback node) {
        Pre_treeRenderCallbackSuperEllipsoid = node;
    }

    static Pre_SuperEllipsoidDoWithDataCallback Pre_doWithDataCallbackSuperEllipsoid;
    static void setPre_SuperEllipsoidDoWithDataCallback(Pre_SuperEllipsoidDoWithDataCallback node) {
        Pre_doWithDataCallbackSuperEllipsoid = node;
    }

    static Pre_SuperEllipsoidTreeDoWithDataCallback Pre_treeDoWithDataCallbackSuperEllipsoid;
    static void setPre_SuperEllipsoidTreeDoWithDataCallback(Pre_SuperEllipsoidTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSuperEllipsoid = node;

    }

    static Pre_SuperEllipsoidEventsProcessedCallback Pre_eventsProcessedCallbackSuperEllipsoid;
    static void setPre_SuperEllipsoidEventsProcessedCallback(Pre_SuperEllipsoidEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSuperEllipsoid = node;
    }

    Pre_SuperEllipsoid() {
    }
    void treeRender() {
        if (Pre_SuperEllipsoid.Pre_treeRenderCallbackSuperEllipsoid != null) {
            Pre_SuperEllipsoid.Pre_treeRenderCallbackSuperEllipsoid.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SuperEllipsoid.Pre_renderCallbackSuperEllipsoid != null)
            Pre_SuperEllipsoid.Pre_renderCallbackSuperEllipsoid.render(this);
    }
    void treeDoWithData() {
        if (Pre_SuperEllipsoid.Pre_treeDoWithDataCallbackSuperEllipsoid != null) {
            Pre_SuperEllipsoid.Pre_treeDoWithDataCallbackSuperEllipsoid.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SuperEllipsoid.Pre_doWithDataCallbackSuperEllipsoid != null)
            Pre_SuperEllipsoid.Pre_doWithDataCallbackSuperEllipsoid.doWithData(this);
    }
}

class Pre_SuperEllipsoidRenderCallback extends RenderCallback {}
class Pre_SuperEllipsoidTreeRenderCallback extends TreeRenderCallback {}
class Pre_SuperEllipsoidDoWithDataCallback extends DoWithDataCallback {};
class Pre_SuperEllipsoidTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SuperEllipsoidEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BooleanToggle extends Pre_Node {
    Pre_Node metadata;
    boolean toggle;
    static Pre_BooleanToggleRenderCallback Pre_renderCallbackBooleanToggle;
    static void setPre_BooleanToggleRenderCallback(Pre_BooleanToggleRenderCallback node) {
        Pre_renderCallbackBooleanToggle = node;
    }

    static Pre_BooleanToggleTreeRenderCallback Pre_treeRenderCallbackBooleanToggle;
    static void setPre_BooleanToggleTreeRenderCallback(Pre_BooleanToggleTreeRenderCallback node) {
        Pre_treeRenderCallbackBooleanToggle = node;
    }

    static Pre_BooleanToggleDoWithDataCallback Pre_doWithDataCallbackBooleanToggle;
    static void setPre_BooleanToggleDoWithDataCallback(Pre_BooleanToggleDoWithDataCallback node) {
        Pre_doWithDataCallbackBooleanToggle = node;
    }

    static Pre_BooleanToggleTreeDoWithDataCallback Pre_treeDoWithDataCallbackBooleanToggle;
    static void setPre_BooleanToggleTreeDoWithDataCallback(Pre_BooleanToggleTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBooleanToggle = node;

    }

    static Pre_BooleanToggleEventsProcessedCallback Pre_eventsProcessedCallbackBooleanToggle;
    static void setPre_BooleanToggleEventsProcessedCallback(Pre_BooleanToggleEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBooleanToggle = node;
    }

    Pre_BooleanToggle() {
    }
    void treeRender() {
        if (Pre_BooleanToggle.Pre_treeRenderCallbackBooleanToggle != null) {
            Pre_BooleanToggle.Pre_treeRenderCallbackBooleanToggle.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BooleanToggle.Pre_renderCallbackBooleanToggle != null)
            Pre_BooleanToggle.Pre_renderCallbackBooleanToggle.render(this);
    }
    void treeDoWithData() {
        if (Pre_BooleanToggle.Pre_treeDoWithDataCallbackBooleanToggle != null) {
            Pre_BooleanToggle.Pre_treeDoWithDataCallbackBooleanToggle.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BooleanToggle.Pre_doWithDataCallbackBooleanToggle != null)
            Pre_BooleanToggle.Pre_doWithDataCallbackBooleanToggle.doWithData(this);
    }
}

class Pre_BooleanToggleRenderCallback extends RenderCallback {}
class Pre_BooleanToggleTreeRenderCallback extends TreeRenderCallback {}
class Pre_BooleanToggleDoWithDataCallback extends DoWithDataCallback {};
class Pre_BooleanToggleTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BooleanToggleEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Matrix3VertexAttribute extends Pre_Node {
    Pre_Node metadata;
    float[] value;
    String name;
    static Pre_Matrix3VertexAttributeRenderCallback Pre_renderCallbackMatrix3VertexAttribute;
    static void setPre_Matrix3VertexAttributeRenderCallback(Pre_Matrix3VertexAttributeRenderCallback node) {
        Pre_renderCallbackMatrix3VertexAttribute = node;
    }

    static Pre_Matrix3VertexAttributeTreeRenderCallback Pre_treeRenderCallbackMatrix3VertexAttribute;
    static void setPre_Matrix3VertexAttributeTreeRenderCallback(Pre_Matrix3VertexAttributeTreeRenderCallback node) {
        Pre_treeRenderCallbackMatrix3VertexAttribute = node;
    }

    static Pre_Matrix3VertexAttributeDoWithDataCallback Pre_doWithDataCallbackMatrix3VertexAttribute;
    static void setPre_Matrix3VertexAttributeDoWithDataCallback(Pre_Matrix3VertexAttributeDoWithDataCallback node) {
        Pre_doWithDataCallbackMatrix3VertexAttribute = node;
    }

    static Pre_Matrix3VertexAttributeTreeDoWithDataCallback Pre_treeDoWithDataCallbackMatrix3VertexAttribute;
    static void setPre_Matrix3VertexAttributeTreeDoWithDataCallback(Pre_Matrix3VertexAttributeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMatrix3VertexAttribute = node;

    }

    static Pre_Matrix3VertexAttributeEventsProcessedCallback Pre_eventsProcessedCallbackMatrix3VertexAttribute;
    static void setPre_Matrix3VertexAttributeEventsProcessedCallback(Pre_Matrix3VertexAttributeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMatrix3VertexAttribute = node;
    }

    Pre_Matrix3VertexAttribute() {
    }
    void treeRender() {
        if (Pre_Matrix3VertexAttribute.Pre_treeRenderCallbackMatrix3VertexAttribute != null) {
            Pre_Matrix3VertexAttribute.Pre_treeRenderCallbackMatrix3VertexAttribute.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Matrix3VertexAttribute.Pre_renderCallbackMatrix3VertexAttribute != null)
            Pre_Matrix3VertexAttribute.Pre_renderCallbackMatrix3VertexAttribute.render(this);
    }
    void treeDoWithData() {
        if (Pre_Matrix3VertexAttribute.Pre_treeDoWithDataCallbackMatrix3VertexAttribute != null) {
            Pre_Matrix3VertexAttribute.Pre_treeDoWithDataCallbackMatrix3VertexAttribute.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Matrix3VertexAttribute.Pre_doWithDataCallbackMatrix3VertexAttribute != null)
            Pre_Matrix3VertexAttribute.Pre_doWithDataCallbackMatrix3VertexAttribute.doWithData(this);
    }
}

class Pre_Matrix3VertexAttributeRenderCallback extends RenderCallback {}
class Pre_Matrix3VertexAttributeTreeRenderCallback extends TreeRenderCallback {}
class Pre_Matrix3VertexAttributeDoWithDataCallback extends DoWithDataCallback {};
class Pre_Matrix3VertexAttributeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Matrix3VertexAttributeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Matrix4VertexAttribute extends Pre_Node {
    Pre_Node metadata;
    float[] value;
    String name;
    static Pre_Matrix4VertexAttributeRenderCallback Pre_renderCallbackMatrix4VertexAttribute;
    static void setPre_Matrix4VertexAttributeRenderCallback(Pre_Matrix4VertexAttributeRenderCallback node) {
        Pre_renderCallbackMatrix4VertexAttribute = node;
    }

    static Pre_Matrix4VertexAttributeTreeRenderCallback Pre_treeRenderCallbackMatrix4VertexAttribute;
    static void setPre_Matrix4VertexAttributeTreeRenderCallback(Pre_Matrix4VertexAttributeTreeRenderCallback node) {
        Pre_treeRenderCallbackMatrix4VertexAttribute = node;
    }

    static Pre_Matrix4VertexAttributeDoWithDataCallback Pre_doWithDataCallbackMatrix4VertexAttribute;
    static void setPre_Matrix4VertexAttributeDoWithDataCallback(Pre_Matrix4VertexAttributeDoWithDataCallback node) {
        Pre_doWithDataCallbackMatrix4VertexAttribute = node;
    }

    static Pre_Matrix4VertexAttributeTreeDoWithDataCallback Pre_treeDoWithDataCallbackMatrix4VertexAttribute;
    static void setPre_Matrix4VertexAttributeTreeDoWithDataCallback(Pre_Matrix4VertexAttributeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMatrix4VertexAttribute = node;

    }

    static Pre_Matrix4VertexAttributeEventsProcessedCallback Pre_eventsProcessedCallbackMatrix4VertexAttribute;
    static void setPre_Matrix4VertexAttributeEventsProcessedCallback(Pre_Matrix4VertexAttributeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMatrix4VertexAttribute = node;
    }

    Pre_Matrix4VertexAttribute() {
    }
    void treeRender() {
        if (Pre_Matrix4VertexAttribute.Pre_treeRenderCallbackMatrix4VertexAttribute != null) {
            Pre_Matrix4VertexAttribute.Pre_treeRenderCallbackMatrix4VertexAttribute.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Matrix4VertexAttribute.Pre_renderCallbackMatrix4VertexAttribute != null)
            Pre_Matrix4VertexAttribute.Pre_renderCallbackMatrix4VertexAttribute.render(this);
    }
    void treeDoWithData() {
        if (Pre_Matrix4VertexAttribute.Pre_treeDoWithDataCallbackMatrix4VertexAttribute != null) {
            Pre_Matrix4VertexAttribute.Pre_treeDoWithDataCallbackMatrix4VertexAttribute.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Matrix4VertexAttribute.Pre_doWithDataCallbackMatrix4VertexAttribute != null)
            Pre_Matrix4VertexAttribute.Pre_doWithDataCallbackMatrix4VertexAttribute.doWithData(this);
    }
}

class Pre_Matrix4VertexAttributeRenderCallback extends RenderCallback {}
class Pre_Matrix4VertexAttributeTreeRenderCallback extends TreeRenderCallback {}
class Pre_Matrix4VertexAttributeDoWithDataCallback extends DoWithDataCallback {};
class Pre_Matrix4VertexAttributeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Matrix4VertexAttributeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MultiTexture extends Pre_Node {
    Pre_Node metadata;
    float alpha;
    float[] color;
    String[] function;
    String[] mode;
    String[] source;
    Pre_Node [] texture;
    static Pre_MultiTextureRenderCallback Pre_renderCallbackMultiTexture;
    static void setPre_MultiTextureRenderCallback(Pre_MultiTextureRenderCallback node) {
        Pre_renderCallbackMultiTexture = node;
    }

    static Pre_MultiTextureTreeRenderCallback Pre_treeRenderCallbackMultiTexture;
    static void setPre_MultiTextureTreeRenderCallback(Pre_MultiTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackMultiTexture = node;
    }

    static Pre_MultiTextureDoWithDataCallback Pre_doWithDataCallbackMultiTexture;
    static void setPre_MultiTextureDoWithDataCallback(Pre_MultiTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackMultiTexture = node;
    }

    static Pre_MultiTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackMultiTexture;
    static void setPre_MultiTextureTreeDoWithDataCallback(Pre_MultiTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMultiTexture = node;

    }

    static Pre_MultiTextureEventsProcessedCallback Pre_eventsProcessedCallbackMultiTexture;
    static void setPre_MultiTextureEventsProcessedCallback(Pre_MultiTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMultiTexture = node;
    }

    Pre_MultiTexture() {
    }
    void treeRender() {
        if (Pre_MultiTexture.Pre_treeRenderCallbackMultiTexture != null) {
            Pre_MultiTexture.Pre_treeRenderCallbackMultiTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texture != null)
            for (int i = 0; i < texture.length; i++)
                if (texture[i] != null)
                    texture[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MultiTexture.Pre_renderCallbackMultiTexture != null)
            Pre_MultiTexture.Pre_renderCallbackMultiTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_MultiTexture.Pre_treeDoWithDataCallbackMultiTexture != null) {
            Pre_MultiTexture.Pre_treeDoWithDataCallbackMultiTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texture != null)
            for (int i = 0; i < texture.length; i++)
                if (texture[i] != null)
                    texture[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MultiTexture.Pre_doWithDataCallbackMultiTexture != null)
            Pre_MultiTexture.Pre_doWithDataCallbackMultiTexture.doWithData(this);
    }
}

class Pre_MultiTextureRenderCallback extends RenderCallback {}
class Pre_MultiTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_MultiTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_MultiTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MultiTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Shape extends Pre_Node {
    Pre_Node metadata;
    Pre_Node appearance;
    Pre_Node geometry;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node octreeTriangles;
    static Pre_ShapeRenderCallback Pre_renderCallbackShape;
    static void setPre_ShapeRenderCallback(Pre_ShapeRenderCallback node) {
        Pre_renderCallbackShape = node;
    }

    static Pre_ShapeTreeRenderCallback Pre_treeRenderCallbackShape;
    static void setPre_ShapeTreeRenderCallback(Pre_ShapeTreeRenderCallback node) {
        Pre_treeRenderCallbackShape = node;
    }

    static Pre_ShapeDoWithDataCallback Pre_doWithDataCallbackShape;
    static void setPre_ShapeDoWithDataCallback(Pre_ShapeDoWithDataCallback node) {
        Pre_doWithDataCallbackShape = node;
    }

    static Pre_ShapeTreeDoWithDataCallback Pre_treeDoWithDataCallbackShape;
    static void setPre_ShapeTreeDoWithDataCallback(Pre_ShapeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackShape = node;

    }

    static Pre_ShapeEventsProcessedCallback Pre_eventsProcessedCallbackShape;
    static void setPre_ShapeEventsProcessedCallback(Pre_ShapeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackShape = node;
    }

    Pre_Shape() {
    }
    void treeRender() {
        if (Pre_Shape.Pre_treeRenderCallbackShape != null) {
            Pre_Shape.Pre_treeRenderCallbackShape.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (appearance != null)
            appearance.treeRender();
        if (geometry != null)
            geometry.treeRender();
        if (octreeTriangles != null)
            octreeTriangles.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Shape.Pre_renderCallbackShape != null)
            Pre_Shape.Pre_renderCallbackShape.render(this);
    }
    void treeDoWithData() {
        if (Pre_Shape.Pre_treeDoWithDataCallbackShape != null) {
            Pre_Shape.Pre_treeDoWithDataCallbackShape.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (appearance != null)
            appearance.treeDoWithData();
        if (geometry != null)
            geometry.treeDoWithData();
        if (octreeTriangles != null)
            octreeTriangles.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Shape.Pre_doWithDataCallbackShape != null)
            Pre_Shape.Pre_doWithDataCallbackShape.doWithData(this);
    }
}

class Pre_ShapeRenderCallback extends RenderCallback {}
class Pre_ShapeTreeRenderCallback extends TreeRenderCallback {}
class Pre_ShapeDoWithDataCallback extends DoWithDataCallback {};
class Pre_ShapeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ShapeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_DISEntityTypeMapping extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    int category;
    int country;
    int domain;
    int extra;
    int kind;
    int specific;
    int subcategory;
    static Pre_DISEntityTypeMappingRenderCallback Pre_renderCallbackDISEntityTypeMapping;
    static void setPre_DISEntityTypeMappingRenderCallback(Pre_DISEntityTypeMappingRenderCallback node) {
        Pre_renderCallbackDISEntityTypeMapping = node;
    }

    static Pre_DISEntityTypeMappingTreeRenderCallback Pre_treeRenderCallbackDISEntityTypeMapping;
    static void setPre_DISEntityTypeMappingTreeRenderCallback(Pre_DISEntityTypeMappingTreeRenderCallback node) {
        Pre_treeRenderCallbackDISEntityTypeMapping = node;
    }

    static Pre_DISEntityTypeMappingDoWithDataCallback Pre_doWithDataCallbackDISEntityTypeMapping;
    static void setPre_DISEntityTypeMappingDoWithDataCallback(Pre_DISEntityTypeMappingDoWithDataCallback node) {
        Pre_doWithDataCallbackDISEntityTypeMapping = node;
    }

    static Pre_DISEntityTypeMappingTreeDoWithDataCallback Pre_treeDoWithDataCallbackDISEntityTypeMapping;
    static void setPre_DISEntityTypeMappingTreeDoWithDataCallback(Pre_DISEntityTypeMappingTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackDISEntityTypeMapping = node;

    }

    static Pre_DISEntityTypeMappingEventsProcessedCallback Pre_eventsProcessedCallbackDISEntityTypeMapping;
    static void setPre_DISEntityTypeMappingEventsProcessedCallback(Pre_DISEntityTypeMappingEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackDISEntityTypeMapping = node;
    }

    Pre_DISEntityTypeMapping() {
    }
    void treeRender() {
        if (Pre_DISEntityTypeMapping.Pre_treeRenderCallbackDISEntityTypeMapping != null) {
            Pre_DISEntityTypeMapping.Pre_treeRenderCallbackDISEntityTypeMapping.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_DISEntityTypeMapping.Pre_renderCallbackDISEntityTypeMapping != null)
            Pre_DISEntityTypeMapping.Pre_renderCallbackDISEntityTypeMapping.render(this);
    }
    void treeDoWithData() {
        if (Pre_DISEntityTypeMapping.Pre_treeDoWithDataCallbackDISEntityTypeMapping != null) {
            Pre_DISEntityTypeMapping.Pre_treeDoWithDataCallbackDISEntityTypeMapping.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_DISEntityTypeMapping.Pre_doWithDataCallbackDISEntityTypeMapping != null)
            Pre_DISEntityTypeMapping.Pre_doWithDataCallbackDISEntityTypeMapping.doWithData(this);
    }
}

class Pre_DISEntityTypeMappingRenderCallback extends RenderCallback {}
class Pre_DISEntityTypeMappingTreeRenderCallback extends TreeRenderCallback {}
class Pre_DISEntityTypeMappingDoWithDataCallback extends DoWithDataCallback {};
class Pre_DISEntityTypeMappingTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_DISEntityTypeMappingEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LocalFog extends Pre_Node {
    Pre_Node metadata;
    float[] color;
    boolean enabled;
    String fogType;
    float visibilityRange;
    static Pre_LocalFogRenderCallback Pre_renderCallbackLocalFog;
    static void setPre_LocalFogRenderCallback(Pre_LocalFogRenderCallback node) {
        Pre_renderCallbackLocalFog = node;
    }

    static Pre_LocalFogTreeRenderCallback Pre_treeRenderCallbackLocalFog;
    static void setPre_LocalFogTreeRenderCallback(Pre_LocalFogTreeRenderCallback node) {
        Pre_treeRenderCallbackLocalFog = node;
    }

    static Pre_LocalFogDoWithDataCallback Pre_doWithDataCallbackLocalFog;
    static void setPre_LocalFogDoWithDataCallback(Pre_LocalFogDoWithDataCallback node) {
        Pre_doWithDataCallbackLocalFog = node;
    }

    static Pre_LocalFogTreeDoWithDataCallback Pre_treeDoWithDataCallbackLocalFog;
    static void setPre_LocalFogTreeDoWithDataCallback(Pre_LocalFogTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLocalFog = node;

    }

    static Pre_LocalFogEventsProcessedCallback Pre_eventsProcessedCallbackLocalFog;
    static void setPre_LocalFogEventsProcessedCallback(Pre_LocalFogEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLocalFog = node;
    }

    Pre_LocalFog() {
    }
    void treeRender() {
        if (Pre_LocalFog.Pre_treeRenderCallbackLocalFog != null) {
            Pre_LocalFog.Pre_treeRenderCallbackLocalFog.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LocalFog.Pre_renderCallbackLocalFog != null)
            Pre_LocalFog.Pre_renderCallbackLocalFog.render(this);
    }
    void treeDoWithData() {
        if (Pre_LocalFog.Pre_treeDoWithDataCallbackLocalFog != null) {
            Pre_LocalFog.Pre_treeDoWithDataCallbackLocalFog.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LocalFog.Pre_doWithDataCallbackLocalFog != null)
            Pre_LocalFog.Pre_doWithDataCallbackLocalFog.doWithData(this);
    }
}

class Pre_LocalFogRenderCallback extends RenderCallback {}
class Pre_LocalFogTreeRenderCallback extends TreeRenderCallback {}
class Pre_LocalFogDoWithDataCallback extends DoWithDataCallback {};
class Pre_LocalFogTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LocalFogEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Box extends Pre_Node {
    Pre_Node metadata;
    float[] size;
    boolean solid;
    Pre_Node texCoord;
    static Pre_BoxRenderCallback Pre_renderCallbackBox;
    static void setPre_BoxRenderCallback(Pre_BoxRenderCallback node) {
        Pre_renderCallbackBox = node;
    }

    static Pre_BoxTreeRenderCallback Pre_treeRenderCallbackBox;
    static void setPre_BoxTreeRenderCallback(Pre_BoxTreeRenderCallback node) {
        Pre_treeRenderCallbackBox = node;
    }

    static Pre_BoxDoWithDataCallback Pre_doWithDataCallbackBox;
    static void setPre_BoxDoWithDataCallback(Pre_BoxDoWithDataCallback node) {
        Pre_doWithDataCallbackBox = node;
    }

    static Pre_BoxTreeDoWithDataCallback Pre_treeDoWithDataCallbackBox;
    static void setPre_BoxTreeDoWithDataCallback(Pre_BoxTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBox = node;

    }

    static Pre_BoxEventsProcessedCallback Pre_eventsProcessedCallbackBox;
    static void setPre_BoxEventsProcessedCallback(Pre_BoxEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBox = node;
    }

    Pre_Box() {
    }
    void treeRender() {
        if (Pre_Box.Pre_treeRenderCallbackBox != null) {
            Pre_Box.Pre_treeRenderCallbackBox.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Box.Pre_renderCallbackBox != null)
            Pre_Box.Pre_renderCallbackBox.render(this);
    }
    void treeDoWithData() {
        if (Pre_Box.Pre_treeDoWithDataCallbackBox != null) {
            Pre_Box.Pre_treeDoWithDataCallbackBox.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Box.Pre_doWithDataCallbackBox != null)
            Pre_Box.Pre_doWithDataCallbackBox.doWithData(this);
    }
}

class Pre_BoxRenderCallback extends RenderCallback {}
class Pre_BoxTreeRenderCallback extends TreeRenderCallback {}
class Pre_BoxDoWithDataCallback extends DoWithDataCallback {};
class Pre_BoxTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BoxEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoLocation extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    Pre_Node [] children;
    double[] geoCoords;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_GeoLocationRenderCallback Pre_renderCallbackGeoLocation;
    static void setPre_GeoLocationRenderCallback(Pre_GeoLocationRenderCallback node) {
        Pre_renderCallbackGeoLocation = node;
    }

    static Pre_GeoLocationTreeRenderCallback Pre_treeRenderCallbackGeoLocation;
    static void setPre_GeoLocationTreeRenderCallback(Pre_GeoLocationTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoLocation = node;
    }

    static Pre_GeoLocationDoWithDataCallback Pre_doWithDataCallbackGeoLocation;
    static void setPre_GeoLocationDoWithDataCallback(Pre_GeoLocationDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoLocation = node;
    }

    static Pre_GeoLocationTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoLocation;
    static void setPre_GeoLocationTreeDoWithDataCallback(Pre_GeoLocationTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoLocation = node;

    }

    static Pre_GeoLocationEventsProcessedCallback Pre_eventsProcessedCallbackGeoLocation;
    static void setPre_GeoLocationEventsProcessedCallback(Pre_GeoLocationEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoLocation = node;
    }

    Pre_GeoLocation() {
    }
    void treeRender() {
        if (Pre_GeoLocation.Pre_treeRenderCallbackGeoLocation != null) {
            Pre_GeoLocation.Pre_treeRenderCallbackGeoLocation.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoLocation.Pre_renderCallbackGeoLocation != null)
            Pre_GeoLocation.Pre_renderCallbackGeoLocation.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoLocation.Pre_treeDoWithDataCallbackGeoLocation != null) {
            Pre_GeoLocation.Pre_treeDoWithDataCallbackGeoLocation.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoLocation.Pre_doWithDataCallbackGeoLocation != null)
            Pre_GeoLocation.Pre_doWithDataCallbackGeoLocation.doWithData(this);
    }
}

class Pre_GeoLocationRenderCallback extends RenderCallback {}
class Pre_GeoLocationTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoLocationDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoLocationTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoLocationEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ScreenGroup extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_ScreenGroupRenderCallback Pre_renderCallbackScreenGroup;
    static void setPre_ScreenGroupRenderCallback(Pre_ScreenGroupRenderCallback node) {
        Pre_renderCallbackScreenGroup = node;
    }

    static Pre_ScreenGroupTreeRenderCallback Pre_treeRenderCallbackScreenGroup;
    static void setPre_ScreenGroupTreeRenderCallback(Pre_ScreenGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackScreenGroup = node;
    }

    static Pre_ScreenGroupDoWithDataCallback Pre_doWithDataCallbackScreenGroup;
    static void setPre_ScreenGroupDoWithDataCallback(Pre_ScreenGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackScreenGroup = node;
    }

    static Pre_ScreenGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackScreenGroup;
    static void setPre_ScreenGroupTreeDoWithDataCallback(Pre_ScreenGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackScreenGroup = node;

    }

    static Pre_ScreenGroupEventsProcessedCallback Pre_eventsProcessedCallbackScreenGroup;
    static void setPre_ScreenGroupEventsProcessedCallback(Pre_ScreenGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackScreenGroup = node;
    }

    Pre_ScreenGroup() {
    }
    void treeRender() {
        if (Pre_ScreenGroup.Pre_treeRenderCallbackScreenGroup != null) {
            Pre_ScreenGroup.Pre_treeRenderCallbackScreenGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ScreenGroup.Pre_renderCallbackScreenGroup != null)
            Pre_ScreenGroup.Pre_renderCallbackScreenGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_ScreenGroup.Pre_treeDoWithDataCallbackScreenGroup != null) {
            Pre_ScreenGroup.Pre_treeDoWithDataCallbackScreenGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ScreenGroup.Pre_doWithDataCallbackScreenGroup != null)
            Pre_ScreenGroup.Pre_doWithDataCallbackScreenGroup.doWithData(this);
    }
}

class Pre_ScreenGroupRenderCallback extends RenderCallback {}
class Pre_ScreenGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_ScreenGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_ScreenGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ScreenGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Cylinder extends Pre_Node {
    Pre_Node metadata;
    boolean bottom;
    float height;
    float radius;
    boolean side;
    boolean top;
    boolean solid;
    Pre_Node texCoord;
    static Pre_CylinderRenderCallback Pre_renderCallbackCylinder;
    static void setPre_CylinderRenderCallback(Pre_CylinderRenderCallback node) {
        Pre_renderCallbackCylinder = node;
    }

    static Pre_CylinderTreeRenderCallback Pre_treeRenderCallbackCylinder;
    static void setPre_CylinderTreeRenderCallback(Pre_CylinderTreeRenderCallback node) {
        Pre_treeRenderCallbackCylinder = node;
    }

    static Pre_CylinderDoWithDataCallback Pre_doWithDataCallbackCylinder;
    static void setPre_CylinderDoWithDataCallback(Pre_CylinderDoWithDataCallback node) {
        Pre_doWithDataCallbackCylinder = node;
    }

    static Pre_CylinderTreeDoWithDataCallback Pre_treeDoWithDataCallbackCylinder;
    static void setPre_CylinderTreeDoWithDataCallback(Pre_CylinderTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCylinder = node;

    }

    static Pre_CylinderEventsProcessedCallback Pre_eventsProcessedCallbackCylinder;
    static void setPre_CylinderEventsProcessedCallback(Pre_CylinderEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCylinder = node;
    }

    Pre_Cylinder() {
    }
    void treeRender() {
        if (Pre_Cylinder.Pre_treeRenderCallbackCylinder != null) {
            Pre_Cylinder.Pre_treeRenderCallbackCylinder.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Cylinder.Pre_renderCallbackCylinder != null)
            Pre_Cylinder.Pre_renderCallbackCylinder.render(this);
    }
    void treeDoWithData() {
        if (Pre_Cylinder.Pre_treeDoWithDataCallbackCylinder != null) {
            Pre_Cylinder.Pre_treeDoWithDataCallbackCylinder.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Cylinder.Pre_doWithDataCallbackCylinder != null)
            Pre_Cylinder.Pre_doWithDataCallbackCylinder.doWithData(this);
    }
}

class Pre_CylinderRenderCallback extends RenderCallback {}
class Pre_CylinderTreeRenderCallback extends TreeRenderCallback {}
class Pre_CylinderDoWithDataCallback extends DoWithDataCallback {};
class Pre_CylinderTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CylinderEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_DISEntityManager extends Pre_Node {
    Pre_Node metadata;
    String address;
    int applicationID;
    Pre_Node [] mapping;
    int port;
    int siteID;
    static Pre_DISEntityManagerRenderCallback Pre_renderCallbackDISEntityManager;
    static void setPre_DISEntityManagerRenderCallback(Pre_DISEntityManagerRenderCallback node) {
        Pre_renderCallbackDISEntityManager = node;
    }

    static Pre_DISEntityManagerTreeRenderCallback Pre_treeRenderCallbackDISEntityManager;
    static void setPre_DISEntityManagerTreeRenderCallback(Pre_DISEntityManagerTreeRenderCallback node) {
        Pre_treeRenderCallbackDISEntityManager = node;
    }

    static Pre_DISEntityManagerDoWithDataCallback Pre_doWithDataCallbackDISEntityManager;
    static void setPre_DISEntityManagerDoWithDataCallback(Pre_DISEntityManagerDoWithDataCallback node) {
        Pre_doWithDataCallbackDISEntityManager = node;
    }

    static Pre_DISEntityManagerTreeDoWithDataCallback Pre_treeDoWithDataCallbackDISEntityManager;
    static void setPre_DISEntityManagerTreeDoWithDataCallback(Pre_DISEntityManagerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackDISEntityManager = node;

    }

    static Pre_DISEntityManagerEventsProcessedCallback Pre_eventsProcessedCallbackDISEntityManager;
    static void setPre_DISEntityManagerEventsProcessedCallback(Pre_DISEntityManagerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackDISEntityManager = node;
    }

    Pre_DISEntityManager() {
    }
    void treeRender() {
        if (Pre_DISEntityManager.Pre_treeRenderCallbackDISEntityManager != null) {
            Pre_DISEntityManager.Pre_treeRenderCallbackDISEntityManager.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (mapping != null)
            for (int i = 0; i < mapping.length; i++)
                if (mapping[i] != null)
                    mapping[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_DISEntityManager.Pre_renderCallbackDISEntityManager != null)
            Pre_DISEntityManager.Pre_renderCallbackDISEntityManager.render(this);
    }
    void treeDoWithData() {
        if (Pre_DISEntityManager.Pre_treeDoWithDataCallbackDISEntityManager != null) {
            Pre_DISEntityManager.Pre_treeDoWithDataCallbackDISEntityManager.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (mapping != null)
            for (int i = 0; i < mapping.length; i++)
                if (mapping[i] != null)
                    mapping[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_DISEntityManager.Pre_doWithDataCallbackDISEntityManager != null)
            Pre_DISEntityManager.Pre_doWithDataCallbackDISEntityManager.doWithData(this);
    }
}

class Pre_DISEntityManagerRenderCallback extends RenderCallback {}
class Pre_DISEntityManagerTreeRenderCallback extends TreeRenderCallback {}
class Pre_DISEntityManagerDoWithDataCallback extends DoWithDataCallback {};
class Pre_DISEntityManagerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_DISEntityManagerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NormalInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_NormalInterpolatorRenderCallback Pre_renderCallbackNormalInterpolator;
    static void setPre_NormalInterpolatorRenderCallback(Pre_NormalInterpolatorRenderCallback node) {
        Pre_renderCallbackNormalInterpolator = node;
    }

    static Pre_NormalInterpolatorTreeRenderCallback Pre_treeRenderCallbackNormalInterpolator;
    static void setPre_NormalInterpolatorTreeRenderCallback(Pre_NormalInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackNormalInterpolator = node;
    }

    static Pre_NormalInterpolatorDoWithDataCallback Pre_doWithDataCallbackNormalInterpolator;
    static void setPre_NormalInterpolatorDoWithDataCallback(Pre_NormalInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackNormalInterpolator = node;
    }

    static Pre_NormalInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackNormalInterpolator;
    static void setPre_NormalInterpolatorTreeDoWithDataCallback(Pre_NormalInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNormalInterpolator = node;

    }

    static Pre_NormalInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackNormalInterpolator;
    static void setPre_NormalInterpolatorEventsProcessedCallback(Pre_NormalInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNormalInterpolator = node;
    }

    Pre_NormalInterpolator() {
    }
    void treeRender() {
        if (Pre_NormalInterpolator.Pre_treeRenderCallbackNormalInterpolator != null) {
            Pre_NormalInterpolator.Pre_treeRenderCallbackNormalInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NormalInterpolator.Pre_renderCallbackNormalInterpolator != null)
            Pre_NormalInterpolator.Pre_renderCallbackNormalInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_NormalInterpolator.Pre_treeDoWithDataCallbackNormalInterpolator != null) {
            Pre_NormalInterpolator.Pre_treeDoWithDataCallbackNormalInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NormalInterpolator.Pre_doWithDataCallbackNormalInterpolator != null)
            Pre_NormalInterpolator.Pre_doWithDataCallbackNormalInterpolator.doWithData(this);
    }
}

class Pre_NormalInterpolatorRenderCallback extends RenderCallback {}
class Pre_NormalInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_NormalInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_NormalInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NormalInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ScalarInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_ScalarInterpolatorRenderCallback Pre_renderCallbackScalarInterpolator;
    static void setPre_ScalarInterpolatorRenderCallback(Pre_ScalarInterpolatorRenderCallback node) {
        Pre_renderCallbackScalarInterpolator = node;
    }

    static Pre_ScalarInterpolatorTreeRenderCallback Pre_treeRenderCallbackScalarInterpolator;
    static void setPre_ScalarInterpolatorTreeRenderCallback(Pre_ScalarInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackScalarInterpolator = node;
    }

    static Pre_ScalarInterpolatorDoWithDataCallback Pre_doWithDataCallbackScalarInterpolator;
    static void setPre_ScalarInterpolatorDoWithDataCallback(Pre_ScalarInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackScalarInterpolator = node;
    }

    static Pre_ScalarInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackScalarInterpolator;
    static void setPre_ScalarInterpolatorTreeDoWithDataCallback(Pre_ScalarInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackScalarInterpolator = node;

    }

    static Pre_ScalarInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackScalarInterpolator;
    static void setPre_ScalarInterpolatorEventsProcessedCallback(Pre_ScalarInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackScalarInterpolator = node;
    }

    Pre_ScalarInterpolator() {
    }
    void treeRender() {
        if (Pre_ScalarInterpolator.Pre_treeRenderCallbackScalarInterpolator != null) {
            Pre_ScalarInterpolator.Pre_treeRenderCallbackScalarInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ScalarInterpolator.Pre_renderCallbackScalarInterpolator != null)
            Pre_ScalarInterpolator.Pre_renderCallbackScalarInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_ScalarInterpolator.Pre_treeDoWithDataCallbackScalarInterpolator != null) {
            Pre_ScalarInterpolator.Pre_treeDoWithDataCallbackScalarInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ScalarInterpolator.Pre_doWithDataCallbackScalarInterpolator != null)
            Pre_ScalarInterpolator.Pre_doWithDataCallbackScalarInterpolator.doWithData(this);
    }
}

class Pre_ScalarInterpolatorRenderCallback extends RenderCallback {}
class Pre_ScalarInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_ScalarInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_ScalarInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ScalarInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TransformSensor extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    boolean enabled;
    float[] size;
    Pre_Node targetObject;
    static Pre_TransformSensorRenderCallback Pre_renderCallbackTransformSensor;
    static void setPre_TransformSensorRenderCallback(Pre_TransformSensorRenderCallback node) {
        Pre_renderCallbackTransformSensor = node;
    }

    static Pre_TransformSensorTreeRenderCallback Pre_treeRenderCallbackTransformSensor;
    static void setPre_TransformSensorTreeRenderCallback(Pre_TransformSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackTransformSensor = node;
    }

    static Pre_TransformSensorDoWithDataCallback Pre_doWithDataCallbackTransformSensor;
    static void setPre_TransformSensorDoWithDataCallback(Pre_TransformSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackTransformSensor = node;
    }

    static Pre_TransformSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackTransformSensor;
    static void setPre_TransformSensorTreeDoWithDataCallback(Pre_TransformSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTransformSensor = node;

    }

    static Pre_TransformSensorEventsProcessedCallback Pre_eventsProcessedCallbackTransformSensor;
    static void setPre_TransformSensorEventsProcessedCallback(Pre_TransformSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTransformSensor = node;
    }

    Pre_TransformSensor() {
    }
    void treeRender() {
        if (Pre_TransformSensor.Pre_treeRenderCallbackTransformSensor != null) {
            Pre_TransformSensor.Pre_treeRenderCallbackTransformSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (targetObject != null)
            targetObject.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TransformSensor.Pre_renderCallbackTransformSensor != null)
            Pre_TransformSensor.Pre_renderCallbackTransformSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_TransformSensor.Pre_treeDoWithDataCallbackTransformSensor != null) {
            Pre_TransformSensor.Pre_treeDoWithDataCallbackTransformSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (targetObject != null)
            targetObject.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TransformSensor.Pre_doWithDataCallbackTransformSensor != null)
            Pre_TransformSensor.Pre_doWithDataCallbackTransformSensor.doWithData(this);
    }
}

class Pre_TransformSensorRenderCallback extends RenderCallback {}
class Pre_TransformSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_TransformSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_TransformSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TransformSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CattExportRec extends Pre_Node {
    Pre_Node metadata;
    int id;
    float[] receiverLocation;
    String furtherParameters;
    static Pre_CattExportRecRenderCallback Pre_renderCallbackCattExportRec;
    static void setPre_CattExportRecRenderCallback(Pre_CattExportRecRenderCallback node) {
        Pre_renderCallbackCattExportRec = node;
    }

    static Pre_CattExportRecTreeRenderCallback Pre_treeRenderCallbackCattExportRec;
    static void setPre_CattExportRecTreeRenderCallback(Pre_CattExportRecTreeRenderCallback node) {
        Pre_treeRenderCallbackCattExportRec = node;
    }

    static Pre_CattExportRecDoWithDataCallback Pre_doWithDataCallbackCattExportRec;
    static void setPre_CattExportRecDoWithDataCallback(Pre_CattExportRecDoWithDataCallback node) {
        Pre_doWithDataCallbackCattExportRec = node;
    }

    static Pre_CattExportRecTreeDoWithDataCallback Pre_treeDoWithDataCallbackCattExportRec;
    static void setPre_CattExportRecTreeDoWithDataCallback(Pre_CattExportRecTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCattExportRec = node;

    }

    static Pre_CattExportRecEventsProcessedCallback Pre_eventsProcessedCallbackCattExportRec;
    static void setPre_CattExportRecEventsProcessedCallback(Pre_CattExportRecEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCattExportRec = node;
    }

    Pre_CattExportRec() {
    }
    void treeRender() {
        if (Pre_CattExportRec.Pre_treeRenderCallbackCattExportRec != null) {
            Pre_CattExportRec.Pre_treeRenderCallbackCattExportRec.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CattExportRec.Pre_renderCallbackCattExportRec != null)
            Pre_CattExportRec.Pre_renderCallbackCattExportRec.render(this);
    }
    void treeDoWithData() {
        if (Pre_CattExportRec.Pre_treeDoWithDataCallbackCattExportRec != null) {
            Pre_CattExportRec.Pre_treeDoWithDataCallbackCattExportRec.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CattExportRec.Pre_doWithDataCallbackCattExportRec != null)
            Pre_CattExportRec.Pre_doWithDataCallbackCattExportRec.doWithData(this);
    }
}

class Pre_CattExportRecRenderCallback extends RenderCallback {}
class Pre_CattExportRecTreeRenderCallback extends TreeRenderCallback {}
class Pre_CattExportRecDoWithDataCallback extends DoWithDataCallback {};
class Pre_CattExportRecTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CattExportRecEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_FillProperties extends Pre_Node {
    Pre_Node metadata;
    boolean filled;
    float[] hatchColor;
    boolean hatched;
    int hatchStyle;
    static Pre_FillPropertiesRenderCallback Pre_renderCallbackFillProperties;
    static void setPre_FillPropertiesRenderCallback(Pre_FillPropertiesRenderCallback node) {
        Pre_renderCallbackFillProperties = node;
    }

    static Pre_FillPropertiesTreeRenderCallback Pre_treeRenderCallbackFillProperties;
    static void setPre_FillPropertiesTreeRenderCallback(Pre_FillPropertiesTreeRenderCallback node) {
        Pre_treeRenderCallbackFillProperties = node;
    }

    static Pre_FillPropertiesDoWithDataCallback Pre_doWithDataCallbackFillProperties;
    static void setPre_FillPropertiesDoWithDataCallback(Pre_FillPropertiesDoWithDataCallback node) {
        Pre_doWithDataCallbackFillProperties = node;
    }

    static Pre_FillPropertiesTreeDoWithDataCallback Pre_treeDoWithDataCallbackFillProperties;
    static void setPre_FillPropertiesTreeDoWithDataCallback(Pre_FillPropertiesTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackFillProperties = node;

    }

    static Pre_FillPropertiesEventsProcessedCallback Pre_eventsProcessedCallbackFillProperties;
    static void setPre_FillPropertiesEventsProcessedCallback(Pre_FillPropertiesEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackFillProperties = node;
    }

    Pre_FillProperties() {
    }
    void treeRender() {
        if (Pre_FillProperties.Pre_treeRenderCallbackFillProperties != null) {
            Pre_FillProperties.Pre_treeRenderCallbackFillProperties.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_FillProperties.Pre_renderCallbackFillProperties != null)
            Pre_FillProperties.Pre_renderCallbackFillProperties.render(this);
    }
    void treeDoWithData() {
        if (Pre_FillProperties.Pre_treeDoWithDataCallbackFillProperties != null) {
            Pre_FillProperties.Pre_treeDoWithDataCallbackFillProperties.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_FillProperties.Pre_doWithDataCallbackFillProperties != null)
            Pre_FillProperties.Pre_doWithDataCallbackFillProperties.doWithData(this);
    }
}

class Pre_FillPropertiesRenderCallback extends RenderCallback {}
class Pre_FillPropertiesTreeRenderCallback extends TreeRenderCallback {}
class Pre_FillPropertiesDoWithDataCallback extends DoWithDataCallback {};
class Pre_FillPropertiesTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_FillPropertiesEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_HAnimSegment extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    float[] centerOfMass;
    Pre_Node coord;
    Pre_Node [] displacers;
    float mass;
    float[] momentsOfInertia;
    String name;
    static Pre_HAnimSegmentRenderCallback Pre_renderCallbackHAnimSegment;
    static void setPre_HAnimSegmentRenderCallback(Pre_HAnimSegmentRenderCallback node) {
        Pre_renderCallbackHAnimSegment = node;
    }

    static Pre_HAnimSegmentTreeRenderCallback Pre_treeRenderCallbackHAnimSegment;
    static void setPre_HAnimSegmentTreeRenderCallback(Pre_HAnimSegmentTreeRenderCallback node) {
        Pre_treeRenderCallbackHAnimSegment = node;
    }

    static Pre_HAnimSegmentDoWithDataCallback Pre_doWithDataCallbackHAnimSegment;
    static void setPre_HAnimSegmentDoWithDataCallback(Pre_HAnimSegmentDoWithDataCallback node) {
        Pre_doWithDataCallbackHAnimSegment = node;
    }

    static Pre_HAnimSegmentTreeDoWithDataCallback Pre_treeDoWithDataCallbackHAnimSegment;
    static void setPre_HAnimSegmentTreeDoWithDataCallback(Pre_HAnimSegmentTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackHAnimSegment = node;

    }

    static Pre_HAnimSegmentEventsProcessedCallback Pre_eventsProcessedCallbackHAnimSegment;
    static void setPre_HAnimSegmentEventsProcessedCallback(Pre_HAnimSegmentEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackHAnimSegment = node;
    }

    Pre_HAnimSegment() {
    }
    void treeRender() {
        if (Pre_HAnimSegment.Pre_treeRenderCallbackHAnimSegment != null) {
            Pre_HAnimSegment.Pre_treeRenderCallbackHAnimSegment.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (coord != null)
            coord.treeRender();
        if (displacers != null)
            for (int i = 0; i < displacers.length; i++)
                if (displacers[i] != null)
                    displacers[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_HAnimSegment.Pre_renderCallbackHAnimSegment != null)
            Pre_HAnimSegment.Pre_renderCallbackHAnimSegment.render(this);
    }
    void treeDoWithData() {
        if (Pre_HAnimSegment.Pre_treeDoWithDataCallbackHAnimSegment != null) {
            Pre_HAnimSegment.Pre_treeDoWithDataCallbackHAnimSegment.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (displacers != null)
            for (int i = 0; i < displacers.length; i++)
                if (displacers[i] != null)
                    displacers[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_HAnimSegment.Pre_doWithDataCallbackHAnimSegment != null)
            Pre_HAnimSegment.Pre_doWithDataCallbackHAnimSegment.doWithData(this);
    }
}

class Pre_HAnimSegmentRenderCallback extends RenderCallback {}
class Pre_HAnimSegmentTreeRenderCallback extends TreeRenderCallback {}
class Pre_HAnimSegmentDoWithDataCallback extends DoWithDataCallback {};
class Pre_HAnimSegmentTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_HAnimSegmentEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SplinePositionInterpolator2D extends Pre_Node {
    Pre_Node metadata;
    boolean closed;
    float[] key;
    float[] keyValue;
    float[] keyVelocity;
    boolean normalizeVelocity;
    static Pre_SplinePositionInterpolator2DRenderCallback Pre_renderCallbackSplinePositionInterpolator2D;
    static void setPre_SplinePositionInterpolator2DRenderCallback(Pre_SplinePositionInterpolator2DRenderCallback node) {
        Pre_renderCallbackSplinePositionInterpolator2D = node;
    }

    static Pre_SplinePositionInterpolator2DTreeRenderCallback Pre_treeRenderCallbackSplinePositionInterpolator2D;
    static void setPre_SplinePositionInterpolator2DTreeRenderCallback(Pre_SplinePositionInterpolator2DTreeRenderCallback node) {
        Pre_treeRenderCallbackSplinePositionInterpolator2D = node;
    }

    static Pre_SplinePositionInterpolator2DDoWithDataCallback Pre_doWithDataCallbackSplinePositionInterpolator2D;
    static void setPre_SplinePositionInterpolator2DDoWithDataCallback(Pre_SplinePositionInterpolator2DDoWithDataCallback node) {
        Pre_doWithDataCallbackSplinePositionInterpolator2D = node;
    }

    static Pre_SplinePositionInterpolator2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackSplinePositionInterpolator2D;
    static void setPre_SplinePositionInterpolator2DTreeDoWithDataCallback(Pre_SplinePositionInterpolator2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSplinePositionInterpolator2D = node;

    }

    static Pre_SplinePositionInterpolator2DEventsProcessedCallback Pre_eventsProcessedCallbackSplinePositionInterpolator2D;
    static void setPre_SplinePositionInterpolator2DEventsProcessedCallback(Pre_SplinePositionInterpolator2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSplinePositionInterpolator2D = node;
    }

    Pre_SplinePositionInterpolator2D() {
    }
    void treeRender() {
        if (Pre_SplinePositionInterpolator2D.Pre_treeRenderCallbackSplinePositionInterpolator2D != null) {
            Pre_SplinePositionInterpolator2D.Pre_treeRenderCallbackSplinePositionInterpolator2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SplinePositionInterpolator2D.Pre_renderCallbackSplinePositionInterpolator2D != null)
            Pre_SplinePositionInterpolator2D.Pre_renderCallbackSplinePositionInterpolator2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_SplinePositionInterpolator2D.Pre_treeDoWithDataCallbackSplinePositionInterpolator2D != null) {
            Pre_SplinePositionInterpolator2D.Pre_treeDoWithDataCallbackSplinePositionInterpolator2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SplinePositionInterpolator2D.Pre_doWithDataCallbackSplinePositionInterpolator2D != null)
            Pre_SplinePositionInterpolator2D.Pre_doWithDataCallbackSplinePositionInterpolator2D.doWithData(this);
    }
}

class Pre_SplinePositionInterpolator2DRenderCallback extends RenderCallback {}
class Pre_SplinePositionInterpolator2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_SplinePositionInterpolator2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_SplinePositionInterpolator2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SplinePositionInterpolator2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureBackground extends Pre_Node {
    Pre_Node metadata;
    float[] groundAngle;
    float[] groundColor;
    Pre_Node backTexture;
    Pre_Node rightTexture;
    Pre_Node frontTexture;
    Pre_Node leftTexture;
    Pre_Node topTexture;
    Pre_Node bottomTexture;
    float[] skyAngle;
    float[] skyColor;
    static Pre_TextureBackgroundRenderCallback Pre_renderCallbackTextureBackground;
    static void setPre_TextureBackgroundRenderCallback(Pre_TextureBackgroundRenderCallback node) {
        Pre_renderCallbackTextureBackground = node;
    }

    static Pre_TextureBackgroundTreeRenderCallback Pre_treeRenderCallbackTextureBackground;
    static void setPre_TextureBackgroundTreeRenderCallback(Pre_TextureBackgroundTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureBackground = node;
    }

    static Pre_TextureBackgroundDoWithDataCallback Pre_doWithDataCallbackTextureBackground;
    static void setPre_TextureBackgroundDoWithDataCallback(Pre_TextureBackgroundDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureBackground = node;
    }

    static Pre_TextureBackgroundTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureBackground;
    static void setPre_TextureBackgroundTreeDoWithDataCallback(Pre_TextureBackgroundTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureBackground = node;

    }

    static Pre_TextureBackgroundEventsProcessedCallback Pre_eventsProcessedCallbackTextureBackground;
    static void setPre_TextureBackgroundEventsProcessedCallback(Pre_TextureBackgroundEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureBackground = node;
    }

    Pre_TextureBackground() {
    }
    void treeRender() {
        if (Pre_TextureBackground.Pre_treeRenderCallbackTextureBackground != null) {
            Pre_TextureBackground.Pre_treeRenderCallbackTextureBackground.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (backTexture != null)
            backTexture.treeRender();
        if (rightTexture != null)
            rightTexture.treeRender();
        if (frontTexture != null)
            frontTexture.treeRender();
        if (leftTexture != null)
            leftTexture.treeRender();
        if (topTexture != null)
            topTexture.treeRender();
        if (bottomTexture != null)
            bottomTexture.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureBackground.Pre_renderCallbackTextureBackground != null)
            Pre_TextureBackground.Pre_renderCallbackTextureBackground.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureBackground.Pre_treeDoWithDataCallbackTextureBackground != null) {
            Pre_TextureBackground.Pre_treeDoWithDataCallbackTextureBackground.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (backTexture != null)
            backTexture.treeDoWithData();
        if (rightTexture != null)
            rightTexture.treeDoWithData();
        if (frontTexture != null)
            frontTexture.treeDoWithData();
        if (leftTexture != null)
            leftTexture.treeDoWithData();
        if (topTexture != null)
            topTexture.treeDoWithData();
        if (bottomTexture != null)
            bottomTexture.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureBackground.Pre_doWithDataCallbackTextureBackground != null)
            Pre_TextureBackground.Pre_doWithDataCallbackTextureBackground.doWithData(this);
    }
}

class Pre_TextureBackgroundRenderCallback extends RenderCallback {}
class Pre_TextureBackgroundTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureBackgroundDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureBackgroundTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureBackgroundEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IMPORT extends Pre_Node {
    Pre_Node metadata;
    String importedDEF;
    String inlineDEF;
    static Pre_IMPORTRenderCallback Pre_renderCallbackIMPORT;
    static void setPre_IMPORTRenderCallback(Pre_IMPORTRenderCallback node) {
        Pre_renderCallbackIMPORT = node;
    }

    static Pre_IMPORTTreeRenderCallback Pre_treeRenderCallbackIMPORT;
    static void setPre_IMPORTTreeRenderCallback(Pre_IMPORTTreeRenderCallback node) {
        Pre_treeRenderCallbackIMPORT = node;
    }

    static Pre_IMPORTDoWithDataCallback Pre_doWithDataCallbackIMPORT;
    static void setPre_IMPORTDoWithDataCallback(Pre_IMPORTDoWithDataCallback node) {
        Pre_doWithDataCallbackIMPORT = node;
    }

    static Pre_IMPORTTreeDoWithDataCallback Pre_treeDoWithDataCallbackIMPORT;
    static void setPre_IMPORTTreeDoWithDataCallback(Pre_IMPORTTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIMPORT = node;

    }

    static Pre_IMPORTEventsProcessedCallback Pre_eventsProcessedCallbackIMPORT;
    static void setPre_IMPORTEventsProcessedCallback(Pre_IMPORTEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIMPORT = node;
    }

    Pre_IMPORT() {
    }
    void treeRender() {
        if (Pre_IMPORT.Pre_treeRenderCallbackIMPORT != null) {
            Pre_IMPORT.Pre_treeRenderCallbackIMPORT.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IMPORT.Pre_renderCallbackIMPORT != null)
            Pre_IMPORT.Pre_renderCallbackIMPORT.render(this);
    }
    void treeDoWithData() {
        if (Pre_IMPORT.Pre_treeDoWithDataCallbackIMPORT != null) {
            Pre_IMPORT.Pre_treeDoWithDataCallbackIMPORT.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IMPORT.Pre_doWithDataCallbackIMPORT != null)
            Pre_IMPORT.Pre_doWithDataCallbackIMPORT.doWithData(this);
    }
}

class Pre_IMPORTRenderCallback extends RenderCallback {}
class Pre_IMPORTTreeRenderCallback extends TreeRenderCallback {}
class Pre_IMPORTDoWithDataCallback extends DoWithDataCallback {};
class Pre_IMPORTTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IMPORTEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_EXPORT extends Pre_Node {
    Pre_Node metadata;
    String localDEF;
    static Pre_EXPORTRenderCallback Pre_renderCallbackEXPORT;
    static void setPre_EXPORTRenderCallback(Pre_EXPORTRenderCallback node) {
        Pre_renderCallbackEXPORT = node;
    }

    static Pre_EXPORTTreeRenderCallback Pre_treeRenderCallbackEXPORT;
    static void setPre_EXPORTTreeRenderCallback(Pre_EXPORTTreeRenderCallback node) {
        Pre_treeRenderCallbackEXPORT = node;
    }

    static Pre_EXPORTDoWithDataCallback Pre_doWithDataCallbackEXPORT;
    static void setPre_EXPORTDoWithDataCallback(Pre_EXPORTDoWithDataCallback node) {
        Pre_doWithDataCallbackEXPORT = node;
    }

    static Pre_EXPORTTreeDoWithDataCallback Pre_treeDoWithDataCallbackEXPORT;
    static void setPre_EXPORTTreeDoWithDataCallback(Pre_EXPORTTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackEXPORT = node;

    }

    static Pre_EXPORTEventsProcessedCallback Pre_eventsProcessedCallbackEXPORT;
    static void setPre_EXPORTEventsProcessedCallback(Pre_EXPORTEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackEXPORT = node;
    }

    Pre_EXPORT() {
    }
    void treeRender() {
        if (Pre_EXPORT.Pre_treeRenderCallbackEXPORT != null) {
            Pre_EXPORT.Pre_treeRenderCallbackEXPORT.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_EXPORT.Pre_renderCallbackEXPORT != null)
            Pre_EXPORT.Pre_renderCallbackEXPORT.render(this);
    }
    void treeDoWithData() {
        if (Pre_EXPORT.Pre_treeDoWithDataCallbackEXPORT != null) {
            Pre_EXPORT.Pre_treeDoWithDataCallbackEXPORT.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_EXPORT.Pre_doWithDataCallbackEXPORT != null)
            Pre_EXPORT.Pre_doWithDataCallbackEXPORT.doWithData(this);
    }
}

class Pre_EXPORTRenderCallback extends RenderCallback {}
class Pre_EXPORTTreeRenderCallback extends TreeRenderCallback {}
class Pre_EXPORTDoWithDataCallback extends DoWithDataCallback {};
class Pre_EXPORTTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_EXPORTEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ClipPlane extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] plane;
    static Pre_ClipPlaneRenderCallback Pre_renderCallbackClipPlane;
    static void setPre_ClipPlaneRenderCallback(Pre_ClipPlaneRenderCallback node) {
        Pre_renderCallbackClipPlane = node;
    }

    static Pre_ClipPlaneTreeRenderCallback Pre_treeRenderCallbackClipPlane;
    static void setPre_ClipPlaneTreeRenderCallback(Pre_ClipPlaneTreeRenderCallback node) {
        Pre_treeRenderCallbackClipPlane = node;
    }

    static Pre_ClipPlaneDoWithDataCallback Pre_doWithDataCallbackClipPlane;
    static void setPre_ClipPlaneDoWithDataCallback(Pre_ClipPlaneDoWithDataCallback node) {
        Pre_doWithDataCallbackClipPlane = node;
    }

    static Pre_ClipPlaneTreeDoWithDataCallback Pre_treeDoWithDataCallbackClipPlane;
    static void setPre_ClipPlaneTreeDoWithDataCallback(Pre_ClipPlaneTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackClipPlane = node;

    }

    static Pre_ClipPlaneEventsProcessedCallback Pre_eventsProcessedCallbackClipPlane;
    static void setPre_ClipPlaneEventsProcessedCallback(Pre_ClipPlaneEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackClipPlane = node;
    }

    Pre_ClipPlane() {
    }
    void treeRender() {
        if (Pre_ClipPlane.Pre_treeRenderCallbackClipPlane != null) {
            Pre_ClipPlane.Pre_treeRenderCallbackClipPlane.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ClipPlane.Pre_renderCallbackClipPlane != null)
            Pre_ClipPlane.Pre_renderCallbackClipPlane.render(this);
    }
    void treeDoWithData() {
        if (Pre_ClipPlane.Pre_treeDoWithDataCallbackClipPlane != null) {
            Pre_ClipPlane.Pre_treeDoWithDataCallbackClipPlane.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ClipPlane.Pre_doWithDataCallbackClipPlane != null)
            Pre_ClipPlane.Pre_doWithDataCallbackClipPlane.doWithData(this);
    }
}

class Pre_ClipPlaneRenderCallback extends RenderCallback {}
class Pre_ClipPlaneTreeRenderCallback extends TreeRenderCallback {}
class Pre_ClipPlaneDoWithDataCallback extends DoWithDataCallback {};
class Pre_ClipPlaneTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ClipPlaneEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ReceiverPdu extends Pre_Node {
    Pre_Node metadata;
    int radioID;
    float receivedPower;
    int receiverState;
    int transmitterApplicationID;
    int transmitterEntityID;
    int transmitterRadioID;
    int transmitterSiteID;
    int whichGeometry;
    float[] bboxCenter;
    float[] bboxSize;
    boolean enabled;
    String networkMode;
    double readInterval;
    double writeInterval;
    int siteID;
    int applicationID;
    int entityID;
    String address;
    int port;
    String multicastRelayHost;
    int multicastRelayPort;
    boolean rtpHeaderExpected;
    static Pre_ReceiverPduRenderCallback Pre_renderCallbackReceiverPdu;
    static void setPre_ReceiverPduRenderCallback(Pre_ReceiverPduRenderCallback node) {
        Pre_renderCallbackReceiverPdu = node;
    }

    static Pre_ReceiverPduTreeRenderCallback Pre_treeRenderCallbackReceiverPdu;
    static void setPre_ReceiverPduTreeRenderCallback(Pre_ReceiverPduTreeRenderCallback node) {
        Pre_treeRenderCallbackReceiverPdu = node;
    }

    static Pre_ReceiverPduDoWithDataCallback Pre_doWithDataCallbackReceiverPdu;
    static void setPre_ReceiverPduDoWithDataCallback(Pre_ReceiverPduDoWithDataCallback node) {
        Pre_doWithDataCallbackReceiverPdu = node;
    }

    static Pre_ReceiverPduTreeDoWithDataCallback Pre_treeDoWithDataCallbackReceiverPdu;
    static void setPre_ReceiverPduTreeDoWithDataCallback(Pre_ReceiverPduTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackReceiverPdu = node;

    }

    static Pre_ReceiverPduEventsProcessedCallback Pre_eventsProcessedCallbackReceiverPdu;
    static void setPre_ReceiverPduEventsProcessedCallback(Pre_ReceiverPduEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackReceiverPdu = node;
    }

    Pre_ReceiverPdu() {
    }
    void treeRender() {
        if (Pre_ReceiverPdu.Pre_treeRenderCallbackReceiverPdu != null) {
            Pre_ReceiverPdu.Pre_treeRenderCallbackReceiverPdu.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ReceiverPdu.Pre_renderCallbackReceiverPdu != null)
            Pre_ReceiverPdu.Pre_renderCallbackReceiverPdu.render(this);
    }
    void treeDoWithData() {
        if (Pre_ReceiverPdu.Pre_treeDoWithDataCallbackReceiverPdu != null) {
            Pre_ReceiverPdu.Pre_treeDoWithDataCallbackReceiverPdu.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ReceiverPdu.Pre_doWithDataCallbackReceiverPdu != null)
            Pre_ReceiverPdu.Pre_doWithDataCallbackReceiverPdu.doWithData(this);
    }
}

class Pre_ReceiverPduRenderCallback extends RenderCallback {}
class Pre_ReceiverPduTreeRenderCallback extends TreeRenderCallback {}
class Pre_ReceiverPduDoWithDataCallback extends DoWithDataCallback {};
class Pre_ReceiverPduTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ReceiverPduEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureCoordinate extends Pre_Node {
    Pre_Node metadata;
    float[] point;
    static Pre_TextureCoordinateRenderCallback Pre_renderCallbackTextureCoordinate;
    static void setPre_TextureCoordinateRenderCallback(Pre_TextureCoordinateRenderCallback node) {
        Pre_renderCallbackTextureCoordinate = node;
    }

    static Pre_TextureCoordinateTreeRenderCallback Pre_treeRenderCallbackTextureCoordinate;
    static void setPre_TextureCoordinateTreeRenderCallback(Pre_TextureCoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureCoordinate = node;
    }

    static Pre_TextureCoordinateDoWithDataCallback Pre_doWithDataCallbackTextureCoordinate;
    static void setPre_TextureCoordinateDoWithDataCallback(Pre_TextureCoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureCoordinate = node;
    }

    static Pre_TextureCoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureCoordinate;
    static void setPre_TextureCoordinateTreeDoWithDataCallback(Pre_TextureCoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureCoordinate = node;

    }

    static Pre_TextureCoordinateEventsProcessedCallback Pre_eventsProcessedCallbackTextureCoordinate;
    static void setPre_TextureCoordinateEventsProcessedCallback(Pre_TextureCoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureCoordinate = node;
    }

    Pre_TextureCoordinate() {
    }
    void treeRender() {
        if (Pre_TextureCoordinate.Pre_treeRenderCallbackTextureCoordinate != null) {
            Pre_TextureCoordinate.Pre_treeRenderCallbackTextureCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureCoordinate.Pre_renderCallbackTextureCoordinate != null)
            Pre_TextureCoordinate.Pre_renderCallbackTextureCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureCoordinate.Pre_treeDoWithDataCallbackTextureCoordinate != null) {
            Pre_TextureCoordinate.Pre_treeDoWithDataCallbackTextureCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureCoordinate.Pre_doWithDataCallbackTextureCoordinate != null)
            Pre_TextureCoordinate.Pre_doWithDataCallbackTextureCoordinate.doWithData(this);
    }
}

class Pre_TextureCoordinateRenderCallback extends RenderCallback {}
class Pre_TextureCoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureCoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureCoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureCoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Normal extends Pre_Node {
    Pre_Node metadata;
    float[] vector;
    static Pre_NormalRenderCallback Pre_renderCallbackNormal;
    static void setPre_NormalRenderCallback(Pre_NormalRenderCallback node) {
        Pre_renderCallbackNormal = node;
    }

    static Pre_NormalTreeRenderCallback Pre_treeRenderCallbackNormal;
    static void setPre_NormalTreeRenderCallback(Pre_NormalTreeRenderCallback node) {
        Pre_treeRenderCallbackNormal = node;
    }

    static Pre_NormalDoWithDataCallback Pre_doWithDataCallbackNormal;
    static void setPre_NormalDoWithDataCallback(Pre_NormalDoWithDataCallback node) {
        Pre_doWithDataCallbackNormal = node;
    }

    static Pre_NormalTreeDoWithDataCallback Pre_treeDoWithDataCallbackNormal;
    static void setPre_NormalTreeDoWithDataCallback(Pre_NormalTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNormal = node;

    }

    static Pre_NormalEventsProcessedCallback Pre_eventsProcessedCallbackNormal;
    static void setPre_NormalEventsProcessedCallback(Pre_NormalEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNormal = node;
    }

    Pre_Normal() {
    }
    void treeRender() {
        if (Pre_Normal.Pre_treeRenderCallbackNormal != null) {
            Pre_Normal.Pre_treeRenderCallbackNormal.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Normal.Pre_renderCallbackNormal != null)
            Pre_Normal.Pre_renderCallbackNormal.render(this);
    }
    void treeDoWithData() {
        if (Pre_Normal.Pre_treeDoWithDataCallbackNormal != null) {
            Pre_Normal.Pre_treeDoWithDataCallbackNormal.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Normal.Pre_doWithDataCallbackNormal != null)
            Pre_Normal.Pre_doWithDataCallbackNormal.doWithData(this);
    }
}

class Pre_NormalRenderCallback extends RenderCallback {}
class Pre_NormalTreeRenderCallback extends TreeRenderCallback {}
class Pre_NormalDoWithDataCallback extends DoWithDataCallback {};
class Pre_NormalTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NormalEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ViewpointGroup extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    Pre_Node [] children;
    String description;
    boolean displayed;
    boolean retainUserOffsets;
    float[] size;
    static Pre_ViewpointGroupRenderCallback Pre_renderCallbackViewpointGroup;
    static void setPre_ViewpointGroupRenderCallback(Pre_ViewpointGroupRenderCallback node) {
        Pre_renderCallbackViewpointGroup = node;
    }

    static Pre_ViewpointGroupTreeRenderCallback Pre_treeRenderCallbackViewpointGroup;
    static void setPre_ViewpointGroupTreeRenderCallback(Pre_ViewpointGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackViewpointGroup = node;
    }

    static Pre_ViewpointGroupDoWithDataCallback Pre_doWithDataCallbackViewpointGroup;
    static void setPre_ViewpointGroupDoWithDataCallback(Pre_ViewpointGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackViewpointGroup = node;
    }

    static Pre_ViewpointGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackViewpointGroup;
    static void setPre_ViewpointGroupTreeDoWithDataCallback(Pre_ViewpointGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackViewpointGroup = node;

    }

    static Pre_ViewpointGroupEventsProcessedCallback Pre_eventsProcessedCallbackViewpointGroup;
    static void setPre_ViewpointGroupEventsProcessedCallback(Pre_ViewpointGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackViewpointGroup = node;
    }

    Pre_ViewpointGroup() {
    }
    void treeRender() {
        if (Pre_ViewpointGroup.Pre_treeRenderCallbackViewpointGroup != null) {
            Pre_ViewpointGroup.Pre_treeRenderCallbackViewpointGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ViewpointGroup.Pre_renderCallbackViewpointGroup != null)
            Pre_ViewpointGroup.Pre_renderCallbackViewpointGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_ViewpointGroup.Pre_treeDoWithDataCallbackViewpointGroup != null) {
            Pre_ViewpointGroup.Pre_treeDoWithDataCallbackViewpointGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ViewpointGroup.Pre_doWithDataCallbackViewpointGroup != null)
            Pre_ViewpointGroup.Pre_doWithDataCallbackViewpointGroup.doWithData(this);
    }
}

class Pre_ViewpointGroupRenderCallback extends RenderCallback {}
class Pre_ViewpointGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_ViewpointGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_ViewpointGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ViewpointGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BooleanFilter extends Pre_Node {
    Pre_Node metadata;
    static Pre_BooleanFilterRenderCallback Pre_renderCallbackBooleanFilter;
    static void setPre_BooleanFilterRenderCallback(Pre_BooleanFilterRenderCallback node) {
        Pre_renderCallbackBooleanFilter = node;
    }

    static Pre_BooleanFilterTreeRenderCallback Pre_treeRenderCallbackBooleanFilter;
    static void setPre_BooleanFilterTreeRenderCallback(Pre_BooleanFilterTreeRenderCallback node) {
        Pre_treeRenderCallbackBooleanFilter = node;
    }

    static Pre_BooleanFilterDoWithDataCallback Pre_doWithDataCallbackBooleanFilter;
    static void setPre_BooleanFilterDoWithDataCallback(Pre_BooleanFilterDoWithDataCallback node) {
        Pre_doWithDataCallbackBooleanFilter = node;
    }

    static Pre_BooleanFilterTreeDoWithDataCallback Pre_treeDoWithDataCallbackBooleanFilter;
    static void setPre_BooleanFilterTreeDoWithDataCallback(Pre_BooleanFilterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBooleanFilter = node;

    }

    static Pre_BooleanFilterEventsProcessedCallback Pre_eventsProcessedCallbackBooleanFilter;
    static void setPre_BooleanFilterEventsProcessedCallback(Pre_BooleanFilterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBooleanFilter = node;
    }

    Pre_BooleanFilter() {
    }
    void treeRender() {
        if (Pre_BooleanFilter.Pre_treeRenderCallbackBooleanFilter != null) {
            Pre_BooleanFilter.Pre_treeRenderCallbackBooleanFilter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BooleanFilter.Pre_renderCallbackBooleanFilter != null)
            Pre_BooleanFilter.Pre_renderCallbackBooleanFilter.render(this);
    }
    void treeDoWithData() {
        if (Pre_BooleanFilter.Pre_treeDoWithDataCallbackBooleanFilter != null) {
            Pre_BooleanFilter.Pre_treeDoWithDataCallbackBooleanFilter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BooleanFilter.Pre_doWithDataCallbackBooleanFilter != null)
            Pre_BooleanFilter.Pre_doWithDataCallbackBooleanFilter.doWithData(this);
    }
}

class Pre_BooleanFilterRenderCallback extends RenderCallback {}
class Pre_BooleanFilterTreeRenderCallback extends TreeRenderCallback {}
class Pre_BooleanFilterDoWithDataCallback extends DoWithDataCallback {};
class Pre_BooleanFilterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BooleanFilterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Billboard extends Pre_Node {
    Pre_Node metadata;
    float[] axisOfRotation;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_BillboardRenderCallback Pre_renderCallbackBillboard;
    static void setPre_BillboardRenderCallback(Pre_BillboardRenderCallback node) {
        Pre_renderCallbackBillboard = node;
    }

    static Pre_BillboardTreeRenderCallback Pre_treeRenderCallbackBillboard;
    static void setPre_BillboardTreeRenderCallback(Pre_BillboardTreeRenderCallback node) {
        Pre_treeRenderCallbackBillboard = node;
    }

    static Pre_BillboardDoWithDataCallback Pre_doWithDataCallbackBillboard;
    static void setPre_BillboardDoWithDataCallback(Pre_BillboardDoWithDataCallback node) {
        Pre_doWithDataCallbackBillboard = node;
    }

    static Pre_BillboardTreeDoWithDataCallback Pre_treeDoWithDataCallbackBillboard;
    static void setPre_BillboardTreeDoWithDataCallback(Pre_BillboardTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBillboard = node;

    }

    static Pre_BillboardEventsProcessedCallback Pre_eventsProcessedCallbackBillboard;
    static void setPre_BillboardEventsProcessedCallback(Pre_BillboardEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBillboard = node;
    }

    Pre_Billboard() {
    }
    void treeRender() {
        if (Pre_Billboard.Pre_treeRenderCallbackBillboard != null) {
            Pre_Billboard.Pre_treeRenderCallbackBillboard.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Billboard.Pre_renderCallbackBillboard != null)
            Pre_Billboard.Pre_renderCallbackBillboard.render(this);
    }
    void treeDoWithData() {
        if (Pre_Billboard.Pre_treeDoWithDataCallbackBillboard != null) {
            Pre_Billboard.Pre_treeDoWithDataCallbackBillboard.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Billboard.Pre_doWithDataCallbackBillboard != null)
            Pre_Billboard.Pre_doWithDataCallbackBillboard.doWithData(this);
    }
}

class Pre_BillboardRenderCallback extends RenderCallback {}
class Pre_BillboardTreeRenderCallback extends TreeRenderCallback {}
class Pre_BillboardDoWithDataCallback extends DoWithDataCallback {};
class Pre_BillboardTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BillboardEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SpotLight extends Pre_Node {
    Pre_Node metadata;
    float ambientIntensity;
    float[] attenuation;
    float beamWidth;
    float[] color;
    float cutOffAngle;
    float[] direction;
    boolean global;
    float intensity;
    float[] location;
    boolean on;
    float radius;
    boolean shadows;
    float projectionNear;
    float projectionFar;
    float[] up;
    Pre_Node defaultShadowMap;
    boolean kambiShadows;
    boolean kambiShadowsMain;
    float projectionAngle;
    static Pre_SpotLightRenderCallback Pre_renderCallbackSpotLight;
    static void setPre_SpotLightRenderCallback(Pre_SpotLightRenderCallback node) {
        Pre_renderCallbackSpotLight = node;
    }

    static Pre_SpotLightTreeRenderCallback Pre_treeRenderCallbackSpotLight;
    static void setPre_SpotLightTreeRenderCallback(Pre_SpotLightTreeRenderCallback node) {
        Pre_treeRenderCallbackSpotLight = node;
    }

    static Pre_SpotLightDoWithDataCallback Pre_doWithDataCallbackSpotLight;
    static void setPre_SpotLightDoWithDataCallback(Pre_SpotLightDoWithDataCallback node) {
        Pre_doWithDataCallbackSpotLight = node;
    }

    static Pre_SpotLightTreeDoWithDataCallback Pre_treeDoWithDataCallbackSpotLight;
    static void setPre_SpotLightTreeDoWithDataCallback(Pre_SpotLightTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSpotLight = node;

    }

    static Pre_SpotLightEventsProcessedCallback Pre_eventsProcessedCallbackSpotLight;
    static void setPre_SpotLightEventsProcessedCallback(Pre_SpotLightEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSpotLight = node;
    }

    Pre_SpotLight() {
    }
    void treeRender() {
        if (Pre_SpotLight.Pre_treeRenderCallbackSpotLight != null) {
            Pre_SpotLight.Pre_treeRenderCallbackSpotLight.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (defaultShadowMap != null)
            defaultShadowMap.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SpotLight.Pre_renderCallbackSpotLight != null)
            Pre_SpotLight.Pre_renderCallbackSpotLight.render(this);
    }
    void treeDoWithData() {
        if (Pre_SpotLight.Pre_treeDoWithDataCallbackSpotLight != null) {
            Pre_SpotLight.Pre_treeDoWithDataCallbackSpotLight.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (defaultShadowMap != null)
            defaultShadowMap.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SpotLight.Pre_doWithDataCallbackSpotLight != null)
            Pre_SpotLight.Pre_doWithDataCallbackSpotLight.doWithData(this);
    }
}

class Pre_SpotLightRenderCallback extends RenderCallback {}
class Pre_SpotLightTreeRenderCallback extends TreeRenderCallback {}
class Pre_SpotLightDoWithDataCallback extends DoWithDataCallback {};
class Pre_SpotLightTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SpotLightEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_FogCoordinate extends Pre_Node {
    Pre_Node metadata;
    float[] depth;
    static Pre_FogCoordinateRenderCallback Pre_renderCallbackFogCoordinate;
    static void setPre_FogCoordinateRenderCallback(Pre_FogCoordinateRenderCallback node) {
        Pre_renderCallbackFogCoordinate = node;
    }

    static Pre_FogCoordinateTreeRenderCallback Pre_treeRenderCallbackFogCoordinate;
    static void setPre_FogCoordinateTreeRenderCallback(Pre_FogCoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackFogCoordinate = node;
    }

    static Pre_FogCoordinateDoWithDataCallback Pre_doWithDataCallbackFogCoordinate;
    static void setPre_FogCoordinateDoWithDataCallback(Pre_FogCoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackFogCoordinate = node;
    }

    static Pre_FogCoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackFogCoordinate;
    static void setPre_FogCoordinateTreeDoWithDataCallback(Pre_FogCoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackFogCoordinate = node;

    }

    static Pre_FogCoordinateEventsProcessedCallback Pre_eventsProcessedCallbackFogCoordinate;
    static void setPre_FogCoordinateEventsProcessedCallback(Pre_FogCoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackFogCoordinate = node;
    }

    Pre_FogCoordinate() {
    }
    void treeRender() {
        if (Pre_FogCoordinate.Pre_treeRenderCallbackFogCoordinate != null) {
            Pre_FogCoordinate.Pre_treeRenderCallbackFogCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_FogCoordinate.Pre_renderCallbackFogCoordinate != null)
            Pre_FogCoordinate.Pre_renderCallbackFogCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_FogCoordinate.Pre_treeDoWithDataCallbackFogCoordinate != null) {
            Pre_FogCoordinate.Pre_treeDoWithDataCallbackFogCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_FogCoordinate.Pre_doWithDataCallbackFogCoordinate != null)
            Pre_FogCoordinate.Pre_doWithDataCallbackFogCoordinate.doWithData(this);
    }
}

class Pre_FogCoordinateRenderCallback extends RenderCallback {}
class Pre_FogCoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_FogCoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_FogCoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_FogCoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoCoordinate extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    double[] point;
    static Pre_GeoCoordinateRenderCallback Pre_renderCallbackGeoCoordinate;
    static void setPre_GeoCoordinateRenderCallback(Pre_GeoCoordinateRenderCallback node) {
        Pre_renderCallbackGeoCoordinate = node;
    }

    static Pre_GeoCoordinateTreeRenderCallback Pre_treeRenderCallbackGeoCoordinate;
    static void setPre_GeoCoordinateTreeRenderCallback(Pre_GeoCoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoCoordinate = node;
    }

    static Pre_GeoCoordinateDoWithDataCallback Pre_doWithDataCallbackGeoCoordinate;
    static void setPre_GeoCoordinateDoWithDataCallback(Pre_GeoCoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoCoordinate = node;
    }

    static Pre_GeoCoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoCoordinate;
    static void setPre_GeoCoordinateTreeDoWithDataCallback(Pre_GeoCoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoCoordinate = node;

    }

    static Pre_GeoCoordinateEventsProcessedCallback Pre_eventsProcessedCallbackGeoCoordinate;
    static void setPre_GeoCoordinateEventsProcessedCallback(Pre_GeoCoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoCoordinate = node;
    }

    Pre_GeoCoordinate() {
    }
    void treeRender() {
        if (Pre_GeoCoordinate.Pre_treeRenderCallbackGeoCoordinate != null) {
            Pre_GeoCoordinate.Pre_treeRenderCallbackGeoCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoCoordinate.Pre_renderCallbackGeoCoordinate != null)
            Pre_GeoCoordinate.Pre_renderCallbackGeoCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoCoordinate.Pre_treeDoWithDataCallbackGeoCoordinate != null) {
            Pre_GeoCoordinate.Pre_treeDoWithDataCallbackGeoCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoCoordinate.Pre_doWithDataCallbackGeoCoordinate != null)
            Pre_GeoCoordinate.Pre_doWithDataCallbackGeoCoordinate.doWithData(this);
    }
}

class Pre_GeoCoordinateRenderCallback extends RenderCallback {}
class Pre_GeoCoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoCoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoCoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoCoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Sphere extends Pre_Node {
    Pre_Node metadata;
    float radius;
    boolean solid;
    Pre_Node texCoord;
    static Pre_SphereRenderCallback Pre_renderCallbackSphere;
    static void setPre_SphereRenderCallback(Pre_SphereRenderCallback node) {
        Pre_renderCallbackSphere = node;
    }

    static Pre_SphereTreeRenderCallback Pre_treeRenderCallbackSphere;
    static void setPre_SphereTreeRenderCallback(Pre_SphereTreeRenderCallback node) {
        Pre_treeRenderCallbackSphere = node;
    }

    static Pre_SphereDoWithDataCallback Pre_doWithDataCallbackSphere;
    static void setPre_SphereDoWithDataCallback(Pre_SphereDoWithDataCallback node) {
        Pre_doWithDataCallbackSphere = node;
    }

    static Pre_SphereTreeDoWithDataCallback Pre_treeDoWithDataCallbackSphere;
    static void setPre_SphereTreeDoWithDataCallback(Pre_SphereTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSphere = node;

    }

    static Pre_SphereEventsProcessedCallback Pre_eventsProcessedCallbackSphere;
    static void setPre_SphereEventsProcessedCallback(Pre_SphereEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSphere = node;
    }

    Pre_Sphere() {
    }
    void treeRender() {
        if (Pre_Sphere.Pre_treeRenderCallbackSphere != null) {
            Pre_Sphere.Pre_treeRenderCallbackSphere.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Sphere.Pre_renderCallbackSphere != null)
            Pre_Sphere.Pre_renderCallbackSphere.render(this);
    }
    void treeDoWithData() {
        if (Pre_Sphere.Pre_treeDoWithDataCallbackSphere != null) {
            Pre_Sphere.Pre_treeDoWithDataCallbackSphere.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Sphere.Pre_doWithDataCallbackSphere != null)
            Pre_Sphere.Pre_doWithDataCallbackSphere.doWithData(this);
    }
}

class Pre_SphereRenderCallback extends RenderCallback {}
class Pre_SphereTreeRenderCallback extends TreeRenderCallback {}
class Pre_SphereDoWithDataCallback extends DoWithDataCallback {};
class Pre_SphereTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SphereEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_RigidBodyCollection extends Pre_Node {
    Pre_Node metadata;
    boolean autoDisable;
    Pre_Node [] bodies;
    float constantForceMix;
    float contactSurfaceThickness;
    float disableAngularSpeed;
    float disableLinearSpeed;
    float disableTime;
    boolean enabled;
    float errorCorrection;
    float[] gravity;
    int iterations;
    Pre_Node [] joints;
    float maxCorrectionSpeed;
    boolean preferAccuracy;
    Pre_Node collider;
    static Pre_RigidBodyCollectionRenderCallback Pre_renderCallbackRigidBodyCollection;
    static void setPre_RigidBodyCollectionRenderCallback(Pre_RigidBodyCollectionRenderCallback node) {
        Pre_renderCallbackRigidBodyCollection = node;
    }

    static Pre_RigidBodyCollectionTreeRenderCallback Pre_treeRenderCallbackRigidBodyCollection;
    static void setPre_RigidBodyCollectionTreeRenderCallback(Pre_RigidBodyCollectionTreeRenderCallback node) {
        Pre_treeRenderCallbackRigidBodyCollection = node;
    }

    static Pre_RigidBodyCollectionDoWithDataCallback Pre_doWithDataCallbackRigidBodyCollection;
    static void setPre_RigidBodyCollectionDoWithDataCallback(Pre_RigidBodyCollectionDoWithDataCallback node) {
        Pre_doWithDataCallbackRigidBodyCollection = node;
    }

    static Pre_RigidBodyCollectionTreeDoWithDataCallback Pre_treeDoWithDataCallbackRigidBodyCollection;
    static void setPre_RigidBodyCollectionTreeDoWithDataCallback(Pre_RigidBodyCollectionTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackRigidBodyCollection = node;

    }

    static Pre_RigidBodyCollectionEventsProcessedCallback Pre_eventsProcessedCallbackRigidBodyCollection;
    static void setPre_RigidBodyCollectionEventsProcessedCallback(Pre_RigidBodyCollectionEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackRigidBodyCollection = node;
    }

    Pre_RigidBodyCollection() {
    }
    void treeRender() {
        if (Pre_RigidBodyCollection.Pre_treeRenderCallbackRigidBodyCollection != null) {
            Pre_RigidBodyCollection.Pre_treeRenderCallbackRigidBodyCollection.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (bodies != null)
            for (int i = 0; i < bodies.length; i++)
                if (bodies[i] != null)
                    bodies[i].treeRender();
        if (joints != null)
            for (int i = 0; i < joints.length; i++)
                if (joints[i] != null)
                    joints[i].treeRender();
        if (collider != null)
            collider.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_RigidBodyCollection.Pre_renderCallbackRigidBodyCollection != null)
            Pre_RigidBodyCollection.Pre_renderCallbackRigidBodyCollection.render(this);
    }
    void treeDoWithData() {
        if (Pre_RigidBodyCollection.Pre_treeDoWithDataCallbackRigidBodyCollection != null) {
            Pre_RigidBodyCollection.Pre_treeDoWithDataCallbackRigidBodyCollection.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (bodies != null)
            for (int i = 0; i < bodies.length; i++)
                if (bodies[i] != null)
                    bodies[i].treeDoWithData();
        if (joints != null)
            for (int i = 0; i < joints.length; i++)
                if (joints[i] != null)
                    joints[i].treeDoWithData();
        if (collider != null)
            collider.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_RigidBodyCollection.Pre_doWithDataCallbackRigidBodyCollection != null)
            Pre_RigidBodyCollection.Pre_doWithDataCallbackRigidBodyCollection.doWithData(this);
    }
}

class Pre_RigidBodyCollectionRenderCallback extends RenderCallback {}
class Pre_RigidBodyCollectionTreeRenderCallback extends TreeRenderCallback {}
class Pre_RigidBodyCollectionDoWithDataCallback extends DoWithDataCallback {};
class Pre_RigidBodyCollectionTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_RigidBodyCollectionEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoMetadata extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] data;
    String[] summary;
    String[] url;
    static Pre_GeoMetadataRenderCallback Pre_renderCallbackGeoMetadata;
    static void setPre_GeoMetadataRenderCallback(Pre_GeoMetadataRenderCallback node) {
        Pre_renderCallbackGeoMetadata = node;
    }

    static Pre_GeoMetadataTreeRenderCallback Pre_treeRenderCallbackGeoMetadata;
    static void setPre_GeoMetadataTreeRenderCallback(Pre_GeoMetadataTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoMetadata = node;
    }

    static Pre_GeoMetadataDoWithDataCallback Pre_doWithDataCallbackGeoMetadata;
    static void setPre_GeoMetadataDoWithDataCallback(Pre_GeoMetadataDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoMetadata = node;
    }

    static Pre_GeoMetadataTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoMetadata;
    static void setPre_GeoMetadataTreeDoWithDataCallback(Pre_GeoMetadataTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoMetadata = node;

    }

    static Pre_GeoMetadataEventsProcessedCallback Pre_eventsProcessedCallbackGeoMetadata;
    static void setPre_GeoMetadataEventsProcessedCallback(Pre_GeoMetadataEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoMetadata = node;
    }

    Pre_GeoMetadata() {
    }
    void treeRender() {
        if (Pre_GeoMetadata.Pre_treeRenderCallbackGeoMetadata != null) {
            Pre_GeoMetadata.Pre_treeRenderCallbackGeoMetadata.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (data != null)
            for (int i = 0; i < data.length; i++)
                if (data[i] != null)
                    data[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoMetadata.Pre_renderCallbackGeoMetadata != null)
            Pre_GeoMetadata.Pre_renderCallbackGeoMetadata.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoMetadata.Pre_treeDoWithDataCallbackGeoMetadata != null) {
            Pre_GeoMetadata.Pre_treeDoWithDataCallbackGeoMetadata.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (data != null)
            for (int i = 0; i < data.length; i++)
                if (data[i] != null)
                    data[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoMetadata.Pre_doWithDataCallbackGeoMetadata != null)
            Pre_GeoMetadata.Pre_doWithDataCallbackGeoMetadata.doWithData(this);
    }
}

class Pre_GeoMetadataRenderCallback extends RenderCallback {}
class Pre_GeoMetadataTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoMetadataDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoMetadataTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoMetadataEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ColorInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_ColorInterpolatorRenderCallback Pre_renderCallbackColorInterpolator;
    static void setPre_ColorInterpolatorRenderCallback(Pre_ColorInterpolatorRenderCallback node) {
        Pre_renderCallbackColorInterpolator = node;
    }

    static Pre_ColorInterpolatorTreeRenderCallback Pre_treeRenderCallbackColorInterpolator;
    static void setPre_ColorInterpolatorTreeRenderCallback(Pre_ColorInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackColorInterpolator = node;
    }

    static Pre_ColorInterpolatorDoWithDataCallback Pre_doWithDataCallbackColorInterpolator;
    static void setPre_ColorInterpolatorDoWithDataCallback(Pre_ColorInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackColorInterpolator = node;
    }

    static Pre_ColorInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackColorInterpolator;
    static void setPre_ColorInterpolatorTreeDoWithDataCallback(Pre_ColorInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackColorInterpolator = node;

    }

    static Pre_ColorInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackColorInterpolator;
    static void setPre_ColorInterpolatorEventsProcessedCallback(Pre_ColorInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackColorInterpolator = node;
    }

    Pre_ColorInterpolator() {
    }
    void treeRender() {
        if (Pre_ColorInterpolator.Pre_treeRenderCallbackColorInterpolator != null) {
            Pre_ColorInterpolator.Pre_treeRenderCallbackColorInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ColorInterpolator.Pre_renderCallbackColorInterpolator != null)
            Pre_ColorInterpolator.Pre_renderCallbackColorInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_ColorInterpolator.Pre_treeDoWithDataCallbackColorInterpolator != null) {
            Pre_ColorInterpolator.Pre_treeDoWithDataCallbackColorInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ColorInterpolator.Pre_doWithDataCallbackColorInterpolator != null)
            Pre_ColorInterpolator.Pre_doWithDataCallbackColorInterpolator.doWithData(this);
    }
}

class Pre_ColorInterpolatorRenderCallback extends RenderCallback {}
class Pre_ColorInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_ColorInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_ColorInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ColorInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ExplosionEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    float[] position;
    static Pre_ExplosionEmitterRenderCallback Pre_renderCallbackExplosionEmitter;
    static void setPre_ExplosionEmitterRenderCallback(Pre_ExplosionEmitterRenderCallback node) {
        Pre_renderCallbackExplosionEmitter = node;
    }

    static Pre_ExplosionEmitterTreeRenderCallback Pre_treeRenderCallbackExplosionEmitter;
    static void setPre_ExplosionEmitterTreeRenderCallback(Pre_ExplosionEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackExplosionEmitter = node;
    }

    static Pre_ExplosionEmitterDoWithDataCallback Pre_doWithDataCallbackExplosionEmitter;
    static void setPre_ExplosionEmitterDoWithDataCallback(Pre_ExplosionEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackExplosionEmitter = node;
    }

    static Pre_ExplosionEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackExplosionEmitter;
    static void setPre_ExplosionEmitterTreeDoWithDataCallback(Pre_ExplosionEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackExplosionEmitter = node;

    }

    static Pre_ExplosionEmitterEventsProcessedCallback Pre_eventsProcessedCallbackExplosionEmitter;
    static void setPre_ExplosionEmitterEventsProcessedCallback(Pre_ExplosionEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackExplosionEmitter = node;
    }

    Pre_ExplosionEmitter() {
    }
    void treeRender() {
        if (Pre_ExplosionEmitter.Pre_treeRenderCallbackExplosionEmitter != null) {
            Pre_ExplosionEmitter.Pre_treeRenderCallbackExplosionEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ExplosionEmitter.Pre_renderCallbackExplosionEmitter != null)
            Pre_ExplosionEmitter.Pre_renderCallbackExplosionEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_ExplosionEmitter.Pre_treeDoWithDataCallbackExplosionEmitter != null) {
            Pre_ExplosionEmitter.Pre_treeDoWithDataCallbackExplosionEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ExplosionEmitter.Pre_doWithDataCallbackExplosionEmitter != null)
            Pre_ExplosionEmitter.Pre_doWithDataCallbackExplosionEmitter.doWithData(this);
    }
}

class Pre_ExplosionEmitterRenderCallback extends RenderCallback {}
class Pre_ExplosionEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_ExplosionEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_ExplosionEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ExplosionEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LineProperties extends Pre_Node {
    Pre_Node metadata;
    boolean applied;
    int linetype;
    float linewidthScaleFactor;
    static Pre_LinePropertiesRenderCallback Pre_renderCallbackLineProperties;
    static void setPre_LinePropertiesRenderCallback(Pre_LinePropertiesRenderCallback node) {
        Pre_renderCallbackLineProperties = node;
    }

    static Pre_LinePropertiesTreeRenderCallback Pre_treeRenderCallbackLineProperties;
    static void setPre_LinePropertiesTreeRenderCallback(Pre_LinePropertiesTreeRenderCallback node) {
        Pre_treeRenderCallbackLineProperties = node;
    }

    static Pre_LinePropertiesDoWithDataCallback Pre_doWithDataCallbackLineProperties;
    static void setPre_LinePropertiesDoWithDataCallback(Pre_LinePropertiesDoWithDataCallback node) {
        Pre_doWithDataCallbackLineProperties = node;
    }

    static Pre_LinePropertiesTreeDoWithDataCallback Pre_treeDoWithDataCallbackLineProperties;
    static void setPre_LinePropertiesTreeDoWithDataCallback(Pre_LinePropertiesTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLineProperties = node;

    }

    static Pre_LinePropertiesEventsProcessedCallback Pre_eventsProcessedCallbackLineProperties;
    static void setPre_LinePropertiesEventsProcessedCallback(Pre_LinePropertiesEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLineProperties = node;
    }

    Pre_LineProperties() {
    }
    void treeRender() {
        if (Pre_LineProperties.Pre_treeRenderCallbackLineProperties != null) {
            Pre_LineProperties.Pre_treeRenderCallbackLineProperties.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LineProperties.Pre_renderCallbackLineProperties != null)
            Pre_LineProperties.Pre_renderCallbackLineProperties.render(this);
    }
    void treeDoWithData() {
        if (Pre_LineProperties.Pre_treeDoWithDataCallbackLineProperties != null) {
            Pre_LineProperties.Pre_treeDoWithDataCallbackLineProperties.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LineProperties.Pre_doWithDataCallbackLineProperties != null)
            Pre_LineProperties.Pre_doWithDataCallbackLineProperties.doWithData(this);
    }
}

class Pre_LinePropertiesRenderCallback extends RenderCallback {}
class Pre_LinePropertiesTreeRenderCallback extends TreeRenderCallback {}
class Pre_LinePropertiesDoWithDataCallback extends DoWithDataCallback {};
class Pre_LinePropertiesTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LinePropertiesEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LineSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] attrib;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node fogCoord;
    int[] vertexCount;
    static Pre_LineSetRenderCallback Pre_renderCallbackLineSet;
    static void setPre_LineSetRenderCallback(Pre_LineSetRenderCallback node) {
        Pre_renderCallbackLineSet = node;
    }

    static Pre_LineSetTreeRenderCallback Pre_treeRenderCallbackLineSet;
    static void setPre_LineSetTreeRenderCallback(Pre_LineSetTreeRenderCallback node) {
        Pre_treeRenderCallbackLineSet = node;
    }

    static Pre_LineSetDoWithDataCallback Pre_doWithDataCallbackLineSet;
    static void setPre_LineSetDoWithDataCallback(Pre_LineSetDoWithDataCallback node) {
        Pre_doWithDataCallbackLineSet = node;
    }

    static Pre_LineSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackLineSet;
    static void setPre_LineSetTreeDoWithDataCallback(Pre_LineSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLineSet = node;

    }

    static Pre_LineSetEventsProcessedCallback Pre_eventsProcessedCallbackLineSet;
    static void setPre_LineSetEventsProcessedCallback(Pre_LineSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLineSet = node;
    }

    Pre_LineSet() {
    }
    void treeRender() {
        if (Pre_LineSet.Pre_treeRenderCallbackLineSet != null) {
            Pre_LineSet.Pre_treeRenderCallbackLineSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LineSet.Pre_renderCallbackLineSet != null)
            Pre_LineSet.Pre_renderCallbackLineSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_LineSet.Pre_treeDoWithDataCallbackLineSet != null) {
            Pre_LineSet.Pre_treeDoWithDataCallbackLineSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LineSet.Pre_doWithDataCallbackLineSet != null)
            Pre_LineSet.Pre_doWithDataCallbackLineSet.doWithData(this);
    }
}

class Pre_LineSetRenderCallback extends RenderCallback {}
class Pre_LineSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_LineSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_LineSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LineSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] geometry;
    float tessellationScale;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_NurbsSetRenderCallback Pre_renderCallbackNurbsSet;
    static void setPre_NurbsSetRenderCallback(Pre_NurbsSetRenderCallback node) {
        Pre_renderCallbackNurbsSet = node;
    }

    static Pre_NurbsSetTreeRenderCallback Pre_treeRenderCallbackNurbsSet;
    static void setPre_NurbsSetTreeRenderCallback(Pre_NurbsSetTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsSet = node;
    }

    static Pre_NurbsSetDoWithDataCallback Pre_doWithDataCallbackNurbsSet;
    static void setPre_NurbsSetDoWithDataCallback(Pre_NurbsSetDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsSet = node;
    }

    static Pre_NurbsSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsSet;
    static void setPre_NurbsSetTreeDoWithDataCallback(Pre_NurbsSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsSet = node;

    }

    static Pre_NurbsSetEventsProcessedCallback Pre_eventsProcessedCallbackNurbsSet;
    static void setPre_NurbsSetEventsProcessedCallback(Pre_NurbsSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsSet = node;
    }

    Pre_NurbsSet() {
    }
    void treeRender() {
        if (Pre_NurbsSet.Pre_treeRenderCallbackNurbsSet != null) {
            Pre_NurbsSet.Pre_treeRenderCallbackNurbsSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geometry != null)
            for (int i = 0; i < geometry.length; i++)
                if (geometry[i] != null)
                    geometry[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsSet.Pre_renderCallbackNurbsSet != null)
            Pre_NurbsSet.Pre_renderCallbackNurbsSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsSet.Pre_treeDoWithDataCallbackNurbsSet != null) {
            Pre_NurbsSet.Pre_treeDoWithDataCallbackNurbsSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geometry != null)
            for (int i = 0; i < geometry.length; i++)
                if (geometry[i] != null)
                    geometry[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsSet.Pre_doWithDataCallbackNurbsSet != null)
            Pre_NurbsSet.Pre_doWithDataCallbackNurbsSet.doWithData(this);
    }
}

class Pre_NurbsSetRenderCallback extends RenderCallback {}
class Pre_NurbsSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_QuadSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    boolean normalPerVertex;
    boolean solid;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_QuadSetRenderCallback Pre_renderCallbackQuadSet;
    static void setPre_QuadSetRenderCallback(Pre_QuadSetRenderCallback node) {
        Pre_renderCallbackQuadSet = node;
    }

    static Pre_QuadSetTreeRenderCallback Pre_treeRenderCallbackQuadSet;
    static void setPre_QuadSetTreeRenderCallback(Pre_QuadSetTreeRenderCallback node) {
        Pre_treeRenderCallbackQuadSet = node;
    }

    static Pre_QuadSetDoWithDataCallback Pre_doWithDataCallbackQuadSet;
    static void setPre_QuadSetDoWithDataCallback(Pre_QuadSetDoWithDataCallback node) {
        Pre_doWithDataCallbackQuadSet = node;
    }

    static Pre_QuadSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackQuadSet;
    static void setPre_QuadSetTreeDoWithDataCallback(Pre_QuadSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackQuadSet = node;

    }

    static Pre_QuadSetEventsProcessedCallback Pre_eventsProcessedCallbackQuadSet;
    static void setPre_QuadSetEventsProcessedCallback(Pre_QuadSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackQuadSet = node;
    }

    Pre_QuadSet() {
    }
    void treeRender() {
        if (Pre_QuadSet.Pre_treeRenderCallbackQuadSet != null) {
            Pre_QuadSet.Pre_treeRenderCallbackQuadSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_QuadSet.Pre_renderCallbackQuadSet != null)
            Pre_QuadSet.Pre_renderCallbackQuadSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_QuadSet.Pre_treeDoWithDataCallbackQuadSet != null) {
            Pre_QuadSet.Pre_treeDoWithDataCallbackQuadSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_QuadSet.Pre_doWithDataCallbackQuadSet != null)
            Pre_QuadSet.Pre_doWithDataCallbackQuadSet.doWithData(this);
    }
}

class Pre_QuadSetRenderCallback extends RenderCallback {}
class Pre_QuadSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_QuadSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_QuadSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_QuadSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TriangleStripSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node normal;
    int[] stripCount;
    Pre_Node texCoord;
    boolean ccw;
    boolean colorPerVertex;
    float creaseAngle;
    boolean normalPerVertex;
    boolean solid;
    float[] radianceTransfer;
    Pre_Node [] attrib;
    Pre_Node fogCoord;
    static Pre_TriangleStripSetRenderCallback Pre_renderCallbackTriangleStripSet;
    static void setPre_TriangleStripSetRenderCallback(Pre_TriangleStripSetRenderCallback node) {
        Pre_renderCallbackTriangleStripSet = node;
    }

    static Pre_TriangleStripSetTreeRenderCallback Pre_treeRenderCallbackTriangleStripSet;
    static void setPre_TriangleStripSetTreeRenderCallback(Pre_TriangleStripSetTreeRenderCallback node) {
        Pre_treeRenderCallbackTriangleStripSet = node;
    }

    static Pre_TriangleStripSetDoWithDataCallback Pre_doWithDataCallbackTriangleStripSet;
    static void setPre_TriangleStripSetDoWithDataCallback(Pre_TriangleStripSetDoWithDataCallback node) {
        Pre_doWithDataCallbackTriangleStripSet = node;
    }

    static Pre_TriangleStripSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackTriangleStripSet;
    static void setPre_TriangleStripSetTreeDoWithDataCallback(Pre_TriangleStripSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTriangleStripSet = node;

    }

    static Pre_TriangleStripSetEventsProcessedCallback Pre_eventsProcessedCallbackTriangleStripSet;
    static void setPre_TriangleStripSetEventsProcessedCallback(Pre_TriangleStripSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTriangleStripSet = node;
    }

    Pre_TriangleStripSet() {
    }
    void treeRender() {
        if (Pre_TriangleStripSet.Pre_treeRenderCallbackTriangleStripSet != null) {
            Pre_TriangleStripSet.Pre_treeRenderCallbackTriangleStripSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (normal != null)
            normal.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TriangleStripSet.Pre_renderCallbackTriangleStripSet != null)
            Pre_TriangleStripSet.Pre_renderCallbackTriangleStripSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_TriangleStripSet.Pre_treeDoWithDataCallbackTriangleStripSet != null) {
            Pre_TriangleStripSet.Pre_treeDoWithDataCallbackTriangleStripSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (normal != null)
            normal.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TriangleStripSet.Pre_doWithDataCallbackTriangleStripSet != null)
            Pre_TriangleStripSet.Pre_doWithDataCallbackTriangleStripSet.doWithData(this);
    }
}

class Pre_TriangleStripSetRenderCallback extends RenderCallback {}
class Pre_TriangleStripSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_TriangleStripSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_TriangleStripSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TriangleStripSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_VrmlCut extends Pre_Node {
    Pre_Node metadata;
    double[] sceneLengths;
    int sceneNumber;
    Pre_Node [] scenes;
    int numberPreviousScenes;
    int numberNextScenes;
    static Pre_VrmlCutRenderCallback Pre_renderCallbackVrmlCut;
    static void setPre_VrmlCutRenderCallback(Pre_VrmlCutRenderCallback node) {
        Pre_renderCallbackVrmlCut = node;
    }

    static Pre_VrmlCutTreeRenderCallback Pre_treeRenderCallbackVrmlCut;
    static void setPre_VrmlCutTreeRenderCallback(Pre_VrmlCutTreeRenderCallback node) {
        Pre_treeRenderCallbackVrmlCut = node;
    }

    static Pre_VrmlCutDoWithDataCallback Pre_doWithDataCallbackVrmlCut;
    static void setPre_VrmlCutDoWithDataCallback(Pre_VrmlCutDoWithDataCallback node) {
        Pre_doWithDataCallbackVrmlCut = node;
    }

    static Pre_VrmlCutTreeDoWithDataCallback Pre_treeDoWithDataCallbackVrmlCut;
    static void setPre_VrmlCutTreeDoWithDataCallback(Pre_VrmlCutTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackVrmlCut = node;

    }

    static Pre_VrmlCutEventsProcessedCallback Pre_eventsProcessedCallbackVrmlCut;
    static void setPre_VrmlCutEventsProcessedCallback(Pre_VrmlCutEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackVrmlCut = node;
    }

    Pre_VrmlCut() {
    }
    void treeRender() {
        if (Pre_VrmlCut.Pre_treeRenderCallbackVrmlCut != null) {
            Pre_VrmlCut.Pre_treeRenderCallbackVrmlCut.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (scenes != null)
            for (int i = 0; i < scenes.length; i++)
                if (scenes[i] != null)
                    scenes[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_VrmlCut.Pre_renderCallbackVrmlCut != null)
            Pre_VrmlCut.Pre_renderCallbackVrmlCut.render(this);
    }
    void treeDoWithData() {
        if (Pre_VrmlCut.Pre_treeDoWithDataCallbackVrmlCut != null) {
            Pre_VrmlCut.Pre_treeDoWithDataCallbackVrmlCut.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (scenes != null)
            for (int i = 0; i < scenes.length; i++)
                if (scenes[i] != null)
                    scenes[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_VrmlCut.Pre_doWithDataCallbackVrmlCut != null)
            Pre_VrmlCut.Pre_doWithDataCallbackVrmlCut.doWithData(this);
    }
}

class Pre_VrmlCutRenderCallback extends RenderCallback {}
class Pre_VrmlCutTreeRenderCallback extends TreeRenderCallback {}
class Pre_VrmlCutDoWithDataCallback extends DoWithDataCallback {};
class Pre_VrmlCutTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_VrmlCutEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CollidableShape extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node shape;
    static Pre_CollidableShapeRenderCallback Pre_renderCallbackCollidableShape;
    static void setPre_CollidableShapeRenderCallback(Pre_CollidableShapeRenderCallback node) {
        Pre_renderCallbackCollidableShape = node;
    }

    static Pre_CollidableShapeTreeRenderCallback Pre_treeRenderCallbackCollidableShape;
    static void setPre_CollidableShapeTreeRenderCallback(Pre_CollidableShapeTreeRenderCallback node) {
        Pre_treeRenderCallbackCollidableShape = node;
    }

    static Pre_CollidableShapeDoWithDataCallback Pre_doWithDataCallbackCollidableShape;
    static void setPre_CollidableShapeDoWithDataCallback(Pre_CollidableShapeDoWithDataCallback node) {
        Pre_doWithDataCallbackCollidableShape = node;
    }

    static Pre_CollidableShapeTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollidableShape;
    static void setPre_CollidableShapeTreeDoWithDataCallback(Pre_CollidableShapeTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollidableShape = node;

    }

    static Pre_CollidableShapeEventsProcessedCallback Pre_eventsProcessedCallbackCollidableShape;
    static void setPre_CollidableShapeEventsProcessedCallback(Pre_CollidableShapeEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollidableShape = node;
    }

    Pre_CollidableShape() {
    }
    void treeRender() {
        if (Pre_CollidableShape.Pre_treeRenderCallbackCollidableShape != null) {
            Pre_CollidableShape.Pre_treeRenderCallbackCollidableShape.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (shape != null)
            shape.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CollidableShape.Pre_renderCallbackCollidableShape != null)
            Pre_CollidableShape.Pre_renderCallbackCollidableShape.render(this);
    }
    void treeDoWithData() {
        if (Pre_CollidableShape.Pre_treeDoWithDataCallbackCollidableShape != null) {
            Pre_CollidableShape.Pre_treeDoWithDataCallbackCollidableShape.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (shape != null)
            shape.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CollidableShape.Pre_doWithDataCallbackCollidableShape != null)
            Pre_CollidableShape.Pre_doWithDataCallbackCollidableShape.doWithData(this);
    }
}

class Pre_CollidableShapeRenderCallback extends RenderCallback {}
class Pre_CollidableShapeTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollidableShapeDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollidableShapeTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollidableShapeEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsSweptSurface extends Pre_Node {
    Pre_Node metadata;
    Pre_Node crossSectionCurve;
    Pre_Node trajectoryCurve;
    boolean ccw;
    boolean solid;
    static Pre_NurbsSweptSurfaceRenderCallback Pre_renderCallbackNurbsSweptSurface;
    static void setPre_NurbsSweptSurfaceRenderCallback(Pre_NurbsSweptSurfaceRenderCallback node) {
        Pre_renderCallbackNurbsSweptSurface = node;
    }

    static Pre_NurbsSweptSurfaceTreeRenderCallback Pre_treeRenderCallbackNurbsSweptSurface;
    static void setPre_NurbsSweptSurfaceTreeRenderCallback(Pre_NurbsSweptSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsSweptSurface = node;
    }

    static Pre_NurbsSweptSurfaceDoWithDataCallback Pre_doWithDataCallbackNurbsSweptSurface;
    static void setPre_NurbsSweptSurfaceDoWithDataCallback(Pre_NurbsSweptSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsSweptSurface = node;
    }

    static Pre_NurbsSweptSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsSweptSurface;
    static void setPre_NurbsSweptSurfaceTreeDoWithDataCallback(Pre_NurbsSweptSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsSweptSurface = node;

    }

    static Pre_NurbsSweptSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackNurbsSweptSurface;
    static void setPre_NurbsSweptSurfaceEventsProcessedCallback(Pre_NurbsSweptSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsSweptSurface = node;
    }

    Pre_NurbsSweptSurface() {
    }
    void treeRender() {
        if (Pre_NurbsSweptSurface.Pre_treeRenderCallbackNurbsSweptSurface != null) {
            Pre_NurbsSweptSurface.Pre_treeRenderCallbackNurbsSweptSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (crossSectionCurve != null)
            crossSectionCurve.treeRender();
        if (trajectoryCurve != null)
            trajectoryCurve.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsSweptSurface.Pre_renderCallbackNurbsSweptSurface != null)
            Pre_NurbsSweptSurface.Pre_renderCallbackNurbsSweptSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsSweptSurface.Pre_treeDoWithDataCallbackNurbsSweptSurface != null) {
            Pre_NurbsSweptSurface.Pre_treeDoWithDataCallbackNurbsSweptSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (crossSectionCurve != null)
            crossSectionCurve.treeDoWithData();
        if (trajectoryCurve != null)
            trajectoryCurve.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsSweptSurface.Pre_doWithDataCallbackNurbsSweptSurface != null)
            Pre_NurbsSweptSurface.Pre_doWithDataCallbackNurbsSweptSurface.doWithData(this);
    }
}

class Pre_NurbsSweptSurfaceRenderCallback extends RenderCallback {}
class Pre_NurbsSweptSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsSweptSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsSweptSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsSweptSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PixelTexture extends Pre_Node {
    Pre_Node metadata;
    int[] image;
    boolean repeatS;
    boolean repeatT;
    String alphaChannel;
    Pre_Node textureProperties;
    static Pre_PixelTextureRenderCallback Pre_renderCallbackPixelTexture;
    static void setPre_PixelTextureRenderCallback(Pre_PixelTextureRenderCallback node) {
        Pre_renderCallbackPixelTexture = node;
    }

    static Pre_PixelTextureTreeRenderCallback Pre_treeRenderCallbackPixelTexture;
    static void setPre_PixelTextureTreeRenderCallback(Pre_PixelTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackPixelTexture = node;
    }

    static Pre_PixelTextureDoWithDataCallback Pre_doWithDataCallbackPixelTexture;
    static void setPre_PixelTextureDoWithDataCallback(Pre_PixelTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackPixelTexture = node;
    }

    static Pre_PixelTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackPixelTexture;
    static void setPre_PixelTextureTreeDoWithDataCallback(Pre_PixelTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPixelTexture = node;

    }

    static Pre_PixelTextureEventsProcessedCallback Pre_eventsProcessedCallbackPixelTexture;
    static void setPre_PixelTextureEventsProcessedCallback(Pre_PixelTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPixelTexture = node;
    }

    Pre_PixelTexture() {
    }
    void treeRender() {
        if (Pre_PixelTexture.Pre_treeRenderCallbackPixelTexture != null) {
            Pre_PixelTexture.Pre_treeRenderCallbackPixelTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PixelTexture.Pre_renderCallbackPixelTexture != null)
            Pre_PixelTexture.Pre_renderCallbackPixelTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_PixelTexture.Pre_treeDoWithDataCallbackPixelTexture != null) {
            Pre_PixelTexture.Pre_treeDoWithDataCallbackPixelTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PixelTexture.Pre_doWithDataCallbackPixelTexture != null)
            Pre_PixelTexture.Pre_doWithDataCallbackPixelTexture.doWithData(this);
    }
}

class Pre_PixelTextureRenderCallback extends RenderCallback {}
class Pre_PixelTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_PixelTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_PixelTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PixelTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Switch extends Pre_Node {
    Pre_Node metadata;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node [] children;
    int whichChoice;
    static Pre_SwitchRenderCallback Pre_renderCallbackSwitch;
    static void setPre_SwitchRenderCallback(Pre_SwitchRenderCallback node) {
        Pre_renderCallbackSwitch = node;
    }

    static Pre_SwitchTreeRenderCallback Pre_treeRenderCallbackSwitch;
    static void setPre_SwitchTreeRenderCallback(Pre_SwitchTreeRenderCallback node) {
        Pre_treeRenderCallbackSwitch = node;
    }

    static Pre_SwitchDoWithDataCallback Pre_doWithDataCallbackSwitch;
    static void setPre_SwitchDoWithDataCallback(Pre_SwitchDoWithDataCallback node) {
        Pre_doWithDataCallbackSwitch = node;
    }

    static Pre_SwitchTreeDoWithDataCallback Pre_treeDoWithDataCallbackSwitch;
    static void setPre_SwitchTreeDoWithDataCallback(Pre_SwitchTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSwitch = node;

    }

    static Pre_SwitchEventsProcessedCallback Pre_eventsProcessedCallbackSwitch;
    static void setPre_SwitchEventsProcessedCallback(Pre_SwitchEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSwitch = node;
    }

    Pre_Switch() {
    }
    void treeRender() {
        if (Pre_Switch.Pre_treeRenderCallbackSwitch != null) {
            Pre_Switch.Pre_treeRenderCallbackSwitch.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Switch.Pre_renderCallbackSwitch != null)
            Pre_Switch.Pre_renderCallbackSwitch.render(this);
    }
    void treeDoWithData() {
        if (Pre_Switch.Pre_treeDoWithDataCallbackSwitch != null) {
            Pre_Switch.Pre_treeDoWithDataCallbackSwitch.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Switch.Pre_doWithDataCallbackSwitch != null)
            Pre_Switch.Pre_doWithDataCallbackSwitch.doWithData(this);
    }
}

class Pre_SwitchRenderCallback extends RenderCallback {}
class Pre_SwitchTreeRenderCallback extends TreeRenderCallback {}
class Pre_SwitchDoWithDataCallback extends DoWithDataCallback {};
class Pre_SwitchTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SwitchEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LayoutGroup extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    Pre_Node layout;
    Pre_Node viewport;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_LayoutGroupRenderCallback Pre_renderCallbackLayoutGroup;
    static void setPre_LayoutGroupRenderCallback(Pre_LayoutGroupRenderCallback node) {
        Pre_renderCallbackLayoutGroup = node;
    }

    static Pre_LayoutGroupTreeRenderCallback Pre_treeRenderCallbackLayoutGroup;
    static void setPre_LayoutGroupTreeRenderCallback(Pre_LayoutGroupTreeRenderCallback node) {
        Pre_treeRenderCallbackLayoutGroup = node;
    }

    static Pre_LayoutGroupDoWithDataCallback Pre_doWithDataCallbackLayoutGroup;
    static void setPre_LayoutGroupDoWithDataCallback(Pre_LayoutGroupDoWithDataCallback node) {
        Pre_doWithDataCallbackLayoutGroup = node;
    }

    static Pre_LayoutGroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackLayoutGroup;
    static void setPre_LayoutGroupTreeDoWithDataCallback(Pre_LayoutGroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLayoutGroup = node;

    }

    static Pre_LayoutGroupEventsProcessedCallback Pre_eventsProcessedCallbackLayoutGroup;
    static void setPre_LayoutGroupEventsProcessedCallback(Pre_LayoutGroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLayoutGroup = node;
    }

    Pre_LayoutGroup() {
    }
    void treeRender() {
        if (Pre_LayoutGroup.Pre_treeRenderCallbackLayoutGroup != null) {
            Pre_LayoutGroup.Pre_treeRenderCallbackLayoutGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (layout != null)
            layout.treeRender();
        if (viewport != null)
            viewport.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LayoutGroup.Pre_renderCallbackLayoutGroup != null)
            Pre_LayoutGroup.Pre_renderCallbackLayoutGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_LayoutGroup.Pre_treeDoWithDataCallbackLayoutGroup != null) {
            Pre_LayoutGroup.Pre_treeDoWithDataCallbackLayoutGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (layout != null)
            layout.treeDoWithData();
        if (viewport != null)
            viewport.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LayoutGroup.Pre_doWithDataCallbackLayoutGroup != null)
            Pre_LayoutGroup.Pre_doWithDataCallbackLayoutGroup.doWithData(this);
    }
}

class Pre_LayoutGroupRenderCallback extends RenderCallback {}
class Pre_LayoutGroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_LayoutGroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_LayoutGroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LayoutGroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ColorDamper extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_ColorDamperRenderCallback Pre_renderCallbackColorDamper;
    static void setPre_ColorDamperRenderCallback(Pre_ColorDamperRenderCallback node) {
        Pre_renderCallbackColorDamper = node;
    }

    static Pre_ColorDamperTreeRenderCallback Pre_treeRenderCallbackColorDamper;
    static void setPre_ColorDamperTreeRenderCallback(Pre_ColorDamperTreeRenderCallback node) {
        Pre_treeRenderCallbackColorDamper = node;
    }

    static Pre_ColorDamperDoWithDataCallback Pre_doWithDataCallbackColorDamper;
    static void setPre_ColorDamperDoWithDataCallback(Pre_ColorDamperDoWithDataCallback node) {
        Pre_doWithDataCallbackColorDamper = node;
    }

    static Pre_ColorDamperTreeDoWithDataCallback Pre_treeDoWithDataCallbackColorDamper;
    static void setPre_ColorDamperTreeDoWithDataCallback(Pre_ColorDamperTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackColorDamper = node;

    }

    static Pre_ColorDamperEventsProcessedCallback Pre_eventsProcessedCallbackColorDamper;
    static void setPre_ColorDamperEventsProcessedCallback(Pre_ColorDamperEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackColorDamper = node;
    }

    Pre_ColorDamper() {
    }
    void treeRender() {
        if (Pre_ColorDamper.Pre_treeRenderCallbackColorDamper != null) {
            Pre_ColorDamper.Pre_treeRenderCallbackColorDamper.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ColorDamper.Pre_renderCallbackColorDamper != null)
            Pre_ColorDamper.Pre_renderCallbackColorDamper.render(this);
    }
    void treeDoWithData() {
        if (Pre_ColorDamper.Pre_treeDoWithDataCallbackColorDamper != null) {
            Pre_ColorDamper.Pre_treeDoWithDataCallbackColorDamper.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ColorDamper.Pre_doWithDataCallbackColorDamper != null)
            Pre_ColorDamper.Pre_doWithDataCallbackColorDamper.doWithData(this);
    }
}

class Pre_ColorDamperRenderCallback extends RenderCallback {}
class Pre_ColorDamperTreeRenderCallback extends TreeRenderCallback {}
class Pre_ColorDamperDoWithDataCallback extends DoWithDataCallback {};
class Pre_ColorDamperTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ColorDamperEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IntegerSequencer extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    int[] keyValue;
    static Pre_IntegerSequencerRenderCallback Pre_renderCallbackIntegerSequencer;
    static void setPre_IntegerSequencerRenderCallback(Pre_IntegerSequencerRenderCallback node) {
        Pre_renderCallbackIntegerSequencer = node;
    }

    static Pre_IntegerSequencerTreeRenderCallback Pre_treeRenderCallbackIntegerSequencer;
    static void setPre_IntegerSequencerTreeRenderCallback(Pre_IntegerSequencerTreeRenderCallback node) {
        Pre_treeRenderCallbackIntegerSequencer = node;
    }

    static Pre_IntegerSequencerDoWithDataCallback Pre_doWithDataCallbackIntegerSequencer;
    static void setPre_IntegerSequencerDoWithDataCallback(Pre_IntegerSequencerDoWithDataCallback node) {
        Pre_doWithDataCallbackIntegerSequencer = node;
    }

    static Pre_IntegerSequencerTreeDoWithDataCallback Pre_treeDoWithDataCallbackIntegerSequencer;
    static void setPre_IntegerSequencerTreeDoWithDataCallback(Pre_IntegerSequencerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIntegerSequencer = node;

    }

    static Pre_IntegerSequencerEventsProcessedCallback Pre_eventsProcessedCallbackIntegerSequencer;
    static void setPre_IntegerSequencerEventsProcessedCallback(Pre_IntegerSequencerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIntegerSequencer = node;
    }

    Pre_IntegerSequencer() {
    }
    void treeRender() {
        if (Pre_IntegerSequencer.Pre_treeRenderCallbackIntegerSequencer != null) {
            Pre_IntegerSequencer.Pre_treeRenderCallbackIntegerSequencer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IntegerSequencer.Pre_renderCallbackIntegerSequencer != null)
            Pre_IntegerSequencer.Pre_renderCallbackIntegerSequencer.render(this);
    }
    void treeDoWithData() {
        if (Pre_IntegerSequencer.Pre_treeDoWithDataCallbackIntegerSequencer != null) {
            Pre_IntegerSequencer.Pre_treeDoWithDataCallbackIntegerSequencer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IntegerSequencer.Pre_doWithDataCallbackIntegerSequencer != null)
            Pre_IntegerSequencer.Pre_doWithDataCallbackIntegerSequencer.doWithData(this);
    }
}

class Pre_IntegerSequencerRenderCallback extends RenderCallback {}
class Pre_IntegerSequencerTreeRenderCallback extends TreeRenderCallback {}
class Pre_IntegerSequencerDoWithDataCallback extends DoWithDataCallback {};
class Pre_IntegerSequencerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IntegerSequencerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_KeySensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    static Pre_KeySensorRenderCallback Pre_renderCallbackKeySensor;
    static void setPre_KeySensorRenderCallback(Pre_KeySensorRenderCallback node) {
        Pre_renderCallbackKeySensor = node;
    }

    static Pre_KeySensorTreeRenderCallback Pre_treeRenderCallbackKeySensor;
    static void setPre_KeySensorTreeRenderCallback(Pre_KeySensorTreeRenderCallback node) {
        Pre_treeRenderCallbackKeySensor = node;
    }

    static Pre_KeySensorDoWithDataCallback Pre_doWithDataCallbackKeySensor;
    static void setPre_KeySensorDoWithDataCallback(Pre_KeySensorDoWithDataCallback node) {
        Pre_doWithDataCallbackKeySensor = node;
    }

    static Pre_KeySensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackKeySensor;
    static void setPre_KeySensorTreeDoWithDataCallback(Pre_KeySensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackKeySensor = node;

    }

    static Pre_KeySensorEventsProcessedCallback Pre_eventsProcessedCallbackKeySensor;
    static void setPre_KeySensorEventsProcessedCallback(Pre_KeySensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackKeySensor = node;
    }

    Pre_KeySensor() {
    }
    void treeRender() {
        if (Pre_KeySensor.Pre_treeRenderCallbackKeySensor != null) {
            Pre_KeySensor.Pre_treeRenderCallbackKeySensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_KeySensor.Pre_renderCallbackKeySensor != null)
            Pre_KeySensor.Pre_renderCallbackKeySensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_KeySensor.Pre_treeDoWithDataCallbackKeySensor != null) {
            Pre_KeySensor.Pre_treeDoWithDataCallbackKeySensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_KeySensor.Pre_doWithDataCallbackKeySensor != null)
            Pre_KeySensor.Pre_doWithDataCallbackKeySensor.doWithData(this);
    }
}

class Pre_KeySensorRenderCallback extends RenderCallback {}
class Pre_KeySensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_KeySensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_KeySensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_KeySensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LayoutLayer extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    boolean isPickable;
    Pre_Node layout;
    Pre_Node viewport;
    static Pre_LayoutLayerRenderCallback Pre_renderCallbackLayoutLayer;
    static void setPre_LayoutLayerRenderCallback(Pre_LayoutLayerRenderCallback node) {
        Pre_renderCallbackLayoutLayer = node;
    }

    static Pre_LayoutLayerTreeRenderCallback Pre_treeRenderCallbackLayoutLayer;
    static void setPre_LayoutLayerTreeRenderCallback(Pre_LayoutLayerTreeRenderCallback node) {
        Pre_treeRenderCallbackLayoutLayer = node;
    }

    static Pre_LayoutLayerDoWithDataCallback Pre_doWithDataCallbackLayoutLayer;
    static void setPre_LayoutLayerDoWithDataCallback(Pre_LayoutLayerDoWithDataCallback node) {
        Pre_doWithDataCallbackLayoutLayer = node;
    }

    static Pre_LayoutLayerTreeDoWithDataCallback Pre_treeDoWithDataCallbackLayoutLayer;
    static void setPre_LayoutLayerTreeDoWithDataCallback(Pre_LayoutLayerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLayoutLayer = node;

    }

    static Pre_LayoutLayerEventsProcessedCallback Pre_eventsProcessedCallbackLayoutLayer;
    static void setPre_LayoutLayerEventsProcessedCallback(Pre_LayoutLayerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLayoutLayer = node;
    }

    Pre_LayoutLayer() {
    }
    void treeRender() {
        if (Pre_LayoutLayer.Pre_treeRenderCallbackLayoutLayer != null) {
            Pre_LayoutLayer.Pre_treeRenderCallbackLayoutLayer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (layout != null)
            layout.treeRender();
        if (viewport != null)
            viewport.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LayoutLayer.Pre_renderCallbackLayoutLayer != null)
            Pre_LayoutLayer.Pre_renderCallbackLayoutLayer.render(this);
    }
    void treeDoWithData() {
        if (Pre_LayoutLayer.Pre_treeDoWithDataCallbackLayoutLayer != null) {
            Pre_LayoutLayer.Pre_treeDoWithDataCallbackLayoutLayer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (layout != null)
            layout.treeDoWithData();
        if (viewport != null)
            viewport.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LayoutLayer.Pre_doWithDataCallbackLayoutLayer != null)
            Pre_LayoutLayer.Pre_doWithDataCallbackLayoutLayer.doWithData(this);
    }
}

class Pre_LayoutLayerRenderCallback extends RenderCallback {}
class Pre_LayoutLayerTreeRenderCallback extends TreeRenderCallback {}
class Pre_LayoutLayerDoWithDataCallback extends DoWithDataCallback {};
class Pre_LayoutLayerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LayoutLayerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SquadOrientationInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    boolean normalizeVelocity;
    static Pre_SquadOrientationInterpolatorRenderCallback Pre_renderCallbackSquadOrientationInterpolator;
    static void setPre_SquadOrientationInterpolatorRenderCallback(Pre_SquadOrientationInterpolatorRenderCallback node) {
        Pre_renderCallbackSquadOrientationInterpolator = node;
    }

    static Pre_SquadOrientationInterpolatorTreeRenderCallback Pre_treeRenderCallbackSquadOrientationInterpolator;
    static void setPre_SquadOrientationInterpolatorTreeRenderCallback(Pre_SquadOrientationInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackSquadOrientationInterpolator = node;
    }

    static Pre_SquadOrientationInterpolatorDoWithDataCallback Pre_doWithDataCallbackSquadOrientationInterpolator;
    static void setPre_SquadOrientationInterpolatorDoWithDataCallback(Pre_SquadOrientationInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackSquadOrientationInterpolator = node;
    }

    static Pre_SquadOrientationInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackSquadOrientationInterpolator;
    static void setPre_SquadOrientationInterpolatorTreeDoWithDataCallback(Pre_SquadOrientationInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSquadOrientationInterpolator = node;

    }

    static Pre_SquadOrientationInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackSquadOrientationInterpolator;
    static void setPre_SquadOrientationInterpolatorEventsProcessedCallback(Pre_SquadOrientationInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSquadOrientationInterpolator = node;
    }

    Pre_SquadOrientationInterpolator() {
    }
    void treeRender() {
        if (Pre_SquadOrientationInterpolator.Pre_treeRenderCallbackSquadOrientationInterpolator != null) {
            Pre_SquadOrientationInterpolator.Pre_treeRenderCallbackSquadOrientationInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SquadOrientationInterpolator.Pre_renderCallbackSquadOrientationInterpolator != null)
            Pre_SquadOrientationInterpolator.Pre_renderCallbackSquadOrientationInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_SquadOrientationInterpolator.Pre_treeDoWithDataCallbackSquadOrientationInterpolator != null) {
            Pre_SquadOrientationInterpolator.Pre_treeDoWithDataCallbackSquadOrientationInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SquadOrientationInterpolator.Pre_doWithDataCallbackSquadOrientationInterpolator != null)
            Pre_SquadOrientationInterpolator.Pre_doWithDataCallbackSquadOrientationInterpolator.doWithData(this);
    }
}

class Pre_SquadOrientationInterpolatorRenderCallback extends RenderCallback {}
class Pre_SquadOrientationInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_SquadOrientationInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_SquadOrientationInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SquadOrientationInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_EaseInEaseOut extends Pre_Node {
    Pre_Node metadata;
    float[] easeInEaseOut;
    float[] key;
    static Pre_EaseInEaseOutRenderCallback Pre_renderCallbackEaseInEaseOut;
    static void setPre_EaseInEaseOutRenderCallback(Pre_EaseInEaseOutRenderCallback node) {
        Pre_renderCallbackEaseInEaseOut = node;
    }

    static Pre_EaseInEaseOutTreeRenderCallback Pre_treeRenderCallbackEaseInEaseOut;
    static void setPre_EaseInEaseOutTreeRenderCallback(Pre_EaseInEaseOutTreeRenderCallback node) {
        Pre_treeRenderCallbackEaseInEaseOut = node;
    }

    static Pre_EaseInEaseOutDoWithDataCallback Pre_doWithDataCallbackEaseInEaseOut;
    static void setPre_EaseInEaseOutDoWithDataCallback(Pre_EaseInEaseOutDoWithDataCallback node) {
        Pre_doWithDataCallbackEaseInEaseOut = node;
    }

    static Pre_EaseInEaseOutTreeDoWithDataCallback Pre_treeDoWithDataCallbackEaseInEaseOut;
    static void setPre_EaseInEaseOutTreeDoWithDataCallback(Pre_EaseInEaseOutTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackEaseInEaseOut = node;

    }

    static Pre_EaseInEaseOutEventsProcessedCallback Pre_eventsProcessedCallbackEaseInEaseOut;
    static void setPre_EaseInEaseOutEventsProcessedCallback(Pre_EaseInEaseOutEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackEaseInEaseOut = node;
    }

    Pre_EaseInEaseOut() {
    }
    void treeRender() {
        if (Pre_EaseInEaseOut.Pre_treeRenderCallbackEaseInEaseOut != null) {
            Pre_EaseInEaseOut.Pre_treeRenderCallbackEaseInEaseOut.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_EaseInEaseOut.Pre_renderCallbackEaseInEaseOut != null)
            Pre_EaseInEaseOut.Pre_renderCallbackEaseInEaseOut.render(this);
    }
    void treeDoWithData() {
        if (Pre_EaseInEaseOut.Pre_treeDoWithDataCallbackEaseInEaseOut != null) {
            Pre_EaseInEaseOut.Pre_treeDoWithDataCallbackEaseInEaseOut.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_EaseInEaseOut.Pre_doWithDataCallbackEaseInEaseOut != null)
            Pre_EaseInEaseOut.Pre_doWithDataCallbackEaseInEaseOut.doWithData(this);
    }
}

class Pre_EaseInEaseOutRenderCallback extends RenderCallback {}
class Pre_EaseInEaseOutTreeRenderCallback extends TreeRenderCallback {}
class Pre_EaseInEaseOutDoWithDataCallback extends DoWithDataCallback {};
class Pre_EaseInEaseOutTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_EaseInEaseOutEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoViewpoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    String description;
    float fieldOfView;
    boolean headlight;
    boolean jump;
    String[] navType;
    float[] orientation;
    double[] position;
    float speedFactor;
    static Pre_GeoViewpointRenderCallback Pre_renderCallbackGeoViewpoint;
    static void setPre_GeoViewpointRenderCallback(Pre_GeoViewpointRenderCallback node) {
        Pre_renderCallbackGeoViewpoint = node;
    }

    static Pre_GeoViewpointTreeRenderCallback Pre_treeRenderCallbackGeoViewpoint;
    static void setPre_GeoViewpointTreeRenderCallback(Pre_GeoViewpointTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoViewpoint = node;
    }

    static Pre_GeoViewpointDoWithDataCallback Pre_doWithDataCallbackGeoViewpoint;
    static void setPre_GeoViewpointDoWithDataCallback(Pre_GeoViewpointDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoViewpoint = node;
    }

    static Pre_GeoViewpointTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoViewpoint;
    static void setPre_GeoViewpointTreeDoWithDataCallback(Pre_GeoViewpointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoViewpoint = node;

    }

    static Pre_GeoViewpointEventsProcessedCallback Pre_eventsProcessedCallbackGeoViewpoint;
    static void setPre_GeoViewpointEventsProcessedCallback(Pre_GeoViewpointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoViewpoint = node;
    }

    Pre_GeoViewpoint() {
    }
    void treeRender() {
        if (Pre_GeoViewpoint.Pre_treeRenderCallbackGeoViewpoint != null) {
            Pre_GeoViewpoint.Pre_treeRenderCallbackGeoViewpoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoViewpoint.Pre_renderCallbackGeoViewpoint != null)
            Pre_GeoViewpoint.Pre_renderCallbackGeoViewpoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoViewpoint.Pre_treeDoWithDataCallbackGeoViewpoint != null) {
            Pre_GeoViewpoint.Pre_treeDoWithDataCallbackGeoViewpoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoViewpoint.Pre_doWithDataCallbackGeoViewpoint != null)
            Pre_GeoViewpoint.Pre_doWithDataCallbackGeoViewpoint.doWithData(this);
    }
}

class Pre_GeoViewpointRenderCallback extends RenderCallback {}
class Pre_GeoViewpointTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoViewpointDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoViewpointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoViewpointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OdeSliderJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] axis;
    float maxSeparation;
    float minSeparation;
    float stopBounce;
    float stopErrorCorrection;
    float fMax;
    static Pre_OdeSliderJointRenderCallback Pre_renderCallbackOdeSliderJoint;
    static void setPre_OdeSliderJointRenderCallback(Pre_OdeSliderJointRenderCallback node) {
        Pre_renderCallbackOdeSliderJoint = node;
    }

    static Pre_OdeSliderJointTreeRenderCallback Pre_treeRenderCallbackOdeSliderJoint;
    static void setPre_OdeSliderJointTreeRenderCallback(Pre_OdeSliderJointTreeRenderCallback node) {
        Pre_treeRenderCallbackOdeSliderJoint = node;
    }

    static Pre_OdeSliderJointDoWithDataCallback Pre_doWithDataCallbackOdeSliderJoint;
    static void setPre_OdeSliderJointDoWithDataCallback(Pre_OdeSliderJointDoWithDataCallback node) {
        Pre_doWithDataCallbackOdeSliderJoint = node;
    }

    static Pre_OdeSliderJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackOdeSliderJoint;
    static void setPre_OdeSliderJointTreeDoWithDataCallback(Pre_OdeSliderJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOdeSliderJoint = node;

    }

    static Pre_OdeSliderJointEventsProcessedCallback Pre_eventsProcessedCallbackOdeSliderJoint;
    static void setPre_OdeSliderJointEventsProcessedCallback(Pre_OdeSliderJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOdeSliderJoint = node;
    }

    Pre_OdeSliderJoint() {
    }
    void treeRender() {
        if (Pre_OdeSliderJoint.Pre_treeRenderCallbackOdeSliderJoint != null) {
            Pre_OdeSliderJoint.Pre_treeRenderCallbackOdeSliderJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OdeSliderJoint.Pre_renderCallbackOdeSliderJoint != null)
            Pre_OdeSliderJoint.Pre_renderCallbackOdeSliderJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_OdeSliderJoint.Pre_treeDoWithDataCallbackOdeSliderJoint != null) {
            Pre_OdeSliderJoint.Pre_treeDoWithDataCallbackOdeSliderJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OdeSliderJoint.Pre_doWithDataCallbackOdeSliderJoint != null)
            Pre_OdeSliderJoint.Pre_doWithDataCallbackOdeSliderJoint.doWithData(this);
    }
}

class Pre_OdeSliderJointRenderCallback extends RenderCallback {}
class Pre_OdeSliderJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_OdeSliderJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_OdeSliderJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OdeSliderJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Viewpoint extends Pre_Node {
    Pre_Node metadata;
    float[] centerOfRotation;
    String description;
    float fieldOfView;
    boolean jump;
    float[] orientation;
    float[] position;
    boolean retainUserOffsets;
    static Pre_ViewpointRenderCallback Pre_renderCallbackViewpoint;
    static void setPre_ViewpointRenderCallback(Pre_ViewpointRenderCallback node) {
        Pre_renderCallbackViewpoint = node;
    }

    static Pre_ViewpointTreeRenderCallback Pre_treeRenderCallbackViewpoint;
    static void setPre_ViewpointTreeRenderCallback(Pre_ViewpointTreeRenderCallback node) {
        Pre_treeRenderCallbackViewpoint = node;
    }

    static Pre_ViewpointDoWithDataCallback Pre_doWithDataCallbackViewpoint;
    static void setPre_ViewpointDoWithDataCallback(Pre_ViewpointDoWithDataCallback node) {
        Pre_doWithDataCallbackViewpoint = node;
    }

    static Pre_ViewpointTreeDoWithDataCallback Pre_treeDoWithDataCallbackViewpoint;
    static void setPre_ViewpointTreeDoWithDataCallback(Pre_ViewpointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackViewpoint = node;

    }

    static Pre_ViewpointEventsProcessedCallback Pre_eventsProcessedCallbackViewpoint;
    static void setPre_ViewpointEventsProcessedCallback(Pre_ViewpointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackViewpoint = node;
    }

    Pre_Viewpoint() {
    }
    void treeRender() {
        if (Pre_Viewpoint.Pre_treeRenderCallbackViewpoint != null) {
            Pre_Viewpoint.Pre_treeRenderCallbackViewpoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Viewpoint.Pre_renderCallbackViewpoint != null)
            Pre_Viewpoint.Pre_renderCallbackViewpoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_Viewpoint.Pre_treeDoWithDataCallbackViewpoint != null) {
            Pre_Viewpoint.Pre_treeDoWithDataCallbackViewpoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Viewpoint.Pre_doWithDataCallbackViewpoint != null)
            Pre_Viewpoint.Pre_doWithDataCallbackViewpoint.doWithData(this);
    }
}

class Pre_ViewpointRenderCallback extends RenderCallback {}
class Pre_ViewpointTreeRenderCallback extends TreeRenderCallback {}
class Pre_ViewpointDoWithDataCallback extends DoWithDataCallback {};
class Pre_ViewpointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ViewpointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Test extends Pre_Node {
    Pre_Node metadata;
    float[] diffuseColor;
    String[] string;
    static Pre_TestRenderCallback Pre_renderCallbackTest;
    static void setPre_TestRenderCallback(Pre_TestRenderCallback node) {
        Pre_renderCallbackTest = node;
    }

    static Pre_TestTreeRenderCallback Pre_treeRenderCallbackTest;
    static void setPre_TestTreeRenderCallback(Pre_TestTreeRenderCallback node) {
        Pre_treeRenderCallbackTest = node;
    }

    static Pre_TestDoWithDataCallback Pre_doWithDataCallbackTest;
    static void setPre_TestDoWithDataCallback(Pre_TestDoWithDataCallback node) {
        Pre_doWithDataCallbackTest = node;
    }

    static Pre_TestTreeDoWithDataCallback Pre_treeDoWithDataCallbackTest;
    static void setPre_TestTreeDoWithDataCallback(Pre_TestTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTest = node;

    }

    static Pre_TestEventsProcessedCallback Pre_eventsProcessedCallbackTest;
    static void setPre_TestEventsProcessedCallback(Pre_TestEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTest = node;
    }

    Pre_Test() {
    }
    void treeRender() {
        if (Pre_Test.Pre_treeRenderCallbackTest != null) {
            Pre_Test.Pre_treeRenderCallbackTest.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Test.Pre_renderCallbackTest != null)
            Pre_Test.Pre_renderCallbackTest.render(this);
    }
    void treeDoWithData() {
        if (Pre_Test.Pre_treeDoWithDataCallbackTest != null) {
            Pre_Test.Pre_treeDoWithDataCallbackTest.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Test.Pre_doWithDataCallbackTest != null)
            Pre_Test.Pre_doWithDataCallbackTest.doWithData(this);
    }
}

class Pre_TestRenderCallback extends RenderCallback {}
class Pre_TestTreeRenderCallback extends TreeRenderCallback {}
class Pre_TestDoWithDataCallback extends DoWithDataCallback {};
class Pre_TestTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TestEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Extrusion extends Pre_Node {
    Pre_Node metadata;
    boolean beginCap;
    boolean ccw;
    boolean convex;
    float creaseAngle;
    float[] crossSection;
    boolean endCap;
    float[] orientation;
    float[] scale;
    boolean solid;
    float[] spine;
    static Pre_ExtrusionRenderCallback Pre_renderCallbackExtrusion;
    static void setPre_ExtrusionRenderCallback(Pre_ExtrusionRenderCallback node) {
        Pre_renderCallbackExtrusion = node;
    }

    static Pre_ExtrusionTreeRenderCallback Pre_treeRenderCallbackExtrusion;
    static void setPre_ExtrusionTreeRenderCallback(Pre_ExtrusionTreeRenderCallback node) {
        Pre_treeRenderCallbackExtrusion = node;
    }

    static Pre_ExtrusionDoWithDataCallback Pre_doWithDataCallbackExtrusion;
    static void setPre_ExtrusionDoWithDataCallback(Pre_ExtrusionDoWithDataCallback node) {
        Pre_doWithDataCallbackExtrusion = node;
    }

    static Pre_ExtrusionTreeDoWithDataCallback Pre_treeDoWithDataCallbackExtrusion;
    static void setPre_ExtrusionTreeDoWithDataCallback(Pre_ExtrusionTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackExtrusion = node;

    }

    static Pre_ExtrusionEventsProcessedCallback Pre_eventsProcessedCallbackExtrusion;
    static void setPre_ExtrusionEventsProcessedCallback(Pre_ExtrusionEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackExtrusion = node;
    }

    Pre_Extrusion() {
    }
    void treeRender() {
        if (Pre_Extrusion.Pre_treeRenderCallbackExtrusion != null) {
            Pre_Extrusion.Pre_treeRenderCallbackExtrusion.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Extrusion.Pre_renderCallbackExtrusion != null)
            Pre_Extrusion.Pre_renderCallbackExtrusion.render(this);
    }
    void treeDoWithData() {
        if (Pre_Extrusion.Pre_treeDoWithDataCallbackExtrusion != null) {
            Pre_Extrusion.Pre_treeDoWithDataCallbackExtrusion.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Extrusion.Pre_doWithDataCallbackExtrusion != null)
            Pre_Extrusion.Pre_doWithDataCallbackExtrusion.doWithData(this);
    }
}

class Pre_ExtrusionRenderCallback extends RenderCallback {}
class Pre_ExtrusionTreeRenderCallback extends TreeRenderCallback {}
class Pre_ExtrusionDoWithDataCallback extends DoWithDataCallback {};
class Pre_ExtrusionTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ExtrusionEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_WorldInfo extends Pre_Node {
    Pre_Node metadata;
    String[] info;
    String title;
    static Pre_WorldInfoRenderCallback Pre_renderCallbackWorldInfo;
    static void setPre_WorldInfoRenderCallback(Pre_WorldInfoRenderCallback node) {
        Pre_renderCallbackWorldInfo = node;
    }

    static Pre_WorldInfoTreeRenderCallback Pre_treeRenderCallbackWorldInfo;
    static void setPre_WorldInfoTreeRenderCallback(Pre_WorldInfoTreeRenderCallback node) {
        Pre_treeRenderCallbackWorldInfo = node;
    }

    static Pre_WorldInfoDoWithDataCallback Pre_doWithDataCallbackWorldInfo;
    static void setPre_WorldInfoDoWithDataCallback(Pre_WorldInfoDoWithDataCallback node) {
        Pre_doWithDataCallbackWorldInfo = node;
    }

    static Pre_WorldInfoTreeDoWithDataCallback Pre_treeDoWithDataCallbackWorldInfo;
    static void setPre_WorldInfoTreeDoWithDataCallback(Pre_WorldInfoTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackWorldInfo = node;

    }

    static Pre_WorldInfoEventsProcessedCallback Pre_eventsProcessedCallbackWorldInfo;
    static void setPre_WorldInfoEventsProcessedCallback(Pre_WorldInfoEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackWorldInfo = node;
    }

    Pre_WorldInfo() {
    }
    void treeRender() {
        if (Pre_WorldInfo.Pre_treeRenderCallbackWorldInfo != null) {
            Pre_WorldInfo.Pre_treeRenderCallbackWorldInfo.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_WorldInfo.Pre_renderCallbackWorldInfo != null)
            Pre_WorldInfo.Pre_renderCallbackWorldInfo.render(this);
    }
    void treeDoWithData() {
        if (Pre_WorldInfo.Pre_treeDoWithDataCallbackWorldInfo != null) {
            Pre_WorldInfo.Pre_treeDoWithDataCallbackWorldInfo.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_WorldInfo.Pre_doWithDataCallbackWorldInfo != null)
            Pre_WorldInfo.Pre_doWithDataCallbackWorldInfo.doWithData(this);
    }
}

class Pre_WorldInfoRenderCallback extends RenderCallback {}
class Pre_WorldInfoTreeRenderCallback extends TreeRenderCallback {}
class Pre_WorldInfoDoWithDataCallback extends DoWithDataCallback {};
class Pre_WorldInfoTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_WorldInfoEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_AudioClip extends Pre_Node {
    Pre_Node metadata;
    String description;
    boolean loop;
    double pauseTime;
    float pitch;
    double resumeTime;
    double startTime;
    double stopTime;
    String[] url;
    static Pre_AudioClipRenderCallback Pre_renderCallbackAudioClip;
    static void setPre_AudioClipRenderCallback(Pre_AudioClipRenderCallback node) {
        Pre_renderCallbackAudioClip = node;
    }

    static Pre_AudioClipTreeRenderCallback Pre_treeRenderCallbackAudioClip;
    static void setPre_AudioClipTreeRenderCallback(Pre_AudioClipTreeRenderCallback node) {
        Pre_treeRenderCallbackAudioClip = node;
    }

    static Pre_AudioClipDoWithDataCallback Pre_doWithDataCallbackAudioClip;
    static void setPre_AudioClipDoWithDataCallback(Pre_AudioClipDoWithDataCallback node) {
        Pre_doWithDataCallbackAudioClip = node;
    }

    static Pre_AudioClipTreeDoWithDataCallback Pre_treeDoWithDataCallbackAudioClip;
    static void setPre_AudioClipTreeDoWithDataCallback(Pre_AudioClipTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackAudioClip = node;

    }

    static Pre_AudioClipEventsProcessedCallback Pre_eventsProcessedCallbackAudioClip;
    static void setPre_AudioClipEventsProcessedCallback(Pre_AudioClipEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackAudioClip = node;
    }

    Pre_AudioClip() {
    }
    void treeRender() {
        if (Pre_AudioClip.Pre_treeRenderCallbackAudioClip != null) {
            Pre_AudioClip.Pre_treeRenderCallbackAudioClip.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_AudioClip.Pre_renderCallbackAudioClip != null)
            Pre_AudioClip.Pre_renderCallbackAudioClip.render(this);
    }
    void treeDoWithData() {
        if (Pre_AudioClip.Pre_treeDoWithDataCallbackAudioClip != null) {
            Pre_AudioClip.Pre_treeDoWithDataCallbackAudioClip.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_AudioClip.Pre_doWithDataCallbackAudioClip != null)
            Pre_AudioClip.Pre_doWithDataCallbackAudioClip.doWithData(this);
    }
}

class Pre_AudioClipRenderCallback extends RenderCallback {}
class Pre_AudioClipTreeRenderCallback extends TreeRenderCallback {}
class Pre_AudioClipDoWithDataCallback extends DoWithDataCallback {};
class Pre_AudioClipTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_AudioClipEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CADLayer extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    String name;
    boolean[] visible;
    static Pre_CADLayerRenderCallback Pre_renderCallbackCADLayer;
    static void setPre_CADLayerRenderCallback(Pre_CADLayerRenderCallback node) {
        Pre_renderCallbackCADLayer = node;
    }

    static Pre_CADLayerTreeRenderCallback Pre_treeRenderCallbackCADLayer;
    static void setPre_CADLayerTreeRenderCallback(Pre_CADLayerTreeRenderCallback node) {
        Pre_treeRenderCallbackCADLayer = node;
    }

    static Pre_CADLayerDoWithDataCallback Pre_doWithDataCallbackCADLayer;
    static void setPre_CADLayerDoWithDataCallback(Pre_CADLayerDoWithDataCallback node) {
        Pre_doWithDataCallbackCADLayer = node;
    }

    static Pre_CADLayerTreeDoWithDataCallback Pre_treeDoWithDataCallbackCADLayer;
    static void setPre_CADLayerTreeDoWithDataCallback(Pre_CADLayerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCADLayer = node;

    }

    static Pre_CADLayerEventsProcessedCallback Pre_eventsProcessedCallbackCADLayer;
    static void setPre_CADLayerEventsProcessedCallback(Pre_CADLayerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCADLayer = node;
    }

    Pre_CADLayer() {
    }
    void treeRender() {
        if (Pre_CADLayer.Pre_treeRenderCallbackCADLayer != null) {
            Pre_CADLayer.Pre_treeRenderCallbackCADLayer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CADLayer.Pre_renderCallbackCADLayer != null)
            Pre_CADLayer.Pre_renderCallbackCADLayer.render(this);
    }
    void treeDoWithData() {
        if (Pre_CADLayer.Pre_treeDoWithDataCallbackCADLayer != null) {
            Pre_CADLayer.Pre_treeDoWithDataCallbackCADLayer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CADLayer.Pre_doWithDataCallbackCADLayer != null)
            Pre_CADLayer.Pre_doWithDataCallbackCADLayer.doWithData(this);
    }
}

class Pre_CADLayerRenderCallback extends RenderCallback {}
class Pre_CADLayerTreeRenderCallback extends TreeRenderCallback {}
class Pre_CADLayerDoWithDataCallback extends DoWithDataCallback {};
class Pre_CADLayerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CADLayerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_IntegerTrigger extends Pre_Node {
    Pre_Node metadata;
    int integerKey;
    static Pre_IntegerTriggerRenderCallback Pre_renderCallbackIntegerTrigger;
    static void setPre_IntegerTriggerRenderCallback(Pre_IntegerTriggerRenderCallback node) {
        Pre_renderCallbackIntegerTrigger = node;
    }

    static Pre_IntegerTriggerTreeRenderCallback Pre_treeRenderCallbackIntegerTrigger;
    static void setPre_IntegerTriggerTreeRenderCallback(Pre_IntegerTriggerTreeRenderCallback node) {
        Pre_treeRenderCallbackIntegerTrigger = node;
    }

    static Pre_IntegerTriggerDoWithDataCallback Pre_doWithDataCallbackIntegerTrigger;
    static void setPre_IntegerTriggerDoWithDataCallback(Pre_IntegerTriggerDoWithDataCallback node) {
        Pre_doWithDataCallbackIntegerTrigger = node;
    }

    static Pre_IntegerTriggerTreeDoWithDataCallback Pre_treeDoWithDataCallbackIntegerTrigger;
    static void setPre_IntegerTriggerTreeDoWithDataCallback(Pre_IntegerTriggerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackIntegerTrigger = node;

    }

    static Pre_IntegerTriggerEventsProcessedCallback Pre_eventsProcessedCallbackIntegerTrigger;
    static void setPre_IntegerTriggerEventsProcessedCallback(Pre_IntegerTriggerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackIntegerTrigger = node;
    }

    Pre_IntegerTrigger() {
    }
    void treeRender() {
        if (Pre_IntegerTrigger.Pre_treeRenderCallbackIntegerTrigger != null) {
            Pre_IntegerTrigger.Pre_treeRenderCallbackIntegerTrigger.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_IntegerTrigger.Pre_renderCallbackIntegerTrigger != null)
            Pre_IntegerTrigger.Pre_renderCallbackIntegerTrigger.render(this);
    }
    void treeDoWithData() {
        if (Pre_IntegerTrigger.Pre_treeDoWithDataCallbackIntegerTrigger != null) {
            Pre_IntegerTrigger.Pre_treeDoWithDataCallbackIntegerTrigger.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_IntegerTrigger.Pre_doWithDataCallbackIntegerTrigger != null)
            Pre_IntegerTrigger.Pre_doWithDataCallbackIntegerTrigger.doWithData(this);
    }
}

class Pre_IntegerTriggerRenderCallback extends RenderCallback {}
class Pre_IntegerTriggerTreeRenderCallback extends TreeRenderCallback {}
class Pre_IntegerTriggerDoWithDataCallback extends DoWithDataCallback {};
class Pre_IntegerTriggerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_IntegerTriggerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ImageTexture3D extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    boolean repeatS;
    boolean repeatT;
    boolean repeatR;
    Pre_Node textureProperties;
    static Pre_ImageTexture3DRenderCallback Pre_renderCallbackImageTexture3D;
    static void setPre_ImageTexture3DRenderCallback(Pre_ImageTexture3DRenderCallback node) {
        Pre_renderCallbackImageTexture3D = node;
    }

    static Pre_ImageTexture3DTreeRenderCallback Pre_treeRenderCallbackImageTexture3D;
    static void setPre_ImageTexture3DTreeRenderCallback(Pre_ImageTexture3DTreeRenderCallback node) {
        Pre_treeRenderCallbackImageTexture3D = node;
    }

    static Pre_ImageTexture3DDoWithDataCallback Pre_doWithDataCallbackImageTexture3D;
    static void setPre_ImageTexture3DDoWithDataCallback(Pre_ImageTexture3DDoWithDataCallback node) {
        Pre_doWithDataCallbackImageTexture3D = node;
    }

    static Pre_ImageTexture3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackImageTexture3D;
    static void setPre_ImageTexture3DTreeDoWithDataCallback(Pre_ImageTexture3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackImageTexture3D = node;

    }

    static Pre_ImageTexture3DEventsProcessedCallback Pre_eventsProcessedCallbackImageTexture3D;
    static void setPre_ImageTexture3DEventsProcessedCallback(Pre_ImageTexture3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackImageTexture3D = node;
    }

    Pre_ImageTexture3D() {
    }
    void treeRender() {
        if (Pre_ImageTexture3D.Pre_treeRenderCallbackImageTexture3D != null) {
            Pre_ImageTexture3D.Pre_treeRenderCallbackImageTexture3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ImageTexture3D.Pre_renderCallbackImageTexture3D != null)
            Pre_ImageTexture3D.Pre_renderCallbackImageTexture3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_ImageTexture3D.Pre_treeDoWithDataCallbackImageTexture3D != null) {
            Pre_ImageTexture3D.Pre_treeDoWithDataCallbackImageTexture3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ImageTexture3D.Pre_doWithDataCallbackImageTexture3D != null)
            Pre_ImageTexture3D.Pre_doWithDataCallbackImageTexture3D.doWithData(this);
    }
}

class Pre_ImageTexture3DRenderCallback extends RenderCallback {}
class Pre_ImageTexture3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_ImageTexture3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_ImageTexture3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ImageTexture3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MetadataSet extends Pre_Node {
    Pre_Node metadata;
    String name;
    String reference;
    Pre_Node [] value;
    static Pre_MetadataSetRenderCallback Pre_renderCallbackMetadataSet;
    static void setPre_MetadataSetRenderCallback(Pre_MetadataSetRenderCallback node) {
        Pre_renderCallbackMetadataSet = node;
    }

    static Pre_MetadataSetTreeRenderCallback Pre_treeRenderCallbackMetadataSet;
    static void setPre_MetadataSetTreeRenderCallback(Pre_MetadataSetTreeRenderCallback node) {
        Pre_treeRenderCallbackMetadataSet = node;
    }

    static Pre_MetadataSetDoWithDataCallback Pre_doWithDataCallbackMetadataSet;
    static void setPre_MetadataSetDoWithDataCallback(Pre_MetadataSetDoWithDataCallback node) {
        Pre_doWithDataCallbackMetadataSet = node;
    }

    static Pre_MetadataSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackMetadataSet;
    static void setPre_MetadataSetTreeDoWithDataCallback(Pre_MetadataSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMetadataSet = node;

    }

    static Pre_MetadataSetEventsProcessedCallback Pre_eventsProcessedCallbackMetadataSet;
    static void setPre_MetadataSetEventsProcessedCallback(Pre_MetadataSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMetadataSet = node;
    }

    Pre_MetadataSet() {
    }
    void treeRender() {
        if (Pre_MetadataSet.Pre_treeRenderCallbackMetadataSet != null) {
            Pre_MetadataSet.Pre_treeRenderCallbackMetadataSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (value != null)
            for (int i = 0; i < value.length; i++)
                if (value[i] != null)
                    value[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MetadataSet.Pre_renderCallbackMetadataSet != null)
            Pre_MetadataSet.Pre_renderCallbackMetadataSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_MetadataSet.Pre_treeDoWithDataCallbackMetadataSet != null) {
            Pre_MetadataSet.Pre_treeDoWithDataCallbackMetadataSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (value != null)
            for (int i = 0; i < value.length; i++)
                if (value[i] != null)
                    value[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MetadataSet.Pre_doWithDataCallbackMetadataSet != null)
            Pre_MetadataSet.Pre_doWithDataCallbackMetadataSet.doWithData(this);
    }
}

class Pre_MetadataSetRenderCallback extends RenderCallback {}
class Pre_MetadataSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_MetadataSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_MetadataSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MetadataSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PointSet extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] attrib;
    Pre_Node color;
    Pre_Node coord;
    Pre_Node fogCoord;
    static Pre_PointSetRenderCallback Pre_renderCallbackPointSet;
    static void setPre_PointSetRenderCallback(Pre_PointSetRenderCallback node) {
        Pre_renderCallbackPointSet = node;
    }

    static Pre_PointSetTreeRenderCallback Pre_treeRenderCallbackPointSet;
    static void setPre_PointSetTreeRenderCallback(Pre_PointSetTreeRenderCallback node) {
        Pre_treeRenderCallbackPointSet = node;
    }

    static Pre_PointSetDoWithDataCallback Pre_doWithDataCallbackPointSet;
    static void setPre_PointSetDoWithDataCallback(Pre_PointSetDoWithDataCallback node) {
        Pre_doWithDataCallbackPointSet = node;
    }

    static Pre_PointSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackPointSet;
    static void setPre_PointSetTreeDoWithDataCallback(Pre_PointSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPointSet = node;

    }

    static Pre_PointSetEventsProcessedCallback Pre_eventsProcessedCallbackPointSet;
    static void setPre_PointSetEventsProcessedCallback(Pre_PointSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPointSet = node;
    }

    Pre_PointSet() {
    }
    void treeRender() {
        if (Pre_PointSet.Pre_treeRenderCallbackPointSet != null) {
            Pre_PointSet.Pre_treeRenderCallbackPointSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeRender();
        if (color != null)
            color.treeRender();
        if (coord != null)
            coord.treeRender();
        if (fogCoord != null)
            fogCoord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PointSet.Pre_renderCallbackPointSet != null)
            Pre_PointSet.Pre_renderCallbackPointSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_PointSet.Pre_treeDoWithDataCallbackPointSet != null) {
            Pre_PointSet.Pre_treeDoWithDataCallbackPointSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (attrib != null)
            for (int i = 0; i < attrib.length; i++)
                if (attrib[i] != null)
                    attrib[i].treeDoWithData();
        if (color != null)
            color.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (fogCoord != null)
            fogCoord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PointSet.Pre_doWithDataCallbackPointSet != null)
            Pre_PointSet.Pre_doWithDataCallbackPointSet.doWithData(this);
    }
}

class Pre_PointSetRenderCallback extends RenderCallback {}
class Pre_PointSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_PointSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_PointSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PointSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeneratedCubeMapTexture extends Pre_Node {
    Pre_Node metadata;
    String update;
    int size;
    Pre_Node textureProperties;
    static Pre_GeneratedCubeMapTextureRenderCallback Pre_renderCallbackGeneratedCubeMapTexture;
    static void setPre_GeneratedCubeMapTextureRenderCallback(Pre_GeneratedCubeMapTextureRenderCallback node) {
        Pre_renderCallbackGeneratedCubeMapTexture = node;
    }

    static Pre_GeneratedCubeMapTextureTreeRenderCallback Pre_treeRenderCallbackGeneratedCubeMapTexture;
    static void setPre_GeneratedCubeMapTextureTreeRenderCallback(Pre_GeneratedCubeMapTextureTreeRenderCallback node) {
        Pre_treeRenderCallbackGeneratedCubeMapTexture = node;
    }

    static Pre_GeneratedCubeMapTextureDoWithDataCallback Pre_doWithDataCallbackGeneratedCubeMapTexture;
    static void setPre_GeneratedCubeMapTextureDoWithDataCallback(Pre_GeneratedCubeMapTextureDoWithDataCallback node) {
        Pre_doWithDataCallbackGeneratedCubeMapTexture = node;
    }

    static Pre_GeneratedCubeMapTextureTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeneratedCubeMapTexture;
    static void setPre_GeneratedCubeMapTextureTreeDoWithDataCallback(Pre_GeneratedCubeMapTextureTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeneratedCubeMapTexture = node;

    }

    static Pre_GeneratedCubeMapTextureEventsProcessedCallback Pre_eventsProcessedCallbackGeneratedCubeMapTexture;
    static void setPre_GeneratedCubeMapTextureEventsProcessedCallback(Pre_GeneratedCubeMapTextureEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeneratedCubeMapTexture = node;
    }

    Pre_GeneratedCubeMapTexture() {
    }
    void treeRender() {
        if (Pre_GeneratedCubeMapTexture.Pre_treeRenderCallbackGeneratedCubeMapTexture != null) {
            Pre_GeneratedCubeMapTexture.Pre_treeRenderCallbackGeneratedCubeMapTexture.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeneratedCubeMapTexture.Pre_renderCallbackGeneratedCubeMapTexture != null)
            Pre_GeneratedCubeMapTexture.Pre_renderCallbackGeneratedCubeMapTexture.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeneratedCubeMapTexture.Pre_treeDoWithDataCallbackGeneratedCubeMapTexture != null) {
            Pre_GeneratedCubeMapTexture.Pre_treeDoWithDataCallbackGeneratedCubeMapTexture.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeneratedCubeMapTexture.Pre_doWithDataCallbackGeneratedCubeMapTexture != null)
            Pre_GeneratedCubeMapTexture.Pre_doWithDataCallbackGeneratedCubeMapTexture.doWithData(this);
    }
}

class Pre_GeneratedCubeMapTextureRenderCallback extends RenderCallback {}
class Pre_GeneratedCubeMapTextureTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeneratedCubeMapTextureDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeneratedCubeMapTextureTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeneratedCubeMapTextureEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MetadataDouble extends Pre_Node {
    Pre_Node metadata;
    String name;
    String reference;
    double[] value;
    static Pre_MetadataDoubleRenderCallback Pre_renderCallbackMetadataDouble;
    static void setPre_MetadataDoubleRenderCallback(Pre_MetadataDoubleRenderCallback node) {
        Pre_renderCallbackMetadataDouble = node;
    }

    static Pre_MetadataDoubleTreeRenderCallback Pre_treeRenderCallbackMetadataDouble;
    static void setPre_MetadataDoubleTreeRenderCallback(Pre_MetadataDoubleTreeRenderCallback node) {
        Pre_treeRenderCallbackMetadataDouble = node;
    }

    static Pre_MetadataDoubleDoWithDataCallback Pre_doWithDataCallbackMetadataDouble;
    static void setPre_MetadataDoubleDoWithDataCallback(Pre_MetadataDoubleDoWithDataCallback node) {
        Pre_doWithDataCallbackMetadataDouble = node;
    }

    static Pre_MetadataDoubleTreeDoWithDataCallback Pre_treeDoWithDataCallbackMetadataDouble;
    static void setPre_MetadataDoubleTreeDoWithDataCallback(Pre_MetadataDoubleTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMetadataDouble = node;

    }

    static Pre_MetadataDoubleEventsProcessedCallback Pre_eventsProcessedCallbackMetadataDouble;
    static void setPre_MetadataDoubleEventsProcessedCallback(Pre_MetadataDoubleEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMetadataDouble = node;
    }

    Pre_MetadataDouble() {
    }
    void treeRender() {
        if (Pre_MetadataDouble.Pre_treeRenderCallbackMetadataDouble != null) {
            Pre_MetadataDouble.Pre_treeRenderCallbackMetadataDouble.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MetadataDouble.Pre_renderCallbackMetadataDouble != null)
            Pre_MetadataDouble.Pre_renderCallbackMetadataDouble.render(this);
    }
    void treeDoWithData() {
        if (Pre_MetadataDouble.Pre_treeDoWithDataCallbackMetadataDouble != null) {
            Pre_MetadataDouble.Pre_treeDoWithDataCallbackMetadataDouble.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MetadataDouble.Pre_doWithDataCallbackMetadataDouble != null)
            Pre_MetadataDouble.Pre_doWithDataCallbackMetadataDouble.doWithData(this);
    }
}

class Pre_MetadataDoubleRenderCallback extends RenderCallback {}
class Pre_MetadataDoubleTreeRenderCallback extends TreeRenderCallback {}
class Pre_MetadataDoubleDoWithDataCallback extends DoWithDataCallback {};
class Pre_MetadataDoubleTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MetadataDoubleEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TrimmedSurface extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] trimmingContour;
    Pre_Node surface;
    static Pre_TrimmedSurfaceRenderCallback Pre_renderCallbackTrimmedSurface;
    static void setPre_TrimmedSurfaceRenderCallback(Pre_TrimmedSurfaceRenderCallback node) {
        Pre_renderCallbackTrimmedSurface = node;
    }

    static Pre_TrimmedSurfaceTreeRenderCallback Pre_treeRenderCallbackTrimmedSurface;
    static void setPre_TrimmedSurfaceTreeRenderCallback(Pre_TrimmedSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackTrimmedSurface = node;
    }

    static Pre_TrimmedSurfaceDoWithDataCallback Pre_doWithDataCallbackTrimmedSurface;
    static void setPre_TrimmedSurfaceDoWithDataCallback(Pre_TrimmedSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackTrimmedSurface = node;
    }

    static Pre_TrimmedSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackTrimmedSurface;
    static void setPre_TrimmedSurfaceTreeDoWithDataCallback(Pre_TrimmedSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTrimmedSurface = node;

    }

    static Pre_TrimmedSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackTrimmedSurface;
    static void setPre_TrimmedSurfaceEventsProcessedCallback(Pre_TrimmedSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTrimmedSurface = node;
    }

    Pre_TrimmedSurface() {
    }
    void treeRender() {
        if (Pre_TrimmedSurface.Pre_treeRenderCallbackTrimmedSurface != null) {
            Pre_TrimmedSurface.Pre_treeRenderCallbackTrimmedSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (trimmingContour != null)
            for (int i = 0; i < trimmingContour.length; i++)
                if (trimmingContour[i] != null)
                    trimmingContour[i].treeRender();
        if (surface != null)
            surface.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TrimmedSurface.Pre_renderCallbackTrimmedSurface != null)
            Pre_TrimmedSurface.Pre_renderCallbackTrimmedSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_TrimmedSurface.Pre_treeDoWithDataCallbackTrimmedSurface != null) {
            Pre_TrimmedSurface.Pre_treeDoWithDataCallbackTrimmedSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (trimmingContour != null)
            for (int i = 0; i < trimmingContour.length; i++)
                if (trimmingContour[i] != null)
                    trimmingContour[i].treeDoWithData();
        if (surface != null)
            surface.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TrimmedSurface.Pre_doWithDataCallbackTrimmedSurface != null)
            Pre_TrimmedSurface.Pre_doWithDataCallbackTrimmedSurface.doWithData(this);
    }
}

class Pre_TrimmedSurfaceRenderCallback extends RenderCallback {}
class Pre_TrimmedSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_TrimmedSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_TrimmedSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TrimmedSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CADAssembly extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    String name;
    static Pre_CADAssemblyRenderCallback Pre_renderCallbackCADAssembly;
    static void setPre_CADAssemblyRenderCallback(Pre_CADAssemblyRenderCallback node) {
        Pre_renderCallbackCADAssembly = node;
    }

    static Pre_CADAssemblyTreeRenderCallback Pre_treeRenderCallbackCADAssembly;
    static void setPre_CADAssemblyTreeRenderCallback(Pre_CADAssemblyTreeRenderCallback node) {
        Pre_treeRenderCallbackCADAssembly = node;
    }

    static Pre_CADAssemblyDoWithDataCallback Pre_doWithDataCallbackCADAssembly;
    static void setPre_CADAssemblyDoWithDataCallback(Pre_CADAssemblyDoWithDataCallback node) {
        Pre_doWithDataCallbackCADAssembly = node;
    }

    static Pre_CADAssemblyTreeDoWithDataCallback Pre_treeDoWithDataCallbackCADAssembly;
    static void setPre_CADAssemblyTreeDoWithDataCallback(Pre_CADAssemblyTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCADAssembly = node;

    }

    static Pre_CADAssemblyEventsProcessedCallback Pre_eventsProcessedCallbackCADAssembly;
    static void setPre_CADAssemblyEventsProcessedCallback(Pre_CADAssemblyEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCADAssembly = node;
    }

    Pre_CADAssembly() {
    }
    void treeRender() {
        if (Pre_CADAssembly.Pre_treeRenderCallbackCADAssembly != null) {
            Pre_CADAssembly.Pre_treeRenderCallbackCADAssembly.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CADAssembly.Pre_renderCallbackCADAssembly != null)
            Pre_CADAssembly.Pre_renderCallbackCADAssembly.render(this);
    }
    void treeDoWithData() {
        if (Pre_CADAssembly.Pre_treeDoWithDataCallbackCADAssembly != null) {
            Pre_CADAssembly.Pre_treeDoWithDataCallbackCADAssembly.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CADAssembly.Pre_doWithDataCallbackCADAssembly != null)
            Pre_CADAssembly.Pre_doWithDataCallbackCADAssembly.doWithData(this);
    }
}

class Pre_CADAssemblyRenderCallback extends RenderCallback {}
class Pre_CADAssemblyTreeRenderCallback extends TreeRenderCallback {}
class Pre_CADAssemblyDoWithDataCallback extends DoWithDataCallback {};
class Pre_CADAssemblyTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CADAssemblyEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BoundedPhysicsModel extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    Pre_Node geometry;
    static Pre_BoundedPhysicsModelRenderCallback Pre_renderCallbackBoundedPhysicsModel;
    static void setPre_BoundedPhysicsModelRenderCallback(Pre_BoundedPhysicsModelRenderCallback node) {
        Pre_renderCallbackBoundedPhysicsModel = node;
    }

    static Pre_BoundedPhysicsModelTreeRenderCallback Pre_treeRenderCallbackBoundedPhysicsModel;
    static void setPre_BoundedPhysicsModelTreeRenderCallback(Pre_BoundedPhysicsModelTreeRenderCallback node) {
        Pre_treeRenderCallbackBoundedPhysicsModel = node;
    }

    static Pre_BoundedPhysicsModelDoWithDataCallback Pre_doWithDataCallbackBoundedPhysicsModel;
    static void setPre_BoundedPhysicsModelDoWithDataCallback(Pre_BoundedPhysicsModelDoWithDataCallback node) {
        Pre_doWithDataCallbackBoundedPhysicsModel = node;
    }

    static Pre_BoundedPhysicsModelTreeDoWithDataCallback Pre_treeDoWithDataCallbackBoundedPhysicsModel;
    static void setPre_BoundedPhysicsModelTreeDoWithDataCallback(Pre_BoundedPhysicsModelTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBoundedPhysicsModel = node;

    }

    static Pre_BoundedPhysicsModelEventsProcessedCallback Pre_eventsProcessedCallbackBoundedPhysicsModel;
    static void setPre_BoundedPhysicsModelEventsProcessedCallback(Pre_BoundedPhysicsModelEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBoundedPhysicsModel = node;
    }

    Pre_BoundedPhysicsModel() {
    }
    void treeRender() {
        if (Pre_BoundedPhysicsModel.Pre_treeRenderCallbackBoundedPhysicsModel != null) {
            Pre_BoundedPhysicsModel.Pre_treeRenderCallbackBoundedPhysicsModel.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geometry != null)
            geometry.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BoundedPhysicsModel.Pre_renderCallbackBoundedPhysicsModel != null)
            Pre_BoundedPhysicsModel.Pre_renderCallbackBoundedPhysicsModel.render(this);
    }
    void treeDoWithData() {
        if (Pre_BoundedPhysicsModel.Pre_treeDoWithDataCallbackBoundedPhysicsModel != null) {
            Pre_BoundedPhysicsModel.Pre_treeDoWithDataCallbackBoundedPhysicsModel.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geometry != null)
            geometry.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BoundedPhysicsModel.Pre_doWithDataCallbackBoundedPhysicsModel != null)
            Pre_BoundedPhysicsModel.Pre_doWithDataCallbackBoundedPhysicsModel.doWithData(this);
    }
}

class Pre_BoundedPhysicsModelRenderCallback extends RenderCallback {}
class Pre_BoundedPhysicsModelTreeRenderCallback extends TreeRenderCallback {}
class Pre_BoundedPhysicsModelDoWithDataCallback extends DoWithDataCallback {};
class Pre_BoundedPhysicsModelTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BoundedPhysicsModelEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GravityPhysicsModel extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] force;
    static Pre_GravityPhysicsModelRenderCallback Pre_renderCallbackGravityPhysicsModel;
    static void setPre_GravityPhysicsModelRenderCallback(Pre_GravityPhysicsModelRenderCallback node) {
        Pre_renderCallbackGravityPhysicsModel = node;
    }

    static Pre_GravityPhysicsModelTreeRenderCallback Pre_treeRenderCallbackGravityPhysicsModel;
    static void setPre_GravityPhysicsModelTreeRenderCallback(Pre_GravityPhysicsModelTreeRenderCallback node) {
        Pre_treeRenderCallbackGravityPhysicsModel = node;
    }

    static Pre_GravityPhysicsModelDoWithDataCallback Pre_doWithDataCallbackGravityPhysicsModel;
    static void setPre_GravityPhysicsModelDoWithDataCallback(Pre_GravityPhysicsModelDoWithDataCallback node) {
        Pre_doWithDataCallbackGravityPhysicsModel = node;
    }

    static Pre_GravityPhysicsModelTreeDoWithDataCallback Pre_treeDoWithDataCallbackGravityPhysicsModel;
    static void setPre_GravityPhysicsModelTreeDoWithDataCallback(Pre_GravityPhysicsModelTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGravityPhysicsModel = node;

    }

    static Pre_GravityPhysicsModelEventsProcessedCallback Pre_eventsProcessedCallbackGravityPhysicsModel;
    static void setPre_GravityPhysicsModelEventsProcessedCallback(Pre_GravityPhysicsModelEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGravityPhysicsModel = node;
    }

    Pre_GravityPhysicsModel() {
    }
    void treeRender() {
        if (Pre_GravityPhysicsModel.Pre_treeRenderCallbackGravityPhysicsModel != null) {
            Pre_GravityPhysicsModel.Pre_treeRenderCallbackGravityPhysicsModel.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GravityPhysicsModel.Pre_renderCallbackGravityPhysicsModel != null)
            Pre_GravityPhysicsModel.Pre_renderCallbackGravityPhysicsModel.render(this);
    }
    void treeDoWithData() {
        if (Pre_GravityPhysicsModel.Pre_treeDoWithDataCallbackGravityPhysicsModel != null) {
            Pre_GravityPhysicsModel.Pre_treeDoWithDataCallbackGravityPhysicsModel.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GravityPhysicsModel.Pre_doWithDataCallbackGravityPhysicsModel != null)
            Pre_GravityPhysicsModel.Pre_doWithDataCallbackGravityPhysicsModel.doWithData(this);
    }
}

class Pre_GravityPhysicsModelRenderCallback extends RenderCallback {}
class Pre_GravityPhysicsModelTreeRenderCallback extends TreeRenderCallback {}
class Pre_GravityPhysicsModelDoWithDataCallback extends DoWithDataCallback {};
class Pre_GravityPhysicsModelTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GravityPhysicsModelEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Group extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_GroupRenderCallback Pre_renderCallbackGroup;
    static void setPre_GroupRenderCallback(Pre_GroupRenderCallback node) {
        Pre_renderCallbackGroup = node;
    }

    static Pre_GroupTreeRenderCallback Pre_treeRenderCallbackGroup;
    static void setPre_GroupTreeRenderCallback(Pre_GroupTreeRenderCallback node) {
        Pre_treeRenderCallbackGroup = node;
    }

    static Pre_GroupDoWithDataCallback Pre_doWithDataCallbackGroup;
    static void setPre_GroupDoWithDataCallback(Pre_GroupDoWithDataCallback node) {
        Pre_doWithDataCallbackGroup = node;
    }

    static Pre_GroupTreeDoWithDataCallback Pre_treeDoWithDataCallbackGroup;
    static void setPre_GroupTreeDoWithDataCallback(Pre_GroupTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGroup = node;

    }

    static Pre_GroupEventsProcessedCallback Pre_eventsProcessedCallbackGroup;
    static void setPre_GroupEventsProcessedCallback(Pre_GroupEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGroup = node;
    }

    Pre_Group() {
    }
    void treeRender() {
        if (Pre_Group.Pre_treeRenderCallbackGroup != null) {
            Pre_Group.Pre_treeRenderCallbackGroup.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Group.Pre_renderCallbackGroup != null)
            Pre_Group.Pre_renderCallbackGroup.render(this);
    }
    void treeDoWithData() {
        if (Pre_Group.Pre_treeDoWithDataCallbackGroup != null) {
            Pre_Group.Pre_treeDoWithDataCallbackGroup.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Group.Pre_doWithDataCallbackGroup != null)
            Pre_Group.Pre_doWithDataCallbackGroup.doWithData(this);
    }
}

class Pre_GroupRenderCallback extends RenderCallback {}
class Pre_GroupTreeRenderCallback extends TreeRenderCallback {}
class Pre_GroupDoWithDataCallback extends DoWithDataCallback {};
class Pre_GroupTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GroupEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CoordinateDeformer extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    Pre_Node [] outputCoord;
    Pre_Node [] inputCoord;
    float[] controlPoint;
    float[] bboxCenter;
    float[] bboxSize;
    float[] weight;
    int uDimension;
    int vDimension;
    int wDimension;
    float[] uKnot;
    float[] vKnot;
    float[] wKnot;
    int uOrder;
    int vOrder;
    int wOrder;
    static Pre_CoordinateDeformerRenderCallback Pre_renderCallbackCoordinateDeformer;
    static void setPre_CoordinateDeformerRenderCallback(Pre_CoordinateDeformerRenderCallback node) {
        Pre_renderCallbackCoordinateDeformer = node;
    }

    static Pre_CoordinateDeformerTreeRenderCallback Pre_treeRenderCallbackCoordinateDeformer;
    static void setPre_CoordinateDeformerTreeRenderCallback(Pre_CoordinateDeformerTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinateDeformer = node;
    }

    static Pre_CoordinateDeformerDoWithDataCallback Pre_doWithDataCallbackCoordinateDeformer;
    static void setPre_CoordinateDeformerDoWithDataCallback(Pre_CoordinateDeformerDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinateDeformer = node;
    }

    static Pre_CoordinateDeformerTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinateDeformer;
    static void setPre_CoordinateDeformerTreeDoWithDataCallback(Pre_CoordinateDeformerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinateDeformer = node;

    }

    static Pre_CoordinateDeformerEventsProcessedCallback Pre_eventsProcessedCallbackCoordinateDeformer;
    static void setPre_CoordinateDeformerEventsProcessedCallback(Pre_CoordinateDeformerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinateDeformer = node;
    }

    Pre_CoordinateDeformer() {
    }
    void treeRender() {
        if (Pre_CoordinateDeformer.Pre_treeRenderCallbackCoordinateDeformer != null) {
            Pre_CoordinateDeformer.Pre_treeRenderCallbackCoordinateDeformer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (outputCoord != null)
            for (int i = 0; i < outputCoord.length; i++)
                if (outputCoord[i] != null)
                    outputCoord[i].treeRender();
        if (inputCoord != null)
            for (int i = 0; i < inputCoord.length; i++)
                if (inputCoord[i] != null)
                    inputCoord[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CoordinateDeformer.Pre_renderCallbackCoordinateDeformer != null)
            Pre_CoordinateDeformer.Pre_renderCallbackCoordinateDeformer.render(this);
    }
    void treeDoWithData() {
        if (Pre_CoordinateDeformer.Pre_treeDoWithDataCallbackCoordinateDeformer != null) {
            Pre_CoordinateDeformer.Pre_treeDoWithDataCallbackCoordinateDeformer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (outputCoord != null)
            for (int i = 0; i < outputCoord.length; i++)
                if (outputCoord[i] != null)
                    outputCoord[i].treeDoWithData();
        if (inputCoord != null)
            for (int i = 0; i < inputCoord.length; i++)
                if (inputCoord[i] != null)
                    inputCoord[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CoordinateDeformer.Pre_doWithDataCallbackCoordinateDeformer != null)
            Pre_CoordinateDeformer.Pre_doWithDataCallbackCoordinateDeformer.doWithData(this);
    }
}

class Pre_CoordinateDeformerRenderCallback extends RenderCallback {}
class Pre_CoordinateDeformerTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateDeformerDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateDeformerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateDeformerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_GeoPositionInterpolator extends Pre_Node {
    Pre_Node metadata;
    Pre_Node geoOrigin;
    String[] geoSystem;
    float[] key;
    double[] keyValue;
    static Pre_GeoPositionInterpolatorRenderCallback Pre_renderCallbackGeoPositionInterpolator;
    static void setPre_GeoPositionInterpolatorRenderCallback(Pre_GeoPositionInterpolatorRenderCallback node) {
        Pre_renderCallbackGeoPositionInterpolator = node;
    }

    static Pre_GeoPositionInterpolatorTreeRenderCallback Pre_treeRenderCallbackGeoPositionInterpolator;
    static void setPre_GeoPositionInterpolatorTreeRenderCallback(Pre_GeoPositionInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackGeoPositionInterpolator = node;
    }

    static Pre_GeoPositionInterpolatorDoWithDataCallback Pre_doWithDataCallbackGeoPositionInterpolator;
    static void setPre_GeoPositionInterpolatorDoWithDataCallback(Pre_GeoPositionInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackGeoPositionInterpolator = node;
    }

    static Pre_GeoPositionInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackGeoPositionInterpolator;
    static void setPre_GeoPositionInterpolatorTreeDoWithDataCallback(Pre_GeoPositionInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackGeoPositionInterpolator = node;

    }

    static Pre_GeoPositionInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackGeoPositionInterpolator;
    static void setPre_GeoPositionInterpolatorEventsProcessedCallback(Pre_GeoPositionInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackGeoPositionInterpolator = node;
    }

    Pre_GeoPositionInterpolator() {
    }
    void treeRender() {
        if (Pre_GeoPositionInterpolator.Pre_treeRenderCallbackGeoPositionInterpolator != null) {
            Pre_GeoPositionInterpolator.Pre_treeRenderCallbackGeoPositionInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (geoOrigin != null)
            geoOrigin.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_GeoPositionInterpolator.Pre_renderCallbackGeoPositionInterpolator != null)
            Pre_GeoPositionInterpolator.Pre_renderCallbackGeoPositionInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_GeoPositionInterpolator.Pre_treeDoWithDataCallbackGeoPositionInterpolator != null) {
            Pre_GeoPositionInterpolator.Pre_treeDoWithDataCallbackGeoPositionInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (geoOrigin != null)
            geoOrigin.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_GeoPositionInterpolator.Pre_doWithDataCallbackGeoPositionInterpolator != null)
            Pre_GeoPositionInterpolator.Pre_doWithDataCallbackGeoPositionInterpolator.doWithData(this);
    }
}

class Pre_GeoPositionInterpolatorRenderCallback extends RenderCallback {}
class Pre_GeoPositionInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_GeoPositionInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_GeoPositionInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_GeoPositionInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Layer extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    boolean isPickable;
    Pre_Node viewport;
    static Pre_LayerRenderCallback Pre_renderCallbackLayer;
    static void setPre_LayerRenderCallback(Pre_LayerRenderCallback node) {
        Pre_renderCallbackLayer = node;
    }

    static Pre_LayerTreeRenderCallback Pre_treeRenderCallbackLayer;
    static void setPre_LayerTreeRenderCallback(Pre_LayerTreeRenderCallback node) {
        Pre_treeRenderCallbackLayer = node;
    }

    static Pre_LayerDoWithDataCallback Pre_doWithDataCallbackLayer;
    static void setPre_LayerDoWithDataCallback(Pre_LayerDoWithDataCallback node) {
        Pre_doWithDataCallbackLayer = node;
    }

    static Pre_LayerTreeDoWithDataCallback Pre_treeDoWithDataCallbackLayer;
    static void setPre_LayerTreeDoWithDataCallback(Pre_LayerTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLayer = node;

    }

    static Pre_LayerEventsProcessedCallback Pre_eventsProcessedCallbackLayer;
    static void setPre_LayerEventsProcessedCallback(Pre_LayerEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLayer = node;
    }

    Pre_Layer() {
    }
    void treeRender() {
        if (Pre_Layer.Pre_treeRenderCallbackLayer != null) {
            Pre_Layer.Pre_treeRenderCallbackLayer.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (viewport != null)
            viewport.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Layer.Pre_renderCallbackLayer != null)
            Pre_Layer.Pre_renderCallbackLayer.render(this);
    }
    void treeDoWithData() {
        if (Pre_Layer.Pre_treeDoWithDataCallbackLayer != null) {
            Pre_Layer.Pre_treeDoWithDataCallbackLayer.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (viewport != null)
            viewport.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Layer.Pre_doWithDataCallbackLayer != null)
            Pre_Layer.Pre_doWithDataCallbackLayer.doWithData(this);
    }
}

class Pre_LayerRenderCallback extends RenderCallback {}
class Pre_LayerTreeRenderCallback extends TreeRenderCallback {}
class Pre_LayerDoWithDataCallback extends DoWithDataCallback {};
class Pre_LayerTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LayerEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PolylineEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    Pre_Node coord;
    float[] direction;
    int[] coordIndex;
    static Pre_PolylineEmitterRenderCallback Pre_renderCallbackPolylineEmitter;
    static void setPre_PolylineEmitterRenderCallback(Pre_PolylineEmitterRenderCallback node) {
        Pre_renderCallbackPolylineEmitter = node;
    }

    static Pre_PolylineEmitterTreeRenderCallback Pre_treeRenderCallbackPolylineEmitter;
    static void setPre_PolylineEmitterTreeRenderCallback(Pre_PolylineEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackPolylineEmitter = node;
    }

    static Pre_PolylineEmitterDoWithDataCallback Pre_doWithDataCallbackPolylineEmitter;
    static void setPre_PolylineEmitterDoWithDataCallback(Pre_PolylineEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackPolylineEmitter = node;
    }

    static Pre_PolylineEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackPolylineEmitter;
    static void setPre_PolylineEmitterTreeDoWithDataCallback(Pre_PolylineEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPolylineEmitter = node;

    }

    static Pre_PolylineEmitterEventsProcessedCallback Pre_eventsProcessedCallbackPolylineEmitter;
    static void setPre_PolylineEmitterEventsProcessedCallback(Pre_PolylineEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPolylineEmitter = node;
    }

    Pre_PolylineEmitter() {
    }
    void treeRender() {
        if (Pre_PolylineEmitter.Pre_treeRenderCallbackPolylineEmitter != null) {
            Pre_PolylineEmitter.Pre_treeRenderCallbackPolylineEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (coord != null)
            coord.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PolylineEmitter.Pre_renderCallbackPolylineEmitter != null)
            Pre_PolylineEmitter.Pre_renderCallbackPolylineEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_PolylineEmitter.Pre_treeDoWithDataCallbackPolylineEmitter != null) {
            Pre_PolylineEmitter.Pre_treeDoWithDataCallbackPolylineEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (coord != null)
            coord.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PolylineEmitter.Pre_doWithDataCallbackPolylineEmitter != null)
            Pre_PolylineEmitter.Pre_doWithDataCallbackPolylineEmitter.doWithData(this);
    }
}

class Pre_PolylineEmitterRenderCallback extends RenderCallback {}
class Pre_PolylineEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_PolylineEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_PolylineEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PolylineEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionInterpolator extends Pre_Node {
    Pre_Node metadata;
    float[] key;
    float[] keyValue;
    static Pre_PositionInterpolatorRenderCallback Pre_renderCallbackPositionInterpolator;
    static void setPre_PositionInterpolatorRenderCallback(Pre_PositionInterpolatorRenderCallback node) {
        Pre_renderCallbackPositionInterpolator = node;
    }

    static Pre_PositionInterpolatorTreeRenderCallback Pre_treeRenderCallbackPositionInterpolator;
    static void setPre_PositionInterpolatorTreeRenderCallback(Pre_PositionInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionInterpolator = node;
    }

    static Pre_PositionInterpolatorDoWithDataCallback Pre_doWithDataCallbackPositionInterpolator;
    static void setPre_PositionInterpolatorDoWithDataCallback(Pre_PositionInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionInterpolator = node;
    }

    static Pre_PositionInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionInterpolator;
    static void setPre_PositionInterpolatorTreeDoWithDataCallback(Pre_PositionInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionInterpolator = node;

    }

    static Pre_PositionInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackPositionInterpolator;
    static void setPre_PositionInterpolatorEventsProcessedCallback(Pre_PositionInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionInterpolator = node;
    }

    Pre_PositionInterpolator() {
    }
    void treeRender() {
        if (Pre_PositionInterpolator.Pre_treeRenderCallbackPositionInterpolator != null) {
            Pre_PositionInterpolator.Pre_treeRenderCallbackPositionInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionInterpolator.Pre_renderCallbackPositionInterpolator != null)
            Pre_PositionInterpolator.Pre_renderCallbackPositionInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionInterpolator.Pre_treeDoWithDataCallbackPositionInterpolator != null) {
            Pre_PositionInterpolator.Pre_treeDoWithDataCallbackPositionInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionInterpolator.Pre_doWithDataCallbackPositionInterpolator != null)
            Pre_PositionInterpolator.Pre_doWithDataCallbackPositionInterpolator.doWithData(this);
    }
}

class Pre_PositionInterpolatorRenderCallback extends RenderCallback {}
class Pre_PositionInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PrimitivePickSensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    String[] objectType;
    Pre_Node pickingGeometry;
    Pre_Node [] pickTarget;
    String intersectionType;
    String sortOrder;
    static Pre_PrimitivePickSensorRenderCallback Pre_renderCallbackPrimitivePickSensor;
    static void setPre_PrimitivePickSensorRenderCallback(Pre_PrimitivePickSensorRenderCallback node) {
        Pre_renderCallbackPrimitivePickSensor = node;
    }

    static Pre_PrimitivePickSensorTreeRenderCallback Pre_treeRenderCallbackPrimitivePickSensor;
    static void setPre_PrimitivePickSensorTreeRenderCallback(Pre_PrimitivePickSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackPrimitivePickSensor = node;
    }

    static Pre_PrimitivePickSensorDoWithDataCallback Pre_doWithDataCallbackPrimitivePickSensor;
    static void setPre_PrimitivePickSensorDoWithDataCallback(Pre_PrimitivePickSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackPrimitivePickSensor = node;
    }

    static Pre_PrimitivePickSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackPrimitivePickSensor;
    static void setPre_PrimitivePickSensorTreeDoWithDataCallback(Pre_PrimitivePickSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPrimitivePickSensor = node;

    }

    static Pre_PrimitivePickSensorEventsProcessedCallback Pre_eventsProcessedCallbackPrimitivePickSensor;
    static void setPre_PrimitivePickSensorEventsProcessedCallback(Pre_PrimitivePickSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPrimitivePickSensor = node;
    }

    Pre_PrimitivePickSensor() {
    }
    void treeRender() {
        if (Pre_PrimitivePickSensor.Pre_treeRenderCallbackPrimitivePickSensor != null) {
            Pre_PrimitivePickSensor.Pre_treeRenderCallbackPrimitivePickSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (pickingGeometry != null)
            pickingGeometry.treeRender();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PrimitivePickSensor.Pre_renderCallbackPrimitivePickSensor != null)
            Pre_PrimitivePickSensor.Pre_renderCallbackPrimitivePickSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_PrimitivePickSensor.Pre_treeDoWithDataCallbackPrimitivePickSensor != null) {
            Pre_PrimitivePickSensor.Pre_treeDoWithDataCallbackPrimitivePickSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (pickingGeometry != null)
            pickingGeometry.treeDoWithData();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PrimitivePickSensor.Pre_doWithDataCallbackPrimitivePickSensor != null)
            Pre_PrimitivePickSensor.Pre_doWithDataCallbackPrimitivePickSensor.doWithData(this);
    }
}

class Pre_PrimitivePickSensorRenderCallback extends RenderCallback {}
class Pre_PrimitivePickSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_PrimitivePickSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_PrimitivePickSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PrimitivePickSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Circle2D extends Pre_Node {
    Pre_Node metadata;
    float radius;
    static Pre_Circle2DRenderCallback Pre_renderCallbackCircle2D;
    static void setPre_Circle2DRenderCallback(Pre_Circle2DRenderCallback node) {
        Pre_renderCallbackCircle2D = node;
    }

    static Pre_Circle2DTreeRenderCallback Pre_treeRenderCallbackCircle2D;
    static void setPre_Circle2DTreeRenderCallback(Pre_Circle2DTreeRenderCallback node) {
        Pre_treeRenderCallbackCircle2D = node;
    }

    static Pre_Circle2DDoWithDataCallback Pre_doWithDataCallbackCircle2D;
    static void setPre_Circle2DDoWithDataCallback(Pre_Circle2DDoWithDataCallback node) {
        Pre_doWithDataCallbackCircle2D = node;
    }

    static Pre_Circle2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackCircle2D;
    static void setPre_Circle2DTreeDoWithDataCallback(Pre_Circle2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCircle2D = node;

    }

    static Pre_Circle2DEventsProcessedCallback Pre_eventsProcessedCallbackCircle2D;
    static void setPre_Circle2DEventsProcessedCallback(Pre_Circle2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCircle2D = node;
    }

    Pre_Circle2D() {
    }
    void treeRender() {
        if (Pre_Circle2D.Pre_treeRenderCallbackCircle2D != null) {
            Pre_Circle2D.Pre_treeRenderCallbackCircle2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Circle2D.Pre_renderCallbackCircle2D != null)
            Pre_Circle2D.Pre_renderCallbackCircle2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Circle2D.Pre_treeDoWithDataCallbackCircle2D != null) {
            Pre_Circle2D.Pre_treeDoWithDataCallbackCircle2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Circle2D.Pre_doWithDataCallbackCircle2D != null)
            Pre_Circle2D.Pre_doWithDataCallbackCircle2D.doWithData(this);
    }
}

class Pre_Circle2DRenderCallback extends RenderCallback {}
class Pre_Circle2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Circle2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Circle2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Circle2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ComposedTexture3D extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] texture;
    boolean repeatS;
    boolean repeatR;
    boolean repeatT;
    static Pre_ComposedTexture3DRenderCallback Pre_renderCallbackComposedTexture3D;
    static void setPre_ComposedTexture3DRenderCallback(Pre_ComposedTexture3DRenderCallback node) {
        Pre_renderCallbackComposedTexture3D = node;
    }

    static Pre_ComposedTexture3DTreeRenderCallback Pre_treeRenderCallbackComposedTexture3D;
    static void setPre_ComposedTexture3DTreeRenderCallback(Pre_ComposedTexture3DTreeRenderCallback node) {
        Pre_treeRenderCallbackComposedTexture3D = node;
    }

    static Pre_ComposedTexture3DDoWithDataCallback Pre_doWithDataCallbackComposedTexture3D;
    static void setPre_ComposedTexture3DDoWithDataCallback(Pre_ComposedTexture3DDoWithDataCallback node) {
        Pre_doWithDataCallbackComposedTexture3D = node;
    }

    static Pre_ComposedTexture3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackComposedTexture3D;
    static void setPre_ComposedTexture3DTreeDoWithDataCallback(Pre_ComposedTexture3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackComposedTexture3D = node;

    }

    static Pre_ComposedTexture3DEventsProcessedCallback Pre_eventsProcessedCallbackComposedTexture3D;
    static void setPre_ComposedTexture3DEventsProcessedCallback(Pre_ComposedTexture3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackComposedTexture3D = node;
    }

    Pre_ComposedTexture3D() {
    }
    void treeRender() {
        if (Pre_ComposedTexture3D.Pre_treeRenderCallbackComposedTexture3D != null) {
            Pre_ComposedTexture3D.Pre_treeRenderCallbackComposedTexture3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (texture != null)
            for (int i = 0; i < texture.length; i++)
                if (texture[i] != null)
                    texture[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ComposedTexture3D.Pre_renderCallbackComposedTexture3D != null)
            Pre_ComposedTexture3D.Pre_renderCallbackComposedTexture3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_ComposedTexture3D.Pre_treeDoWithDataCallbackComposedTexture3D != null) {
            Pre_ComposedTexture3D.Pre_treeDoWithDataCallbackComposedTexture3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (texture != null)
            for (int i = 0; i < texture.length; i++)
                if (texture[i] != null)
                    texture[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ComposedTexture3D.Pre_doWithDataCallbackComposedTexture3D != null)
            Pre_ComposedTexture3D.Pre_doWithDataCallbackComposedTexture3D.doWithData(this);
    }
}

class Pre_ComposedTexture3DRenderCallback extends RenderCallback {}
class Pre_ComposedTexture3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_ComposedTexture3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_ComposedTexture3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ComposedTexture3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Layout extends Pre_Node {
    Pre_Node metadata;
    String[] align;
    float[] offset;
    String[] offsetUnits;
    String[] scaleMode;
    float[] size;
    String[] sizeUnits;
    static Pre_LayoutRenderCallback Pre_renderCallbackLayout;
    static void setPre_LayoutRenderCallback(Pre_LayoutRenderCallback node) {
        Pre_renderCallbackLayout = node;
    }

    static Pre_LayoutTreeRenderCallback Pre_treeRenderCallbackLayout;
    static void setPre_LayoutTreeRenderCallback(Pre_LayoutTreeRenderCallback node) {
        Pre_treeRenderCallbackLayout = node;
    }

    static Pre_LayoutDoWithDataCallback Pre_doWithDataCallbackLayout;
    static void setPre_LayoutDoWithDataCallback(Pre_LayoutDoWithDataCallback node) {
        Pre_doWithDataCallbackLayout = node;
    }

    static Pre_LayoutTreeDoWithDataCallback Pre_treeDoWithDataCallbackLayout;
    static void setPre_LayoutTreeDoWithDataCallback(Pre_LayoutTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLayout = node;

    }

    static Pre_LayoutEventsProcessedCallback Pre_eventsProcessedCallbackLayout;
    static void setPre_LayoutEventsProcessedCallback(Pre_LayoutEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLayout = node;
    }

    Pre_Layout() {
    }
    void treeRender() {
        if (Pre_Layout.Pre_treeRenderCallbackLayout != null) {
            Pre_Layout.Pre_treeRenderCallbackLayout.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Layout.Pre_renderCallbackLayout != null)
            Pre_Layout.Pre_renderCallbackLayout.render(this);
    }
    void treeDoWithData() {
        if (Pre_Layout.Pre_treeDoWithDataCallbackLayout != null) {
            Pre_Layout.Pre_treeDoWithDataCallbackLayout.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Layout.Pre_doWithDataCallbackLayout != null)
            Pre_Layout.Pre_doWithDataCallbackLayout.doWithData(this);
    }
}

class Pre_LayoutRenderCallback extends RenderCallback {}
class Pre_LayoutTreeRenderCallback extends TreeRenderCallback {}
class Pre_LayoutDoWithDataCallback extends DoWithDataCallback {};
class Pre_LayoutTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LayoutEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_OrthoViewpoint extends Pre_Node {
    Pre_Node metadata;
    float[] centerOfRotation;
    String description;
    float[] fieldOfView;
    boolean jump;
    float[] orientation;
    float[] position;
    boolean retainUserOffsets;
    static Pre_OrthoViewpointRenderCallback Pre_renderCallbackOrthoViewpoint;
    static void setPre_OrthoViewpointRenderCallback(Pre_OrthoViewpointRenderCallback node) {
        Pre_renderCallbackOrthoViewpoint = node;
    }

    static Pre_OrthoViewpointTreeRenderCallback Pre_treeRenderCallbackOrthoViewpoint;
    static void setPre_OrthoViewpointTreeRenderCallback(Pre_OrthoViewpointTreeRenderCallback node) {
        Pre_treeRenderCallbackOrthoViewpoint = node;
    }

    static Pre_OrthoViewpointDoWithDataCallback Pre_doWithDataCallbackOrthoViewpoint;
    static void setPre_OrthoViewpointDoWithDataCallback(Pre_OrthoViewpointDoWithDataCallback node) {
        Pre_doWithDataCallbackOrthoViewpoint = node;
    }

    static Pre_OrthoViewpointTreeDoWithDataCallback Pre_treeDoWithDataCallbackOrthoViewpoint;
    static void setPre_OrthoViewpointTreeDoWithDataCallback(Pre_OrthoViewpointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackOrthoViewpoint = node;

    }

    static Pre_OrthoViewpointEventsProcessedCallback Pre_eventsProcessedCallbackOrthoViewpoint;
    static void setPre_OrthoViewpointEventsProcessedCallback(Pre_OrthoViewpointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackOrthoViewpoint = node;
    }

    Pre_OrthoViewpoint() {
    }
    void treeRender() {
        if (Pre_OrthoViewpoint.Pre_treeRenderCallbackOrthoViewpoint != null) {
            Pre_OrthoViewpoint.Pre_treeRenderCallbackOrthoViewpoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_OrthoViewpoint.Pre_renderCallbackOrthoViewpoint != null)
            Pre_OrthoViewpoint.Pre_renderCallbackOrthoViewpoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_OrthoViewpoint.Pre_treeDoWithDataCallbackOrthoViewpoint != null) {
            Pre_OrthoViewpoint.Pre_treeDoWithDataCallbackOrthoViewpoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_OrthoViewpoint.Pre_doWithDataCallbackOrthoViewpoint != null)
            Pre_OrthoViewpoint.Pre_doWithDataCallbackOrthoViewpoint.doWithData(this);
    }
}

class Pre_OrthoViewpointRenderCallback extends RenderCallback {}
class Pre_OrthoViewpointTreeRenderCallback extends TreeRenderCallback {}
class Pre_OrthoViewpointDoWithDataCallback extends DoWithDataCallback {};
class Pre_OrthoViewpointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_OrthoViewpointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PixelTexture3D extends Pre_Node {
    Pre_Node metadata;
    int[] image;
    boolean repeatS;
    boolean repeatR;
    boolean repeatT;
    Pre_Node textureProperties;
    static Pre_PixelTexture3DRenderCallback Pre_renderCallbackPixelTexture3D;
    static void setPre_PixelTexture3DRenderCallback(Pre_PixelTexture3DRenderCallback node) {
        Pre_renderCallbackPixelTexture3D = node;
    }

    static Pre_PixelTexture3DTreeRenderCallback Pre_treeRenderCallbackPixelTexture3D;
    static void setPre_PixelTexture3DTreeRenderCallback(Pre_PixelTexture3DTreeRenderCallback node) {
        Pre_treeRenderCallbackPixelTexture3D = node;
    }

    static Pre_PixelTexture3DDoWithDataCallback Pre_doWithDataCallbackPixelTexture3D;
    static void setPre_PixelTexture3DDoWithDataCallback(Pre_PixelTexture3DDoWithDataCallback node) {
        Pre_doWithDataCallbackPixelTexture3D = node;
    }

    static Pre_PixelTexture3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackPixelTexture3D;
    static void setPre_PixelTexture3DTreeDoWithDataCallback(Pre_PixelTexture3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPixelTexture3D = node;

    }

    static Pre_PixelTexture3DEventsProcessedCallback Pre_eventsProcessedCallbackPixelTexture3D;
    static void setPre_PixelTexture3DEventsProcessedCallback(Pre_PixelTexture3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPixelTexture3D = node;
    }

    Pre_PixelTexture3D() {
    }
    void treeRender() {
        if (Pre_PixelTexture3D.Pre_treeRenderCallbackPixelTexture3D != null) {
            Pre_PixelTexture3D.Pre_treeRenderCallbackPixelTexture3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (textureProperties != null)
            textureProperties.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PixelTexture3D.Pre_renderCallbackPixelTexture3D != null)
            Pre_PixelTexture3D.Pre_renderCallbackPixelTexture3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_PixelTexture3D.Pre_treeDoWithDataCallbackPixelTexture3D != null) {
            Pre_PixelTexture3D.Pre_treeDoWithDataCallbackPixelTexture3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (textureProperties != null)
            textureProperties.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PixelTexture3D.Pre_doWithDataCallbackPixelTexture3D != null)
            Pre_PixelTexture3D.Pre_doWithDataCallbackPixelTexture3D.doWithData(this);
    }
}

class Pre_PixelTexture3DRenderCallback extends RenderCallback {}
class Pre_PixelTexture3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_PixelTexture3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_PixelTexture3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PixelTexture3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Rectangle2D extends Pre_Node {
    Pre_Node metadata;
    float[] size;
    boolean solid;
    static Pre_Rectangle2DRenderCallback Pre_renderCallbackRectangle2D;
    static void setPre_Rectangle2DRenderCallback(Pre_Rectangle2DRenderCallback node) {
        Pre_renderCallbackRectangle2D = node;
    }

    static Pre_Rectangle2DTreeRenderCallback Pre_treeRenderCallbackRectangle2D;
    static void setPre_Rectangle2DTreeRenderCallback(Pre_Rectangle2DTreeRenderCallback node) {
        Pre_treeRenderCallbackRectangle2D = node;
    }

    static Pre_Rectangle2DDoWithDataCallback Pre_doWithDataCallbackRectangle2D;
    static void setPre_Rectangle2DDoWithDataCallback(Pre_Rectangle2DDoWithDataCallback node) {
        Pre_doWithDataCallbackRectangle2D = node;
    }

    static Pre_Rectangle2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackRectangle2D;
    static void setPre_Rectangle2DTreeDoWithDataCallback(Pre_Rectangle2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackRectangle2D = node;

    }

    static Pre_Rectangle2DEventsProcessedCallback Pre_eventsProcessedCallbackRectangle2D;
    static void setPre_Rectangle2DEventsProcessedCallback(Pre_Rectangle2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackRectangle2D = node;
    }

    Pre_Rectangle2D() {
    }
    void treeRender() {
        if (Pre_Rectangle2D.Pre_treeRenderCallbackRectangle2D != null) {
            Pre_Rectangle2D.Pre_treeRenderCallbackRectangle2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Rectangle2D.Pre_renderCallbackRectangle2D != null)
            Pre_Rectangle2D.Pre_renderCallbackRectangle2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_Rectangle2D.Pre_treeDoWithDataCallbackRectangle2D != null) {
            Pre_Rectangle2D.Pre_treeDoWithDataCallbackRectangle2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Rectangle2D.Pre_doWithDataCallbackRectangle2D != null)
            Pre_Rectangle2D.Pre_doWithDataCallbackRectangle2D.doWithData(this);
    }
}

class Pre_Rectangle2DRenderCallback extends RenderCallback {}
class Pre_Rectangle2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_Rectangle2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_Rectangle2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_Rectangle2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Coordinate extends Pre_Node {
    Pre_Node metadata;
    float[] point;
    static Pre_CoordinateRenderCallback Pre_renderCallbackCoordinate;
    static void setPre_CoordinateRenderCallback(Pre_CoordinateRenderCallback node) {
        Pre_renderCallbackCoordinate = node;
    }

    static Pre_CoordinateTreeRenderCallback Pre_treeRenderCallbackCoordinate;
    static void setPre_CoordinateTreeRenderCallback(Pre_CoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinate = node;
    }

    static Pre_CoordinateDoWithDataCallback Pre_doWithDataCallbackCoordinate;
    static void setPre_CoordinateDoWithDataCallback(Pre_CoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinate = node;
    }

    static Pre_CoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinate;
    static void setPre_CoordinateTreeDoWithDataCallback(Pre_CoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinate = node;

    }

    static Pre_CoordinateEventsProcessedCallback Pre_eventsProcessedCallbackCoordinate;
    static void setPre_CoordinateEventsProcessedCallback(Pre_CoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinate = node;
    }

    Pre_Coordinate() {
    }
    void treeRender() {
        if (Pre_Coordinate.Pre_treeRenderCallbackCoordinate != null) {
            Pre_Coordinate.Pre_treeRenderCallbackCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Coordinate.Pre_renderCallbackCoordinate != null)
            Pre_Coordinate.Pre_renderCallbackCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_Coordinate.Pre_treeDoWithDataCallbackCoordinate != null) {
            Pre_Coordinate.Pre_treeDoWithDataCallbackCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Coordinate.Pre_doWithDataCallbackCoordinate != null)
            Pre_Coordinate.Pre_doWithDataCallbackCoordinate.doWithData(this);
    }
}

class Pre_CoordinateRenderCallback extends RenderCallback {}
class Pre_CoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsSwungSurface extends Pre_Node {
    Pre_Node metadata;
    Pre_Node profileCurve;
    Pre_Node trajectoryCurve;
    boolean ccw;
    boolean solid;
    static Pre_NurbsSwungSurfaceRenderCallback Pre_renderCallbackNurbsSwungSurface;
    static void setPre_NurbsSwungSurfaceRenderCallback(Pre_NurbsSwungSurfaceRenderCallback node) {
        Pre_renderCallbackNurbsSwungSurface = node;
    }

    static Pre_NurbsSwungSurfaceTreeRenderCallback Pre_treeRenderCallbackNurbsSwungSurface;
    static void setPre_NurbsSwungSurfaceTreeRenderCallback(Pre_NurbsSwungSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsSwungSurface = node;
    }

    static Pre_NurbsSwungSurfaceDoWithDataCallback Pre_doWithDataCallbackNurbsSwungSurface;
    static void setPre_NurbsSwungSurfaceDoWithDataCallback(Pre_NurbsSwungSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsSwungSurface = node;
    }

    static Pre_NurbsSwungSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsSwungSurface;
    static void setPre_NurbsSwungSurfaceTreeDoWithDataCallback(Pre_NurbsSwungSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsSwungSurface = node;

    }

    static Pre_NurbsSwungSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackNurbsSwungSurface;
    static void setPre_NurbsSwungSurfaceEventsProcessedCallback(Pre_NurbsSwungSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsSwungSurface = node;
    }

    Pre_NurbsSwungSurface() {
    }
    void treeRender() {
        if (Pre_NurbsSwungSurface.Pre_treeRenderCallbackNurbsSwungSurface != null) {
            Pre_NurbsSwungSurface.Pre_treeRenderCallbackNurbsSwungSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (profileCurve != null)
            profileCurve.treeRender();
        if (trajectoryCurve != null)
            trajectoryCurve.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsSwungSurface.Pre_renderCallbackNurbsSwungSurface != null)
            Pre_NurbsSwungSurface.Pre_renderCallbackNurbsSwungSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsSwungSurface.Pre_treeDoWithDataCallbackNurbsSwungSurface != null) {
            Pre_NurbsSwungSurface.Pre_treeDoWithDataCallbackNurbsSwungSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (profileCurve != null)
            profileCurve.treeDoWithData();
        if (trajectoryCurve != null)
            trajectoryCurve.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsSwungSurface.Pre_doWithDataCallbackNurbsSwungSurface != null)
            Pre_NurbsSwungSurface.Pre_doWithDataCallbackNurbsSwungSurface.doWithData(this);
    }
}

class Pre_NurbsSwungSurfaceRenderCallback extends RenderCallback {}
class Pre_NurbsSwungSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsSwungSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsSwungSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsSwungSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsTextureSurface extends Pre_Node {
    Pre_Node metadata;
    int uDimension;
    int vDimension;
    float[] uKnot;
    float[] vKnot;
    int uOrder;
    int vOrder;
    float[] controlPoint;
    float[] weight;
    int uTessellation;
    int vTessellation;
    static Pre_NurbsTextureSurfaceRenderCallback Pre_renderCallbackNurbsTextureSurface;
    static void setPre_NurbsTextureSurfaceRenderCallback(Pre_NurbsTextureSurfaceRenderCallback node) {
        Pre_renderCallbackNurbsTextureSurface = node;
    }

    static Pre_NurbsTextureSurfaceTreeRenderCallback Pre_treeRenderCallbackNurbsTextureSurface;
    static void setPre_NurbsTextureSurfaceTreeRenderCallback(Pre_NurbsTextureSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsTextureSurface = node;
    }

    static Pre_NurbsTextureSurfaceDoWithDataCallback Pre_doWithDataCallbackNurbsTextureSurface;
    static void setPre_NurbsTextureSurfaceDoWithDataCallback(Pre_NurbsTextureSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsTextureSurface = node;
    }

    static Pre_NurbsTextureSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsTextureSurface;
    static void setPre_NurbsTextureSurfaceTreeDoWithDataCallback(Pre_NurbsTextureSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsTextureSurface = node;

    }

    static Pre_NurbsTextureSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackNurbsTextureSurface;
    static void setPre_NurbsTextureSurfaceEventsProcessedCallback(Pre_NurbsTextureSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsTextureSurface = node;
    }

    Pre_NurbsTextureSurface() {
    }
    void treeRender() {
        if (Pre_NurbsTextureSurface.Pre_treeRenderCallbackNurbsTextureSurface != null) {
            Pre_NurbsTextureSurface.Pre_treeRenderCallbackNurbsTextureSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsTextureSurface.Pre_renderCallbackNurbsTextureSurface != null)
            Pre_NurbsTextureSurface.Pre_renderCallbackNurbsTextureSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsTextureSurface.Pre_treeDoWithDataCallbackNurbsTextureSurface != null) {
            Pre_NurbsTextureSurface.Pre_treeDoWithDataCallbackNurbsTextureSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsTextureSurface.Pre_doWithDataCallbackNurbsTextureSurface != null)
            Pre_NurbsTextureSurface.Pre_doWithDataCallbackNurbsTextureSurface.doWithData(this);
    }
}

class Pre_NurbsTextureSurfaceRenderCallback extends RenderCallback {}
class Pre_NurbsTextureSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsTextureSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsTextureSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsTextureSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_EspduTransform extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    Pre_Node [] children;
    int articulationParameterCount;
    int[] articulationParameterDesignatorArray;
    int[] articulationParameterChangeIndicatorArray;
    int[] articulationParameterIdPartAttachedToArray;
    int[] articulationParameterTypeArray;
    float[] articulationParameterArray;
    int collisionType;
    int deadReckoning;
    float[] detonationLocation;
    float[] detonationRelativeLocation;
    int detonationResult;
    int entityCategory;
    int entityCountry;
    int entityDomain;
    int entityExtra;
    int entityKind;
    int entitySpecific;
    int eventApplicationID;
    int eventEntityID;
    int eventNumber;
    int eventSiteID;
    boolean fired1;
    boolean fired2;
    int fireMissionIndex;
    float firingRange;
    int firingRate;
    int forceID;
    int fuse;
    float[] linearVelocity;
    float[] linearAcceleration;
    String marking;
    int munitionApplicationID;
    float[] munitionEndPoint;
    int munitionEntityID;
    int munitionQuantity;
    int munitionSiteID;
    float[] munitionStartPoint;
    int warhead;
    boolean enabled;
    String networkMode;
    double readInterval;
    double writeInterval;
    int siteID;
    int applicationID;
    int entityID;
    String address;
    int port;
    String multicastRelayHost;
    int multicastRelayPort;
    boolean rtpHeaderExpected;
    static Pre_EspduTransformRenderCallback Pre_renderCallbackEspduTransform;
    static void setPre_EspduTransformRenderCallback(Pre_EspduTransformRenderCallback node) {
        Pre_renderCallbackEspduTransform = node;
    }

    static Pre_EspduTransformTreeRenderCallback Pre_treeRenderCallbackEspduTransform;
    static void setPre_EspduTransformTreeRenderCallback(Pre_EspduTransformTreeRenderCallback node) {
        Pre_treeRenderCallbackEspduTransform = node;
    }

    static Pre_EspduTransformDoWithDataCallback Pre_doWithDataCallbackEspduTransform;
    static void setPre_EspduTransformDoWithDataCallback(Pre_EspduTransformDoWithDataCallback node) {
        Pre_doWithDataCallbackEspduTransform = node;
    }

    static Pre_EspduTransformTreeDoWithDataCallback Pre_treeDoWithDataCallbackEspduTransform;
    static void setPre_EspduTransformTreeDoWithDataCallback(Pre_EspduTransformTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackEspduTransform = node;

    }

    static Pre_EspduTransformEventsProcessedCallback Pre_eventsProcessedCallbackEspduTransform;
    static void setPre_EspduTransformEventsProcessedCallback(Pre_EspduTransformEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackEspduTransform = node;
    }

    Pre_EspduTransform() {
    }
    void treeRender() {
        if (Pre_EspduTransform.Pre_treeRenderCallbackEspduTransform != null) {
            Pre_EspduTransform.Pre_treeRenderCallbackEspduTransform.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_EspduTransform.Pre_renderCallbackEspduTransform != null)
            Pre_EspduTransform.Pre_renderCallbackEspduTransform.render(this);
    }
    void treeDoWithData() {
        if (Pre_EspduTransform.Pre_treeDoWithDataCallbackEspduTransform != null) {
            Pre_EspduTransform.Pre_treeDoWithDataCallbackEspduTransform.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_EspduTransform.Pre_doWithDataCallbackEspduTransform != null)
            Pre_EspduTransform.Pre_doWithDataCallbackEspduTransform.doWithData(this);
    }
}

class Pre_EspduTransformRenderCallback extends RenderCallback {}
class Pre_EspduTransformTreeRenderCallback extends TreeRenderCallback {}
class Pre_EspduTransformDoWithDataCallback extends DoWithDataCallback {};
class Pre_EspduTransformTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_EspduTransformEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ComposedShader extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] parts;
    String language;
    static Pre_ComposedShaderRenderCallback Pre_renderCallbackComposedShader;
    static void setPre_ComposedShaderRenderCallback(Pre_ComposedShaderRenderCallback node) {
        Pre_renderCallbackComposedShader = node;
    }

    static Pre_ComposedShaderTreeRenderCallback Pre_treeRenderCallbackComposedShader;
    static void setPre_ComposedShaderTreeRenderCallback(Pre_ComposedShaderTreeRenderCallback node) {
        Pre_treeRenderCallbackComposedShader = node;
    }

    static Pre_ComposedShaderDoWithDataCallback Pre_doWithDataCallbackComposedShader;
    static void setPre_ComposedShaderDoWithDataCallback(Pre_ComposedShaderDoWithDataCallback node) {
        Pre_doWithDataCallbackComposedShader = node;
    }

    static Pre_ComposedShaderTreeDoWithDataCallback Pre_treeDoWithDataCallbackComposedShader;
    static void setPre_ComposedShaderTreeDoWithDataCallback(Pre_ComposedShaderTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackComposedShader = node;

    }

    static Pre_ComposedShaderEventsProcessedCallback Pre_eventsProcessedCallbackComposedShader;
    static void setPre_ComposedShaderEventsProcessedCallback(Pre_ComposedShaderEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackComposedShader = node;
    }

    Pre_ComposedShader() {
    }
    void treeRender() {
        if (Pre_ComposedShader.Pre_treeRenderCallbackComposedShader != null) {
            Pre_ComposedShader.Pre_treeRenderCallbackComposedShader.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (parts != null)
            for (int i = 0; i < parts.length; i++)
                if (parts[i] != null)
                    parts[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ComposedShader.Pre_renderCallbackComposedShader != null)
            Pre_ComposedShader.Pre_renderCallbackComposedShader.render(this);
    }
    void treeDoWithData() {
        if (Pre_ComposedShader.Pre_treeDoWithDataCallbackComposedShader != null) {
            Pre_ComposedShader.Pre_treeDoWithDataCallbackComposedShader.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (parts != null)
            for (int i = 0; i < parts.length; i++)
                if (parts[i] != null)
                    parts[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ComposedShader.Pre_doWithDataCallbackComposedShader != null)
            Pre_ComposedShader.Pre_doWithDataCallbackComposedShader.doWithData(this);
    }
}

class Pre_ComposedShaderRenderCallback extends RenderCallback {}
class Pre_ComposedShaderTreeRenderCallback extends TreeRenderCallback {}
class Pre_ComposedShaderDoWithDataCallback extends DoWithDataCallback {};
class Pre_ComposedShaderTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ComposedShaderEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LinePickSensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    String[] objectType;
    Pre_Node pickingGeometry;
    Pre_Node [] pickTarget;
    String intersectionType;
    String sortOrder;
    static Pre_LinePickSensorRenderCallback Pre_renderCallbackLinePickSensor;
    static void setPre_LinePickSensorRenderCallback(Pre_LinePickSensorRenderCallback node) {
        Pre_renderCallbackLinePickSensor = node;
    }

    static Pre_LinePickSensorTreeRenderCallback Pre_treeRenderCallbackLinePickSensor;
    static void setPre_LinePickSensorTreeRenderCallback(Pre_LinePickSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackLinePickSensor = node;
    }

    static Pre_LinePickSensorDoWithDataCallback Pre_doWithDataCallbackLinePickSensor;
    static void setPre_LinePickSensorDoWithDataCallback(Pre_LinePickSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackLinePickSensor = node;
    }

    static Pre_LinePickSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackLinePickSensor;
    static void setPre_LinePickSensorTreeDoWithDataCallback(Pre_LinePickSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLinePickSensor = node;

    }

    static Pre_LinePickSensorEventsProcessedCallback Pre_eventsProcessedCallbackLinePickSensor;
    static void setPre_LinePickSensorEventsProcessedCallback(Pre_LinePickSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLinePickSensor = node;
    }

    Pre_LinePickSensor() {
    }
    void treeRender() {
        if (Pre_LinePickSensor.Pre_treeRenderCallbackLinePickSensor != null) {
            Pre_LinePickSensor.Pre_treeRenderCallbackLinePickSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (pickingGeometry != null)
            pickingGeometry.treeRender();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LinePickSensor.Pre_renderCallbackLinePickSensor != null)
            Pre_LinePickSensor.Pre_renderCallbackLinePickSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_LinePickSensor.Pre_treeDoWithDataCallbackLinePickSensor != null) {
            Pre_LinePickSensor.Pre_treeDoWithDataCallbackLinePickSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (pickingGeometry != null)
            pickingGeometry.treeDoWithData();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LinePickSensor.Pre_doWithDataCallbackLinePickSensor != null)
            Pre_LinePickSensor.Pre_doWithDataCallbackLinePickSensor.doWithData(this);
    }
}

class Pre_LinePickSensorRenderCallback extends RenderCallback {}
class Pre_LinePickSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_LinePickSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_LinePickSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LinePickSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsOrientationInterpolator extends Pre_Node {
    Pre_Node metadata;
    Pre_Node controlPoints;
    double[] knot;
    int order;
    double[] weight;
    static Pre_NurbsOrientationInterpolatorRenderCallback Pre_renderCallbackNurbsOrientationInterpolator;
    static void setPre_NurbsOrientationInterpolatorRenderCallback(Pre_NurbsOrientationInterpolatorRenderCallback node) {
        Pre_renderCallbackNurbsOrientationInterpolator = node;
    }

    static Pre_NurbsOrientationInterpolatorTreeRenderCallback Pre_treeRenderCallbackNurbsOrientationInterpolator;
    static void setPre_NurbsOrientationInterpolatorTreeRenderCallback(Pre_NurbsOrientationInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsOrientationInterpolator = node;
    }

    static Pre_NurbsOrientationInterpolatorDoWithDataCallback Pre_doWithDataCallbackNurbsOrientationInterpolator;
    static void setPre_NurbsOrientationInterpolatorDoWithDataCallback(Pre_NurbsOrientationInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsOrientationInterpolator = node;
    }

    static Pre_NurbsOrientationInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsOrientationInterpolator;
    static void setPre_NurbsOrientationInterpolatorTreeDoWithDataCallback(Pre_NurbsOrientationInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsOrientationInterpolator = node;

    }

    static Pre_NurbsOrientationInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackNurbsOrientationInterpolator;
    static void setPre_NurbsOrientationInterpolatorEventsProcessedCallback(Pre_NurbsOrientationInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsOrientationInterpolator = node;
    }

    Pre_NurbsOrientationInterpolator() {
    }
    void treeRender() {
        if (Pre_NurbsOrientationInterpolator.Pre_treeRenderCallbackNurbsOrientationInterpolator != null) {
            Pre_NurbsOrientationInterpolator.Pre_treeRenderCallbackNurbsOrientationInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoints != null)
            controlPoints.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsOrientationInterpolator.Pre_renderCallbackNurbsOrientationInterpolator != null)
            Pre_NurbsOrientationInterpolator.Pre_renderCallbackNurbsOrientationInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsOrientationInterpolator.Pre_treeDoWithDataCallbackNurbsOrientationInterpolator != null) {
            Pre_NurbsOrientationInterpolator.Pre_treeDoWithDataCallbackNurbsOrientationInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoints != null)
            controlPoints.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsOrientationInterpolator.Pre_doWithDataCallbackNurbsOrientationInterpolator != null)
            Pre_NurbsOrientationInterpolator.Pre_doWithDataCallbackNurbsOrientationInterpolator.doWithData(this);
    }
}

class Pre_NurbsOrientationInterpolatorRenderCallback extends RenderCallback {}
class Pre_NurbsOrientationInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsOrientationInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsOrientationInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsOrientationInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsPositionInterpolator extends Pre_Node {
    Pre_Node metadata;
    Pre_Node controlPoint;
    float[] weight;
    float[] knot;
    int order;
    static Pre_NurbsPositionInterpolatorRenderCallback Pre_renderCallbackNurbsPositionInterpolator;
    static void setPre_NurbsPositionInterpolatorRenderCallback(Pre_NurbsPositionInterpolatorRenderCallback node) {
        Pre_renderCallbackNurbsPositionInterpolator = node;
    }

    static Pre_NurbsPositionInterpolatorTreeRenderCallback Pre_treeRenderCallbackNurbsPositionInterpolator;
    static void setPre_NurbsPositionInterpolatorTreeRenderCallback(Pre_NurbsPositionInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsPositionInterpolator = node;
    }

    static Pre_NurbsPositionInterpolatorDoWithDataCallback Pre_doWithDataCallbackNurbsPositionInterpolator;
    static void setPre_NurbsPositionInterpolatorDoWithDataCallback(Pre_NurbsPositionInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsPositionInterpolator = node;
    }

    static Pre_NurbsPositionInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsPositionInterpolator;
    static void setPre_NurbsPositionInterpolatorTreeDoWithDataCallback(Pre_NurbsPositionInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsPositionInterpolator = node;

    }

    static Pre_NurbsPositionInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackNurbsPositionInterpolator;
    static void setPre_NurbsPositionInterpolatorEventsProcessedCallback(Pre_NurbsPositionInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsPositionInterpolator = node;
    }

    Pre_NurbsPositionInterpolator() {
    }
    void treeRender() {
        if (Pre_NurbsPositionInterpolator.Pre_treeRenderCallbackNurbsPositionInterpolator != null) {
            Pre_NurbsPositionInterpolator.Pre_treeRenderCallbackNurbsPositionInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoint != null)
            controlPoint.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsPositionInterpolator.Pre_renderCallbackNurbsPositionInterpolator != null)
            Pre_NurbsPositionInterpolator.Pre_renderCallbackNurbsPositionInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsPositionInterpolator.Pre_treeDoWithDataCallbackNurbsPositionInterpolator != null) {
            Pre_NurbsPositionInterpolator.Pre_treeDoWithDataCallbackNurbsPositionInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoint != null)
            controlPoint.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsPositionInterpolator.Pre_doWithDataCallbackNurbsPositionInterpolator != null)
            Pre_NurbsPositionInterpolator.Pre_doWithDataCallbackNurbsPositionInterpolator.doWithData(this);
    }
}

class Pre_NurbsPositionInterpolatorRenderCallback extends RenderCallback {}
class Pre_NurbsPositionInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsPositionInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsPositionInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsPositionInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_VolumePickSensor extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    String[] objectType;
    Pre_Node pickingGeometry;
    Pre_Node [] pickTarget;
    String intersectionType;
    String sortOrder;
    static Pre_VolumePickSensorRenderCallback Pre_renderCallbackVolumePickSensor;
    static void setPre_VolumePickSensorRenderCallback(Pre_VolumePickSensorRenderCallback node) {
        Pre_renderCallbackVolumePickSensor = node;
    }

    static Pre_VolumePickSensorTreeRenderCallback Pre_treeRenderCallbackVolumePickSensor;
    static void setPre_VolumePickSensorTreeRenderCallback(Pre_VolumePickSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackVolumePickSensor = node;
    }

    static Pre_VolumePickSensorDoWithDataCallback Pre_doWithDataCallbackVolumePickSensor;
    static void setPre_VolumePickSensorDoWithDataCallback(Pre_VolumePickSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackVolumePickSensor = node;
    }

    static Pre_VolumePickSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackVolumePickSensor;
    static void setPre_VolumePickSensorTreeDoWithDataCallback(Pre_VolumePickSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackVolumePickSensor = node;

    }

    static Pre_VolumePickSensorEventsProcessedCallback Pre_eventsProcessedCallbackVolumePickSensor;
    static void setPre_VolumePickSensorEventsProcessedCallback(Pre_VolumePickSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackVolumePickSensor = node;
    }

    Pre_VolumePickSensor() {
    }
    void treeRender() {
        if (Pre_VolumePickSensor.Pre_treeRenderCallbackVolumePickSensor != null) {
            Pre_VolumePickSensor.Pre_treeRenderCallbackVolumePickSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (pickingGeometry != null)
            pickingGeometry.treeRender();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_VolumePickSensor.Pre_renderCallbackVolumePickSensor != null)
            Pre_VolumePickSensor.Pre_renderCallbackVolumePickSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_VolumePickSensor.Pre_treeDoWithDataCallbackVolumePickSensor != null) {
            Pre_VolumePickSensor.Pre_treeDoWithDataCallbackVolumePickSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (pickingGeometry != null)
            pickingGeometry.treeDoWithData();
        if (pickTarget != null)
            for (int i = 0; i < pickTarget.length; i++)
                if (pickTarget[i] != null)
                    pickTarget[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_VolumePickSensor.Pre_doWithDataCallbackVolumePickSensor != null)
            Pre_VolumePickSensor.Pre_doWithDataCallbackVolumePickSensor.doWithData(this);
    }
}

class Pre_VolumePickSensorRenderCallback extends RenderCallback {}
class Pre_VolumePickSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_VolumePickSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_VolumePickSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_VolumePickSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_BallJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] anchorPoint;
    static Pre_BallJointRenderCallback Pre_renderCallbackBallJoint;
    static void setPre_BallJointRenderCallback(Pre_BallJointRenderCallback node) {
        Pre_renderCallbackBallJoint = node;
    }

    static Pre_BallJointTreeRenderCallback Pre_treeRenderCallbackBallJoint;
    static void setPre_BallJointTreeRenderCallback(Pre_BallJointTreeRenderCallback node) {
        Pre_treeRenderCallbackBallJoint = node;
    }

    static Pre_BallJointDoWithDataCallback Pre_doWithDataCallbackBallJoint;
    static void setPre_BallJointDoWithDataCallback(Pre_BallJointDoWithDataCallback node) {
        Pre_doWithDataCallbackBallJoint = node;
    }

    static Pre_BallJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackBallJoint;
    static void setPre_BallJointTreeDoWithDataCallback(Pre_BallJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBallJoint = node;

    }

    static Pre_BallJointEventsProcessedCallback Pre_eventsProcessedCallbackBallJoint;
    static void setPre_BallJointEventsProcessedCallback(Pre_BallJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBallJoint = node;
    }

    Pre_BallJoint() {
    }
    void treeRender() {
        if (Pre_BallJoint.Pre_treeRenderCallbackBallJoint != null) {
            Pre_BallJoint.Pre_treeRenderCallbackBallJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_BallJoint.Pre_renderCallbackBallJoint != null)
            Pre_BallJoint.Pre_renderCallbackBallJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_BallJoint.Pre_treeDoWithDataCallbackBallJoint != null) {
            Pre_BallJoint.Pre_treeDoWithDataCallbackBallJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_BallJoint.Pre_doWithDataCallbackBallJoint != null)
            Pre_BallJoint.Pre_doWithDataCallbackBallJoint.doWithData(this);
    }
}

class Pre_BallJointRenderCallback extends RenderCallback {}
class Pre_BallJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_BallJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_BallJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BallJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CADPart extends Pre_Node {
    Pre_Node metadata;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    float[] center;
    float[] scale;
    float[] scaleOrientation;
    Pre_Node [] children;
    String name;
    static Pre_CADPartRenderCallback Pre_renderCallbackCADPart;
    static void setPre_CADPartRenderCallback(Pre_CADPartRenderCallback node) {
        Pre_renderCallbackCADPart = node;
    }

    static Pre_CADPartTreeRenderCallback Pre_treeRenderCallbackCADPart;
    static void setPre_CADPartTreeRenderCallback(Pre_CADPartTreeRenderCallback node) {
        Pre_treeRenderCallbackCADPart = node;
    }

    static Pre_CADPartDoWithDataCallback Pre_doWithDataCallbackCADPart;
    static void setPre_CADPartDoWithDataCallback(Pre_CADPartDoWithDataCallback node) {
        Pre_doWithDataCallbackCADPart = node;
    }

    static Pre_CADPartTreeDoWithDataCallback Pre_treeDoWithDataCallbackCADPart;
    static void setPre_CADPartTreeDoWithDataCallback(Pre_CADPartTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCADPart = node;

    }

    static Pre_CADPartEventsProcessedCallback Pre_eventsProcessedCallbackCADPart;
    static void setPre_CADPartEventsProcessedCallback(Pre_CADPartEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCADPart = node;
    }

    Pre_CADPart() {
    }
    void treeRender() {
        if (Pre_CADPart.Pre_treeRenderCallbackCADPart != null) {
            Pre_CADPart.Pre_treeRenderCallbackCADPart.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CADPart.Pre_renderCallbackCADPart != null)
            Pre_CADPart.Pre_renderCallbackCADPart.render(this);
    }
    void treeDoWithData() {
        if (Pre_CADPart.Pre_treeDoWithDataCallbackCADPart != null) {
            Pre_CADPart.Pre_treeDoWithDataCallbackCADPart.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CADPart.Pre_doWithDataCallbackCADPart != null)
            Pre_CADPart.Pre_doWithDataCallbackCADPart.doWithData(this);
    }
}

class Pre_CADPartRenderCallback extends RenderCallback {}
class Pre_CADPartTreeRenderCallback extends TreeRenderCallback {}
class Pre_CADPartDoWithDataCallback extends DoWithDataCallback {};
class Pre_CADPartTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CADPartEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CollidableOffset extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] rotation;
    float[] translation;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node collidable;
    static Pre_CollidableOffsetRenderCallback Pre_renderCallbackCollidableOffset;
    static void setPre_CollidableOffsetRenderCallback(Pre_CollidableOffsetRenderCallback node) {
        Pre_renderCallbackCollidableOffset = node;
    }

    static Pre_CollidableOffsetTreeRenderCallback Pre_treeRenderCallbackCollidableOffset;
    static void setPre_CollidableOffsetTreeRenderCallback(Pre_CollidableOffsetTreeRenderCallback node) {
        Pre_treeRenderCallbackCollidableOffset = node;
    }

    static Pre_CollidableOffsetDoWithDataCallback Pre_doWithDataCallbackCollidableOffset;
    static void setPre_CollidableOffsetDoWithDataCallback(Pre_CollidableOffsetDoWithDataCallback node) {
        Pre_doWithDataCallbackCollidableOffset = node;
    }

    static Pre_CollidableOffsetTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollidableOffset;
    static void setPre_CollidableOffsetTreeDoWithDataCallback(Pre_CollidableOffsetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollidableOffset = node;

    }

    static Pre_CollidableOffsetEventsProcessedCallback Pre_eventsProcessedCallbackCollidableOffset;
    static void setPre_CollidableOffsetEventsProcessedCallback(Pre_CollidableOffsetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollidableOffset = node;
    }

    Pre_CollidableOffset() {
    }
    void treeRender() {
        if (Pre_CollidableOffset.Pre_treeRenderCallbackCollidableOffset != null) {
            Pre_CollidableOffset.Pre_treeRenderCallbackCollidableOffset.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (collidable != null)
            collidable.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CollidableOffset.Pre_renderCallbackCollidableOffset != null)
            Pre_CollidableOffset.Pre_renderCallbackCollidableOffset.render(this);
    }
    void treeDoWithData() {
        if (Pre_CollidableOffset.Pre_treeDoWithDataCallbackCollidableOffset != null) {
            Pre_CollidableOffset.Pre_treeDoWithDataCallbackCollidableOffset.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (collidable != null)
            collidable.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CollidableOffset.Pre_doWithDataCallbackCollidableOffset != null)
            Pre_CollidableOffset.Pre_doWithDataCallbackCollidableOffset.doWithData(this);
    }
}

class Pre_CollidableOffsetRenderCallback extends RenderCallback {}
class Pre_CollidableOffsetTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollidableOffsetDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollidableOffsetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollidableOffsetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureTransformMatrix3D extends Pre_Node {
    Pre_Node metadata;
    float[] matrix;
    static Pre_TextureTransformMatrix3DRenderCallback Pre_renderCallbackTextureTransformMatrix3D;
    static void setPre_TextureTransformMatrix3DRenderCallback(Pre_TextureTransformMatrix3DRenderCallback node) {
        Pre_renderCallbackTextureTransformMatrix3D = node;
    }

    static Pre_TextureTransformMatrix3DTreeRenderCallback Pre_treeRenderCallbackTextureTransformMatrix3D;
    static void setPre_TextureTransformMatrix3DTreeRenderCallback(Pre_TextureTransformMatrix3DTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureTransformMatrix3D = node;
    }

    static Pre_TextureTransformMatrix3DDoWithDataCallback Pre_doWithDataCallbackTextureTransformMatrix3D;
    static void setPre_TextureTransformMatrix3DDoWithDataCallback(Pre_TextureTransformMatrix3DDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureTransformMatrix3D = node;
    }

    static Pre_TextureTransformMatrix3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureTransformMatrix3D;
    static void setPre_TextureTransformMatrix3DTreeDoWithDataCallback(Pre_TextureTransformMatrix3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureTransformMatrix3D = node;

    }

    static Pre_TextureTransformMatrix3DEventsProcessedCallback Pre_eventsProcessedCallbackTextureTransformMatrix3D;
    static void setPre_TextureTransformMatrix3DEventsProcessedCallback(Pre_TextureTransformMatrix3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureTransformMatrix3D = node;
    }

    Pre_TextureTransformMatrix3D() {
    }
    void treeRender() {
        if (Pre_TextureTransformMatrix3D.Pre_treeRenderCallbackTextureTransformMatrix3D != null) {
            Pre_TextureTransformMatrix3D.Pre_treeRenderCallbackTextureTransformMatrix3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureTransformMatrix3D.Pre_renderCallbackTextureTransformMatrix3D != null)
            Pre_TextureTransformMatrix3D.Pre_renderCallbackTextureTransformMatrix3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureTransformMatrix3D.Pre_treeDoWithDataCallbackTextureTransformMatrix3D != null) {
            Pre_TextureTransformMatrix3D.Pre_treeDoWithDataCallbackTextureTransformMatrix3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureTransformMatrix3D.Pre_doWithDataCallbackTextureTransformMatrix3D != null)
            Pre_TextureTransformMatrix3D.Pre_doWithDataCallbackTextureTransformMatrix3D.doWithData(this);
    }
}

class Pre_TextureTransformMatrix3DRenderCallback extends RenderCallback {}
class Pre_TextureTransformMatrix3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureTransformMatrix3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureTransformMatrix3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureTransformMatrix3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CADFace extends Pre_Node {
    Pre_Node metadata;
    String name;
    Pre_Node shape;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_CADFaceRenderCallback Pre_renderCallbackCADFace;
    static void setPre_CADFaceRenderCallback(Pre_CADFaceRenderCallback node) {
        Pre_renderCallbackCADFace = node;
    }

    static Pre_CADFaceTreeRenderCallback Pre_treeRenderCallbackCADFace;
    static void setPre_CADFaceTreeRenderCallback(Pre_CADFaceTreeRenderCallback node) {
        Pre_treeRenderCallbackCADFace = node;
    }

    static Pre_CADFaceDoWithDataCallback Pre_doWithDataCallbackCADFace;
    static void setPre_CADFaceDoWithDataCallback(Pre_CADFaceDoWithDataCallback node) {
        Pre_doWithDataCallbackCADFace = node;
    }

    static Pre_CADFaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackCADFace;
    static void setPre_CADFaceTreeDoWithDataCallback(Pre_CADFaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCADFace = node;

    }

    static Pre_CADFaceEventsProcessedCallback Pre_eventsProcessedCallbackCADFace;
    static void setPre_CADFaceEventsProcessedCallback(Pre_CADFaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCADFace = node;
    }

    Pre_CADFace() {
    }
    void treeRender() {
        if (Pre_CADFace.Pre_treeRenderCallbackCADFace != null) {
            Pre_CADFace.Pre_treeRenderCallbackCADFace.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (shape != null)
            shape.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CADFace.Pre_renderCallbackCADFace != null)
            Pre_CADFace.Pre_renderCallbackCADFace.render(this);
    }
    void treeDoWithData() {
        if (Pre_CADFace.Pre_treeDoWithDataCallbackCADFace != null) {
            Pre_CADFace.Pre_treeDoWithDataCallbackCADFace.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (shape != null)
            shape.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CADFace.Pre_doWithDataCallbackCADFace != null)
            Pre_CADFace.Pre_doWithDataCallbackCADFace.doWithData(this);
    }
}

class Pre_CADFaceRenderCallback extends RenderCallback {}
class Pre_CADFaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_CADFaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_CADFaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CADFaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_VrmlScene extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] children;
    static Pre_VrmlSceneRenderCallback Pre_renderCallbackVrmlScene;
    static void setPre_VrmlSceneRenderCallback(Pre_VrmlSceneRenderCallback node) {
        Pre_renderCallbackVrmlScene = node;
    }

    static Pre_VrmlSceneTreeRenderCallback Pre_treeRenderCallbackVrmlScene;
    static void setPre_VrmlSceneTreeRenderCallback(Pre_VrmlSceneTreeRenderCallback node) {
        Pre_treeRenderCallbackVrmlScene = node;
    }

    static Pre_VrmlSceneDoWithDataCallback Pre_doWithDataCallbackVrmlScene;
    static void setPre_VrmlSceneDoWithDataCallback(Pre_VrmlSceneDoWithDataCallback node) {
        Pre_doWithDataCallbackVrmlScene = node;
    }

    static Pre_VrmlSceneTreeDoWithDataCallback Pre_treeDoWithDataCallbackVrmlScene;
    static void setPre_VrmlSceneTreeDoWithDataCallback(Pre_VrmlSceneTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackVrmlScene = node;

    }

    static Pre_VrmlSceneEventsProcessedCallback Pre_eventsProcessedCallbackVrmlScene;
    static void setPre_VrmlSceneEventsProcessedCallback(Pre_VrmlSceneEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackVrmlScene = node;
    }

    Pre_VrmlScene() {
    }
    void treeRender() {
        if (Pre_VrmlScene.Pre_treeRenderCallbackVrmlScene != null) {
            Pre_VrmlScene.Pre_treeRenderCallbackVrmlScene.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_VrmlScene.Pre_renderCallbackVrmlScene != null)
            Pre_VrmlScene.Pre_renderCallbackVrmlScene.render(this);
    }
    void treeDoWithData() {
        if (Pre_VrmlScene.Pre_treeDoWithDataCallbackVrmlScene != null) {
            Pre_VrmlScene.Pre_treeDoWithDataCallbackVrmlScene.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (children != null)
            for (int i = 0; i < children.length; i++)
                if (children[i] != null)
                    children[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_VrmlScene.Pre_doWithDataCallbackVrmlScene != null)
            Pre_VrmlScene.Pre_doWithDataCallbackVrmlScene.doWithData(this);
    }
}

class Pre_VrmlSceneRenderCallback extends RenderCallback {}
class Pre_VrmlSceneTreeRenderCallback extends TreeRenderCallback {}
class Pre_VrmlSceneDoWithDataCallback extends DoWithDataCallback {};
class Pre_VrmlSceneTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_VrmlSceneEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MetadataString extends Pre_Node {
    Pre_Node metadata;
    String name;
    String reference;
    String[] value;
    static Pre_MetadataStringRenderCallback Pre_renderCallbackMetadataString;
    static void setPre_MetadataStringRenderCallback(Pre_MetadataStringRenderCallback node) {
        Pre_renderCallbackMetadataString = node;
    }

    static Pre_MetadataStringTreeRenderCallback Pre_treeRenderCallbackMetadataString;
    static void setPre_MetadataStringTreeRenderCallback(Pre_MetadataStringTreeRenderCallback node) {
        Pre_treeRenderCallbackMetadataString = node;
    }

    static Pre_MetadataStringDoWithDataCallback Pre_doWithDataCallbackMetadataString;
    static void setPre_MetadataStringDoWithDataCallback(Pre_MetadataStringDoWithDataCallback node) {
        Pre_doWithDataCallbackMetadataString = node;
    }

    static Pre_MetadataStringTreeDoWithDataCallback Pre_treeDoWithDataCallbackMetadataString;
    static void setPre_MetadataStringTreeDoWithDataCallback(Pre_MetadataStringTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMetadataString = node;

    }

    static Pre_MetadataStringEventsProcessedCallback Pre_eventsProcessedCallbackMetadataString;
    static void setPre_MetadataStringEventsProcessedCallback(Pre_MetadataStringEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMetadataString = node;
    }

    Pre_MetadataString() {
    }
    void treeRender() {
        if (Pre_MetadataString.Pre_treeRenderCallbackMetadataString != null) {
            Pre_MetadataString.Pre_treeRenderCallbackMetadataString.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MetadataString.Pre_renderCallbackMetadataString != null)
            Pre_MetadataString.Pre_renderCallbackMetadataString.render(this);
    }
    void treeDoWithData() {
        if (Pre_MetadataString.Pre_treeDoWithDataCallbackMetadataString != null) {
            Pre_MetadataString.Pre_treeDoWithDataCallbackMetadataString.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MetadataString.Pre_doWithDataCallbackMetadataString != null)
            Pre_MetadataString.Pre_doWithDataCallbackMetadataString.doWithData(this);
    }
}

class Pre_MetadataStringRenderCallback extends RenderCallback {}
class Pre_MetadataStringTreeRenderCallback extends TreeRenderCallback {}
class Pre_MetadataStringDoWithDataCallback extends DoWithDataCallback {};
class Pre_MetadataStringTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MetadataStringEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ForcePhysicsModel extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] force;
    static Pre_ForcePhysicsModelRenderCallback Pre_renderCallbackForcePhysicsModel;
    static void setPre_ForcePhysicsModelRenderCallback(Pre_ForcePhysicsModelRenderCallback node) {
        Pre_renderCallbackForcePhysicsModel = node;
    }

    static Pre_ForcePhysicsModelTreeRenderCallback Pre_treeRenderCallbackForcePhysicsModel;
    static void setPre_ForcePhysicsModelTreeRenderCallback(Pre_ForcePhysicsModelTreeRenderCallback node) {
        Pre_treeRenderCallbackForcePhysicsModel = node;
    }

    static Pre_ForcePhysicsModelDoWithDataCallback Pre_doWithDataCallbackForcePhysicsModel;
    static void setPre_ForcePhysicsModelDoWithDataCallback(Pre_ForcePhysicsModelDoWithDataCallback node) {
        Pre_doWithDataCallbackForcePhysicsModel = node;
    }

    static Pre_ForcePhysicsModelTreeDoWithDataCallback Pre_treeDoWithDataCallbackForcePhysicsModel;
    static void setPre_ForcePhysicsModelTreeDoWithDataCallback(Pre_ForcePhysicsModelTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackForcePhysicsModel = node;

    }

    static Pre_ForcePhysicsModelEventsProcessedCallback Pre_eventsProcessedCallbackForcePhysicsModel;
    static void setPre_ForcePhysicsModelEventsProcessedCallback(Pre_ForcePhysicsModelEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackForcePhysicsModel = node;
    }

    Pre_ForcePhysicsModel() {
    }
    void treeRender() {
        if (Pre_ForcePhysicsModel.Pre_treeRenderCallbackForcePhysicsModel != null) {
            Pre_ForcePhysicsModel.Pre_treeRenderCallbackForcePhysicsModel.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ForcePhysicsModel.Pre_renderCallbackForcePhysicsModel != null)
            Pre_ForcePhysicsModel.Pre_renderCallbackForcePhysicsModel.render(this);
    }
    void treeDoWithData() {
        if (Pre_ForcePhysicsModel.Pre_treeDoWithDataCallbackForcePhysicsModel != null) {
            Pre_ForcePhysicsModel.Pre_treeDoWithDataCallbackForcePhysicsModel.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ForcePhysicsModel.Pre_doWithDataCallbackForcePhysicsModel != null)
            Pre_ForcePhysicsModel.Pre_doWithDataCallbackForcePhysicsModel.doWithData(this);
    }
}

class Pre_ForcePhysicsModelRenderCallback extends RenderCallback {}
class Pre_ForcePhysicsModelTreeRenderCallback extends TreeRenderCallback {}
class Pre_ForcePhysicsModelDoWithDataCallback extends DoWithDataCallback {};
class Pre_ForcePhysicsModelTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ForcePhysicsModelEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_InlineLoadControl extends Pre_Node {
    Pre_Node metadata;
    boolean load;
    String[] url;
    float[] bboxCenter;
    float[] bboxSize;
    static Pre_InlineLoadControlRenderCallback Pre_renderCallbackInlineLoadControl;
    static void setPre_InlineLoadControlRenderCallback(Pre_InlineLoadControlRenderCallback node) {
        Pre_renderCallbackInlineLoadControl = node;
    }

    static Pre_InlineLoadControlTreeRenderCallback Pre_treeRenderCallbackInlineLoadControl;
    static void setPre_InlineLoadControlTreeRenderCallback(Pre_InlineLoadControlTreeRenderCallback node) {
        Pre_treeRenderCallbackInlineLoadControl = node;
    }

    static Pre_InlineLoadControlDoWithDataCallback Pre_doWithDataCallbackInlineLoadControl;
    static void setPre_InlineLoadControlDoWithDataCallback(Pre_InlineLoadControlDoWithDataCallback node) {
        Pre_doWithDataCallbackInlineLoadControl = node;
    }

    static Pre_InlineLoadControlTreeDoWithDataCallback Pre_treeDoWithDataCallbackInlineLoadControl;
    static void setPre_InlineLoadControlTreeDoWithDataCallback(Pre_InlineLoadControlTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackInlineLoadControl = node;

    }

    static Pre_InlineLoadControlEventsProcessedCallback Pre_eventsProcessedCallbackInlineLoadControl;
    static void setPre_InlineLoadControlEventsProcessedCallback(Pre_InlineLoadControlEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackInlineLoadControl = node;
    }

    Pre_InlineLoadControl() {
    }
    void treeRender() {
        if (Pre_InlineLoadControl.Pre_treeRenderCallbackInlineLoadControl != null) {
            Pre_InlineLoadControl.Pre_treeRenderCallbackInlineLoadControl.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_InlineLoadControl.Pre_renderCallbackInlineLoadControl != null)
            Pre_InlineLoadControl.Pre_renderCallbackInlineLoadControl.render(this);
    }
    void treeDoWithData() {
        if (Pre_InlineLoadControl.Pre_treeDoWithDataCallbackInlineLoadControl != null) {
            Pre_InlineLoadControl.Pre_treeDoWithDataCallbackInlineLoadControl.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_InlineLoadControl.Pre_doWithDataCallbackInlineLoadControl != null)
            Pre_InlineLoadControl.Pre_doWithDataCallbackInlineLoadControl.doWithData(this);
    }
}

class Pre_InlineLoadControlRenderCallback extends RenderCallback {}
class Pre_InlineLoadControlTreeRenderCallback extends TreeRenderCallback {}
class Pre_InlineLoadControlDoWithDataCallback extends DoWithDataCallback {};
class Pre_InlineLoadControlTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_InlineLoadControlEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsSurfaceInterpolator extends Pre_Node {
    Pre_Node metadata;
    int uDimension;
    int vDimension;
    double[] uKnot;
    double[] vKnot;
    int uOrder;
    int vOrder;
    Pre_Node controlPoint;
    double[] weight;
    static Pre_NurbsSurfaceInterpolatorRenderCallback Pre_renderCallbackNurbsSurfaceInterpolator;
    static void setPre_NurbsSurfaceInterpolatorRenderCallback(Pre_NurbsSurfaceInterpolatorRenderCallback node) {
        Pre_renderCallbackNurbsSurfaceInterpolator = node;
    }

    static Pre_NurbsSurfaceInterpolatorTreeRenderCallback Pre_treeRenderCallbackNurbsSurfaceInterpolator;
    static void setPre_NurbsSurfaceInterpolatorTreeRenderCallback(Pre_NurbsSurfaceInterpolatorTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsSurfaceInterpolator = node;
    }

    static Pre_NurbsSurfaceInterpolatorDoWithDataCallback Pre_doWithDataCallbackNurbsSurfaceInterpolator;
    static void setPre_NurbsSurfaceInterpolatorDoWithDataCallback(Pre_NurbsSurfaceInterpolatorDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsSurfaceInterpolator = node;
    }

    static Pre_NurbsSurfaceInterpolatorTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsSurfaceInterpolator;
    static void setPre_NurbsSurfaceInterpolatorTreeDoWithDataCallback(Pre_NurbsSurfaceInterpolatorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsSurfaceInterpolator = node;

    }

    static Pre_NurbsSurfaceInterpolatorEventsProcessedCallback Pre_eventsProcessedCallbackNurbsSurfaceInterpolator;
    static void setPre_NurbsSurfaceInterpolatorEventsProcessedCallback(Pre_NurbsSurfaceInterpolatorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsSurfaceInterpolator = node;
    }

    Pre_NurbsSurfaceInterpolator() {
    }
    void treeRender() {
        if (Pre_NurbsSurfaceInterpolator.Pre_treeRenderCallbackNurbsSurfaceInterpolator != null) {
            Pre_NurbsSurfaceInterpolator.Pre_treeRenderCallbackNurbsSurfaceInterpolator.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoint != null)
            controlPoint.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsSurfaceInterpolator.Pre_renderCallbackNurbsSurfaceInterpolator != null)
            Pre_NurbsSurfaceInterpolator.Pre_renderCallbackNurbsSurfaceInterpolator.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsSurfaceInterpolator.Pre_treeDoWithDataCallbackNurbsSurfaceInterpolator != null) {
            Pre_NurbsSurfaceInterpolator.Pre_treeDoWithDataCallbackNurbsSurfaceInterpolator.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoint != null)
            controlPoint.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsSurfaceInterpolator.Pre_doWithDataCallbackNurbsSurfaceInterpolator != null)
            Pre_NurbsSurfaceInterpolator.Pre_doWithDataCallbackNurbsSurfaceInterpolator.doWithData(this);
    }
}

class Pre_NurbsSurfaceInterpolatorRenderCallback extends RenderCallback {}
class Pre_NurbsSurfaceInterpolatorTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsSurfaceInterpolatorDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsSurfaceInterpolatorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsSurfaceInterpolatorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SphereSensor extends Pre_Node {
    Pre_Node metadata;
    boolean autoOffset;
    String description;
    boolean enabled;
    float[] offset;
    static Pre_SphereSensorRenderCallback Pre_renderCallbackSphereSensor;
    static void setPre_SphereSensorRenderCallback(Pre_SphereSensorRenderCallback node) {
        Pre_renderCallbackSphereSensor = node;
    }

    static Pre_SphereSensorTreeRenderCallback Pre_treeRenderCallbackSphereSensor;
    static void setPre_SphereSensorTreeRenderCallback(Pre_SphereSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackSphereSensor = node;
    }

    static Pre_SphereSensorDoWithDataCallback Pre_doWithDataCallbackSphereSensor;
    static void setPre_SphereSensorDoWithDataCallback(Pre_SphereSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackSphereSensor = node;
    }

    static Pre_SphereSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackSphereSensor;
    static void setPre_SphereSensorTreeDoWithDataCallback(Pre_SphereSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSphereSensor = node;

    }

    static Pre_SphereSensorEventsProcessedCallback Pre_eventsProcessedCallbackSphereSensor;
    static void setPre_SphereSensorEventsProcessedCallback(Pre_SphereSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSphereSensor = node;
    }

    Pre_SphereSensor() {
    }
    void treeRender() {
        if (Pre_SphereSensor.Pre_treeRenderCallbackSphereSensor != null) {
            Pre_SphereSensor.Pre_treeRenderCallbackSphereSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SphereSensor.Pre_renderCallbackSphereSensor != null)
            Pre_SphereSensor.Pre_renderCallbackSphereSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_SphereSensor.Pre_treeDoWithDataCallbackSphereSensor != null) {
            Pre_SphereSensor.Pre_treeDoWithDataCallbackSphereSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SphereSensor.Pre_doWithDataCallbackSphereSensor != null)
            Pre_SphereSensor.Pre_doWithDataCallbackSphereSensor.doWithData(this);
    }
}

class Pre_SphereSensorRenderCallback extends RenderCallback {}
class Pre_SphereSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_SphereSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_SphereSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SphereSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TexCoordDamper extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_TexCoordDamperRenderCallback Pre_renderCallbackTexCoordDamper;
    static void setPre_TexCoordDamperRenderCallback(Pre_TexCoordDamperRenderCallback node) {
        Pre_renderCallbackTexCoordDamper = node;
    }

    static Pre_TexCoordDamperTreeRenderCallback Pre_treeRenderCallbackTexCoordDamper;
    static void setPre_TexCoordDamperTreeRenderCallback(Pre_TexCoordDamperTreeRenderCallback node) {
        Pre_treeRenderCallbackTexCoordDamper = node;
    }

    static Pre_TexCoordDamperDoWithDataCallback Pre_doWithDataCallbackTexCoordDamper;
    static void setPre_TexCoordDamperDoWithDataCallback(Pre_TexCoordDamperDoWithDataCallback node) {
        Pre_doWithDataCallbackTexCoordDamper = node;
    }

    static Pre_TexCoordDamperTreeDoWithDataCallback Pre_treeDoWithDataCallbackTexCoordDamper;
    static void setPre_TexCoordDamperTreeDoWithDataCallback(Pre_TexCoordDamperTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTexCoordDamper = node;

    }

    static Pre_TexCoordDamperEventsProcessedCallback Pre_eventsProcessedCallbackTexCoordDamper;
    static void setPre_TexCoordDamperEventsProcessedCallback(Pre_TexCoordDamperEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTexCoordDamper = node;
    }

    Pre_TexCoordDamper() {
    }
    void treeRender() {
        if (Pre_TexCoordDamper.Pre_treeRenderCallbackTexCoordDamper != null) {
            Pre_TexCoordDamper.Pre_treeRenderCallbackTexCoordDamper.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TexCoordDamper.Pre_renderCallbackTexCoordDamper != null)
            Pre_TexCoordDamper.Pre_renderCallbackTexCoordDamper.render(this);
    }
    void treeDoWithData() {
        if (Pre_TexCoordDamper.Pre_treeDoWithDataCallbackTexCoordDamper != null) {
            Pre_TexCoordDamper.Pre_treeDoWithDataCallbackTexCoordDamper.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TexCoordDamper.Pre_doWithDataCallbackTexCoordDamper != null)
            Pre_TexCoordDamper.Pre_doWithDataCallbackTexCoordDamper.doWithData(this);
    }
}

class Pre_TexCoordDamperRenderCallback extends RenderCallback {}
class Pre_TexCoordDamperTreeRenderCallback extends TreeRenderCallback {}
class Pre_TexCoordDamperDoWithDataCallback extends DoWithDataCallback {};
class Pre_TexCoordDamperTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TexCoordDamperEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Background extends Pre_Node {
    Pre_Node metadata;
    float[] groundAngle;
    float[] groundColor;
    String[] backUrl;
    String[] rightUrl;
    String[] frontUrl;
    String[] leftUrl;
    String[] topUrl;
    String[] bottomUrl;
    float[] skyAngle;
    float transparency;
    float[] skyColor;
    static Pre_BackgroundRenderCallback Pre_renderCallbackBackground;
    static void setPre_BackgroundRenderCallback(Pre_BackgroundRenderCallback node) {
        Pre_renderCallbackBackground = node;
    }

    static Pre_BackgroundTreeRenderCallback Pre_treeRenderCallbackBackground;
    static void setPre_BackgroundTreeRenderCallback(Pre_BackgroundTreeRenderCallback node) {
        Pre_treeRenderCallbackBackground = node;
    }

    static Pre_BackgroundDoWithDataCallback Pre_doWithDataCallbackBackground;
    static void setPre_BackgroundDoWithDataCallback(Pre_BackgroundDoWithDataCallback node) {
        Pre_doWithDataCallbackBackground = node;
    }

    static Pre_BackgroundTreeDoWithDataCallback Pre_treeDoWithDataCallbackBackground;
    static void setPre_BackgroundTreeDoWithDataCallback(Pre_BackgroundTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackBackground = node;

    }

    static Pre_BackgroundEventsProcessedCallback Pre_eventsProcessedCallbackBackground;
    static void setPre_BackgroundEventsProcessedCallback(Pre_BackgroundEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackBackground = node;
    }

    Pre_Background() {
    }
    void treeRender() {
        if (Pre_Background.Pre_treeRenderCallbackBackground != null) {
            Pre_Background.Pre_treeRenderCallbackBackground.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Background.Pre_renderCallbackBackground != null)
            Pre_Background.Pre_renderCallbackBackground.render(this);
    }
    void treeDoWithData() {
        if (Pre_Background.Pre_treeDoWithDataCallbackBackground != null) {
            Pre_Background.Pre_treeDoWithDataCallbackBackground.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Background.Pre_doWithDataCallbackBackground != null)
            Pre_Background.Pre_doWithDataCallbackBackground.doWithData(this);
    }
}

class Pre_BackgroundRenderCallback extends RenderCallback {}
class Pre_BackgroundTreeRenderCallback extends TreeRenderCallback {}
class Pre_BackgroundDoWithDataCallback extends DoWithDataCallback {};
class Pre_BackgroundTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_BackgroundEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Contact extends Pre_Node {
    Pre_Node metadata;
    String[] appliedParameters;
    Pre_Node body1;
    Pre_Node body2;
    float bounce;
    float[] contactNormal;
    float depth;
    float[] frictionCoefficients;
    float[] frictionDirection;
    Pre_Node geometry1;
    Pre_Node geometry2;
    float minBounceSpeed;
    float[] position;
    float[] slipCoefficients;
    float softnessConstantForceMix;
    float softnessErrorCorrection;
    float[] surfaceSpeed;
    static Pre_ContactRenderCallback Pre_renderCallbackContact;
    static void setPre_ContactRenderCallback(Pre_ContactRenderCallback node) {
        Pre_renderCallbackContact = node;
    }

    static Pre_ContactTreeRenderCallback Pre_treeRenderCallbackContact;
    static void setPre_ContactTreeRenderCallback(Pre_ContactTreeRenderCallback node) {
        Pre_treeRenderCallbackContact = node;
    }

    static Pre_ContactDoWithDataCallback Pre_doWithDataCallbackContact;
    static void setPre_ContactDoWithDataCallback(Pre_ContactDoWithDataCallback node) {
        Pre_doWithDataCallbackContact = node;
    }

    static Pre_ContactTreeDoWithDataCallback Pre_treeDoWithDataCallbackContact;
    static void setPre_ContactTreeDoWithDataCallback(Pre_ContactTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackContact = node;

    }

    static Pre_ContactEventsProcessedCallback Pre_eventsProcessedCallbackContact;
    static void setPre_ContactEventsProcessedCallback(Pre_ContactEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackContact = node;
    }

    Pre_Contact() {
    }
    void treeRender() {
        if (Pre_Contact.Pre_treeRenderCallbackContact != null) {
            Pre_Contact.Pre_treeRenderCallbackContact.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (geometry1 != null)
            geometry1.treeRender();
        if (geometry2 != null)
            geometry2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Contact.Pre_renderCallbackContact != null)
            Pre_Contact.Pre_renderCallbackContact.render(this);
    }
    void treeDoWithData() {
        if (Pre_Contact.Pre_treeDoWithDataCallbackContact != null) {
            Pre_Contact.Pre_treeDoWithDataCallbackContact.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (geometry1 != null)
            geometry1.treeDoWithData();
        if (geometry2 != null)
            geometry2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Contact.Pre_doWithDataCallbackContact != null)
            Pre_Contact.Pre_doWithDataCallbackContact.doWithData(this);
    }
}

class Pre_ContactRenderCallback extends RenderCallback {}
class Pre_ContactTreeRenderCallback extends TreeRenderCallback {}
class Pre_ContactDoWithDataCallback extends DoWithDataCallback {};
class Pre_ContactTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ContactEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ContourPolyline2D extends Pre_Node {
    Pre_Node metadata;
    float[] lineSegments;
    static Pre_ContourPolyline2DRenderCallback Pre_renderCallbackContourPolyline2D;
    static void setPre_ContourPolyline2DRenderCallback(Pre_ContourPolyline2DRenderCallback node) {
        Pre_renderCallbackContourPolyline2D = node;
    }

    static Pre_ContourPolyline2DTreeRenderCallback Pre_treeRenderCallbackContourPolyline2D;
    static void setPre_ContourPolyline2DTreeRenderCallback(Pre_ContourPolyline2DTreeRenderCallback node) {
        Pre_treeRenderCallbackContourPolyline2D = node;
    }

    static Pre_ContourPolyline2DDoWithDataCallback Pre_doWithDataCallbackContourPolyline2D;
    static void setPre_ContourPolyline2DDoWithDataCallback(Pre_ContourPolyline2DDoWithDataCallback node) {
        Pre_doWithDataCallbackContourPolyline2D = node;
    }

    static Pre_ContourPolyline2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackContourPolyline2D;
    static void setPre_ContourPolyline2DTreeDoWithDataCallback(Pre_ContourPolyline2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackContourPolyline2D = node;

    }

    static Pre_ContourPolyline2DEventsProcessedCallback Pre_eventsProcessedCallbackContourPolyline2D;
    static void setPre_ContourPolyline2DEventsProcessedCallback(Pre_ContourPolyline2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackContourPolyline2D = node;
    }

    Pre_ContourPolyline2D() {
    }
    void treeRender() {
        if (Pre_ContourPolyline2D.Pre_treeRenderCallbackContourPolyline2D != null) {
            Pre_ContourPolyline2D.Pre_treeRenderCallbackContourPolyline2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ContourPolyline2D.Pre_renderCallbackContourPolyline2D != null)
            Pre_ContourPolyline2D.Pre_renderCallbackContourPolyline2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_ContourPolyline2D.Pre_treeDoWithDataCallbackContourPolyline2D != null) {
            Pre_ContourPolyline2D.Pre_treeDoWithDataCallbackContourPolyline2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ContourPolyline2D.Pre_doWithDataCallbackContourPolyline2D != null)
            Pre_ContourPolyline2D.Pre_doWithDataCallbackContourPolyline2D.doWithData(this);
    }
}

class Pre_ContourPolyline2DRenderCallback extends RenderCallback {}
class Pre_ContourPolyline2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_ContourPolyline2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_ContourPolyline2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ContourPolyline2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_LayerSet extends Pre_Node {
    Pre_Node metadata;
    int activeLayer;
    Pre_Node [] layers;
    int[] order;
    static Pre_LayerSetRenderCallback Pre_renderCallbackLayerSet;
    static void setPre_LayerSetRenderCallback(Pre_LayerSetRenderCallback node) {
        Pre_renderCallbackLayerSet = node;
    }

    static Pre_LayerSetTreeRenderCallback Pre_treeRenderCallbackLayerSet;
    static void setPre_LayerSetTreeRenderCallback(Pre_LayerSetTreeRenderCallback node) {
        Pre_treeRenderCallbackLayerSet = node;
    }

    static Pre_LayerSetDoWithDataCallback Pre_doWithDataCallbackLayerSet;
    static void setPre_LayerSetDoWithDataCallback(Pre_LayerSetDoWithDataCallback node) {
        Pre_doWithDataCallbackLayerSet = node;
    }

    static Pre_LayerSetTreeDoWithDataCallback Pre_treeDoWithDataCallbackLayerSet;
    static void setPre_LayerSetTreeDoWithDataCallback(Pre_LayerSetTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackLayerSet = node;

    }

    static Pre_LayerSetEventsProcessedCallback Pre_eventsProcessedCallbackLayerSet;
    static void setPre_LayerSetEventsProcessedCallback(Pre_LayerSetEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackLayerSet = node;
    }

    Pre_LayerSet() {
    }
    void treeRender() {
        if (Pre_LayerSet.Pre_treeRenderCallbackLayerSet != null) {
            Pre_LayerSet.Pre_treeRenderCallbackLayerSet.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (layers != null)
            for (int i = 0; i < layers.length; i++)
                if (layers[i] != null)
                    layers[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_LayerSet.Pre_renderCallbackLayerSet != null)
            Pre_LayerSet.Pre_renderCallbackLayerSet.render(this);
    }
    void treeDoWithData() {
        if (Pre_LayerSet.Pre_treeDoWithDataCallbackLayerSet != null) {
            Pre_LayerSet.Pre_treeDoWithDataCallbackLayerSet.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (layers != null)
            for (int i = 0; i < layers.length; i++)
                if (layers[i] != null)
                    layers[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_LayerSet.Pre_doWithDataCallbackLayerSet != null)
            Pre_LayerSet.Pre_doWithDataCallbackLayerSet.doWithData(this);
    }
}

class Pre_LayerSetRenderCallback extends RenderCallback {}
class Pre_LayerSetTreeRenderCallback extends TreeRenderCallback {}
class Pre_LayerSetDoWithDataCallback extends DoWithDataCallback {};
class Pre_LayerSetTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_LayerSetEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_MotorJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float axis1Angle;
    float axis1Torque;
    float axis2Angle;
    float axis2Torque;
    float axis3Angle;
    float axis3Torque;
    int enabledAxes;
    float[] motor1Axis;
    float[] motor2Axis;
    float[] motor3Axis;
    float stop1Bounce;
    float stop1ErrorCorrection;
    float stop2Bounce;
    float stop2ErrorCorrection;
    float stop3Bounce;
    float stop3ErrorCorrection;
    boolean autoCalc;
    static Pre_MotorJointRenderCallback Pre_renderCallbackMotorJoint;
    static void setPre_MotorJointRenderCallback(Pre_MotorJointRenderCallback node) {
        Pre_renderCallbackMotorJoint = node;
    }

    static Pre_MotorJointTreeRenderCallback Pre_treeRenderCallbackMotorJoint;
    static void setPre_MotorJointTreeRenderCallback(Pre_MotorJointTreeRenderCallback node) {
        Pre_treeRenderCallbackMotorJoint = node;
    }

    static Pre_MotorJointDoWithDataCallback Pre_doWithDataCallbackMotorJoint;
    static void setPre_MotorJointDoWithDataCallback(Pre_MotorJointDoWithDataCallback node) {
        Pre_doWithDataCallbackMotorJoint = node;
    }

    static Pre_MotorJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackMotorJoint;
    static void setPre_MotorJointTreeDoWithDataCallback(Pre_MotorJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackMotorJoint = node;

    }

    static Pre_MotorJointEventsProcessedCallback Pre_eventsProcessedCallbackMotorJoint;
    static void setPre_MotorJointEventsProcessedCallback(Pre_MotorJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackMotorJoint = node;
    }

    Pre_MotorJoint() {
    }
    void treeRender() {
        if (Pre_MotorJoint.Pre_treeRenderCallbackMotorJoint != null) {
            Pre_MotorJoint.Pre_treeRenderCallbackMotorJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_MotorJoint.Pre_renderCallbackMotorJoint != null)
            Pre_MotorJoint.Pre_renderCallbackMotorJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_MotorJoint.Pre_treeDoWithDataCallbackMotorJoint != null) {
            Pre_MotorJoint.Pre_treeDoWithDataCallbackMotorJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_MotorJoint.Pre_doWithDataCallbackMotorJoint != null)
            Pre_MotorJoint.Pre_doWithDataCallbackMotorJoint.doWithData(this);
    }
}

class Pre_MotorJointRenderCallback extends RenderCallback {}
class Pre_MotorJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_MotorJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_MotorJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_MotorJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ShaderPart extends Pre_Node {
    Pre_Node metadata;
    String[] url;
    String type;
    static Pre_ShaderPartRenderCallback Pre_renderCallbackShaderPart;
    static void setPre_ShaderPartRenderCallback(Pre_ShaderPartRenderCallback node) {
        Pre_renderCallbackShaderPart = node;
    }

    static Pre_ShaderPartTreeRenderCallback Pre_treeRenderCallbackShaderPart;
    static void setPre_ShaderPartTreeRenderCallback(Pre_ShaderPartTreeRenderCallback node) {
        Pre_treeRenderCallbackShaderPart = node;
    }

    static Pre_ShaderPartDoWithDataCallback Pre_doWithDataCallbackShaderPart;
    static void setPre_ShaderPartDoWithDataCallback(Pre_ShaderPartDoWithDataCallback node) {
        Pre_doWithDataCallbackShaderPart = node;
    }

    static Pre_ShaderPartTreeDoWithDataCallback Pre_treeDoWithDataCallbackShaderPart;
    static void setPre_ShaderPartTreeDoWithDataCallback(Pre_ShaderPartTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackShaderPart = node;

    }

    static Pre_ShaderPartEventsProcessedCallback Pre_eventsProcessedCallbackShaderPart;
    static void setPre_ShaderPartEventsProcessedCallback(Pre_ShaderPartEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackShaderPart = node;
    }

    Pre_ShaderPart() {
    }
    void treeRender() {
        if (Pre_ShaderPart.Pre_treeRenderCallbackShaderPart != null) {
            Pre_ShaderPart.Pre_treeRenderCallbackShaderPart.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ShaderPart.Pre_renderCallbackShaderPart != null)
            Pre_ShaderPart.Pre_renderCallbackShaderPart.render(this);
    }
    void treeDoWithData() {
        if (Pre_ShaderPart.Pre_treeDoWithDataCallbackShaderPart != null) {
            Pre_ShaderPart.Pre_treeDoWithDataCallbackShaderPart.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ShaderPart.Pre_doWithDataCallbackShaderPart != null)
            Pre_ShaderPart.Pre_doWithDataCallbackShaderPart.doWithData(this);
    }
}

class Pre_ShaderPartRenderCallback extends RenderCallback {}
class Pre_ShaderPartTreeRenderCallback extends TreeRenderCallback {}
class Pre_ShaderPartDoWithDataCallback extends DoWithDataCallback {};
class Pre_ShaderPartTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ShaderPartEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureTransform3D extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    float[] rotation;
    float[] scale;
    float[] translation;
    static Pre_TextureTransform3DRenderCallback Pre_renderCallbackTextureTransform3D;
    static void setPre_TextureTransform3DRenderCallback(Pre_TextureTransform3DRenderCallback node) {
        Pre_renderCallbackTextureTransform3D = node;
    }

    static Pre_TextureTransform3DTreeRenderCallback Pre_treeRenderCallbackTextureTransform3D;
    static void setPre_TextureTransform3DTreeRenderCallback(Pre_TextureTransform3DTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureTransform3D = node;
    }

    static Pre_TextureTransform3DDoWithDataCallback Pre_doWithDataCallbackTextureTransform3D;
    static void setPre_TextureTransform3DDoWithDataCallback(Pre_TextureTransform3DDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureTransform3D = node;
    }

    static Pre_TextureTransform3DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureTransform3D;
    static void setPre_TextureTransform3DTreeDoWithDataCallback(Pre_TextureTransform3DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureTransform3D = node;

    }

    static Pre_TextureTransform3DEventsProcessedCallback Pre_eventsProcessedCallbackTextureTransform3D;
    static void setPre_TextureTransform3DEventsProcessedCallback(Pre_TextureTransform3DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureTransform3D = node;
    }

    Pre_TextureTransform3D() {
    }
    void treeRender() {
        if (Pre_TextureTransform3D.Pre_treeRenderCallbackTextureTransform3D != null) {
            Pre_TextureTransform3D.Pre_treeRenderCallbackTextureTransform3D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureTransform3D.Pre_renderCallbackTextureTransform3D != null)
            Pre_TextureTransform3D.Pre_renderCallbackTextureTransform3D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureTransform3D.Pre_treeDoWithDataCallbackTextureTransform3D != null) {
            Pre_TextureTransform3D.Pre_treeDoWithDataCallbackTextureTransform3D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureTransform3D.Pre_doWithDataCallbackTextureTransform3D != null)
            Pre_TextureTransform3D.Pre_doWithDataCallbackTextureTransform3D.doWithData(this);
    }
}

class Pre_TextureTransform3DRenderCallback extends RenderCallback {}
class Pre_TextureTransform3DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureTransform3DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureTransform3DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureTransform3DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CoordinateDouble extends Pre_Node {
    Pre_Node metadata;
    double[] point;
    static Pre_CoordinateDoubleRenderCallback Pre_renderCallbackCoordinateDouble;
    static void setPre_CoordinateDoubleRenderCallback(Pre_CoordinateDoubleRenderCallback node) {
        Pre_renderCallbackCoordinateDouble = node;
    }

    static Pre_CoordinateDoubleTreeRenderCallback Pre_treeRenderCallbackCoordinateDouble;
    static void setPre_CoordinateDoubleTreeRenderCallback(Pre_CoordinateDoubleTreeRenderCallback node) {
        Pre_treeRenderCallbackCoordinateDouble = node;
    }

    static Pre_CoordinateDoubleDoWithDataCallback Pre_doWithDataCallbackCoordinateDouble;
    static void setPre_CoordinateDoubleDoWithDataCallback(Pre_CoordinateDoubleDoWithDataCallback node) {
        Pre_doWithDataCallbackCoordinateDouble = node;
    }

    static Pre_CoordinateDoubleTreeDoWithDataCallback Pre_treeDoWithDataCallbackCoordinateDouble;
    static void setPre_CoordinateDoubleTreeDoWithDataCallback(Pre_CoordinateDoubleTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCoordinateDouble = node;

    }

    static Pre_CoordinateDoubleEventsProcessedCallback Pre_eventsProcessedCallbackCoordinateDouble;
    static void setPre_CoordinateDoubleEventsProcessedCallback(Pre_CoordinateDoubleEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCoordinateDouble = node;
    }

    Pre_CoordinateDouble() {
    }
    void treeRender() {
        if (Pre_CoordinateDouble.Pre_treeRenderCallbackCoordinateDouble != null) {
            Pre_CoordinateDouble.Pre_treeRenderCallbackCoordinateDouble.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CoordinateDouble.Pre_renderCallbackCoordinateDouble != null)
            Pre_CoordinateDouble.Pre_renderCallbackCoordinateDouble.render(this);
    }
    void treeDoWithData() {
        if (Pre_CoordinateDouble.Pre_treeDoWithDataCallbackCoordinateDouble != null) {
            Pre_CoordinateDouble.Pre_treeDoWithDataCallbackCoordinateDouble.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CoordinateDouble.Pre_doWithDataCallbackCoordinateDouble != null)
            Pre_CoordinateDouble.Pre_doWithDataCallbackCoordinateDouble.doWithData(this);
    }
}

class Pre_CoordinateDoubleRenderCallback extends RenderCallback {}
class Pre_CoordinateDoubleTreeRenderCallback extends TreeRenderCallback {}
class Pre_CoordinateDoubleDoWithDataCallback extends DoWithDataCallback {};
class Pre_CoordinateDoubleTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CoordinateDoubleEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsTrimmedSurface extends Pre_Node {
    Pre_Node metadata;
    int uDimension;
    int vDimension;
    float[] uKnot;
    float[] vKnot;
    int uOrder;
    int vOrder;
    Pre_Node controlPoint;
    float[] weight;
    int uTessellation;
    int vTessellation;
    Pre_Node texCoord;
    boolean uClosed;
    boolean vClosed;
    boolean solid;
    Pre_Node [] trimmingContour;
    static Pre_NurbsTrimmedSurfaceRenderCallback Pre_renderCallbackNurbsTrimmedSurface;
    static void setPre_NurbsTrimmedSurfaceRenderCallback(Pre_NurbsTrimmedSurfaceRenderCallback node) {
        Pre_renderCallbackNurbsTrimmedSurface = node;
    }

    static Pre_NurbsTrimmedSurfaceTreeRenderCallback Pre_treeRenderCallbackNurbsTrimmedSurface;
    static void setPre_NurbsTrimmedSurfaceTreeRenderCallback(Pre_NurbsTrimmedSurfaceTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsTrimmedSurface = node;
    }

    static Pre_NurbsTrimmedSurfaceDoWithDataCallback Pre_doWithDataCallbackNurbsTrimmedSurface;
    static void setPre_NurbsTrimmedSurfaceDoWithDataCallback(Pre_NurbsTrimmedSurfaceDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsTrimmedSurface = node;
    }

    static Pre_NurbsTrimmedSurfaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsTrimmedSurface;
    static void setPre_NurbsTrimmedSurfaceTreeDoWithDataCallback(Pre_NurbsTrimmedSurfaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsTrimmedSurface = node;

    }

    static Pre_NurbsTrimmedSurfaceEventsProcessedCallback Pre_eventsProcessedCallbackNurbsTrimmedSurface;
    static void setPre_NurbsTrimmedSurfaceEventsProcessedCallback(Pre_NurbsTrimmedSurfaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsTrimmedSurface = node;
    }

    Pre_NurbsTrimmedSurface() {
    }
    void treeRender() {
        if (Pre_NurbsTrimmedSurface.Pre_treeRenderCallbackNurbsTrimmedSurface != null) {
            Pre_NurbsTrimmedSurface.Pre_treeRenderCallbackNurbsTrimmedSurface.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (controlPoint != null)
            controlPoint.treeRender();
        if (texCoord != null)
            texCoord.treeRender();
        if (trimmingContour != null)
            for (int i = 0; i < trimmingContour.length; i++)
                if (trimmingContour[i] != null)
                    trimmingContour[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsTrimmedSurface.Pre_renderCallbackNurbsTrimmedSurface != null)
            Pre_NurbsTrimmedSurface.Pre_renderCallbackNurbsTrimmedSurface.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsTrimmedSurface.Pre_treeDoWithDataCallbackNurbsTrimmedSurface != null) {
            Pre_NurbsTrimmedSurface.Pre_treeDoWithDataCallbackNurbsTrimmedSurface.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (controlPoint != null)
            controlPoint.treeDoWithData();
        if (texCoord != null)
            texCoord.treeDoWithData();
        if (trimmingContour != null)
            for (int i = 0; i < trimmingContour.length; i++)
                if (trimmingContour[i] != null)
                    trimmingContour[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsTrimmedSurface.Pre_doWithDataCallbackNurbsTrimmedSurface != null)
            Pre_NurbsTrimmedSurface.Pre_doWithDataCallbackNurbsTrimmedSurface.doWithData(this);
    }
}

class Pre_NurbsTrimmedSurfaceRenderCallback extends RenderCallback {}
class Pre_NurbsTrimmedSurfaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsTrimmedSurfaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsTrimmedSurfaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsTrimmedSurfaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionChaser extends Pre_Node {
    Pre_Node metadata;
    double duration;
    float[] initialDestination;
    float[] initialValue;
    static Pre_PositionChaserRenderCallback Pre_renderCallbackPositionChaser;
    static void setPre_PositionChaserRenderCallback(Pre_PositionChaserRenderCallback node) {
        Pre_renderCallbackPositionChaser = node;
    }

    static Pre_PositionChaserTreeRenderCallback Pre_treeRenderCallbackPositionChaser;
    static void setPre_PositionChaserTreeRenderCallback(Pre_PositionChaserTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionChaser = node;
    }

    static Pre_PositionChaserDoWithDataCallback Pre_doWithDataCallbackPositionChaser;
    static void setPre_PositionChaserDoWithDataCallback(Pre_PositionChaserDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionChaser = node;
    }

    static Pre_PositionChaserTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionChaser;
    static void setPre_PositionChaserTreeDoWithDataCallback(Pre_PositionChaserTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionChaser = node;

    }

    static Pre_PositionChaserEventsProcessedCallback Pre_eventsProcessedCallbackPositionChaser;
    static void setPre_PositionChaserEventsProcessedCallback(Pre_PositionChaserEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionChaser = node;
    }

    Pre_PositionChaser() {
    }
    void treeRender() {
        if (Pre_PositionChaser.Pre_treeRenderCallbackPositionChaser != null) {
            Pre_PositionChaser.Pre_treeRenderCallbackPositionChaser.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionChaser.Pre_renderCallbackPositionChaser != null)
            Pre_PositionChaser.Pre_renderCallbackPositionChaser.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionChaser.Pre_treeDoWithDataCallbackPositionChaser != null) {
            Pre_PositionChaser.Pre_treeDoWithDataCallbackPositionChaser.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionChaser.Pre_doWithDataCallbackPositionChaser != null)
            Pre_PositionChaser.Pre_doWithDataCallbackPositionChaser.doWithData(this);
    }
}

class Pre_PositionChaserRenderCallback extends RenderCallback {}
class Pre_PositionChaserTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionChaserDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionChaserTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionChaserEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PositionDamper extends Pre_Node {
    Pre_Node metadata;
    double tau;
    float tolerance;
    float[] initialDestination;
    float[] initialValue;
    int order;
    static Pre_PositionDamperRenderCallback Pre_renderCallbackPositionDamper;
    static void setPre_PositionDamperRenderCallback(Pre_PositionDamperRenderCallback node) {
        Pre_renderCallbackPositionDamper = node;
    }

    static Pre_PositionDamperTreeRenderCallback Pre_treeRenderCallbackPositionDamper;
    static void setPre_PositionDamperTreeRenderCallback(Pre_PositionDamperTreeRenderCallback node) {
        Pre_treeRenderCallbackPositionDamper = node;
    }

    static Pre_PositionDamperDoWithDataCallback Pre_doWithDataCallbackPositionDamper;
    static void setPre_PositionDamperDoWithDataCallback(Pre_PositionDamperDoWithDataCallback node) {
        Pre_doWithDataCallbackPositionDamper = node;
    }

    static Pre_PositionDamperTreeDoWithDataCallback Pre_treeDoWithDataCallbackPositionDamper;
    static void setPre_PositionDamperTreeDoWithDataCallback(Pre_PositionDamperTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPositionDamper = node;

    }

    static Pre_PositionDamperEventsProcessedCallback Pre_eventsProcessedCallbackPositionDamper;
    static void setPre_PositionDamperEventsProcessedCallback(Pre_PositionDamperEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPositionDamper = node;
    }

    Pre_PositionDamper() {
    }
    void treeRender() {
        if (Pre_PositionDamper.Pre_treeRenderCallbackPositionDamper != null) {
            Pre_PositionDamper.Pre_treeRenderCallbackPositionDamper.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PositionDamper.Pre_renderCallbackPositionDamper != null)
            Pre_PositionDamper.Pre_renderCallbackPositionDamper.render(this);
    }
    void treeDoWithData() {
        if (Pre_PositionDamper.Pre_treeDoWithDataCallbackPositionDamper != null) {
            Pre_PositionDamper.Pre_treeDoWithDataCallbackPositionDamper.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PositionDamper.Pre_doWithDataCallbackPositionDamper != null)
            Pre_PositionDamper.Pre_doWithDataCallbackPositionDamper.doWithData(this);
    }
}

class Pre_PositionDamperRenderCallback extends RenderCallback {}
class Pre_PositionDamperTreeRenderCallback extends TreeRenderCallback {}
class Pre_PositionDamperDoWithDataCallback extends DoWithDataCallback {};
class Pre_PositionDamperTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PositionDamperEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ProgramShader extends Pre_Node {
    Pre_Node metadata;
    Pre_Node [] programs;
    String language;
    static Pre_ProgramShaderRenderCallback Pre_renderCallbackProgramShader;
    static void setPre_ProgramShaderRenderCallback(Pre_ProgramShaderRenderCallback node) {
        Pre_renderCallbackProgramShader = node;
    }

    static Pre_ProgramShaderTreeRenderCallback Pre_treeRenderCallbackProgramShader;
    static void setPre_ProgramShaderTreeRenderCallback(Pre_ProgramShaderTreeRenderCallback node) {
        Pre_treeRenderCallbackProgramShader = node;
    }

    static Pre_ProgramShaderDoWithDataCallback Pre_doWithDataCallbackProgramShader;
    static void setPre_ProgramShaderDoWithDataCallback(Pre_ProgramShaderDoWithDataCallback node) {
        Pre_doWithDataCallbackProgramShader = node;
    }

    static Pre_ProgramShaderTreeDoWithDataCallback Pre_treeDoWithDataCallbackProgramShader;
    static void setPre_ProgramShaderTreeDoWithDataCallback(Pre_ProgramShaderTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackProgramShader = node;

    }

    static Pre_ProgramShaderEventsProcessedCallback Pre_eventsProcessedCallbackProgramShader;
    static void setPre_ProgramShaderEventsProcessedCallback(Pre_ProgramShaderEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackProgramShader = node;
    }

    Pre_ProgramShader() {
    }
    void treeRender() {
        if (Pre_ProgramShader.Pre_treeRenderCallbackProgramShader != null) {
            Pre_ProgramShader.Pre_treeRenderCallbackProgramShader.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (programs != null)
            for (int i = 0; i < programs.length; i++)
                if (programs[i] != null)
                    programs[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ProgramShader.Pre_renderCallbackProgramShader != null)
            Pre_ProgramShader.Pre_renderCallbackProgramShader.render(this);
    }
    void treeDoWithData() {
        if (Pre_ProgramShader.Pre_treeDoWithDataCallbackProgramShader != null) {
            Pre_ProgramShader.Pre_treeDoWithDataCallbackProgramShader.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (programs != null)
            for (int i = 0; i < programs.length; i++)
                if (programs[i] != null)
                    programs[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ProgramShader.Pre_doWithDataCallbackProgramShader != null)
            Pre_ProgramShader.Pre_doWithDataCallbackProgramShader.doWithData(this);
    }
}

class Pre_ProgramShaderRenderCallback extends RenderCallback {}
class Pre_ProgramShaderTreeRenderCallback extends TreeRenderCallback {}
class Pre_ProgramShaderDoWithDataCallback extends DoWithDataCallback {};
class Pre_ProgramShaderTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ProgramShaderEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SurfaceEmitter extends Pre_Node {
    Pre_Node metadata;
    float speed;
    float variation;
    float mass;
    float surfaceArea;
    int[] coordIndex;
    Pre_Node surface;
    static Pre_SurfaceEmitterRenderCallback Pre_renderCallbackSurfaceEmitter;
    static void setPre_SurfaceEmitterRenderCallback(Pre_SurfaceEmitterRenderCallback node) {
        Pre_renderCallbackSurfaceEmitter = node;
    }

    static Pre_SurfaceEmitterTreeRenderCallback Pre_treeRenderCallbackSurfaceEmitter;
    static void setPre_SurfaceEmitterTreeRenderCallback(Pre_SurfaceEmitterTreeRenderCallback node) {
        Pre_treeRenderCallbackSurfaceEmitter = node;
    }

    static Pre_SurfaceEmitterDoWithDataCallback Pre_doWithDataCallbackSurfaceEmitter;
    static void setPre_SurfaceEmitterDoWithDataCallback(Pre_SurfaceEmitterDoWithDataCallback node) {
        Pre_doWithDataCallbackSurfaceEmitter = node;
    }

    static Pre_SurfaceEmitterTreeDoWithDataCallback Pre_treeDoWithDataCallbackSurfaceEmitter;
    static void setPre_SurfaceEmitterTreeDoWithDataCallback(Pre_SurfaceEmitterTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSurfaceEmitter = node;

    }

    static Pre_SurfaceEmitterEventsProcessedCallback Pre_eventsProcessedCallbackSurfaceEmitter;
    static void setPre_SurfaceEmitterEventsProcessedCallback(Pre_SurfaceEmitterEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSurfaceEmitter = node;
    }

    Pre_SurfaceEmitter() {
    }
    void treeRender() {
        if (Pre_SurfaceEmitter.Pre_treeRenderCallbackSurfaceEmitter != null) {
            Pre_SurfaceEmitter.Pre_treeRenderCallbackSurfaceEmitter.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (surface != null)
            surface.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SurfaceEmitter.Pre_renderCallbackSurfaceEmitter != null)
            Pre_SurfaceEmitter.Pre_renderCallbackSurfaceEmitter.render(this);
    }
    void treeDoWithData() {
        if (Pre_SurfaceEmitter.Pre_treeDoWithDataCallbackSurfaceEmitter != null) {
            Pre_SurfaceEmitter.Pre_treeDoWithDataCallbackSurfaceEmitter.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (surface != null)
            surface.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SurfaceEmitter.Pre_doWithDataCallbackSurfaceEmitter != null)
            Pre_SurfaceEmitter.Pre_doWithDataCallbackSurfaceEmitter.doWithData(this);
    }
}

class Pre_SurfaceEmitterRenderCallback extends RenderCallback {}
class Pre_SurfaceEmitterTreeRenderCallback extends TreeRenderCallback {}
class Pre_SurfaceEmitterDoWithDataCallback extends DoWithDataCallback {};
class Pre_SurfaceEmitterTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SurfaceEmitterEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TouchSensor extends Pre_Node {
    Pre_Node metadata;
    String description;
    boolean enabled;
    static Pre_TouchSensorRenderCallback Pre_renderCallbackTouchSensor;
    static void setPre_TouchSensorRenderCallback(Pre_TouchSensorRenderCallback node) {
        Pre_renderCallbackTouchSensor = node;
    }

    static Pre_TouchSensorTreeRenderCallback Pre_treeRenderCallbackTouchSensor;
    static void setPre_TouchSensorTreeRenderCallback(Pre_TouchSensorTreeRenderCallback node) {
        Pre_treeRenderCallbackTouchSensor = node;
    }

    static Pre_TouchSensorDoWithDataCallback Pre_doWithDataCallbackTouchSensor;
    static void setPre_TouchSensorDoWithDataCallback(Pre_TouchSensorDoWithDataCallback node) {
        Pre_doWithDataCallbackTouchSensor = node;
    }

    static Pre_TouchSensorTreeDoWithDataCallback Pre_treeDoWithDataCallbackTouchSensor;
    static void setPre_TouchSensorTreeDoWithDataCallback(Pre_TouchSensorTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTouchSensor = node;

    }

    static Pre_TouchSensorEventsProcessedCallback Pre_eventsProcessedCallbackTouchSensor;
    static void setPre_TouchSensorEventsProcessedCallback(Pre_TouchSensorEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTouchSensor = node;
    }

    Pre_TouchSensor() {
    }
    void treeRender() {
        if (Pre_TouchSensor.Pre_treeRenderCallbackTouchSensor != null) {
            Pre_TouchSensor.Pre_treeRenderCallbackTouchSensor.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TouchSensor.Pre_renderCallbackTouchSensor != null)
            Pre_TouchSensor.Pre_renderCallbackTouchSensor.render(this);
    }
    void treeDoWithData() {
        if (Pre_TouchSensor.Pre_treeDoWithDataCallbackTouchSensor != null) {
            Pre_TouchSensor.Pre_treeDoWithDataCallbackTouchSensor.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TouchSensor.Pre_doWithDataCallbackTouchSensor != null)
            Pre_TouchSensor.Pre_doWithDataCallbackTouchSensor.doWithData(this);
    }
}

class Pre_TouchSensorRenderCallback extends RenderCallback {}
class Pre_TouchSensorTreeRenderCallback extends TreeRenderCallback {}
class Pre_TouchSensorDoWithDataCallback extends DoWithDataCallback {};
class Pre_TouchSensorTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TouchSensorEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_PointLight extends Pre_Node {
    Pre_Node metadata;
    float ambientIntensity;
    float[] attenuation;
    float[] color;
    boolean global;
    float intensity;
    float[] location;
    boolean on;
    float radius;
    boolean shadows;
    float projectionNear;
    float projectionFar;
    float[] up;
    Pre_Node defaultShadowMap;
    boolean kambiShadows;
    boolean kambiShadowsMain;
    static Pre_PointLightRenderCallback Pre_renderCallbackPointLight;
    static void setPre_PointLightRenderCallback(Pre_PointLightRenderCallback node) {
        Pre_renderCallbackPointLight = node;
    }

    static Pre_PointLightTreeRenderCallback Pre_treeRenderCallbackPointLight;
    static void setPre_PointLightTreeRenderCallback(Pre_PointLightTreeRenderCallback node) {
        Pre_treeRenderCallbackPointLight = node;
    }

    static Pre_PointLightDoWithDataCallback Pre_doWithDataCallbackPointLight;
    static void setPre_PointLightDoWithDataCallback(Pre_PointLightDoWithDataCallback node) {
        Pre_doWithDataCallbackPointLight = node;
    }

    static Pre_PointLightTreeDoWithDataCallback Pre_treeDoWithDataCallbackPointLight;
    static void setPre_PointLightTreeDoWithDataCallback(Pre_PointLightTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackPointLight = node;

    }

    static Pre_PointLightEventsProcessedCallback Pre_eventsProcessedCallbackPointLight;
    static void setPre_PointLightEventsProcessedCallback(Pre_PointLightEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackPointLight = node;
    }

    Pre_PointLight() {
    }
    void treeRender() {
        if (Pre_PointLight.Pre_treeRenderCallbackPointLight != null) {
            Pre_PointLight.Pre_treeRenderCallbackPointLight.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (defaultShadowMap != null)
            defaultShadowMap.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_PointLight.Pre_renderCallbackPointLight != null)
            Pre_PointLight.Pre_renderCallbackPointLight.render(this);
    }
    void treeDoWithData() {
        if (Pre_PointLight.Pre_treeDoWithDataCallbackPointLight != null) {
            Pre_PointLight.Pre_treeDoWithDataCallbackPointLight.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (defaultShadowMap != null)
            defaultShadowMap.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_PointLight.Pre_doWithDataCallbackPointLight != null)
            Pre_PointLight.Pre_doWithDataCallbackPointLight.doWithData(this);
    }
}

class Pre_PointLightRenderCallback extends RenderCallback {}
class Pre_PointLightTreeRenderCallback extends TreeRenderCallback {}
class Pre_PointLightDoWithDataCallback extends DoWithDataCallback {};
class Pre_PointLightTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_PointLightEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_SliderJoint extends Pre_Node {
    Pre_Node metadata;
    Pre_Node body1;
    Pre_Node body2;
    String[] forceOutput;
    float[] axis;
    float maxSeparation;
    float minSeparation;
    float stopBounce;
    float stopErrorCorrection;
    static Pre_SliderJointRenderCallback Pre_renderCallbackSliderJoint;
    static void setPre_SliderJointRenderCallback(Pre_SliderJointRenderCallback node) {
        Pre_renderCallbackSliderJoint = node;
    }

    static Pre_SliderJointTreeRenderCallback Pre_treeRenderCallbackSliderJoint;
    static void setPre_SliderJointTreeRenderCallback(Pre_SliderJointTreeRenderCallback node) {
        Pre_treeRenderCallbackSliderJoint = node;
    }

    static Pre_SliderJointDoWithDataCallback Pre_doWithDataCallbackSliderJoint;
    static void setPre_SliderJointDoWithDataCallback(Pre_SliderJointDoWithDataCallback node) {
        Pre_doWithDataCallbackSliderJoint = node;
    }

    static Pre_SliderJointTreeDoWithDataCallback Pre_treeDoWithDataCallbackSliderJoint;
    static void setPre_SliderJointTreeDoWithDataCallback(Pre_SliderJointTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackSliderJoint = node;

    }

    static Pre_SliderJointEventsProcessedCallback Pre_eventsProcessedCallbackSliderJoint;
    static void setPre_SliderJointEventsProcessedCallback(Pre_SliderJointEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackSliderJoint = node;
    }

    Pre_SliderJoint() {
    }
    void treeRender() {
        if (Pre_SliderJoint.Pre_treeRenderCallbackSliderJoint != null) {
            Pre_SliderJoint.Pre_treeRenderCallbackSliderJoint.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (body1 != null)
            body1.treeRender();
        if (body2 != null)
            body2.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_SliderJoint.Pre_renderCallbackSliderJoint != null)
            Pre_SliderJoint.Pre_renderCallbackSliderJoint.render(this);
    }
    void treeDoWithData() {
        if (Pre_SliderJoint.Pre_treeDoWithDataCallbackSliderJoint != null) {
            Pre_SliderJoint.Pre_treeDoWithDataCallbackSliderJoint.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (body1 != null)
            body1.treeDoWithData();
        if (body2 != null)
            body2.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_SliderJoint.Pre_doWithDataCallbackSliderJoint != null)
            Pre_SliderJoint.Pre_doWithDataCallbackSliderJoint.doWithData(this);
    }
}

class Pre_SliderJointRenderCallback extends RenderCallback {}
class Pre_SliderJointTreeRenderCallback extends TreeRenderCallback {}
class Pre_SliderJointDoWithDataCallback extends DoWithDataCallback {};
class Pre_SliderJointTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_SliderJointEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Text extends Pre_Node {
    Pre_Node metadata;
    Pre_Node fontStyle;
    float[] length;
    float maxExtent;
    String[] string;
    boolean solid;
    static Pre_TextRenderCallback Pre_renderCallbackText;
    static void setPre_TextRenderCallback(Pre_TextRenderCallback node) {
        Pre_renderCallbackText = node;
    }

    static Pre_TextTreeRenderCallback Pre_treeRenderCallbackText;
    static void setPre_TextTreeRenderCallback(Pre_TextTreeRenderCallback node) {
        Pre_treeRenderCallbackText = node;
    }

    static Pre_TextDoWithDataCallback Pre_doWithDataCallbackText;
    static void setPre_TextDoWithDataCallback(Pre_TextDoWithDataCallback node) {
        Pre_doWithDataCallbackText = node;
    }

    static Pre_TextTreeDoWithDataCallback Pre_treeDoWithDataCallbackText;
    static void setPre_TextTreeDoWithDataCallback(Pre_TextTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackText = node;

    }

    static Pre_TextEventsProcessedCallback Pre_eventsProcessedCallbackText;
    static void setPre_TextEventsProcessedCallback(Pre_TextEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackText = node;
    }

    Pre_Text() {
    }
    void treeRender() {
        if (Pre_Text.Pre_treeRenderCallbackText != null) {
            Pre_Text.Pre_treeRenderCallbackText.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (fontStyle != null)
            fontStyle.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Text.Pre_renderCallbackText != null)
            Pre_Text.Pre_renderCallbackText.render(this);
    }
    void treeDoWithData() {
        if (Pre_Text.Pre_treeDoWithDataCallbackText != null) {
            Pre_Text.Pre_treeDoWithDataCallbackText.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (fontStyle != null)
            fontStyle.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Text.Pre_doWithDataCallbackText != null)
            Pre_Text.Pre_doWithDataCallbackText.doWithData(this);
    }
}

class Pre_TextRenderCallback extends RenderCallback {}
class Pre_TextTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureCoordinate4D extends Pre_Node {
    Pre_Node metadata;
    float[] point;
    static Pre_TextureCoordinate4DRenderCallback Pre_renderCallbackTextureCoordinate4D;
    static void setPre_TextureCoordinate4DRenderCallback(Pre_TextureCoordinate4DRenderCallback node) {
        Pre_renderCallbackTextureCoordinate4D = node;
    }

    static Pre_TextureCoordinate4DTreeRenderCallback Pre_treeRenderCallbackTextureCoordinate4D;
    static void setPre_TextureCoordinate4DTreeRenderCallback(Pre_TextureCoordinate4DTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureCoordinate4D = node;
    }

    static Pre_TextureCoordinate4DDoWithDataCallback Pre_doWithDataCallbackTextureCoordinate4D;
    static void setPre_TextureCoordinate4DDoWithDataCallback(Pre_TextureCoordinate4DDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureCoordinate4D = node;
    }

    static Pre_TextureCoordinate4DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureCoordinate4D;
    static void setPre_TextureCoordinate4DTreeDoWithDataCallback(Pre_TextureCoordinate4DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureCoordinate4D = node;

    }

    static Pre_TextureCoordinate4DEventsProcessedCallback Pre_eventsProcessedCallbackTextureCoordinate4D;
    static void setPre_TextureCoordinate4DEventsProcessedCallback(Pre_TextureCoordinate4DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureCoordinate4D = node;
    }

    Pre_TextureCoordinate4D() {
    }
    void treeRender() {
        if (Pre_TextureCoordinate4D.Pre_treeRenderCallbackTextureCoordinate4D != null) {
            Pre_TextureCoordinate4D.Pre_treeRenderCallbackTextureCoordinate4D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureCoordinate4D.Pre_renderCallbackTextureCoordinate4D != null)
            Pre_TextureCoordinate4D.Pre_renderCallbackTextureCoordinate4D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureCoordinate4D.Pre_treeDoWithDataCallbackTextureCoordinate4D != null) {
            Pre_TextureCoordinate4D.Pre_treeDoWithDataCallbackTextureCoordinate4D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureCoordinate4D.Pre_doWithDataCallbackTextureCoordinate4D != null)
            Pre_TextureCoordinate4D.Pre_doWithDataCallbackTextureCoordinate4D.doWithData(this);
    }
}

class Pre_TextureCoordinate4DRenderCallback extends RenderCallback {}
class Pre_TextureCoordinate4DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureCoordinate4DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureCoordinate4DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureCoordinate4DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TriangleSet2D extends Pre_Node {
    Pre_Node metadata;
    float[] vertices;
    boolean solid;
    static Pre_TriangleSet2DRenderCallback Pre_renderCallbackTriangleSet2D;
    static void setPre_TriangleSet2DRenderCallback(Pre_TriangleSet2DRenderCallback node) {
        Pre_renderCallbackTriangleSet2D = node;
    }

    static Pre_TriangleSet2DTreeRenderCallback Pre_treeRenderCallbackTriangleSet2D;
    static void setPre_TriangleSet2DTreeRenderCallback(Pre_TriangleSet2DTreeRenderCallback node) {
        Pre_treeRenderCallbackTriangleSet2D = node;
    }

    static Pre_TriangleSet2DDoWithDataCallback Pre_doWithDataCallbackTriangleSet2D;
    static void setPre_TriangleSet2DDoWithDataCallback(Pre_TriangleSet2DDoWithDataCallback node) {
        Pre_doWithDataCallbackTriangleSet2D = node;
    }

    static Pre_TriangleSet2DTreeDoWithDataCallback Pre_treeDoWithDataCallbackTriangleSet2D;
    static void setPre_TriangleSet2DTreeDoWithDataCallback(Pre_TriangleSet2DTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTriangleSet2D = node;

    }

    static Pre_TriangleSet2DEventsProcessedCallback Pre_eventsProcessedCallbackTriangleSet2D;
    static void setPre_TriangleSet2DEventsProcessedCallback(Pre_TriangleSet2DEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTriangleSet2D = node;
    }

    Pre_TriangleSet2D() {
    }
    void treeRender() {
        if (Pre_TriangleSet2D.Pre_treeRenderCallbackTriangleSet2D != null) {
            Pre_TriangleSet2D.Pre_treeRenderCallbackTriangleSet2D.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TriangleSet2D.Pre_renderCallbackTriangleSet2D != null)
            Pre_TriangleSet2D.Pre_renderCallbackTriangleSet2D.render(this);
    }
    void treeDoWithData() {
        if (Pre_TriangleSet2D.Pre_treeDoWithDataCallbackTriangleSet2D != null) {
            Pre_TriangleSet2D.Pre_treeDoWithDataCallbackTriangleSet2D.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TriangleSet2D.Pre_doWithDataCallbackTriangleSet2D != null)
            Pre_TriangleSet2D.Pre_doWithDataCallbackTriangleSet2D.doWithData(this);
    }
}

class Pre_TriangleSet2DRenderCallback extends RenderCallback {}
class Pre_TriangleSet2DTreeRenderCallback extends TreeRenderCallback {}
class Pre_TriangleSet2DDoWithDataCallback extends DoWithDataCallback {};
class Pre_TriangleSet2DTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TriangleSet2DEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_Appearance extends Pre_Node {
    Pre_Node metadata;
    Pre_Node material;
    Pre_Node texture;
    Pre_Node textureTransform;
    Pre_Node fillProperties;
    Pre_Node lineProperties;
    Pre_Node [] shaders;
    Pre_Node [] receiveShadows;
    boolean shadowCaster;
    static Pre_AppearanceRenderCallback Pre_renderCallbackAppearance;
    static void setPre_AppearanceRenderCallback(Pre_AppearanceRenderCallback node) {
        Pre_renderCallbackAppearance = node;
    }

    static Pre_AppearanceTreeRenderCallback Pre_treeRenderCallbackAppearance;
    static void setPre_AppearanceTreeRenderCallback(Pre_AppearanceTreeRenderCallback node) {
        Pre_treeRenderCallbackAppearance = node;
    }

    static Pre_AppearanceDoWithDataCallback Pre_doWithDataCallbackAppearance;
    static void setPre_AppearanceDoWithDataCallback(Pre_AppearanceDoWithDataCallback node) {
        Pre_doWithDataCallbackAppearance = node;
    }

    static Pre_AppearanceTreeDoWithDataCallback Pre_treeDoWithDataCallbackAppearance;
    static void setPre_AppearanceTreeDoWithDataCallback(Pre_AppearanceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackAppearance = node;

    }

    static Pre_AppearanceEventsProcessedCallback Pre_eventsProcessedCallbackAppearance;
    static void setPre_AppearanceEventsProcessedCallback(Pre_AppearanceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackAppearance = node;
    }

    Pre_Appearance() {
    }
    void treeRender() {
        if (Pre_Appearance.Pre_treeRenderCallbackAppearance != null) {
            Pre_Appearance.Pre_treeRenderCallbackAppearance.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (material != null)
            material.treeRender();
        if (texture != null)
            texture.treeRender();
        if (textureTransform != null)
            textureTransform.treeRender();
        if (fillProperties != null)
            fillProperties.treeRender();
        if (lineProperties != null)
            lineProperties.treeRender();
        if (shaders != null)
            for (int i = 0; i < shaders.length; i++)
                if (shaders[i] != null)
                    shaders[i].treeRender();
        if (receiveShadows != null)
            for (int i = 0; i < receiveShadows.length; i++)
                if (receiveShadows[i] != null)
                    receiveShadows[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_Appearance.Pre_renderCallbackAppearance != null)
            Pre_Appearance.Pre_renderCallbackAppearance.render(this);
    }
    void treeDoWithData() {
        if (Pre_Appearance.Pre_treeDoWithDataCallbackAppearance != null) {
            Pre_Appearance.Pre_treeDoWithDataCallbackAppearance.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (material != null)
            material.treeDoWithData();
        if (texture != null)
            texture.treeDoWithData();
        if (textureTransform != null)
            textureTransform.treeDoWithData();
        if (fillProperties != null)
            fillProperties.treeDoWithData();
        if (lineProperties != null)
            lineProperties.treeDoWithData();
        if (shaders != null)
            for (int i = 0; i < shaders.length; i++)
                if (shaders[i] != null)
                    shaders[i].treeDoWithData();
        if (receiveShadows != null)
            for (int i = 0; i < receiveShadows.length; i++)
                if (receiveShadows[i] != null)
                    receiveShadows[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_Appearance.Pre_doWithDataCallbackAppearance != null)
            Pre_Appearance.Pre_doWithDataCallbackAppearance.doWithData(this);
    }
}

class Pre_AppearanceRenderCallback extends RenderCallback {}
class Pre_AppearanceTreeRenderCallback extends TreeRenderCallback {}
class Pre_AppearanceDoWithDataCallback extends DoWithDataCallback {};
class Pre_AppearanceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_AppearanceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_CollisionSpace extends Pre_Node {
    Pre_Node metadata;
    boolean enabled;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node [] collidables;
    boolean useGeometry;
    static Pre_CollisionSpaceRenderCallback Pre_renderCallbackCollisionSpace;
    static void setPre_CollisionSpaceRenderCallback(Pre_CollisionSpaceRenderCallback node) {
        Pre_renderCallbackCollisionSpace = node;
    }

    static Pre_CollisionSpaceTreeRenderCallback Pre_treeRenderCallbackCollisionSpace;
    static void setPre_CollisionSpaceTreeRenderCallback(Pre_CollisionSpaceTreeRenderCallback node) {
        Pre_treeRenderCallbackCollisionSpace = node;
    }

    static Pre_CollisionSpaceDoWithDataCallback Pre_doWithDataCallbackCollisionSpace;
    static void setPre_CollisionSpaceDoWithDataCallback(Pre_CollisionSpaceDoWithDataCallback node) {
        Pre_doWithDataCallbackCollisionSpace = node;
    }

    static Pre_CollisionSpaceTreeDoWithDataCallback Pre_treeDoWithDataCallbackCollisionSpace;
    static void setPre_CollisionSpaceTreeDoWithDataCallback(Pre_CollisionSpaceTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackCollisionSpace = node;

    }

    static Pre_CollisionSpaceEventsProcessedCallback Pre_eventsProcessedCallbackCollisionSpace;
    static void setPre_CollisionSpaceEventsProcessedCallback(Pre_CollisionSpaceEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackCollisionSpace = node;
    }

    Pre_CollisionSpace() {
    }
    void treeRender() {
        if (Pre_CollisionSpace.Pre_treeRenderCallbackCollisionSpace != null) {
            Pre_CollisionSpace.Pre_treeRenderCallbackCollisionSpace.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (collidables != null)
            for (int i = 0; i < collidables.length; i++)
                if (collidables[i] != null)
                    collidables[i].treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_CollisionSpace.Pre_renderCallbackCollisionSpace != null)
            Pre_CollisionSpace.Pre_renderCallbackCollisionSpace.render(this);
    }
    void treeDoWithData() {
        if (Pre_CollisionSpace.Pre_treeDoWithDataCallbackCollisionSpace != null) {
            Pre_CollisionSpace.Pre_treeDoWithDataCallbackCollisionSpace.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (collidables != null)
            for (int i = 0; i < collidables.length; i++)
                if (collidables[i] != null)
                    collidables[i].treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_CollisionSpace.Pre_doWithDataCallbackCollisionSpace != null)
            Pre_CollisionSpace.Pre_doWithDataCallbackCollisionSpace.doWithData(this);
    }
}

class Pre_CollisionSpaceRenderCallback extends RenderCallback {}
class Pre_CollisionSpaceTreeRenderCallback extends TreeRenderCallback {}
class Pre_CollisionSpaceDoWithDataCallback extends DoWithDataCallback {};
class Pre_CollisionSpaceTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_CollisionSpaceEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_NurbsTextureCoordinate extends Pre_Node {
    Pre_Node metadata;
    int uDimension;
    int vDimension;
    float[] uKnot;
    float[] vKnot;
    int uOrder;
    int vOrder;
    float[] controlPoint;
    float[] weight;
    static Pre_NurbsTextureCoordinateRenderCallback Pre_renderCallbackNurbsTextureCoordinate;
    static void setPre_NurbsTextureCoordinateRenderCallback(Pre_NurbsTextureCoordinateRenderCallback node) {
        Pre_renderCallbackNurbsTextureCoordinate = node;
    }

    static Pre_NurbsTextureCoordinateTreeRenderCallback Pre_treeRenderCallbackNurbsTextureCoordinate;
    static void setPre_NurbsTextureCoordinateTreeRenderCallback(Pre_NurbsTextureCoordinateTreeRenderCallback node) {
        Pre_treeRenderCallbackNurbsTextureCoordinate = node;
    }

    static Pre_NurbsTextureCoordinateDoWithDataCallback Pre_doWithDataCallbackNurbsTextureCoordinate;
    static void setPre_NurbsTextureCoordinateDoWithDataCallback(Pre_NurbsTextureCoordinateDoWithDataCallback node) {
        Pre_doWithDataCallbackNurbsTextureCoordinate = node;
    }

    static Pre_NurbsTextureCoordinateTreeDoWithDataCallback Pre_treeDoWithDataCallbackNurbsTextureCoordinate;
    static void setPre_NurbsTextureCoordinateTreeDoWithDataCallback(Pre_NurbsTextureCoordinateTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackNurbsTextureCoordinate = node;

    }

    static Pre_NurbsTextureCoordinateEventsProcessedCallback Pre_eventsProcessedCallbackNurbsTextureCoordinate;
    static void setPre_NurbsTextureCoordinateEventsProcessedCallback(Pre_NurbsTextureCoordinateEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackNurbsTextureCoordinate = node;
    }

    Pre_NurbsTextureCoordinate() {
    }
    void treeRender() {
        if (Pre_NurbsTextureCoordinate.Pre_treeRenderCallbackNurbsTextureCoordinate != null) {
            Pre_NurbsTextureCoordinate.Pre_treeRenderCallbackNurbsTextureCoordinate.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_NurbsTextureCoordinate.Pre_renderCallbackNurbsTextureCoordinate != null)
            Pre_NurbsTextureCoordinate.Pre_renderCallbackNurbsTextureCoordinate.render(this);
    }
    void treeDoWithData() {
        if (Pre_NurbsTextureCoordinate.Pre_treeDoWithDataCallbackNurbsTextureCoordinate != null) {
            Pre_NurbsTextureCoordinate.Pre_treeDoWithDataCallbackNurbsTextureCoordinate.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_NurbsTextureCoordinate.Pre_doWithDataCallbackNurbsTextureCoordinate != null)
            Pre_NurbsTextureCoordinate.Pre_doWithDataCallbackNurbsTextureCoordinate.doWithData(this);
    }
}

class Pre_NurbsTextureCoordinateRenderCallback extends RenderCallback {}
class Pre_NurbsTextureCoordinateTreeRenderCallback extends TreeRenderCallback {}
class Pre_NurbsTextureCoordinateDoWithDataCallback extends DoWithDataCallback {};
class Pre_NurbsTextureCoordinateTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_NurbsTextureCoordinateEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_ParticleSystem extends Pre_Node {
    Pre_Node metadata;
    Pre_Node appearance;
    boolean createParticles;
    Pre_Node geometry;
    boolean enabled;
    float lifetimeVariation;
    int maxParticles;
    float particleLifetime;
    float[] particleSize;
    float[] bboxCenter;
    float[] bboxSize;
    Pre_Node colorRamp;
    float[] colorKey;
    Pre_Node emitter;
    String geometryType;
    Pre_Node [] physics;
    Pre_Node texCoordRamp;
    float[] texCoordKey;
    static Pre_ParticleSystemRenderCallback Pre_renderCallbackParticleSystem;
    static void setPre_ParticleSystemRenderCallback(Pre_ParticleSystemRenderCallback node) {
        Pre_renderCallbackParticleSystem = node;
    }

    static Pre_ParticleSystemTreeRenderCallback Pre_treeRenderCallbackParticleSystem;
    static void setPre_ParticleSystemTreeRenderCallback(Pre_ParticleSystemTreeRenderCallback node) {
        Pre_treeRenderCallbackParticleSystem = node;
    }

    static Pre_ParticleSystemDoWithDataCallback Pre_doWithDataCallbackParticleSystem;
    static void setPre_ParticleSystemDoWithDataCallback(Pre_ParticleSystemDoWithDataCallback node) {
        Pre_doWithDataCallbackParticleSystem = node;
    }

    static Pre_ParticleSystemTreeDoWithDataCallback Pre_treeDoWithDataCallbackParticleSystem;
    static void setPre_ParticleSystemTreeDoWithDataCallback(Pre_ParticleSystemTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackParticleSystem = node;

    }

    static Pre_ParticleSystemEventsProcessedCallback Pre_eventsProcessedCallbackParticleSystem;
    static void setPre_ParticleSystemEventsProcessedCallback(Pre_ParticleSystemEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackParticleSystem = node;
    }

    Pre_ParticleSystem() {
    }
    void treeRender() {
        if (Pre_ParticleSystem.Pre_treeRenderCallbackParticleSystem != null) {
            Pre_ParticleSystem.Pre_treeRenderCallbackParticleSystem.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (appearance != null)
            appearance.treeRender();
        if (geometry != null)
            geometry.treeRender();
        if (colorRamp != null)
            colorRamp.treeRender();
        if (emitter != null)
            emitter.treeRender();
        if (physics != null)
            for (int i = 0; i < physics.length; i++)
                if (physics[i] != null)
                    physics[i].treeRender();
        if (texCoordRamp != null)
            texCoordRamp.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_ParticleSystem.Pre_renderCallbackParticleSystem != null)
            Pre_ParticleSystem.Pre_renderCallbackParticleSystem.render(this);
    }
    void treeDoWithData() {
        if (Pre_ParticleSystem.Pre_treeDoWithDataCallbackParticleSystem != null) {
            Pre_ParticleSystem.Pre_treeDoWithDataCallbackParticleSystem.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (appearance != null)
            appearance.treeDoWithData();
        if (geometry != null)
            geometry.treeDoWithData();
        if (colorRamp != null)
            colorRamp.treeDoWithData();
        if (emitter != null)
            emitter.treeDoWithData();
        if (physics != null)
            for (int i = 0; i < physics.length; i++)
                if (physics[i] != null)
                    physics[i].treeDoWithData();
        if (texCoordRamp != null)
            texCoordRamp.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_ParticleSystem.Pre_doWithDataCallbackParticleSystem != null)
            Pre_ParticleSystem.Pre_doWithDataCallbackParticleSystem.doWithData(this);
    }
}

class Pre_ParticleSystemRenderCallback extends RenderCallback {}
class Pre_ParticleSystemTreeRenderCallback extends TreeRenderCallback {}
class Pre_ParticleSystemDoWithDataCallback extends DoWithDataCallback {};
class Pre_ParticleSystemTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_ParticleSystemEventsProcessedCallback extends EventsProcessedCallback {};


class Pre_TextureTransform extends Pre_Node {
    Pre_Node metadata;
    float[] center;
    float rotation;
    float[] scale;
    float[] translation;
    static Pre_TextureTransformRenderCallback Pre_renderCallbackTextureTransform;
    static void setPre_TextureTransformRenderCallback(Pre_TextureTransformRenderCallback node) {
        Pre_renderCallbackTextureTransform = node;
    }

    static Pre_TextureTransformTreeRenderCallback Pre_treeRenderCallbackTextureTransform;
    static void setPre_TextureTransformTreeRenderCallback(Pre_TextureTransformTreeRenderCallback node) {
        Pre_treeRenderCallbackTextureTransform = node;
    }

    static Pre_TextureTransformDoWithDataCallback Pre_doWithDataCallbackTextureTransform;
    static void setPre_TextureTransformDoWithDataCallback(Pre_TextureTransformDoWithDataCallback node) {
        Pre_doWithDataCallbackTextureTransform = node;
    }

    static Pre_TextureTransformTreeDoWithDataCallback Pre_treeDoWithDataCallbackTextureTransform;
    static void setPre_TextureTransformTreeDoWithDataCallback(Pre_TextureTransformTreeDoWithDataCallback node) {
        Pre_treeDoWithDataCallbackTextureTransform = node;

    }

    static Pre_TextureTransformEventsProcessedCallback Pre_eventsProcessedCallbackTextureTransform;
    static void setPre_TextureTransformEventsProcessedCallback(Pre_TextureTransformEventsProcessedCallback node) {
        Pre_eventsProcessedCallbackTextureTransform = node;
    }

    Pre_TextureTransform() {
    }
    void treeRender() {
        if (Pre_TextureTransform.Pre_treeRenderCallbackTextureTransform != null) {
            Pre_TextureTransform.Pre_treeRenderCallbackTextureTransform.treeRender(this);
            return;
        }
        if (metadata != null)
            metadata.treeRender();
        if (m_protoRoot != null)
            m_protoRoot.treeRender();
        if (Pre_TextureTransform.Pre_renderCallbackTextureTransform != null)
            Pre_TextureTransform.Pre_renderCallbackTextureTransform.render(this);
    }
    void treeDoWithData() {
        if (Pre_TextureTransform.Pre_treeDoWithDataCallbackTextureTransform != null) {
            Pre_TextureTransform.Pre_treeDoWithDataCallbackTextureTransform.treeDoWithData(this);
            return;
        }
        if (metadata != null)
            metadata.treeDoWithData();
        if (m_protoRoot != null)
            m_protoRoot.treeDoWithData();
        if (Pre_TextureTransform.Pre_doWithDataCallbackTextureTransform != null)
            Pre_TextureTransform.Pre_doWithDataCallbackTextureTransform.doWithData(this);
    }
}

class Pre_TextureTransformRenderCallback extends RenderCallback {}
class Pre_TextureTransformTreeRenderCallback extends TreeRenderCallback {}
class Pre_TextureTransformDoWithDataCallback extends DoWithDataCallback {};
class Pre_TextureTransformTreeDoWithDataCallback extends TreeDoWithDataCallback {};
class Pre_TextureTransformEventsProcessedCallback extends EventsProcessedCallback {};



class Pre_SceneGraph {
    Pre_Group root;
    Pre_Transform Transform_1;
    Pre_Shape Shape_2;
    Pre_Appearance Appearance_3;
    Pre_Material Material_4;
    Pre_Text Text_5;
    Pre_Anchor Anchor_6;
    Pre_Shape Shape_7;
    Pre_Appearance Appearance_8;
    Pre_Material Material_9;
    Pre_Text Text_10;
    Pre_Shape Shape_11;
    Pre_Appearance Appearance_12;
    Pre_Material Material_13;
    Pre_Text Text_14;
    void rootDataFunction0() {
        Transform_1 = new Pre_Transform();
        Transform_1.m_parent = root;
        Anchor_6 = new Pre_Anchor();
        Anchor_6.m_parent = root;
    }
    void Transform_1DataFunction0() {
        Shape_2 = new Pre_Shape();
        Shape_2.m_parent = Transform_1;
    }
    void Shape_2DataFunction0() {
        Appearance_3 = new Pre_Appearance();
        Appearance_3.m_parent = Shape_2;
    }
    void Appearance_3DataFunction0() {
        Material_4 = new Pre_Material();
        Material_4.m_parent = Appearance_3;
    }
    void Material_4DataFunction0() {
        Material_4.ambientIntensity = 0.200000f;
    }
    void Material_4DataFunction1() {
        {
        float m_diffuseColor[] = { 0.800000f, 0.800000f, 0.800000f };
        Material_4.diffuseColor = m_diffuseColor;
        }
    }
    void Material_4DataFunction2() {
        {
        float m_emissiveColor[] = { 0f, 0f, 0f };
        Material_4.emissiveColor = m_emissiveColor;
        }
    }
    void Material_4DataFunction3() {
        Material_4.shininess = 0.200000f;
    }
    void Material_4DataFunction4() {
        {
        float m_specularColor[] = { 0f, 0f, 0f };
        Material_4.specularColor = m_specularColor;
        }
    }
    void Material_4DataFunction5() {
        Material_4.transparency = 0f;
    }
    void Appearance_3DataFunction1() {
    }
    void Appearance_3DataFunction2() {
    }
    void Appearance_3DataFunction3() {
    }
    void Appearance_3DataFunction4() {
    }
    void Appearance_3DataFunction5() {
    }
    void Appearance_3DataFunction6() {
        Appearance_3.material = Material_4;
    }
    void Appearance_3DataFunction7() {
        Appearance_3.texture = null;
    }
    void Appearance_3DataFunction8() {
        Appearance_3.textureTransform = null;
    }
    void Appearance_3DataFunction9() {
        Appearance_3.fillProperties = null;
    }
    void Appearance_3DataFunction10() {
        Appearance_3.lineProperties = null;
    }
    void Appearance_3DataFunction11() {
        {
        Pre_Node [] m_shaders = {  };
        Appearance_3.shaders = m_shaders;
        }
    }
    void Shape_2DataFunction1() {
        Text_5 = new Pre_Text();
        Text_5.m_parent = Shape_2;
    }
    void Text_5DataFunction0() {
    }
    void Text_5DataFunction1() {
        Text_5.fontStyle = null;
    }
    void Text_5DataFunction2() {
        Text_5.length = null;
        Pre_Text v = Text_5;
        int i = 0;
    }
    void Text_5DataFunction3() {
        Text_5.maxExtent = 0f;
    }
    void Text_5DataFunction4() {
        Text_5.string = new String[1];
        Pre_Text v = Text_5;
        int i = 0;
        v.string[i++] = "Hello World";
    }
    void Shape_2DataFunction2() {
        Shape_2.appearance = Appearance_3;
    }
    void Shape_2DataFunction3() {
        Shape_2.geometry = Text_5;
    }
    void Shape_2DataFunction4() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Shape_2.bboxCenter = m_bboxCenter;
        }
    }
    void Shape_2DataFunction5() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Shape_2.bboxSize = m_bboxSize;
        }
    }
    void Transform_1DataFunction1() {
        {
        float m_rotation[] = { 0f, 0f, 1f, 0f };
        Transform_1.rotation = m_rotation;
        }
    }
    void Transform_1DataFunction2() {
        {
        float m_translation[] = { 0f, 0f, 0f };
        Transform_1.translation = m_translation;
        }
    }
    void Transform_1DataFunction3() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Transform_1.bboxCenter = m_bboxCenter;
        }
    }
    void Transform_1DataFunction4() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Transform_1.bboxSize = m_bboxSize;
        }
    }
    void Transform_1DataFunction5() {
        {
        float m_center[] = { 0f, 0f, 0f };
        Transform_1.center = m_center;
        }
    }
    void Transform_1DataFunction6() {
        {
        float m_scale[] = { 1f, 1f, 1f };
        Transform_1.scale = m_scale;
        }
    }
    void Transform_1DataFunction7() {
        {
        float m_scaleOrientation[] = { 0f, 0f, 1f, 0f };
        Transform_1.scaleOrientation = m_scaleOrientation;
        }
    }
    void Transform_1DataFunction8() {
        {
        Pre_Node [] m_children = { Shape_2 };
        Transform_1.children = m_children;
        }
    }
    void Anchor_6DataFunction0() {
        Shape_7 = new Pre_Shape();
        Shape_7.m_parent = Anchor_6;
        Shape_11 = new Pre_Shape();
        Shape_11.m_parent = Anchor_6;
    }
    void Shape_7DataFunction0() {
        Appearance_8 = new Pre_Appearance();
        Appearance_8.m_parent = Shape_7;
    }
    void Appearance_8DataFunction0() {
        Material_9 = new Pre_Material();
        Material_9.m_parent = Appearance_8;
    }
    void Material_9DataFunction0() {
        Material_9.ambientIntensity = 0.200000f;
    }
    void Material_9DataFunction1() {
        {
        float m_diffuseColor[] = { 0.800000f, 0.800000f, 0.800000f };
        Material_9.diffuseColor = m_diffuseColor;
        }
    }
    void Material_9DataFunction2() {
        {
        float m_emissiveColor[] = { 0f, 0f, 0f };
        Material_9.emissiveColor = m_emissiveColor;
        }
    }
    void Material_9DataFunction3() {
        Material_9.shininess = 0.200000f;
    }
    void Material_9DataFunction4() {
        {
        float m_specularColor[] = { 0f, 0f, 0f };
        Material_9.specularColor = m_specularColor;
        }
    }
    void Material_9DataFunction5() {
        Material_9.transparency = 0f;
    }
    void Appearance_8DataFunction1() {
    }
    void Appearance_8DataFunction2() {
    }
    void Appearance_8DataFunction3() {
    }
    void Appearance_8DataFunction4() {
    }
    void Appearance_8DataFunction5() {
    }
    void Appearance_8DataFunction6() {
        Appearance_8.material = Material_9;
    }
    void Appearance_8DataFunction7() {
        Appearance_8.texture = null;
    }
    void Appearance_8DataFunction8() {
        Appearance_8.textureTransform = null;
    }
    void Appearance_8DataFunction9() {
        Appearance_8.fillProperties = null;
    }
    void Appearance_8DataFunction10() {
        Appearance_8.lineProperties = null;
    }
    void Appearance_8DataFunction11() {
        {
        Pre_Node [] m_shaders = {  };
        Appearance_8.shaders = m_shaders;
        }
    }
    void Shape_7DataFunction1() {
        Text_10 = new Pre_Text();
        Text_10.m_parent = Shape_7;
    }
    void Text_10DataFunction0() {
    }
    void Text_10DataFunction1() {
        Text_10.fontStyle = null;
    }
    void Text_10DataFunction2() {
        Text_10.length = null;
        Pre_Text v = Text_10;
        int i = 0;
    }
    void Text_10DataFunction3() {
        Text_10.maxExtent = 0f;
    }
    void Text_10DataFunction4() {
        Text_10.string = new String[1];
        Pre_Text v = Text_10;
        int i = 0;
        v.string[i++] = "Hello World";
    }
    void Shape_7DataFunction2() {
        Shape_7.appearance = Appearance_8;
    }
    void Shape_7DataFunction3() {
        Shape_7.geometry = Text_10;
    }
    void Shape_7DataFunction4() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Shape_7.bboxCenter = m_bboxCenter;
        }
    }
    void Shape_7DataFunction5() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Shape_7.bboxSize = m_bboxSize;
        }
    }
    void Shape_11DataFunction0() {
        Appearance_12 = new Pre_Appearance();
        Appearance_12.m_parent = Shape_11;
    }
    void Appearance_12DataFunction0() {
        Material_13 = new Pre_Material();
        Material_13.m_parent = Appearance_12;
    }
    void Material_13DataFunction0() {
        Material_13.ambientIntensity = 0.200000f;
    }
    void Material_13DataFunction1() {
        {
        float m_diffuseColor[] = { 0.800000f, 0.800000f, 0.800000f };
        Material_13.diffuseColor = m_diffuseColor;
        }
    }
    void Material_13DataFunction2() {
        {
        float m_emissiveColor[] = { 0f, 0f, 0f };
        Material_13.emissiveColor = m_emissiveColor;
        }
    }
    void Material_13DataFunction3() {
        Material_13.shininess = 0.200000f;
    }
    void Material_13DataFunction4() {
        {
        float m_specularColor[] = { 0f, 0f, 0f };
        Material_13.specularColor = m_specularColor;
        }
    }
    void Material_13DataFunction5() {
        Material_13.transparency = 0f;
    }
    void Appearance_12DataFunction1() {
    }
    void Appearance_12DataFunction2() {
    }
    void Appearance_12DataFunction3() {
    }
    void Appearance_12DataFunction4() {
    }
    void Appearance_12DataFunction5() {
    }
    void Appearance_12DataFunction6() {
        Appearance_12.material = Material_13;
    }
    void Appearance_12DataFunction7() {
        Appearance_12.texture = null;
    }
    void Appearance_12DataFunction8() {
        Appearance_12.textureTransform = null;
    }
    void Appearance_12DataFunction9() {
        Appearance_12.fillProperties = null;
    }
    void Appearance_12DataFunction10() {
        Appearance_12.lineProperties = null;
    }
    void Appearance_12DataFunction11() {
        {
        Pre_Node [] m_shaders = {  };
        Appearance_12.shaders = m_shaders;
        }
    }
    void Shape_11DataFunction1() {
        Text_14 = new Pre_Text();
        Text_14.m_parent = Shape_11;
    }
    void Text_14DataFunction0() {
    }
    void Text_14DataFunction1() {
        Text_14.fontStyle = null;
    }
    void Text_14DataFunction2() {
        Text_14.length = null;
        Pre_Text v = Text_14;
        int i = 0;
    }
    void Text_14DataFunction3() {
        Text_14.maxExtent = 0f;
    }
    void Text_14DataFunction4() {
        Text_14.string = new String[1];
        Pre_Text v = Text_14;
        int i = 0;
        v.string[i++] = "Hello World";
    }
    void Shape_11DataFunction2() {
        Shape_11.appearance = Appearance_12;
    }
    void Shape_11DataFunction3() {
        Shape_11.geometry = Text_14;
    }
    void Shape_11DataFunction4() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Shape_11.bboxCenter = m_bboxCenter;
        }
    }
    void Shape_11DataFunction5() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Shape_11.bboxSize = m_bboxSize;
        }
    }
    void Anchor_6DataFunction1() {
        {
        Pre_Node [] m_children = { Shape_7, Shape_11 };
        Anchor_6.children = m_children;
        }
    }
    void Anchor_6DataFunction2() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Anchor_6.bboxCenter = m_bboxCenter;
        }
    }
    void Anchor_6DataFunction3() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Anchor_6.bboxSize = m_bboxSize;
        }
    }
    void Anchor_6DataFunction4() {
        Anchor_6.description = "";
    }
    void Anchor_6DataFunction5() {
        Anchor_6.parameter = null;
        Pre_Anchor v = Anchor_6;
        int i = 0;
    }
    void Anchor_6DataFunction6() {
        Anchor_6.url = null;
        Pre_Anchor v = Anchor_6;
        int i = 0;
    }
    void rootDataFunction1() {
        {
        Pre_Node [] m_children = { Transform_1, Anchor_6 };
        root.children = m_children;
        }
    }
    void rootDataFunction2() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        root.bboxCenter = m_bboxCenter;
        }
    }
    void rootDataFunction3() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        root.bboxSize = m_bboxSize;
        }
    }
    void dataPre_Function0() {
        rootDataFunction0();
        rootDataFunction1();
        rootDataFunction2();
        rootDataFunction3();
    }
    void dataPre_Function1() {
        Transform_1DataFunction0();
        Transform_1DataFunction1();
        Transform_1DataFunction2();
        Transform_1DataFunction3();
        Transform_1DataFunction4();
        Transform_1DataFunction5();
        Transform_1DataFunction6();
        Transform_1DataFunction7();
        Transform_1DataFunction8();
    }
    void dataPre_Function2() {
        Shape_2DataFunction0();
        Shape_2DataFunction1();
        Shape_2DataFunction2();
        Shape_2DataFunction3();
        Shape_2DataFunction4();
        Shape_2DataFunction5();
    }
    void dataPre_Function3() {
        Appearance_3DataFunction0();
        Appearance_3DataFunction1();
        Appearance_3DataFunction2();
        Appearance_3DataFunction3();
        Appearance_3DataFunction4();
        Appearance_3DataFunction5();
        Appearance_3DataFunction6();
        Appearance_3DataFunction7();
        Appearance_3DataFunction8();
        Appearance_3DataFunction9();
        Appearance_3DataFunction10();
        Appearance_3DataFunction11();
    }
    void dataPre_Function4() {
        Material_4DataFunction0();
        Material_4DataFunction1();
        Material_4DataFunction2();
        Material_4DataFunction3();
        Material_4DataFunction4();
        Material_4DataFunction5();
    }
    void dataPre_Function5() {
        Text_5DataFunction0();
        Text_5DataFunction1();
        Text_5DataFunction2();
        Text_5DataFunction3();
        Text_5DataFunction4();
    }
    void dataPre_Function6() {
        Anchor_6DataFunction0();
        Anchor_6DataFunction1();
        Anchor_6DataFunction2();
        Anchor_6DataFunction3();
        Anchor_6DataFunction4();
        Anchor_6DataFunction5();
        Anchor_6DataFunction6();
    }
    void dataPre_Function7() {
        Shape_7DataFunction0();
        Shape_7DataFunction1();
        Shape_7DataFunction2();
        Shape_7DataFunction3();
        Shape_7DataFunction4();
        Shape_7DataFunction5();
    }
    void dataPre_Function8() {
        Appearance_8DataFunction0();
        Appearance_8DataFunction1();
        Appearance_8DataFunction2();
        Appearance_8DataFunction3();
        Appearance_8DataFunction4();
        Appearance_8DataFunction5();
        Appearance_8DataFunction6();
        Appearance_8DataFunction7();
        Appearance_8DataFunction8();
        Appearance_8DataFunction9();
        Appearance_8DataFunction10();
        Appearance_8DataFunction11();
    }
    void dataPre_Function9() {
        Material_9DataFunction0();
        Material_9DataFunction1();
        Material_9DataFunction2();
        Material_9DataFunction3();
        Material_9DataFunction4();
        Material_9DataFunction5();
    }
    void dataPre_Function10() {
        Text_10DataFunction0();
        Text_10DataFunction1();
        Text_10DataFunction2();
        Text_10DataFunction3();
        Text_10DataFunction4();
    }
    void dataPre_Function11() {
        Shape_11DataFunction0();
        Shape_11DataFunction1();
        Shape_11DataFunction2();
        Shape_11DataFunction3();
        Shape_11DataFunction4();
        Shape_11DataFunction5();
    }
    void dataPre_Function12() {
        Appearance_12DataFunction0();
        Appearance_12DataFunction1();
        Appearance_12DataFunction2();
        Appearance_12DataFunction3();
        Appearance_12DataFunction4();
        Appearance_12DataFunction5();
        Appearance_12DataFunction6();
        Appearance_12DataFunction7();
        Appearance_12DataFunction8();
        Appearance_12DataFunction9();
        Appearance_12DataFunction10();
        Appearance_12DataFunction11();
    }
    void dataPre_Function13() {
        Material_13DataFunction0();
        Material_13DataFunction1();
        Material_13DataFunction2();
        Material_13DataFunction3();
        Material_13DataFunction4();
        Material_13DataFunction5();
    }
    void dataPre_Function14() {
        Text_14DataFunction0();
        Text_14DataFunction1();
        Text_14DataFunction2();
        Text_14DataFunction3();
        Text_14DataFunction4();
    }
    void dataPre_Function_0() {
        dataPre_Function0();
        dataPre_Function1();
        dataPre_Function2();
        dataPre_Function3();
        dataPre_Function4();
        dataPre_Function5();
        dataPre_Function6();
        dataPre_Function7();
        dataPre_Function8();
        dataPre_Function9();
        dataPre_Function10();
        dataPre_Function11();
        dataPre_Function12();
        dataPre_Function13();
        dataPre_Function14();
    }
    Pre_SceneGraph() {
        root = new Pre_Group();
        root.m_parent = null;
        dataPre_Function_0();
    }
    void render() { root.treeRender(); }
    void doWithData() { root.treeDoWithData(); }
    void MFBoolSendEvent(boolean[] target, boolean[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFColorSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFColorRGBASendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFDoubleSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFFloatSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFInt32SendEvent(int[] target, int[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFMatrix3fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFMatrix4fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFNodeSendEvent(Pre_Node [][] target, Pre_Node [][] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFRotationSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFStringSendEvent(String[] target, String[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFTimeSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec2fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec3dSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec3fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec4fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void SFColorSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
    void SFColorRGBASendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 4; i++)
            target[i] = source[i];
    }
    void SFImageSendEvent(int[] target, int[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void SFMatrix3fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 9; i++)
            target[i] = source[i];
    }
    void SFMatrix4fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 9; i++)
            target[i] = source[i];
    }
    void SFRotationSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 4; i++)
            target[i] = source[i];
    }
    void SFVec2fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 2; i++)
            target[i] = source[i];
    }
    void SFVec3dSendEvent(double[] target, double[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
    void SFVec3fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
};

// VRML97/X3D file "helloWorld2.x3dv" converted to java with white_dune


class Append_Pre_SceneGraph {
    Pre_Group root;
    Pre_Transform Transform_1;
    Pre_Transform Transform_2;
    Pre_Shape Shape_3;
    Pre_Appearance Appearance_4;
    Pre_Material Material_5;
    Pre_Text Text_6;
    Pre_Test Test_7;
    void rootDataFunction0() {
        Transform_1 = new Pre_Transform();
        Transform_1.m_parent = root;
    }
    void Transform_1DataFunction0() {
        Test_7 = new Pre_Test();
        Test_7.m_parent = Transform_1;
            Transform_2 = new Pre_Transform();
        Transform_2.m_parent = null;
    Test_7.m_protoRoot = Transform_2;
    }
    void Test_7DataFunction0() {
        {
        float m_diffuseColor[] = { 0f, 0f, 0f };
        Test_7.diffuseColor = m_diffuseColor;
        }
    }
    void Test_7DataFunction1() {
        Test_7.string = new String[1];
        Pre_Test v = Test_7;
        int i = 0;
        v.string[i++] = "Hello PROTO";
    }
    void Transform_2DataFunction0() {
        Shape_3 = new Pre_Shape();
        Shape_3.m_parent = Transform_2;
    }
    void Shape_3DataFunction0() {
        Appearance_4 = new Pre_Appearance();
        Appearance_4.m_parent = Shape_3;
    }
    void Appearance_4DataFunction0() {
        Material_5 = new Pre_Material();
        Material_5.m_parent = Appearance_4;
    }
    void Material_5DataFunction0() {
        Material_5.ambientIntensity = 0.200000f;
    }
    void Material_5DataFunction1() {
        {
        float m_diffuseColor[] = { 0.800000f, 0.800000f, 0.800000f };
        Material_5.diffuseColor = m_diffuseColor;
        }
    }
    void Material_5DataFunction2() {
        {
        float m_emissiveColor[] = { 0f, 0f, 0f };
        Material_5.emissiveColor = m_emissiveColor;
        }
    }
    void Material_5DataFunction3() {
        Material_5.shininess = 0.200000f;
    }
    void Material_5DataFunction4() {
        {
        float m_specularColor[] = { 0f, 0f, 0f };
        Material_5.specularColor = m_specularColor;
        }
    }
    void Material_5DataFunction5() {
        Material_5.transparency = 0f;
    }
    void Appearance_4DataFunction1() {
    }
    void Appearance_4DataFunction2() {
    }
    void Appearance_4DataFunction3() {
    }
    void Appearance_4DataFunction4() {
    }
    void Appearance_4DataFunction5() {
    }
    void Appearance_4DataFunction6() {
        Appearance_4.material = Material_5;
    }
    void Appearance_4DataFunction7() {
        Appearance_4.texture = null;
    }
    void Appearance_4DataFunction8() {
        Appearance_4.textureTransform = null;
    }
    void Appearance_4DataFunction9() {
        Appearance_4.fillProperties = null;
    }
    void Appearance_4DataFunction10() {
        Appearance_4.lineProperties = null;
    }
    void Appearance_4DataFunction11() {
        {
        Pre_Node [] m_shaders = {  };
        Appearance_4.shaders = m_shaders;
        }
    }
    void Shape_3DataFunction1() {
        Text_6 = new Pre_Text();
        Text_6.m_parent = Shape_3;
    }
    void Text_6DataFunction0() {
    }
    void Text_6DataFunction1() {
        Text_6.fontStyle = null;
    }
    void Text_6DataFunction2() {
        Text_6.length = null;
        Pre_Text v = Text_6;
        int i = 0;
    }
    void Text_6DataFunction3() {
        Text_6.maxExtent = 0f;
    }
    void Text_6DataFunction4() {
        Text_6.string = new String[1];
        Pre_Text v = Text_6;
        int i = 0;
        v.string[i++] = "Hello PROTO";
    }
    void Shape_3DataFunction2() {
        Shape_3.appearance = Appearance_4;
    }
    void Shape_3DataFunction3() {
        Shape_3.geometry = Text_6;
    }
    void Shape_3DataFunction4() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Shape_3.bboxCenter = m_bboxCenter;
        }
    }
    void Shape_3DataFunction5() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Shape_3.bboxSize = m_bboxSize;
        }
    }
    void Transform_2DataFunction1() {
        {
        float m_rotation[] = { 0f, 0f, 1f, 0f };
        Transform_2.rotation = m_rotation;
        }
    }
    void Transform_2DataFunction2() {
        {
        float m_translation[] = { 0f, 0f, 0f };
        Transform_2.translation = m_translation;
        }
    }
    void Transform_2DataFunction3() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Transform_2.bboxCenter = m_bboxCenter;
        }
    }
    void Transform_2DataFunction4() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Transform_2.bboxSize = m_bboxSize;
        }
    }
    void Transform_2DataFunction5() {
        {
        float m_center[] = { 0f, 0f, 0f };
        Transform_2.center = m_center;
        }
    }
    void Transform_2DataFunction6() {
        {
        float m_scale[] = { 1f, 1f, 1f };
        Transform_2.scale = m_scale;
        }
    }
    void Transform_2DataFunction7() {
        {
        float m_scaleOrientation[] = { 0f, 0f, 1f, 0f };
        Transform_2.scaleOrientation = m_scaleOrientation;
        }
    }
    void Transform_2DataFunction8() {
        {
        Pre_Node [] m_children = { Shape_3 };
        Transform_2.children = m_children;
        }
    }
    void Transform_1DataFunction1() {
        {
        float m_rotation[] = { 0f, 0f, 1f, 0f };
        Transform_1.rotation = m_rotation;
        }
    }
    void Transform_1DataFunction2() {
        {
        float m_translation[] = { -3.379999f, 2.539999f, 0f };
        Transform_1.translation = m_translation;
        }
    }
    void Transform_1DataFunction3() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        Transform_1.bboxCenter = m_bboxCenter;
        }
    }
    void Transform_1DataFunction4() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        Transform_1.bboxSize = m_bboxSize;
        }
    }
    void Transform_1DataFunction5() {
        {
        float m_center[] = { 0f, 0f, 0f };
        Transform_1.center = m_center;
        }
    }
    void Transform_1DataFunction6() {
        {
        float m_scale[] = { 1f, 1f, 1f };
        Transform_1.scale = m_scale;
        }
    }
    void Transform_1DataFunction7() {
        {
        float m_scaleOrientation[] = { 0f, 0f, 1f, 0f };
        Transform_1.scaleOrientation = m_scaleOrientation;
        }
    }
    void Transform_1DataFunction8() {
        {
        Pre_Node [] m_children = { Test_7 };
        Transform_1.children = m_children;
        }
    }
    void rootDataFunction1() {
        {
        Pre_Node [] m_children = { Transform_1 };
        root.children = m_children;
        }
    }
    void rootDataFunction2() {
        {
        float m_bboxCenter[] = { 0f, 0f, 0f };
        root.bboxCenter = m_bboxCenter;
        }
    }
    void rootDataFunction3() {
        {
        float m_bboxSize[] = { -1f, -1f, -1f };
        root.bboxSize = m_bboxSize;
        }
    }
    void dataPre_Function0() {
        rootDataFunction0();
        rootDataFunction1();
        rootDataFunction2();
        rootDataFunction3();
    }
    void dataPre_Function1() {
        Transform_1DataFunction0();
        Transform_1DataFunction1();
        Transform_1DataFunction2();
        Transform_1DataFunction3();
        Transform_1DataFunction4();
        Transform_1DataFunction5();
        Transform_1DataFunction6();
        Transform_1DataFunction7();
        Transform_1DataFunction8();
    }
    void dataPre_Function2() {
        Test_7DataFunction0();
        Test_7DataFunction1();
    }
    void dataPre_Function3() {
        Transform_2DataFunction0();
        Transform_2DataFunction1();
        Transform_2DataFunction2();
        Transform_2DataFunction3();
        Transform_2DataFunction4();
        Transform_2DataFunction5();
        Transform_2DataFunction6();
        Transform_2DataFunction7();
        Transform_2DataFunction8();
    }
    void dataPre_Function4() {
        Shape_3DataFunction0();
        Shape_3DataFunction1();
        Shape_3DataFunction2();
        Shape_3DataFunction3();
        Shape_3DataFunction4();
        Shape_3DataFunction5();
    }
    void dataPre_Function5() {
        Appearance_4DataFunction0();
        Appearance_4DataFunction1();
        Appearance_4DataFunction2();
        Appearance_4DataFunction3();
        Appearance_4DataFunction4();
        Appearance_4DataFunction5();
        Appearance_4DataFunction6();
        Appearance_4DataFunction7();
        Appearance_4DataFunction8();
        Appearance_4DataFunction9();
        Appearance_4DataFunction10();
        Appearance_4DataFunction11();
    }
    void dataPre_Function6() {
        Material_5DataFunction0();
        Material_5DataFunction1();
        Material_5DataFunction2();
        Material_5DataFunction3();
        Material_5DataFunction4();
        Material_5DataFunction5();
    }
    void dataPre_Function7() {
        Text_6DataFunction0();
        Text_6DataFunction1();
        Text_6DataFunction2();
        Text_6DataFunction3();
        Text_6DataFunction4();
    }
    void dataPre_Function_0() {
        dataPre_Function0();
        dataPre_Function1();
        dataPre_Function2();
        dataPre_Function3();
        dataPre_Function4();
        dataPre_Function5();
        dataPre_Function6();
        dataPre_Function7();
    }
    Append_Pre_SceneGraph() {
        root = new Pre_Group();
        root.m_parent = null;
        dataPre_Function_0();
    }
    void render() { root.treeRender(); }
    void doWithData() { root.treeDoWithData(); }
    void MFBoolSendEvent(boolean[] target, boolean[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFColorSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFColorRGBASendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFDoubleSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFFloatSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFInt32SendEvent(int[] target, int[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFMatrix3fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFMatrix4fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFNodeSendEvent(Pre_Node [][] target, Pre_Node [][] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFRotationSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFStringSendEvent(String[] target, String[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFTimeSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec2fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec3dSendEvent(double[] target, double[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec3fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void MFVec4fSendEvent(float[] target, float[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void SFColorSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
    void SFColorRGBASendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 4; i++)
            target[i] = source[i];
    }
    void SFImageSendEvent(int[] target, int[] source, int size) {
        int i;
        for (i = 0; i < size; i++)
            target[i] = source[i];
    }
    void SFMatrix3fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 9; i++)
            target[i] = source[i];
    }
    void SFMatrix4fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 9; i++)
            target[i] = source[i];
    }
    void SFRotationSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 4; i++)
            target[i] = source[i];
    }
    void SFVec2fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 2; i++)
            target[i] = source[i];
    }
    void SFVec3dSendEvent(double[] target, double[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
    void SFVec3fSendEvent(float[] target, float[] source) {
        int i;
        for (i = 0; i < 3; i++)
            target[i] = source[i];
    }
};

/* 
   matching white_dune commandline usage:

        dune -prefix Pre_ -java file1.x3dv 
        dune -prefix Pre_ +java Append_ file2.x3dv 

*/

class MyTextCallback extends Pre_TextDoWithDataCallback {
    public void doWithData(Pre_Node node) {
        Pre_Text text = (Pre_Text) node; 
        if ((text.string != null) && (text.string.length > 0))
            System.out.println(text.string[0]);
    }   
}

class myAnchorTreeCallback extends Pre_AnchorTreeDoWithDataCallback {
    public void treeDoWithData(Pre_Node node) {
        Pre_Anchor anchor = (Pre_Anchor ) node;
        if (anchor.children != null)
            System.out.println("Anchor node has " + anchor.children.length + " children");
    }
}


class helloWorld {

    public static void main(String args[]) {
        Pre_Text.setPre_TextDoWithDataCallback(new MyTextCallback());
        Pre_Anchor.setPre_AnchorTreeDoWithDataCallback(new myAnchorTreeCallback());

        Pre_SceneGraph sceneGraph = new Pre_SceneGraph();
        sceneGraph.doWithData();

        Append_Pre_SceneGraph sceneGraph2 = new Append_Pre_SceneGraph();
        sceneGraph2.doWithData();
    }
};

