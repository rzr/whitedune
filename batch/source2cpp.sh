#!/bin/sh

# This file is free software; the Free Software Foundation    
# gives unlimited permission to copy, distribute and modify it.

awk '{ 
      print "RET_ONERROR( mywritestr(f, \"" $0 "\\n\") )";
      print "TheApp->incSelectionLinenumber();"
     }' $1
