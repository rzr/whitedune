/*
 * PreferencesApp.h
 *
 * Copyright (C) 1999 Stephen F. White, 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _PREFERENCES_APP_H
#define _PREFERENCES_APP_H

#include "swttypedef.h"

typedef enum {
    HM_NONE,
    HM_SELECTED,
    HM_ALL,
    HM_TREE
} HandleMode;

typedef enum {
    MOUSE_FLY,
    MOUSE_WALK,
    MOUSE_EXAMINE,
    MOUSE_ROLL
} MouseMode;

class PreferencesApp {
public:

                        PreferencesApp();

    void                SavePreferences(void);

    STABLE              getPrefs() { return _prefs; }

    // PreferencesApp data
    MouseMode           GetMouseMode() const { return _mouseMode; }
    HandleMode          GetHandleMode() const { return _handleMode; }
    bool                GetHandleMeshAlways() { return  _handleMeshAlways; }
    float               GetHandleSize();
    float               GetPreferenceHandleSize() { return _handleSize; }
    float               GetHandleScale() { return _handleScale; }
    float               GetPointSetSize(void) { return _pointSetSize; }
    bool                GetShowAllFields() const { return _showAllFields; }
    int                 GetRotationOrder() const { return _rotationOrder; }
    const char         *GetRotationTitle() const { return _rotationTitle; }
    float               GetHandleEpsilon(void) const { return _handleEpsilon; }
    bool                GetXSymetricMode() {return _xSymetricMode;}
    float               GetNearClippingPlaneDist(void) 
                            { return _nearClippingPlaneDist; }
    float               GetFarClippingPlaneDist(void) 
                            { return _farClippingPlaneDist; }
    int                 GetMaxInlinesToLoad(void)
                            { return _maxInlinesToLoad; }
    int                 GetMaxKeysInChannelView(void)
                            { return _maxKeysInChannelView; }
    int                 GetX11ErrorsLimit(void)
                            { return _X11ErrorsLimit; }

    void                SetMouseMode(MouseMode mouseMode);
    void                SetHandleMode(HandleMode handleMode);
    void                SetHandleMeshAlways(bool b) {_handleMeshAlways = b; }
    void                SetPreferenceHandleSize(float size);
    void                SetPreferenceHandleScale(float scale);
    void                SetPointSetSize(float size);
    void                SetShowAllFields(bool s);
    void                SetRotationOrder(int rotationOrder);
    void                SetRotationTitle(const char * s) { _rotationTitle = s; }
    void                SetHandleEpsilon(float f) { _handleEpsilon = f; }
    void                SetXSymetricMode(bool flag) 
                            {_xSymetricMode=flag;}
    void                SetNearClippingPlaneDist(float dist) 
                            { _nearClippingPlaneDist = dist; }
    void                SetFarClippingPlaneDist(float dist) 
                            { _farClippingPlaneDist = dist; }
    void                SetMaxInlinesToLoad(int max)
                            { _maxInlinesToLoad = max; }
    void                SetMaxKeysInChannelView(int max)
                            { _maxKeysInChannelView = max; }
    void                SetX11ErrorsLimit(int limit)
                            { _X11ErrorsLimit = limit; }

    void                PreferencesDefaults();

    bool                GetBoolPreference(const char *key, bool defaultValue);
    int                 GetIntPreference(const char *key, int defaultValue);
    const char         *GetPreference(const char *key,
                                      const char *defaultValue);

    void                SetBoolPreference(const char *key, bool value);
    void                SetIntPreference(const char *key, int value);
    void                SetPreference(const char *key, const char *value);

    void                unInstall(void);

private: 

    STABLE              _prefs;

    bool                _showAllFields;
    int                 _rotationOrder;
    const char         *_rotationTitle;
    MouseMode           _mouseMode;
    HandleMode          _handleMode;
    bool                _handleMeshAlways;
    float               _handleSize;
    float               _handleScale;
    float               _handleEpsilon;
    float               _pointSetSize;
    bool                _xSymetricMode;
    float               _nearClippingPlaneDist;
    float               _farClippingPlaneDist;
    int                 _maxInlinesToLoad;
    int                 _maxKeysInChannelView;
    bool                _renderNicerSlower;
    int                 _X11ErrorsLimit;
};

#endif
