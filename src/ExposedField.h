/*
 * ExposedField.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _EXPOSED_FIELD_H
#define _EXPOSED_FIELD_H

#ifndef _ELEMENT_H
# include "Element.h"
#endif
#ifndef _NODE_H   
# include "Node.h"   
#endif

class FieldValue;

class ExposedField : public Element {
public:
                        ExposedField(int type, 
                                     const MyString &name, 
                                     FieldValue *value, 
                                     FieldValue *min = NULL, 
                                     FieldValue *max = NULL,
                                     int nodeType = ANY_NODE,
                                     int flags = 0, 
                                     const char **strings = NULL,
                                     const MyString &x3dName = "");
    virtual            ~ExposedField();
    virtual int         getElementType() const { return EL_EXPOSED_FIELD; }
    virtual const char *getElementName(bool x3d) const;
    virtual int         write(int filedes, int indent, int flags = 0) const;
    FieldValue         *getValue() const { return _value; }
    FieldValue         *getMin() const { return _min; }
    FieldValue         *getMax() const { return _max; }
    int                 getNodeType() const { return _nodeType; }
    void                addToNodeType(int nodeType) { _nodeType += nodeType; }
    const char        **getStrings() const { return _strings; }

    int                 getField() const { return _field; }
    int                 getEventIn() const { return _eventIn; }
    int                 getEventOut() const { return _eventOut; }

    void                setField(int field) { _field = field; }
    void                setEventIn(int eventIn) { _eventIn = eventIn; }
    void                setEventOut(int eventOut) { _eventOut = eventOut; }


private:
    FieldValue     *_value;
    int             _field;
    int             _eventIn;
    int             _eventOut;
    FieldValue     *_min;
    FieldValue     *_max;
    int             _nodeType;
    const char    **_strings;
};

#endif // _EXPOSED_FIELD_H
