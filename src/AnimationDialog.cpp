/*
 * AnimationDialog.cpp
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */
 
#include "AnimationDialog.h"
#include "resource.h"
#include "MyString.h"
#include "FieldValue.h"
#include "SFMFTypes.h"
#include "Field.h"
#include "EventIn.h"
#include "EventOut.h"
#include "ExposedField.h"
#include "Element.h"
#include "DuneApp.h"
#include "NodeTransform.h"

AnimationDialog::AnimationDialog(SWND parent, Node* oldNode)
  : Dialog(parent, IDD_ANIMATION)
{
    _window.initCheckBoxWindow(parent, _dlg);
    _animationNode = oldNode;
    _newTimeSensorSeconds = 10;
    buildInterfaceData();
    LoadData();
    _window.accountYmax();
    _window.invalidateWindow();
}

void
AnimationDialog::buildInterfaceData(void)
{
    Proto* proto = _animationNode->getProto();
    Scene *scene = _animationNode->getScene();
    bool x3d = scene->isX3d();
    if (proto == NULL)
        return;
    int index = 0;
    for (int i = 0; i < proto->getNumEventIns(); i++) {
        if (scene->isInvalidElement(proto->getEventIn(i)))
            continue;
        int type = proto->getEventIn(i)->getType();
        if (typeDefaultValue(type)->supportAnimation(x3d)) {
            _eventInTypes[index] = type;
            _eventInNames[index] = proto->getEventIn(i)->getName(x3d);
            _window.setString(index, _eventInNames[index]);
            _eventInFields[index] = i;
            index++; 
        }
    }
}

bool
AnimationDialog::Validate()
{
    bool valid = true;

/*
    Proto* proto = _animationNode->getProto();
    Scene *scene = _animationNode->getScene();
    bool x3d = scene->isX3d();
    for (int i = 0 ; i < numEventInNames() ; i++) {
        _eventInIsAnimated[i] = _window.getChecked(i);
        if (TheApp->is4Kids() && _eventInIsAnimated[i]) {
            bool eventHandled = false;
            SocketList::Iterator *j;
            int field = _eventInFields[i];
            for (j = _animationNode->getInput(field).first(); 
                 j != NULL; j = j->next()) {
                 Node *inputNode = j->item().getNode();
                 if (inputNode->isInterpolator()) {
                     if (eventHandled)
                         break;
                     char str[256], message[256], title[256];
                     swLoadString(IDS_DUNE, title, 256);
                     swLoadString(IDS_EVENT_ALREADY_ANIMATED, str, 256);
                     const char *name = proto->getEventIn(field)->getName(x3d);
                     mysnprintf(message, 255, str, name);
                     switch(swMessageBox(TheApp->mainWnd(), message, title, 
                                         SW_MB_YESNOCANCEL, SW_MB_WARNING)) {
                       case IDYES:                           
                         break;
                       case IDCANCEL:
                       case IDNO:
                         _eventInIsAnimated[i] = false;
                         _window.setButtonsPressed(i, false);
                    }
                    eventHandled = true;
                }
            }
        }
    }
*/

    _window.accountYmax();
    _window.invalidateWindow();

    for (int i = 0 ; i < numEventInNames() ; i++)
        if (_eventInIsAnimated[i] == true)
            return valid;
    int commentID = _animationNode->getAnmationCommentID();
    if (commentID != -1)
        TheApp->MessageBoxId(IDS_ANIMATE_WHAT);

    return false;
}

void
AnimationDialog::LoadData()
{
    int i;

    SWND comboTimeSensors = swGetDialogItem(_dlg, IDC_TIMESENSORS);
    swComboBoxDeleteAll(comboTimeSensors);

    Array<MyString> timeSensors;
    timeSensors[0] = "new TimeSensor"; 

    const NodeList *nodes = _animationNode->getScene()->getNodes();
    for (i = 0; i < nodes->size(); i++) {
        Node *node = nodes->get(i);
        if (node->isInScene(_animationNode->getScene()))
            if (node->getType() == VRML_TIME_SENSOR)
                if (node->hasName())
                    timeSensors.append(node->getName()); 
    }
    
    for (i = 0;i < timeSensors.size(); i++)
        swComboBoxAppendItem(comboTimeSensors, timeSensors[i]);

    char buf[128];

    mysnprintf(buf, 128, "%g", _newTimeSensorSeconds);
    swSetText(swGetDialogItem(_dlg, IDC_TIMESENSOR_SECONDS), buf);

    for (i = 0; i < _eventInNames.size(); i++) {
        _window.setInitButtonsPressed(i,false);
        if (TheApp->is4Kids())
            if (_animationNode->getType() == VRML_TRANSFORM) {
                if (strcmp((const char *)_eventInNames[i], "set_rotation") == 0)
                    _window.setInitButtonsPressed(i, true);
                if (strcmp((const char *)_eventInNames[i], "set_translation") 
                    == 0)
                    _window.setInitButtonsPressed(i, true);
            }
            if (_animationNode->getType() == VRML_VIEWPOINT) {
                if (strcmp((const char *)_eventInNames[i], "set_orientation") 
                    == 0)
                    _window.setInitButtonsPressed(i, true);
                if (strcmp((const char *)_eventInNames[i], "set_position") 
                    == 0)
                    _window.setInitButtonsPressed(i, true);
            }
            if (_animationNode->getType() == VRML_NURBS_SURFACE) {
                if (strcmp((const char *)_eventInNames[i], "set_controlPoint") 
                    == 0)
                    _window.setInitButtonsPressed(i, true);
            }
            if (_animationNode->getType() == VRML_COORDINATE) {
                if (strcmp((const char *)_eventInNames[i], "set_point") == 0)
                    _window.setInitButtonsPressed(i, true);
            }
            if (_animationNode->getType() == VRML_MATERIAL) {
                if (strcmp((const char *)_eventInNames[i], "set_diffuseColor") 
                    == 0)
                    _window.setInitButtonsPressed(i, true);
            }
       }            
}

void 
AnimationDialog::SaveData()
{
    _timeSensor = NULL;

    int sensor = swComboBoxGetSelection(swGetDialogItem(_dlg, IDC_TIMESENSORS));
    if (sensor > 0) {
        int index = 1;
        const NodeList *nodes = _animationNode->getScene()->getNodes();
        for (int i = 0; i < nodes->size() && (_timeSensor == NULL); i++) {
            Node *node = nodes->get(i);
            if (node->isInScene(_animationNode->getScene()))
                if (node->getType() == VRML_TIME_SENSOR)
                    if (node->hasName())
                        if (index++ == sensor) {
                            _timeSensor = (NodeTimeSensor *)node;
                            break;
                        }
        }
    }

    char buf[128];

    swGetText(swGetDialogItem(_dlg, IDC_TIMESENSOR_SECONDS), buf, 128);
    _newTimeSensorSeconds = atof(buf);

    for (int i = 0 ; i < numEventInNames() ; i++)
        _eventInIsAnimated[i] = _window.getChecked(i);
}



