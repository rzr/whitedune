/*
 * CoverDefDialog.cpp
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"

#include "resource.h"
#include "MyString.h"
#include "Path.h"
#include "Node.h"
#include "CoverDefDialog.h"
#include "DuneApp.h"

static const char *coverNames[] = { 
    "",
    "StaticCave",   // for Transform 
    "Animated",     // for animated objects
    "coShader",     // (coShaderSHADERNAME_something see covise/materials/*.xml)
    "coBump",       // apply builtin normal map shader to object
    "coBumpCube",   // apply builtin normal map shader with a cube map to object
    "coBumpEnv",    // apply builtin normal map shader with a spherical 
                    //                          environment map to object
    "coDepthOnly",  // only render the object into the Z-buffer
    "coMirror",     // reflection for plain objects
    "coNoColli",    // objects below this node are not included in collision 
                    //                                 detection (walk mode)
    "coNoIsect",    // objects below this node are not included in 
                    //                             intersection testing (wand)
    "Cached"        // only for Inline

// deprecated DEF names:
// combineTextures     applies a builtin texture combiner (deprecated, use shaders instead)
// combineEnvTextures  applies a builtin texture combiner (deprecated, use shaders instead)
// coCgShader          not working anymore as far as Uwe Woessner remember
// "coGlass" apply test shader, does not work
// "coTest"   apply test shader, does not work
};

CoverDefDialog::CoverDefDialog(SWND parent, Scene* scene)
  : Dialog(parent, IDD_COVER_DEF)
{
    _scene = scene;
    _node = _scene->getSelection()->getNode();
    _valid = false;
    LoadData();
}

CoverDefDialog::~CoverDefDialog()
{
}

void
CoverDefDialog::OnCommand(int id)
{
    if (id == IDOK) {
        SaveData();
        if (Validate()) {
            swEndDialog(IDOK);
        }
    } else if (id == IDC_COVER_NAMES) {
        int sel = swComboBoxGetSelection(swGetDialogItem(_dlg, 
                                                         IDC_COVER_NAMES));
        char buffer[1024] = "";
        swGetText(swGetDialogItem(_dlg, IDC_DEF_NAME), buffer, 1024);
        char name[1024] = "";
        mysnprintf(name, 1024, "%s%s", coverNames[sel], buffer);
        swSetText(swGetDialogItem(_dlg, IDC_DEF_NAME), name);
    } else if (id == IDCANCEL) {
        swEndDialog(IDCANCEL);
    }
}

bool
CoverDefDialog::Validate()
{
    return _valid;
}

void
CoverDefDialog::LoadData()
{
    SWND comboCoverNames = swGetDialogItem(_dlg, IDC_COVER_NAMES);
    swComboBoxDeleteAll(comboCoverNames);
    for (int i=0;i<(sizeof(coverNames)/sizeof(const char*));i++)
        swComboBoxAppendItem(comboCoverNames, coverNames[i]);

    if (_node->hasName())
        swSetText(swGetDialogItem(_dlg, IDC_DEF_NAME), _node->getName(false));
    else
        swSetText(swGetDialogItem(_dlg, IDC_DEF_NAME), 
                  _scene->getUniqueNodeName(_node));
}

void
CoverDefDialog::SaveData()
{
    char name[1024] = "";
    swGetText(swGetDialogItem(_dlg, IDC_DEF_NAME), name, 1024);
    int positionInvalid = TheApp->searchIllegalChar(name);
    if (positionInvalid != -1) {
        _valid = false;
        char wrongChar[2];
        wrongChar[0] = name[positionInvalid];
        wrongChar[1] = 0;
        if (wrongChar[0] == ' ')
            TheApp->MessageBox(IDS_WRONG_SPACE_IN_DEF_ERROR, wrongChar);
        else
            TheApp->MessageBox(IDS_WRONG_DEF_ERROR, wrongChar);
    } else if ((_node->hasName()) && (strcmp(name, _node->getName()) == 0)) {
        _valid = true;
        _scene->def(_node);
    } else if (_scene->hasAlreadyName(name)) {
        _valid = false;
        TheApp->MessageBoxId(IDS_DEF_ALREADY_ERROR);
    } else {
        _valid = true;
        _scene->def(_node);
        _scene->def(name, _node);
    }
}

