/*
 * CheckBoxWindow.h
 *
 * Copyright (C) 2005 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "swt.h"
#include "swttypedef.h"
#include "MyString.h"
#include "Array.h"
#include "Dialog.h"

#ifndef _CHECK_BOX_WINDOW_H
#define _CHECK_BOX_WINDOW_H

class CheckBoxWindow {
public:
                        CheckBoxWindow(void);
    virtual            ~CheckBoxWindow();
    void                initCheckBoxWindow(SWND parent, SWND dlg);
    void                accountYmax(void);
    void                initaliseCheckBoxWindow(void);
    virtual void        drawInterface(SDC dc);

    void                setString(int i, MyString string)
                           { _strings[i] = string; }
    void                setButtonsPressed(int i, bool pressed)
                           { _buttonsPressed[i] = pressed; }
    void                setInitButtonsPressed(int i, bool pressed)
                           { _initButtonsPressed[i] = pressed; }
    bool                getChecked(int i) 
                           { return swGetCheck(_buttons[i]); }
    void                resize0(void); 
    void                invalidateWindow(void);

protected:
    Array<MyString>     _strings;
    Array<SWND>         _buttons;
    Array<bool>         _buttonsPressed;
    Array<bool>         _initButtonsPressed;
    int                 _fontHeight;
    SWND                _parent_window;
};
#endif
