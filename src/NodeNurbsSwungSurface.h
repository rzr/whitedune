/*
 * NodeNurbsSwungSurface.h
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_NURBS_SWUNG_SURFACE_H
#define _NODE_NURBS_SWUNG_SURFACE_H

#ifndef _MESH_MORPHING_NODE_H
# include "MeshMorphingNode.h"
#endif
#ifndef _PROTO_MACROS_H
# include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
# include "Proto.h"
#endif
#ifndef _VEC3F
# include "Vec3f.h"
#endif
#ifndef _SFMFTYPES_H
# include "SFMFTypes.h"
#endif

class Mesh;
class NodeNurbsGroup;

class ProtoNurbsSwungSurface : public GeometryProto {
public:
                    ProtoNurbsSwungSurface(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return X3D_NURBS_SWUNG_SURFACE; }
    virtual int     getNodeClass() const
                       { return PARAMETRIC_GEOMETRY_NODE | GEOMETRY_NODE; }

    FieldIndex profileCurve;
    FieldIndex trajectoryCurve;
    FieldIndex ccw;
    FieldIndex solid;
};

class NodeNurbsSwungSurface : public GeometryNode {
public:
                    NodeNurbsSwungSurface(Scene *scene, Proto *proto);
protected:
    virtual        ~NodeNurbsSwungSurface();

public:
    virtual const char* getComponentName(void) const { return "NURBS"; }
    virtual int     getComponentLevel(void) const { return 3; }
    virtual Node   *copy() const { return new NodeNurbsSwungSurface(*this); }

    virtual void    draw() { }

    virtual void    flip(int index);
    virtual void    swap(int fromTo);

    virtual bool    hasTwoSides(void) { return true; }
    virtual bool    isDoubleSided(void) { return !solid()->getValue(); }
    virtual void    toggleDoubleSided(void) 
                       { solid(new SFBool(!solid()->getValue())); }
    virtual int     getSolidField() { return solid_Field(); }
    virtual void    flipSide(void) { ccw(new SFBool(!ccw()->getValue())); }

    virtual bool    showFields() { return true; }

    fieldMacros(SFNode, profileCurve, ProtoNurbsSwungSurface)
    fieldMacros(SFNode, trajectoryCurve,   ProtoNurbsSwungSurface)
    fieldMacros(SFBool, ccw,               ProtoNurbsSwungSurface)
    fieldMacros(SFBool, solid,             ProtoNurbsSwungSurface)
};

#endif
