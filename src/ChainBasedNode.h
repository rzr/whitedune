/*
 * ChainBasedNode.h
 *
 * Copyright (C) 2003 Th. Rothermel, 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _CHAIN_BASED_NODE_H
#define _CHAIN_BASED_NODE_H

#ifndef GEOMETRY_NODE_H
#include "GeometryNode.h"
#endif
#ifndef _VEC3F
# include "Vec3f.h"
#endif

class Scene;
class MFVec3f;
class MFInt32;

class ChainBasedProto : public GeometryProto {
public:
                    ChainBasedProto(Scene *scene, const char *name) : 
                          GeometryProto(scene, name) {}
};

class ChainBasedNode : public GeometryNode {
public:
                    ChainBasedNode(Scene *scene, Proto *proto);
    virtual        ~ChainBasedNode();

    virtual void    update(void) { _chainDirty = true; }
    virtual void    reInit(void) { _chain.resize(0); _chainDirty = true; }

    MFVec3f        *getVertices(void);

    virtual void    createChain(void) = 0;

    virtual void    draw(void);

    virtual int     write(int f, int indent);

    virtual void    setField(int index, FieldValue *value);

    virtual bool    canConvertToIndexedLineSet(void) { return true; }
    virtual Node   *toIndexedLineSet(void);

    virtual bool   canConvertToPositionInterpolator(void) { return true; }
    virtual Node   *toPositionInterpolator(void);

    virtual bool    canConvertToOrientationInterpolator(void) { return true; }
    virtual Node   *toOrientationInterpolator(Direction direction);

    virtual int     writeLdrawDat(int f, int indent);

    virtual Vec3f   getMinBoundingBox(void);
    virtual Vec3f   getMaxBoundingBox(void);

    virtual void    flip(int index);
    virtual void    swap(int fromTo);

    const Vec3f    *getChain();
    int             getChainLength();

protected:
    Array<Vec3f>    _chain;
    bool            _chainDirty;
};

#endif 

