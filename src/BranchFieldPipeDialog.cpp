/*
 * FieldPipeDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "FieldPipeDialog.h"
#include <stdio.h>
#include <stdlib.h>
#include "swt.h"
#include "resource.h"

FieldPipeDialog::FieldPipeDialog(SWND parent, MyString filter, 
                                 MyString command)
  : Dialog(parent, IDD_FIELD_PIPE)
{
    _filter = "";
    _filter += filter;
    _command = "";
    _command += command;
    LoadData();
}

FieldPipeDialog::~FieldPipeDialog()
{
}

void
FieldPipeDialog::SaveData()
{
    char buf[1024];
    swGetText(swGetDialogItem(_dlg, IDC_PIPE_FILTER), buf, 1023);
    _filter = "";
    _filter += buf;

    swGetText(swGetDialogItem(_dlg, IDC_PIPE_COMMAND), buf, 1023);
    _command = "";
    _command += buf;
}

bool
FieldPipeDialog::Validate()
{
    // incomplete
    return true;
}

void
FieldPipeDialog::LoadData()
{
    char buf[1024];

    mystrncpy_secure(buf, _filter, 1024);
    swSetText(swGetDialogItem(_dlg, IDC_PIPE_FILTER), buf);

    mystrncpy_secure(buf, _command, 1024);
    swSetText(swGetDialogItem(_dlg, IDC_PIPE_COMMAND), buf);
}

