/*
 * NodeCADAssembly.h
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_CAD_ASSEMBLY_H
#define _NODE_CAD_ASSEMBLY_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"
#include "NodeStaticGroup.h"

class ProtoCADAssembly : public ProtoStaticGroup {
public:
                    ProtoCADAssembly(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return X3D_CAD_ASSEMBLY; }
    virtual int     getNodeClass() const 
                       { return PRODUCT_STRUCTURE_CHILD_NODE | GROUPING_NODE; }

    FieldIndex name;
};

class NodeCADAssembly : public NodeStaticGroup {
public:
                    NodeCADAssembly(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_CAD_INTERCHANGE; }
    virtual const char* getComponentName(void) const { return "CADGeometry"; }
    virtual int         getComponentLevel(void) const { return 2; }
    virtual Node   *copy() const { return new NodeCADAssembly(*this); }

    fieldMacros(SFString, name, ProtoCADAssembly);
};

#endif
