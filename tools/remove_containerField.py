
# remove_containerField.py - declared public domain by author
# dev & tested with python 3.1 sept 2010
import re, os, sys

def run():
    fnamein = sys.argv[1]
    fnameout = sys.argv[2]
    fi = open(fnamein,mode='rt')
    fo = open(fnameout,mode='wt')
    count = 0
    more = True
    while more:
        line = fi.readline()
        #print(line)
        if line:
            more = True
            m = re.search('containerField',line)
            if m:
                #match found - skip
                count = count + 1
                #print('match')
            else:
                #no match
                fo.write(line)
        else:
            more = False
    fo.close()

if __name__ == "__main__":
    run()
