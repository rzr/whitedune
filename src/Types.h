/*
 * Types.h 
 *
 * Copyright (C) 1999 Stephen F. White
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 *
 * automatic (via configure) generated file, direct change is useless.
 * change batch/mkSFMFTypes.sh instead
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _TYPES_H
#define _TYPES_H

#include "MyString.h"
class FieldValue;

enum {
    MFBOOL,
    MFCOLOR,
    MFCOLORRGBA,
    MFDOUBLE,
    MFFLOAT,
    MFINT32,
    MFMATRIX3F,
    MFMATRIX4F,
    MFNODE,
    MFROTATION,
    MFSTRING,
    MFTIME,
    MFVEC2F,
    MFVEC3D,
    MFVEC3F,
    MFVEC4F,
    SFBOOL,
    SFCOLOR,
    SFCOLORRGBA,
    SFDOUBLE,
    SFFLOAT,
    SFIMAGE,
    SFINT32,
    SFMATRIX3F,
    SFMATRIX4F,
    SFNODE,
    SFROTATION,
    SFSTRING,
    SFTIME,
    SFVEC2F,
    SFVEC3D,
    SFVEC3F,
    SFVEC4F
};

#define LAST_TYPE  SFVEC4F

int typeStringToEnum(const char* str);

const char* typeEnumToString(int type);

FieldValue *typeDefaultValue(int type);

int getSFType(int type);

bool isMFType(int type);

FieldValue *newFieldValue(int type, FieldValue *fieldValue);

#endif // _TYPES_H

