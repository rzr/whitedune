/*
 * NurbsCurve2NurbsSurfDialog.cpp
 *
 * Copyright (C) 2003 Th. Rothermel
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "Node.h"
#include "SFBool.h"

#include "NurbsCurve2NurbsSurfDialog.h"
#include "Scene.h"
#include "Proto.h"
#include "resource.h"
#include "NodeNurbsCurve.h"
#include "DuneApp.h"
#include "swt.h"

NurbsCurve2NurbsSurfDialog::NurbsCurve2NurbsSurfDialog(SWND parent, Node *node,
                                                       int narcs, int uDegree,
                                                       float rDegree, int method)
  : Dialog(parent, IDD_NURBS_CURVE_ROTATE) 
{  
  _P1.x = 0; _P1.y = 0; _P1.z = 0; 
  _P2.x = 0; _P2.y = 0; _P2.z = 0; 
  _node = node;
  _narcs = narcs;
  _uDegree = uDegree;
  _rDegree = rDegree;
  _method =  method; 
  LoadData();
}


void
NurbsCurve2NurbsSurfDialog::SaveData()
{
    char buf[128];

    if (swGetCheck(swGetDialogItem(_dlg, IDC_POINTPOINT_ROTATE)))
        _method = NURBS_ROT_POINT_TO_POINT;
    else if (swGetCheck(swGetDialogItem(_dlg, IDC_X_ROTATE)))
        _method = NURBS_ROT_X_AXIS;
    else if (swGetCheck(swGetDialogItem(_dlg, IDC_Y_ROTATE)))
        _method = NURBS_ROT_Y_AXIS;
    else if (swGetCheck(swGetDialogItem(_dlg, IDC_Z_ROTATE)))
        _method = NURBS_ROT_Z_AXIS;

    swGetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_NARCS), buf, 128);
    _narcs = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_U_DEGREE), buf, 128);
    _uDegree = atoi(buf);
    swGetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_ANGLE), buf, 128);
    _rDegree = atof(buf);
    _flatten = swGetCheck(swGetDialogItem(_dlg, IDC_CURVE2SURF_FLATTEN));
}

bool
NurbsCurve2NurbsSurfDialog::Validate()
{

    bool valid = true;

    if (_rDegree >= 180 * _narcs) 
        valid = false;
/*
    if (_uDegree > 2) {
        valid = false;
        TheApp->MessageBox(IDS_UDEGREE_UNSUPPORTED);
    }
*/
    int length;
    NodeNurbsCurve *node = (NodeNurbsCurve *)_node;
    switch (_method) {
    case NURBS_ROT_X_AXIS:    
      _P2.x = 1;
      break;
    case NURBS_ROT_Y_AXIS:    
      _P2.y = 1;
      break;      
    case NURBS_ROT_Z_AXIS:    
      _P2.z = 1;
      break;
    case NURBS_ROT_POINT_TO_POINT:      
      length = node->getControlPoints()->getSFSize();
      _P1 = (Vec3f)node->getControlPoints()->getValue(0);
      _P2 = (Vec3f)node->getControlPoints()->getValue(length-1);
      break;
    }    

    Vec3f vTest = _P2 - _P1;
    if ((vTest.x == 0) && (vTest.y == 0) && (vTest.z == 0))
        valid = false;

    if (valid)
        valid =  _uDegree > 0;
    return valid;
}

void
NurbsCurve2NurbsSurfDialog::LoadData()
{
    char buf[128];

    if (_method == NURBS_ROT_POINT_TO_POINT)
        swSetCheck(swGetDialogItem(_dlg, IDC_POINTPOINT_ROTATE), 1);
    else if (_method == NURBS_ROT_X_AXIS)
        swSetCheck(swGetDialogItem(_dlg, IDC_X_ROTATE), 1);
    else if (_method == NURBS_ROT_Y_AXIS)
        swSetCheck(swGetDialogItem(_dlg, IDC_Y_ROTATE), 1);
    else if (_method == NURBS_ROT_Z_AXIS)
        swSetCheck(swGetDialogItem(_dlg, IDC_Z_ROTATE), 1);
    mysnprintf(buf, 128, "%d", _narcs);
    swSetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_NARCS), buf);
    mysnprintf(buf, 128, "%d", _uDegree);
    swSetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_U_DEGREE), buf);
    mysnprintf(buf, 128, "%g", _rDegree);
    swSetText(swGetDialogItem(_dlg, IDC_CURVE2SURF_ANGLE), buf);
    swSetCheck(swGetDialogItem(_dlg, IDC_CURVE2SURF_FLATTEN), 1);
}

