/*
 * NodeRenderedTexture.cpp
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeRenderedTexture.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFInt32.h"
#include "SFString.h"
#include "SFNode.h"
#include "SFNode.h"
#include "SFBool.h"
#include "SFBool.h"
#include "SFBool.h"
#include "MFBool.h"
#include "DuneApp.h"

ProtoRenderedTexture::ProtoRenderedTexture(Scene *scene)
  : Proto(scene, "RenderedTexture")
{
    int values[] = { 128, 128, 4, 1, 1 };
    int *v = new int[5];  
    memcpy(v, values, 5 * sizeof(int));
    dimensions.set(
        addExposedField(MFINT32, "dimensions", new MFInt32(v, 5)));
    update.set(
        addExposedField(SFSTRING, "update", new SFString("NONE")));
    viewpoint.set(
        addExposedField(SFNODE, "viewpoint", new SFNode(), VRML_VIEWPOINT));
    textureProperties.set(
        addField(SFNODE, "textureProperties", new SFNode(), 
                 X3D_TEXTURE_PROPERTIES));
    repeatS.set(
        addField(SFBOOL, "repeatS", new SFBool(true)));
    repeatT.set(
        addField(SFBOOL, "repeatT", new SFBool(true)));
    repeatR.set(
        addField(SFBOOL, "repeatR", new SFBool(true)));
    depthMap.set(
        addExposedField(MFBOOL, "depthMap", new MFBool()));
}

Node *
ProtoRenderedTexture::create(Scene *scene)
{ 
    return new NodeRenderedTexture(scene, this); 
}

NodeRenderedTexture::NodeRenderedTexture(Scene *scene, Proto *def)
  : Node(scene, def)
{
}
