/*
 * NodeTwoSidedMaterial.cpp
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeTwoSidedMaterial.h"
#include "Proto.h"
#include "FieldValue.h"
#include "SFFloat.h"
#include "SFFloat.h"
#include "SFColor.h"
#include "SFColor.h"
#include "SFFloat.h"
#include "SFColor.h"
#include "SFFloat.h"
#include "SFColor.h"
#include "SFColor.h"
#include "SFFloat.h"
#include "SFBool.h"
#include "SFColor.h"
#include "SFFloat.h"
#include "DuneApp.h"

ProtoTwoSidedMaterial::ProtoTwoSidedMaterial(Scene *scene)
  : Proto(scene, "TwoSidedMaterial")
{
    ambientIntensity.set(
        addExposedField(SFFLOAT, "ambientIntensity", new SFFloat(0.2)));
    backAmbientIntensity.set(
        addExposedField(SFFLOAT, "backAmbientIntensity", new SFFloat(0.2)));
    backDiffuseColor.set(
        addExposedField(SFCOLOR, "backDiffuseColor", new SFColor(0.8, 0.8, 0.8)));
    backEmissiveColor.set(
        addExposedField(SFCOLOR, "backEmissiveColor", new SFColor(0, 0, 0)));
    backShininess.set(
        addExposedField(SFFLOAT, "backShininess", new SFFloat(0.2)));
    backSpecularColor.set(
        addExposedField(SFCOLOR, "backSpecularColor", new SFColor(0, 0, 0)));
    backTransparency.set(
        addExposedField(SFFLOAT, "backTransparency", new SFFloat(0)));
    diffuseColor.set(
        addExposedField(SFCOLOR, "diffuseColor", new SFColor(0.8, 0.8, 0.8)));
    emissiveColor.set(
        addExposedField(SFCOLOR, "emissiveColor", new SFColor(0, 0, 0)));
    shininess.set(
        addExposedField(SFFLOAT, "shininess", new SFFloat(0.2)));
    separateBackColor.set(
        addExposedField(SFBOOL, "separateBackColor", new SFBool(false)));
    specularColor.set(
        addExposedField(SFCOLOR, "specularColor", new SFColor(0, 0, 0)));
    transparency.set(
        addExposedField(SFFLOAT, "transparency", new SFFloat(0)));
}

Node *
ProtoTwoSidedMaterial::create(Scene *scene)
{ 
    return new NodeTwoSidedMaterial(scene, this); 
}

NodeTwoSidedMaterial::NodeTwoSidedMaterial(Scene *scene, Proto *def)
  : Node(scene, def)
{
}
