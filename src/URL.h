/*
 * URL.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _URL_H
#define _URL_H

#ifndef _DUNE_STRING_H
#include "MyString.h"
#endif

class URL
{
public:
// constructors
                        URL(const char *u);
                        URL(const char *base, const char *url);
                        URL(void);

// parsing
    bool                Parse(const char *u = NULL);
    void                Rebuild(bool withTopic = true);
    void                TrimTopic(void) { Rebuild(false); }
#ifdef _WIN32
    void                TruncateToFit(HDC hDC, int pixels, MyString *out);
#endif
    operator const char *(void) { return (const char *) _url; }
    MyString            GetFileName();
    const char         *GetFileNameWithoutExtension();
    const MyString     &GetPath() const { return _path; }
    const MyString     &GetDir();
    MyString            ToPath() const;
    void                FromPath(const char *path);
    MyString            RelativeTo(const char *parentURL);

private:
    void                SimplifyPath();
    bool                isJavascript(void);
    bool                isEcmascript(void);
    bool                isSortOfEcmascript(void);
    bool                notURN(void);
    bool                sameDrive(const char* url1, const char* url2);

private:
    MyString            _url;
    MyString            _scheme;
    MyString            _username;
    MyString            _password;
    MyString            _hostname;
    unsigned short      _port;
    MyString            _path;
    MyString            _topic;
};

MyString rewriteURL(const char *url, const char *oldBase, const char *newBase);
char getDriveLetter(const char* url);
bool isJavascript(const char* url);
bool isEcmascript(const char* url);
bool isSortOfEcmascript(const char* url);
bool notURN(const char* url);

#endif // _URL_H
