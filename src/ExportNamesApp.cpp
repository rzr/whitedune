/*
 * ExportNamesApp.cpp
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "DuneApp.h"
#include "ExportNamesApp.h"


ExportNamesApp:: ExportNamesApp()
{
    _prefix = ""; 
    _appendPrefix = NULL;
    _cNodeName = "Node";
    _cNodeNamePtr = "Node*";
    _cNodeNameArray = "Node []";
    _cSceneGraphName = "SceneGraph";
}

const char *
ExportNamesApp::getCPrefix(void) 
{
    if ((_prefix == NULL) || (strlen(_prefix) == 0))
       return TheApp->GetWonderlandModuleExportPrefix();        
    return _prefix;
}

void
ExportNamesApp::buildCSceneGraphName(void) 
{ 
     _cSceneGraphName = "";
     if (_appendPrefix != NULL)
         _cSceneGraphName += _appendPrefix;
     _cSceneGraphName += getCPrefix();
     _cSceneGraphName += "SceneGraph";
}

void
ExportNamesApp::buildCNodeName(void) 
{ 
     _cNodeName = "";
     _cNodeName += getCPrefix();
     _cNodeName += "Node";

     _cNodeNamePtr = "";
     _cNodeNamePtr += _cNodeName;
     _cNodeNamePtr += "*";
    
     _cNodeNameArray = "";
     _cNodeNameArray += _cNodeName;
     _cNodeNameArray += " []";
}

void
ExportNamesApp::updatePrefix(void)
{
     buildCNodeName();
     buildCSceneGraphName();
}

void
ExportNamesApp::setPrefix(const char* prefix) 
{ 
     _prefix = prefix; 
     updatePrefix();
}

void
ExportNamesApp::setAppendPrefix(const char* prefix)
{
     _appendPrefix = prefix; 
     updatePrefix();
}

