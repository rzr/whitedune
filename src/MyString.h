/*
 * MyString.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _DUNE_STRING_H
#define _DUNE_STRING_H

#include <stdio.h>
#ifndef _STRING_H
#include <string.h>
#endif
#include <stdlib.h>
#include "mysnprintf.h"

class MyString {

    class StringBuf {
    public:
        friend class MyString;
                StringBuf(const char *d)
                { 
                   if ((d != NULL) && strlen(d)) {
                       data = mystrdup(d); 
                   } else {
                       data = (char*) malloc(1);
                       data[0] = 0;
                   }
                   refs = 1; 
                   len = strlen(data); 
                   capacity = len + 1; 
                }
               ~StringBuf()
                {
                    if (refs == 0) {
                        // fixme: why is len !=0 necessary to avoid crash ?
                        if (len != 0)
                            free(data);
                        refs = -1; 
                    }
                }
    protected:
        char   *data;
        int     refs;
        int     len;
        int     capacity;
    };

    public:
                MyString();
                MyString(const char *str);
                MyString(char c);
                MyString(const MyString &s);
               ~MyString();

    MyString   &operator =(const MyString &s);
    MyString   &operator +=(char c);
    MyString   &operator +=(const char *s);
                operator const char *() const
                   { 
                   if (_stringBuf)
                       return _stringBuf->data; 
                   return NULL;
                   }
    int         operator ==(const MyString &str) const;
    MyString   &copy(void);
    int         length() const { return _stringBuf->len; }
    int         write(int filedes);
    bool        gsubOnce(MyString what, MyString with);
    const char *getData(void) const { return _stringBuf->data; }

protected:
    StringBuf  *_stringBuf;
};

extern int hash(MyString key);

#endif // _DUNE_STRING_H
